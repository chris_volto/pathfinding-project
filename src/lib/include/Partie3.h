#ifndef __VOLTO_MINIPROJET_PART3_HEADER__
    #define __VOLTO_MINIPROJET_PART3_HEADER__
    
    #include "BmpLib.h"
    #include <stdbool.h>
    #include "Image.h"

    typedef struct noeudBinaire {
        
        short int value;

        int etiquette;
        int start_x;
        int start_y;
        int size;

        struct noeudBinaire *NW;
        struct noeudBinaire *NE;
        struct noeudBinaire *SE;
        struct noeudBinaire *SW;
    }noeudBinaire;

    /**
     * @brief Determine l'échelle de grix d'un carre dans une matrice, si non uniforme renvoie -1 
     * 
     * @param start_x 
     * @param start_y 
     * @param size 
     * @param matrix 
     * @return short int 
     */
    short int calculCarreNB(int start_x, int start_y, int size, short int **matrix);
    /**
     * @brief Construit un arbre binaire à partir d'une image non colorée
     * 
     * @param start_x 
     * @param start_y 
     * @param imgSideSize 
     * @param img 
     * @return noeudBinaire* 
     */
    noeudBinaire* creeArbreNB(int start_x, int start_y, int imgSideSize, short int **img);
    /**
     * @brief Compte le nombre de feuille d'un arbre noeudBinaire
     * 
     * @param racine 
     * @return int 
     */
    int compteFeuille(noeudBinaire* racine);
    /**
     * @brief Construit une image à partir d'un arbre noeudBinaire 
     * 
     * @param img 
     * @param rcn 
     * @param etiq 
     */
    void creeImageArbreNB(Image* img, noeudBinaire* rcn, bool etiq);
    /**
     * @brief Libère un arbre noeudBinaire
     * 
     * @param rcn 
     */
    void libereArbreBinaire(noeudBinaire *rcn);
    

#endif