#ifndef __VOLTO_MINIPROJET_PART6_HEADER__
    #define __VOLTO_MINIPROJET_PART6_HEADER__

    #include "Partie3.h"

    typedef struct SymbOcc{
        int symb; //symbole
        int occ; //occurrences de ce symbole dans le fichier
    }SymbOcc;

    typedef struct TabSymbOcc{
        int nb;
        SymbOcc *tab;
    }TabSymbOcc;

    typedef struct NoeudCode{
        SymbOcc so;
        struct NoeudCode *ptrfils0,*ptrfils1;
    }NoeudCode;

    typedef struct SymbCode{
        int symb;
        char *symbCode;
    }SymbCode;

    typedef struct TabSymbCode{
        int nb;
        SymbCode *tab;
    }TabSymbCode;

    /**
     * @brief init and return a TabSymbOcc* struct
     * 
     * @param tabSymbOcc 
     * @param path 
     * @return TabSymbOcc* 
     */
    TabSymbOcc* compteOccurrence(char *path);

    /**
     * @brief crée l’arbre de codage associé au contenu de la structure TabSymbOcc passée en paramètre
     * 
     * @param tabSymbOcc 
     * @return NoeudCode* 
     */
    NoeudCode* creeArbreCodeOcc(TabSymbOcc *tabSymbOcc);

    /**
     * @brief remplit une structure TabSymbCode et en renvoie l’adresse.
     * 
     * @param racine 
     * @return TabSymbCode* 
     */
    TabSymbCode* creeCodeAdaptatif(NoeudCode *racine);
    
    /**
     * @brief écrit dans un fichier texte le contenu d’une structure TabSymbCode
     * 
     * @param destPath
     * @param tabSymbCode 
     */
    void sauveCodeAdaptatif(char *destPath, TabSymbCode *tabSymbCode);

    /**
     * @brief lit dans un fichier texte le contenu d’une structure TabSymbCode
     * 
     * @param path 
     */
    TabSymbCode* litCodeAdaptatif(char *path);

    /**
     * @brief  crée l’arbre de codage associé au contenu de la structure
     * 
     * @param tabSymbCode 
     * @return NoeudCode* 
     */
    NoeudCode* creeArbreCodeAdaptatif(TabSymbCode *tabSymbCode);

    /**
     * @brief crée le fichier codé à partir d’un fichier texte.
     * 
     * @param srcPath 
     * @param destPath 
     */
    void codeFichier(char *srcPath, char *destPath, TabSymbCode *tabSymbCode);

    /**
     * @brief  crée le fichier texte en clair associé au fichier codé passé en paramètre
     * 
     * @param srcPath 
     * @param destPath 
     * @param tabSymbCode 
     */
    void decodeFichier(char *srcPath, char *destPath, TabSymbCode *tabSymbCode);

    /**
     * @brief  crée le fichier binaire associé au fichier texte ne contenant que des 0 et des 1
     * 
     * @param srcPath 
     * @param destPath 
     */
    void transcritTexteEnBinaire(char *srcPath, char *destPath);

    /**
     * @brief  crée le fichier texte en clair associé au fichier binaire codé passé en paramètre
     * 
     * @param srcPath 
     * @param destPath 
     */
    void transcritBinaireEnTexte(char *srcPath, char *destPath);

    /**
     * @brief Affiche arbre sur console
     * 
     * @param racine 
     */
    void printArbreCodeAdaptatif(NoeudCode *racine);

    /**
     * @brief Libere l'arbre
     * 
     * @param racine 
     */
    void libereArbreCodeAdaptatif(NoeudCode *racine);

    /**
     * @brief inspirée de compteOccurrence et adaptée à la configuration des fichiers texte créés en partie 4
     * 
     * @param path 
     * @return TabSymbOcc* 
     */
    TabSymbOcc* compteOccurrenceMotif(char * path);

    /**
     * @brief inspirée de codeFichier
     * 
     * @param srcPath 
     * @param destPath 
     * @param tabSymbCode 
     */
    void codeFichierMotif(char *srcPath, char *destPath, TabSymbCode *tabSymbCode);

    /**
     * @brief inspirée de decodeFichier et qui écrit le fichier texte au format précisé en partie 4
     * 
     * @param srcPath 
     * @param destPath 
     * @param tabSymbCode 
     */
    void decodeFichierMotif(char *srcPath, char *destPath, TabSymbCode *tabSymbCode);

    /**
     * @brief inspirée de compteOccurrence et adaptée à la configuration des fichiers texte créés en partie 5
     * 
     * @param path 
     * @return TabSymbOcc* 
     */
    TabSymbOcc* compteOccurrenceTraj(char * path);

    /**
     * @brief inspirée de codeFichier
     * 
     * @param srcPath 
     * @param destPath 
     * @param tabSymbCode 
     */
    void codeFichierTraj(char *srcPath, char *destPath, TabSymbCode *tabSymbCode);

    /**
     * @brief inspirée de decodeFichier et qui écrit le fichier texte au format précisé en partie 5
     * 
     * @param srcPath 
     * @param destPath 
     * @param tabSymbCode 
     */
    void decodeFichierTraj(char *srcPath, char *destPath, TabSymbCode *tabSymbCode);
    
    /**
     * @brief écrit sur la première ligne du fichier le nombre de feuilles de l’arbre, puis les lignes correspondant aux feuilles
     * 
     * @param racine 
     */
    void sauveArbreNB_Texte(noeudBinaire *racine);

    /**
     * @brief inspirée de compteOccurrence et adaptée à la configuration des fichiers texte créés à la question 5a
     * 
     * @param path 
     * @return TabSymbOcc* 
     */
    TabSymbOcc* compteOccurrenceNB(char * path);

    /**
     * @brief inspirée de codeFichier
     * 
     * @param srcPath 
     * @param destPath 
     * @param tabSymbCode 
     */
    void codeFichierNB(char *srcPath, char *destPath, TabSymbCode *tabSymbCode);

    /**
     * @brief inspirée de decodeFichier et qui écrit le fichier texte au format précisé à la question 5a de cette partie
     * 
     * @param srcPath 
     * @param destPath 
     * @param tabSymbCode 
     */
    void decodeFichierNB(char *srcPath, char *destPath, TabSymbCode *tabSymbCode);
    
    /**
     * @brief crée le fichier binaire associé au fichier texte ne contenant que des 0 et des 1
     * 
     * @param srcPath 
     * @param destPath 
     */
    void transcritTexteEnBinaireMotif(char *srcPath, char *destPath);

    /**
     * @brief crée le fichier binaire associé au fichier texte ne contenant que des 0 et des 1
     * 
     * @param srcPath 
     * @param destPath 
     */
    void transcritTexteEnBinaireTraj(char *srcPath, char *destPath);

    /**
     * @brief crée le fichier binaire associé au fichier texte ne contenant que des 0 et des 1
     * 
     * @param srcPath 
     * @param destPath 
     */
    void transcritTexteEnBinaireNB(char *srcPath, char *destPath);

    /**
     * @brief crée le fichier texte en clair associé au fichier binaire codé passé en paramètre
     * 
     * @param srcPath 
     * @param destPath 
     */
    void transcritBinaireEnTexteMotif(char *srcPath, char *destPath);

    /**
     * @brief crée le fichier texte en clair associé au fichier binaire codé passé en paramètre
     * 
     * @param srcPath 
     * @param destPath 
     */
    void transcritBinaireEnTexteTraj(char *srcPath, char *destPath);

    /**
     * @brief crée le fichier texte en clair associé au fichier binaire codé passé en paramètre
     * 
     * @param srcPath 
     * @param destPath 
     */
    void transcritBinaireEnTexteNB(char *srcPath, char *destPath);

#endif