#ifndef __VOLTO_MINIPROJET_PART4_HEADER__
    #define __VOLTO_MINIPROJET_PART4_HEADER__

    #ifndef PATH_SIZE_MAX
    #define PATH_SIZE_MAX 255
    #endif

    #include "Image.h"
    #include <stdbool.h>
    #include <stdio.h>
    
    //Constante conventionnelle pour dire qu'un motif n'a pas été reconnu
    #define PAS_UN_MOTIF (Motif) {.forme = -1, .config = -1, .cfond = -1, .cobjet = -1}
    //Constante conventionnelle pour dire qu'une couleur n'a pas été reconnu
    #define PAS_UNE_COULEUR (Couleur) {.rouge = -1, .vert = -1, .bleu = -1}

    //Définie la taille d'un motif
    #define TAILLE_MOTIF 32

    /**
     * @brief Structure contenant les couleurs RVB
     * 
     */
    typedef struct Couleur{

        short int rouge, vert, bleu;
    }Couleur;

    /**
     * @brief Structure contenant plusieurs structures couleurs
     * 
     */
    typedef struct Palette {
        int nb;
        Couleur *pal;
    }Palette;

    /**
     * @brief Stock un motif en accord avec la partie4
     * 
     */
    typedef struct Motif{
        
        short int forme;
        short int config;

        int cfond;
        int cobjet;
    }Motif;
    

    /**
     * @brief Définie l'arbre contenant les motifs
     * 
     */
    typedef struct noeudMotif{

        struct noeudMotif *NO;
        struct noeudMotif *NE;
        struct noeudMotif *SO;
        struct noeudMotif *SE;

        Motif motif;
        int x,y;
        int largeur;

    }noeudMotif;

    /**
     * @brief Créé&Initialise un pointeur sur une structure palette
     * 
     * @param img 
     * @return Palette* 
     */
    Palette* creePalette(Image *img);

    /**
     * @brief Détermine un motif sur l'image en fonction de sa position et largeur
     * 
     * @param img 
     * @param palette 
     * @param x 
     * @param y 
     * @param largeur 
     * @return Motif 
     */
    Motif identifieMotif(Image *img, Palette *palette, int x, int y, int largeur);

    /**
     * @brief Créé un arbre pour stocker les motifs
     * 
     * @param img 
     * @param palette 
     * @param largeur 
     * @param x 
     * @param y 
     * @return noeudMotif* 
     */
    noeudMotif* creeArbreMotifs(Image *img, Palette *palette, int largeur, int x, int y);
    
    /**
     * @brief Sauve la description d'une image dans ./resources/file/description_image.txt
     * 
     * @param rcn 
     * @param pal 
     * @param path 
     */
    void sauveDescriptionImage(noeudMotif *rcn, Palette* pal,char *path);

    /**
     * @brief Ecrit dans la sortie standart les couleurs d'une palette
     * 
     * @param pal 
     */
    void affichePalette(Palette *pal);

    /**
     * @brief Libère un arbre Motif
     * 
     * @param ptrRcn 
     */
    void libereArbreMotif(noeudMotif *ptrRcn);


    /*************************************** FONCTIONS OUTILS ***************************************/


    /**
     * @brief Fonction Outil pour comparer deux couleurs
     * 
     * @param c1 
     * @param c2 
     * @return true 
     * @return false 
     */
    bool compareCouleur (Couleur c1, Couleur c2);

    /**
     * @brief Fonction Outil variadic pour comparer n>=2 couleurs
     * 
     * @param c1 
     * @param c2 
     * @param nbreArgumentsOptionnels 
     * @param ... 
     * @return true 
     * @return false 
     */
    bool compareCouleurVariadic (Couleur c1, Couleur c2, int nbreArgumentsOptionnels,  ...);

    /**
     * @brief Fonction Outil variadic pour déterminer si n structures couleurs sont réellement des couleurs
     * 
     * @param nbreArguments 
     * @param ... 
     * @return true 
     * @return false 
     */
    bool sontDesCouleursVariadic(int nbreArguments, ...);

    /**
     * @brief Fonction Outil pour comparer deux palette
     * 
     * @param p1 
     * @param p2 
     * @return true 
     * @return false 
     */
    bool comparePalette(Palette p1, Palette p2);

    /**
     * @brief Fonction Outil pour comparer deux motifs
     * 
     * @param m1 
     * @param m2 
     * @return true 
     * @return false 
     */
    bool compareMotif(Motif m1, Motif m2);

    /**
     * @brief Fonction Outil pour verifier si une couleur est présente dans un tableau
     * 
     * @param arr 
     * @param arrSize 
     * @param couleur 
     * @return true 
     * @return false 
     */
    bool estCouleurDans(Couleur *arr, int arrSize, Couleur couleur);

    /**
     * @brief Fonction Outil vérifier si une zone carrée dans une image est de couleur uniforme, si oui renvoie la couleur
     * 
     * @param img 
     * @param depart_x 
     * @param depart_y 
     * @param largeur 
     * @param hauteur 
     * @return Couleur 
     */
    Couleur estCouleurUniforme(Image *img, int depart_x, int depart_y, int largeur, int hauteur);

    /**
     * @brief Version évoluée de la fonction estCouleurUniforme
     * 
     * @param img 
     * @param x1 
     * @param y1 
     * @param x2 
     * @param y2 
     * @return Couleur 
     */
    Couleur estCouleurUniformeDansCarre(Image *img, int x1, int y1, int x2, int y2);

#endif