#ifndef __VOLTO_MINIPROJET_PART5_TOOLS_H__
    #define __VOLTO_MINIPROJET_PART5_H_TOOLS__

    #include "Image.h"
    #include "Partie4.h"
    #include <stdbool.h>

    typedef struct Point{
        unsigned int x,y;
    }Point;

    /**
     * @brief Maillon qui stock une position
     * 
     */
    typedef struct MaillonTraj{
        Point position;
        struct MaillonTraj *next;
    }MaillonTraj;

    /**
     * @brief Structure qui stock une liste chainée (trajectoire du robot), sa position en temps réel, les positions de  départ et d'arrivée du robot
     * 
     */
    typedef struct Robot{
        MaillonTraj *ptrTete;
        Point position;
        Point depart;
        Point arrivee;
    }Robot;
    
    /**
     * @brief Structure qui définie un carré sur une image
     * 
     */
    typedef struct Emplacement{
        Point origine;
        unsigned int largeurMotif;
    }Emplacement;

    /**
     * @brief Fonction qui écrit dans une image le motif 1 avec sa trajectoire associée ou non
     * 
     * @param img 
     * @param palette 
     * @param motif 
     * @param emplacement 
     * @param trajectoire 
     */
    void dessineMotif1(const Image *img, const Palette palette, const  Motif motif, const Emplacement emplacement, const bool trajectoire);
    /**
     * @brief Fonction qui écrit dans une image le motif 2 avec sa trajectoire associée ou non
     * 
     * @param img 
     * @param palette 
     * @param motif 
     * @param emplacement 
     * @param trajectoire 
     */
    void dessineMotif2(const Image *img, const Palette palette, const Motif motif, const Emplacement emplacement, const bool trajectoire);
    /**
     * @brief Fonction qui écrit dans une image le motif 3 avec sa trajectoire associée ou non
     * 
     * @param img 
     * @param palette 
     * @param motif 
     * @param emplacement 
     * @param trajectoire 
     */
    void dessineMotif3(const Image *img, const Palette palette, const Motif motif, const Emplacement emplacement, const bool trajectoire);
    /**
     * @brief Fonction qui écrit dans une image le motif 4 avec sa trajectoire associée ou non
     * 
     * @param img 
     * @param palette 
     * @param motif 
     * @param emplacement 
     * @param trajectoire 
     */
    void dessineMotif4(const Image *img, const Palette palette, const Motif motif, const Emplacement emplacement, const bool trajectoire);
    /**
     * @brief Fonction qui écrit dans une image le motif 5 avec sa trajectoire associée ou non
     * 
     * @param img 
     * @param palette 
     * @param motif 
     * @param emplacement 
     * @param trajectoire 
     */
    void dessineMotif5(const Image *img, const Palette palette, const Motif motif, const Emplacement emplacement, const bool trajectoire);
    /**
     * @brief Fonction qui écrit dans une image le motif 6 avec sa trajectoire associée ou non
     * 
     * @param img 
     * @param palette 
     * @param motif 
     * @param emplacement 
     * @param trajectoire 
     */
    void dessineMotif6(const Image *img, const Palette palette, const Motif motif, const Emplacement emplacement, const bool trajectoire);

    /**
     * @brief Fonction qui appelle une fonction dessineMotifX en fonction de l'id de la forme en paramètre
     * 
     * @param img 
     * @param palette 
     * @param noeud 
     * @param formeId 
     * @param trajectoire 
     */
    void dessineMotifNoeud(const Image *img,const Palette palette, const noeudMotif *noeud, const int formeId, const bool trajectoire);

    /**
     * @brief Fonction qui desssine tous les motifs sur une image avec leurs trajectoires ou non
     * 
     * @param img 
     * @param palette 
     * @param racine 
     * @param trajectoire 
     */
    void dessineCarte(const Image *img, const Palette palette, const noeudMotif *racine, const bool trajectoire);

    /**
     * @brief Calcule et stock la trajectoire d'un robot dans une structure Robot
     * 
     * @param img 
     * @param imgSansTrajectoires 
     * @param palette 
     * @param depart 
     * @param arrivee 
     * @return Robot* 
     */
    Robot* calculeTrajectoire(Image *img, Image *imgSansTrajectoires, Palette *palette, Point depart, Point arrivee);

    /**
     * @brief Sauve les données de trajectoire d'une structure robot
     * 
     * @param robot 
     * @param path 
     */
    void sauveDescriptionChemin(Robot *robot, char* path);

    /**
     * @brief Fonction Outil pour comparer deux structures points
     * 
     * @param p1 
     * @param p2 
     * @return true 
     * @return false 
     */
    bool comparePoints(Point p1, Point p2);

    /**
     * @brief Fonction Outil variadic pour comparer n>=2 structures points
     * 
     * @param p1 
     * @param p2 
     * @param nbreArgumentsOptionnels 
     * @param ... 
     * @return true 
     * @return false 
     */
    bool comparePointsVariadic (Point p1, Point p2, int nbreArgumentsOptionnels,  ...);

    /**
     * @brief Fonction Outil: -1 => n'est pas dans le voisinage/0->haut gauche,1->haut,2->haut droite,3->droite,4->bas droite,5->bas,6->bas gauche, 7->gauche, 8-> même points
     * 
     * @param p1 
     * @param p2 
     * @return int 
     */
    int positionVoisinage(Point p1, Point p2);

    /**
     * @brief Fonction Outil qui dessine un carré dans une image
     * 
     * @param img 
     * @param couleur 
     * @param emplacement 
     */
    void dessineCarre(const Image *img, const Couleur couleur, const Emplacement emplacement);

    /**
     * @brief fonction Outil pour créer le pointeur tête d'une chaine de maillons
     * 
     * @param position 
     * @return MaillonTraj* 
     */
    MaillonTraj* creeMaillon(const Point position);

    /**
     * @brief Fonction Outil pour insérer un maillon avant un autre maillon d'une liste chainée
     * 
     * @param ptrTete 
     * @param index 
     * @param maillonAInserer 
     */
    void insererMaillonAvant(MaillonTraj *ptrTete, const int index, MaillonTraj *maillonAInserer);

    /**
     * @brief Fonction Outil pour ajouter un maillon à la fin d'une liste chainée
     * 
     * @param ptrTete 
     * @param maillonAAjouter 
     */
    void ajouterMaillon(MaillonTraj *ptrTete, MaillonTraj *maillonAAjouter);

    /**
     * @brief Fonction Outil pour supprimer un maillon dans une liste chainée en fonction de sa valeur
     * 
     * @param ptrTete 
     * @param position 
     */
    void supprimerMaillonParValeur(MaillonTraj *ptrTete, const Point position);

    /**
     * @brief Fonction Outil pour supprimer un maillon d'une liste chainée
     * 
     * @param ptrTete 
     * @param maillon 
     */
    void supprimerMaillon(MaillonTraj *ptrTete, MaillonTraj **maillon);
    
    /**
     * @brief Fonction Outil pour trouver l'index d'un maillon dans sa liste chainée
     * 
     * @param ptrTete 
     * @param position 
     * @return int 
     */
    int trouverIndexParValeur(MaillonTraj *ptrTete, const Point position);

    /**
     * @brief Fonction Outil pour libérer un maillon
     * 
     * @param maillon 
     */
    void libereMaillon(MaillonTraj **maillon);

#endif