#include "../include/Partie6.h"
#include "../include/Partie4.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

void triTabSymbOcc(TabSymbOcc *tabSymbOcc)
{
    for (int i = 0; i < tabSymbOcc->nb; i++)
    {
        int min = tabSymbOcc->tab[i].occ;
        int index = -1;
        for (int j = i; j < tabSymbOcc->nb; j++)
        {
            if(tabSymbOcc->tab[j].occ < min)
            {
                min = tabSymbOcc->tab[j].occ;
                index = j;
            }
        }
        if(index > -1)
        {
            SymbOcc tmp3 = (SymbOcc){tabSymbOcc->tab[index].symb, tabSymbOcc->tab[index].occ};
            SymbOcc tmp = (SymbOcc){tabSymbOcc->tab[i].symb, tabSymbOcc->tab[i].occ};
            for(int k = i; k != index; k++)
            {
                SymbOcc tmp2 = (SymbOcc){tabSymbOcc->tab[k+1].symb, tabSymbOcc->tab[k+1].occ};
                tabSymbOcc->tab[k+1].symb = tmp.symb;
                tabSymbOcc->tab[k+1].occ = tmp.occ;
                tmp = tmp2;
                
            }
            tabSymbOcc->tab[i] = tmp3;
        }
        
    }
}

TabSymbOcc* compteOccurrence(char *path){
    FILE * fp;
    
    fp = fopen (path,"r");
    if(!fp) return NULL;

    //Récupère la taille du fichier
    int fileSize;
    fseek(fp, 0L, SEEK_END);
    fileSize = ftell(fp);
    rewind(fp);

    TabSymbOcc *ret = (TabSymbOcc*) malloc(sizeof(TabSymbCode));
    ret->nb = 0;
    ret->tab = NULL;


    char* symb = (char*) malloc(sizeof(char)*fileSize);
    symb[0] = '\0';

    int c = 0;
    while (c != EOF)
    {
        c = fgetc(fp);
        //Si c'est un nombre ou un signe négatif, on le rajoute au symb
        if((c >= 48 && c <= 57) || c == 45)
        {
            sprintf(symb,"%s%c",symb,c);
        }
        else
        {
            int isIn = 0;
            int val = atoi(symb);
            SymbOcc tmp[ret->nb];
            if(ret->nb){
                for(int i=0;i<ret->nb;i++){
                    if(ret->tab[i].symb==val){
                        ret->tab[i].occ++;
                        isIn = 1;
                        break;
                    }
                    else{
                        tmp[i] = ret->tab[i];
                    }
                }
            }
            if(!isIn)
            {
                ret->nb++;
                ret->tab = (SymbOcc*) realloc(ret->tab,sizeof(SymbOcc)*ret->nb);
                if(ret->nb-1) for(int i=0;i<ret->nb;i++) ret->tab[i] = tmp[i];
                ret->tab[ret->nb-1] = (SymbOcc){val,1}; 
            }
            
            symb=strcpy(symb,"");
            
        }
        
    }
    free(symb);
    /* close the file*/  
    fclose (fp);

    triTabSymbOcc(ret);
    return ret;
}

void triNoeudArbreCodeOcc(NoeudCode **arbre, int taille)
{

    for (int i = 0; i < taille; i++)
    {
        int min = arbre[i]->so.occ;
        int index = -1;
        for (int j = i; j < taille; j++)
        {
            if(arbre[j]->so.occ < min)
            {
                min = arbre[j]->so.occ;
                index = j;
            }
        }
        if(index > -1)
        {
            NoeudCode *tmp3 = arbre[index];
            NoeudCode *tmp = arbre[i];
            for(int k = i; k != index; k++)
            {
                NoeudCode *tmp2 = arbre[k+1];
                arbre[k+1] = tmp;
                tmp = tmp2;
                
            }
            arbre[i] = tmp3;
        }
        
    }
}

/**
 * @brief Fonction auxiliaire pour trier une liste de pointeur sur NoeudCode
 * 
 * @param arbre 
 * @param taille 
 */
void deplaceNoeudArbreCodeOcc(NoeudCode **arbre, int taille)
{

    for (int i = 0; i < taille; i++)
    {
        if(arbre[i]->so.symb == -666)
        {
            int min = arbre[i]->so.occ;
            int index = -1;
            for (int j = i; j < taille; j++)
            {
                if(arbre[j]->so.occ <= min)
                {
                    min = arbre[j]->so.occ;
                    index = j;
                }
            }

            if(index > -1)
            {
                SymbOcc tmp3 = (SymbOcc){arbre[i]->so.symb, arbre[i]->so.occ};
                for(int k = i; k != index; k++)
                {
                    arbre[k]->so.symb = arbre[k+1]->so.symb;
                    arbre[k]->so.occ = arbre[k+1]->so.occ;
                }
                arbre[index]->so = tmp3;
            }
        }
        
        
    }
}

NoeudCode* creeArbreCodeOcc(TabSymbOcc *tabSymbOcc){
    NoeudCode **arbre = (NoeudCode**) malloc(sizeof(NoeudCode*)*tabSymbOcc->nb);
    int tailleArbre = tabSymbOcc->nb;

    triTabSymbOcc(tabSymbOcc);
    for (int i = 0; i < tabSymbOcc->nb; i++)
    {
        arbre[i] = (NoeudCode*) malloc(sizeof(NoeudCode));
        arbre[i]->ptrfils0 = NULL;
        arbre[i]->ptrfils1 = NULL;
        arbre[i]->so = tabSymbOcc->tab[i];
    }

    while (tailleArbre > 1) 
    {
        
        for (int i = 0; i < tailleArbre; i++)
        {

            NoeudCode *nouveauNoeud = (NoeudCode*) malloc(sizeof(NoeudCode));
            nouveauNoeud->so.occ = arbre[i]->so.occ + arbre[i+1]->so.occ;
            nouveauNoeud->so.symb = -666;
            nouveauNoeud->ptrfils0 = arbre[i];
            nouveauNoeud->ptrfils1 = arbre[i+1];

            NoeudCode *tmp[tailleArbre-2];
            int index = 0;
            for(int j=0;j<tailleArbre;j++){
                if(j != i && j != i+1){
                    tmp[index] = arbre[j];
                    index++;
                }
            }
            tailleArbre--;
            free(arbre);
            arbre = (NoeudCode**) malloc(sizeof(NoeudCode*)*tailleArbre);

            for(int j=0;j<tailleArbre-1;j++)
            {
                arbre[j] = tmp[j];
            }
            arbre[tailleArbre-1] = nouveauNoeud;
            triNoeudArbreCodeOcc(arbre,tailleArbre);
            deplaceNoeudArbreCodeOcc(arbre,tailleArbre);
            break;
            
                
        }
    }

    NoeudCode *racine = *arbre;
    free(arbre);
    return racine;

}

void printArbreCodeAdaptatif(NoeudCode *racine)
{
    if(racine)
    {
        printArbreCodeAdaptatif(racine->ptrfils0);
        
        printArbreCodeAdaptatif(racine->ptrfils1);
        if(racine->so.symb != -666)
            printf("symb=%d occ=%d %p\n",racine->so.symb,racine->so.occ,racine);
    }
}


void libereArbreCodeAdaptatif(NoeudCode *racine)
{
    if(racine)
    {
        libereArbreCodeAdaptatif(racine->ptrfils0);
        libereArbreCodeAdaptatif(racine->ptrfils1);
        free(racine);
    }
}

TabSymbCode* creeCodeAdaptatif(NoeudCode *racine)
{
    if(racine)
    {
        
        TabSymbCode *ret = (TabSymbCode*) malloc(sizeof(TabSymbCode));

        if(racine->ptrfils0 == NULL && racine->ptrfils1 == NULL){

            ret->nb = 1;
            ret->tab = (SymbCode*) malloc(sizeof(SymbCode));
            
            char *seq = (char*) malloc(sizeof(char));
            seq[0] ='\0';
            ret->tab[0] = (SymbCode){racine->so.symb,seq};
        }
        else{
            
            TabSymbCode *tmp0 = creeCodeAdaptatif(racine->ptrfils0);
            TabSymbCode *tmp1 = creeCodeAdaptatif(racine->ptrfils1);
            
            ret->tab = (SymbCode*) malloc(sizeof(SymbCode)*((tmp0?tmp0->nb:0)+(tmp1?tmp1->nb:0)));
            ret->nb = (tmp0?tmp0->nb:0)+(tmp1?tmp1->nb:0);
            
            int i = 0;
            if(tmp0)
            {
                
                for (int j = 0; j < tmp0->nb; j++)
                {
                    char *seq = (char*) malloc(sizeof(char)*(strlen(tmp0->tab[j].symbCode)+1));
                    seq[0] = '\0';
                    strcat(seq,"0");
                    strcat(seq,tmp0->tab[j].symbCode);
                    free(tmp0->tab[j].symbCode);
                    tmp0->tab[j].symbCode = seq;
                }

                for(int j=0;j<tmp0->nb;j++){
                    ret->tab[j] = tmp0->tab[j];
                    i++;
                }
            }
            
            if(tmp1)
            {
                for (int j = 0; j < tmp1->nb; j++)
                {
                    char *seq = (char*) malloc(sizeof(char)*(strlen(tmp1->tab[j].symbCode)+1));
                    seq[0] = '\0';
                    strcat(seq,"1");
                    strcat(seq,tmp1->tab[j].symbCode);
                    free(tmp1->tab[j].symbCode);
                    tmp1->tab[j].symbCode = seq;
                }
                
                for(int j=0;j<tmp1->nb;j++){
                    ret->tab[i+j] = tmp1->tab[j];
                }
                
            }
            
        }

        return ret;
    }
    
    return NULL;
}

void sauveCodeAdaptatif(char *destPath, TabSymbCode *tabSymbCode)
{
    FILE * pDestFile = NULL;
    FILE * pSrcFile = NULL;

    /* open the file for writing*/
    pDestFile = fopen (destPath,"w");
    if(!pDestFile) return;

    for (int i = 0; i < tabSymbCode->nb; i++)
    {
        fflush(pDestFile);
        fprintf(pDestFile,"%d:%s\n",tabSymbCode->tab[i].symb, tabSymbCode->tab[i].symbCode);
    }
    /* close the file*/
    fclose(pDestFile);
}

TabSymbCode* litCodeAdaptatif(char *path)
{

    FILE * pSrcFile = NULL;

    /* open the file for writing*/
    pSrcFile = fopen (path,"r");
    if(!pSrcFile) return NULL;

    TabSymbCode *ret = (TabSymbCode*) malloc(sizeof(TabSymbCode));

    ret->nb = 0;
    ret->tab = NULL;

    int symb;
    char tmpSymbCode[50];
    while (fscanf(pSrcFile,"%d:%s\n",&symb,tmpSymbCode) != EOF)
    {
        SymbCode tmp[ret->nb];
        if(ret->nb > 0) for(int i = 0;i<ret->nb;i++) tmp[i]=ret->tab[i];
        ret->tab = (SymbCode*) realloc(ret->tab,sizeof(SymbCode)*(ret->nb+1));
        if(ret->nb > 0) for(int i = 0;i<ret->nb;i++) ret->tab[i]=tmp[i];
        char *symbCode = (char*) malloc(sizeof(char)*strlen(tmpSymbCode));
        symbCode = strcpy(symbCode,tmpSymbCode);
        ret->tab[ret->nb] = (SymbCode) {symb,symbCode};
        ret->nb++;
    }

    /* close the file*/
    fflush(pSrcFile);
    fclose(pSrcFile);

    return ret;
}

NoeudCode* creeArbreCodeAdaptatif(TabSymbCode *tabSymbCode)
{
    NoeudCode *racine = (NoeudCode*) malloc(sizeof(NoeudCode));
    racine->ptrfils0 = NULL;
    racine->ptrfils1 = NULL;
    racine->so.symb = -666;
    racine->so.occ = 0;

    for (int i = 0; i < tabSymbCode->nb; i++)
    {
        NoeudCode *current = racine;
        for (int j = 0; j < strlen(tabSymbCode->tab[i].symbCode); j++)
        {
            
            if(tabSymbCode->tab[i].symbCode[j] == '0')
            {
                if(current->ptrfils0 == NULL)
                {
                    current->ptrfils0 = (NoeudCode*) malloc(sizeof(NoeudCode));
                    current->ptrfils0->ptrfils0 = NULL;
                    current->ptrfils0->ptrfils1 = NULL;
                }

                current = current->ptrfils0;
                
                //feuille
                if(j == strlen(tabSymbCode->tab[i].symbCode)-1)
                {
                    current->so.symb = tabSymbCode->tab[i].symb;
                    current->so.occ = 0; //OCCURRENCES ????
                }
                
                else
                {
                    current->so.symb = -666;
                    current->so.occ = 0; //OCCURRENCES ????
                }
            }
            else if(tabSymbCode->tab[i].symbCode[j] == '1')
            {
                if(current->ptrfils1 == NULL)
                {
                    current->ptrfils1 = (NoeudCode*) malloc(sizeof(NoeudCode));
                    current->ptrfils1->ptrfils0 = NULL;
                    current->ptrfils1->ptrfils1 = NULL;
                }
                current = current->ptrfils1;
                
                //feuille
                if(j == strlen(tabSymbCode->tab[i].symbCode)-1)
                {
                    current->so.symb = tabSymbCode->tab[i].symb;
                    current->so.occ = 0; //OCCURENCES ??
                }
                else
                {
                    current->so.symb = -666;
                    current->so.occ = 0; //OCCURRENCES ????
                }
            }
        }
    }

    return racine;
    
}

/**
 * @brief 
 * 
 * @param srcPath 
 * @param destPath 
 */
void codeFichier(char *srcPath, char *destPath, TabSymbCode *tabSymbCode)
{
    FILE * pSrcFile = NULL;
    FILE * pDestFile = NULL;

    /* open the file for writing*/
    pSrcFile = fopen (srcPath,"r");
    if(!pSrcFile) return NULL;
    
    pDestFile = fopen (destPath,"w");
    if(!pSrcFile){
        fclose(pSrcFile);
        return NULL;
    }

    int fileSize;
    fseek(pSrcFile, 0L, SEEK_END);
    fileSize = ftell(pSrcFile);
    rewind(pSrcFile);

    char* symb = (char*) malloc(sizeof(char)*fileSize);
    symb[0] = '\0';

    int c = 0;
    while (c != EOF)
    {
        c = fgetc(pSrcFile);
        if((c >= 48 && c <= 57) || c == 45)
        {
            sprintf(symb,"%s%c",symb,c);
        }
        else
        {
            int val = atoi(symb);
            if(strlen(symb) > 0)
            {
                for (int i = 0; i < tabSymbCode->nb; i++)
                {
                    if(tabSymbCode->tab[i].symb == val)
                    {
                        fflush(pDestFile);
                        fprintf(pDestFile,"%s",tabSymbCode->tab[i].symbCode);
                    }
                }
            }
                

            symb=strcpy(symb,"");
            
        }
        
    }
    free(symb);
    /* close the file*/
    fclose (pSrcFile);
    fclose (pDestFile);
    


}

int isValueIn(int* tab, int nb, int val)
{
    for (int i = 0; i < nb; i++)
    {
        if(tab[i] == val)
            return 1;        
    }

    return 0;
}

void decodeFichier(char *srcPath, char *destPath, TabSymbCode *tabSymbCode)
{
    FILE * pSrcFile = NULL;
    FILE * pDestFile = NULL;

    /* open the file for writing*/
    pSrcFile = fopen (srcPath,"r");
    if(!pSrcFile) 
    {
        return ;
    }
    pDestFile = fopen (destPath,"w");
    if(!pSrcFile){
        fclose(pSrcFile);
        return ;
    }
    
    int fileSize;
    fseek(pSrcFile, 0L, SEEK_END);
    fileSize = ftell(pSrcFile);
    rewind(pSrcFile);

    int *values = NULL;
    int nb = 0;

    char* symbCode = (char*) malloc(sizeof(char)*fileSize);
    symbCode[0] = '\0';

    int c = 0;
    
    while (c != EOF)
    {
        
        c = fgetc(pSrcFile);
        
        if(strlen(symbCode) > 0)
        {
            
            for (int i = 0; i < tabSymbCode->nb; i++)
            {
                
                if(strcmp(tabSymbCode->tab[i].symbCode,symbCode) == 0)
                {
                    
                    if(!isValueIn(values,nb,tabSymbCode->tab[i].symb))
                    {
                        
                        if(nb)
                        {
                            int tmp[nb];
                            for(int i=0;i<nb;i++) tmp[i] = values[i];
                            nb++;
                            values = (int*) realloc(values,sizeof(int)*nb);
                            for(int i=0;i<nb;i++) values[i] = tmp[i];
                        }
                        else{
                            nb++;
                            values = (int*) realloc(values,sizeof(int)*nb);
                        }
                        
                        values[nb-1] = tabSymbCode->tab[i].symb;
                        
                        fprintf(pDestFile,"%d:%s\n",tabSymbCode->tab[i].symb,tabSymbCode->tab[i].symbCode);
                    }
                    symbCode=strcpy(symbCode,"");
                    break;
                }
            }
        }

        if(c == 48 || c == 49)
        {
            sprintf(symbCode,"%s%c",symbCode,c);
        }
        
    }
    free(values);
    free(symbCode);
    /* close the file*/  
    fclose (pSrcFile);
    fclose (pDestFile);
    
}

int interpretStringToBinary(char *string)
{
    int b = 0b0;
    
    for(int i = 0; i < strlen(string); i++)
    {
        if(string[i] == '1')
        {
            if(strlen(string)-i-1>=0) b += 0b1 << (strlen(string)-i-1);
        }
    }
    return b;
}

void transcritTexteEnBinaire(char *srcPath, char *destPath)
{
    FILE * pSrcFile = NULL;
    FILE * pDestFile = NULL;

    /* open the file for writing*/
    pSrcFile = fopen (srcPath,"r");
    if(!pSrcFile) return NULL;
    
    pDestFile = fopen (destPath,"wb");
    if(!pSrcFile){
        fclose(pSrcFile);
        return NULL;
    }

    int fileSize;
    fseek(pSrcFile, 0L, SEEK_END);
    fileSize = ftell(pSrcFile);
    rewind(pSrcFile);

    char* symb = (char*) malloc(sizeof(char)*fileSize);
    symb[0] = '\0';

    int c = 0;
    while (c != EOF)
    {
        c = fgetc(pSrcFile);
        if(strlen(symb) < 7 && c != EOF)
        {
            sprintf(symb,"%s%c",symb,c);
        }
        else
        {
            sprintf(symb,"%s%c",symb,c);
            unsigned char b = interpretStringToBinary(symb);
            if(8-strlen(symb)<8) b <<= 8-strlen(symb);
            fflush(pDestFile);
            fwrite(&b, 1, 1, pDestFile);
                

            symb=strcpy(symb,"");
            
        }
        
    }
    free(symb);
    
    /* close the file*/  
    fclose (pSrcFile);
    fclose (pDestFile);
}


void transcritBinaireEnTexte(char *srcPath, char *destPath)
{
    FILE * pSrcFile = NULL;
    FILE * pDestFile = NULL;

    /* open the file for writing*/
    pSrcFile = fopen (srcPath,"rb");
    if(!pSrcFile) return NULL;
    
    pDestFile = fopen (destPath,"w");
    if(!pSrcFile){
        fclose(pSrcFile);
        return NULL;
    }

    int c = 0;
    c = fgetc(pSrcFile);
    while (c != EOF)
    {
        if(c != EOF)
        {
            int tmpC = c;
            c = fgetc(pSrcFile);

            int lastIndex = -1;
            for (int i = 7; i >=0; i--) if(tmpC&0b1<<i) lastIndex=i;
                
            for (int i = 7; i >=0; i--)
            {
                if(c==EOF)
                {
                    if(lastIndex != -1 && i >= lastIndex)
                    {
                        (tmpC&0b1<<i)?fputc('1',pDestFile):fputc('0',pDestFile);
                    }
                }
                else{
                    (tmpC&0b1<<i)?fputc('1',pDestFile):fputc('0',pDestFile);
                }
            }
            
            
        }
        
        
    }
    
    /* close the file*/  
    fclose (pSrcFile);
    fclose (pDestFile);
}

TabSymbOcc* compteOccurrenceMotif(char * path)
{
    return compteOccurrence(path);
}

void codeFichierMotif(char *srcPath, char *destPath, TabSymbCode *tabSymbCode)
{
    codeFichier(srcPath,destPath,tabSymbCode);
}

void decodeFichierMotif(char *srcPath, char *destPath, TabSymbCode *tabSymbCode)
{
    decodeFichier(srcPath,destPath,tabSymbCode);
}
TabSymbOcc* compteOccurrenceTraj(char * path)
{
    return compteOccurrence(path);
}
void codeFichierTraj(char *srcPath, char *destPath, TabSymbCode *tabSymbCode)
{
    codeFichier(srcPath,destPath,tabSymbCode);
}
void decodeFichierTraj(char *srcPath, char *destPath, TabSymbCode *tabSymbCode)
{
    decodeFichier(srcPath,destPath,tabSymbCode);
}

void fprintfArbreBinaire(FILE * fp,noeudBinaire* rcn)
{
    if(rcn){
        if(rcn->etiquette != -1)
            fprintf(fp, "\n%d %d %d %d",rcn->start_x,rcn->start_y,rcn->size,rcn->value);
        fprintfArbreBinaire(fp, rcn->NW);
        fprintfArbreBinaire(fp, rcn->NE);
        fprintfArbreBinaire(fp, rcn->SW);
        fprintfArbreBinaire(fp, rcn->SE);
    }
}

void sauveArbreNB_Texte(noeudBinaire *racine)
{
    FILE * fp;


    fp = fopen ("./resources/partie6/arbreNB_Texte.txt","w");
    
    
    fprintf (fp, "%d",compteFeuille(racine));
    
    fprintfArbreBinaire(fp, racine);
    fclose (fp);
}


TabSymbOcc* compteOccurrenceNB(char * path)
{
    return compteOccurrence(path);
}
void codeFichierNB(char *srcPath, char *destPath, TabSymbCode *tabSymbCode){
    codeFichier(srcPath,destPath,tabSymbCode);
}
void decodeFichierNB(char *srcPath, char *destPath, TabSymbCode *tabSymbCode)
{
    decodeFichier(srcPath,destPath,tabSymbCode);
}
void transcritTexteEnBinaireMotif(char *srcPath, char *destPath)
{
    transcritTexteEnBinaire(srcPath,destPath);
}
void transcritTexteEnBinaireTraj(char *srcPath, char *destPath)
{
    transcritTexteEnBinaireMotif(srcPath,destPath);
}
void transcritTexteEnBinaireNB(char *srcPath, char *destPath)
{
    transcritTexteEnBinaire(srcPath,destPath);
}
void transcritBinaireEnTexteMotif(char *srcPath, char *destPath)
{
    transcritBinaireEnTexte(srcPath,destPath);
}
void transcritBinaireEnTexteTraj(char *srcPath, char *destPath)
{
    transcritBinaireEnTexte(srcPath,destPath);
}
void transcritBinaireEnTexteNB(char *srcPath, char *destPath)
{
    transcritBinaireEnTexte(srcPath,destPath);
}