#include "../include/Partie4.h"
#include <stdarg.h>
#include <stdlib.h>

bool compareCouleur (Couleur c1, Couleur c2)
{
    if(c1.rouge==c2.rouge&&c1.vert==c2.vert&&c1.bleu==c2.bleu) return true;
    return false;
}

bool compareCouleurVariadic (Couleur c1, Couleur c2, int nbreArgumentsOptionnels,  ...)
{
    if(c1.rouge!=c2.rouge||c1.vert!=c2.vert||c1.bleu!=c2.bleu) return false;

    va_list ap;
    va_start (ap, nbreArgumentsOptionnels);
    Couleur tmp;
    for (int i = 0; i < nbreArgumentsOptionnels; i++){
        tmp = va_arg (ap, Couleur);
        if(c1.rouge!=tmp.rouge||c1.vert!=tmp.vert||c1.bleu!=tmp.bleu) return false;
    }
    va_end (ap);
    return true;
}

bool sontDesCouleursVariadic(int nbreArguments, ...){
    va_list ap;
    va_start (ap, nbreArguments);

    for (int i = 0; i < nbreArguments; i++)
        if(compareCouleur(va_arg (ap, Couleur), PAS_UNE_COULEUR) == true) return false;
    va_end (ap);
    return true;
}

bool comparePalette(Palette p1, Palette p2)
{
    if(p1.nb != p2.nb) return false;
    for(int i = 0; i < p1.nb; i++) if(!compareCouleur(p1.pal[i],p2.pal[i])) return false;
    return true;
}

bool compareMotif(Motif m1, Motif m2)
{
    return (m1.forme==m2.forme&&m1.config==m2.config&&m1.cfond==m2.cfond&&m1.cobjet==m2.cobjet)?true:false;
}

bool estCouleurDans(Couleur *arr, int arrSize, Couleur couleur){

    if(arr == NULL)
        return false;

    for (int i = 0; i < arrSize; i++)
    {
        if(compareCouleur(arr[i],couleur))
            return true;
    }
    
    return false;
}

Palette* creePalette(Image *img){
    Palette* ret = (Palette*) malloc(sizeof(Palette));

    //Init la structure Palette
    ret->nb = 0;
    ret->pal = NULL;

    for (int x = 0; x < img->largeur; x++)
    {
        for (int y = 0; y < img->hauteur; y++)
        {
            //Si la couleur n'est pas déjà dans le tableau
            if(!estCouleurDans(ret->pal, ret->nb, (Couleur) { .rouge = img->rouge[x][y], .vert = img->vert[x][y], .bleu = img->bleu[x][y]})){
                
                //Sauvegarde les valeurs présentes dans le tab
                Couleur tmp[ret->nb];
                for(int i = 0; i < ret->nb; i++)
                    tmp[i] = ret->pal[i];
                
                //Réalloue le tab une taille au dessus
                ret->nb++;
                ret->pal = (Couleur*) realloc(ret->pal, ret->nb * sizeof(Couleur));

                //Remplie le nouveau tableau avec les valeurs sauvegardées
                for(int i = 0; i < ret->nb-1; i++)
                    ret->pal[i] = tmp[i];
                
                //Remplie la nouvelle couleur
                ret->pal[ret->nb-1] = (Couleur) { .rouge = img->rouge[x][y], .vert = img->vert[x][y], .bleu = img->bleu[x][y]};
            }
        }
    }
    return ret;
}


void affichePalette(Palette *pal){
    if(pal == NULL)
        return;

    printf("Nombre de couleurs: %d\n", pal->nb);
    
    for (int i = 0; i < pal->nb; i++)
    {
        printf("R:%d - V:%d - B:%d\n", pal->pal[i].rouge, pal->pal[i].vert, pal->pal[i].bleu);
    }
}


Couleur estCouleurUniformeDansCarre(Image *img, int x1, int y1, int x2, int y2){
    //Stock la première couleur 
    Couleur couleur = (Couleur) {.rouge = img->rouge[x1][y1], .vert = img->vert[x1][y1], .bleu = img->bleu[x1][y1]};

    //Parcours le carré en fonction de x1,y1,x2,y2, peut parcourir le carré à partir de n'importe quel de ses sommets
    for(int y = y1; (y1>y2)?y+3>=y2:y-3<=y2; (y1>y2)?y--:y++){
        for(int x = x1; (x1>x2)?x+3>=x2:x-3<=x2; (x1>x2)?x--:x++){
            Couleur couleurCourante = (Couleur) {.rouge = img->rouge[x][y], .vert = img->vert[x][y], .bleu = img->bleu[x][y]};
            if(compareCouleur(couleur,couleurCourante) == false)
                return PAS_UNE_COULEUR;
        }
    }

    return couleur;
}

//Version obsolette de estCouleurUniformeDansCarre. Cette fonction suppose que depart_x et depart_y sont toujours en bas à gauche du carré
Couleur estCouleurUniforme(Image *img, int depart_x, int depart_y, int largeur, int hauteur){
    Couleur couleur = (Couleur) {.rouge = img->rouge[depart_x][depart_y], .vert = img->vert[depart_x][depart_y], .bleu = img->bleu[depart_x][depart_y]};

    for(int y = depart_y; y < hauteur+depart_y; y++){
        for(int x = depart_x; x < largeur+depart_x; x++){
            Couleur couleurCourante = (Couleur) {.rouge = img->rouge[x][y], .vert = img->vert[x][y], .bleu = img->bleu[x][y]};
            if(compareCouleur(couleur,couleurCourante) == false)
                return PAS_UNE_COULEUR;
        }
    }

    return couleur;
}

/**
 * @brief Fonction auxiliaire qui détermine la configuration d'un motif en fonction de plusieurs paramètres
 * 
 * @param palette 
 * @param forme 
 * @param couleurNO 
 * @param autreCouleur 
 * @return Motif 
 */
Motif determineConfig(Palette *palette, int forme, Couleur couleurNO, Couleur autreCouleur){

    for(int i = 0; i < palette->nb; i++){
        if(compareCouleur(couleurNO, palette->pal[i])){
            //Couleur NO est le fond
            if(i == 0){
                int cobjet = -1;
                for(int j = 0; j < palette->nb; j++){
                    if(compareCouleur(autreCouleur, palette->pal[j])){
                        cobjet = j;
                        break;
                    }
                }
                return (Motif){.forme = forme, .config = (forme == 5)?1:0, .cfond = i, .cobjet = cobjet};
            }
            else{
                int cfond = -1;
                for(int j = 0; j < palette->nb; j++){
                    if(compareCouleur(autreCouleur, palette->pal[j])){
                        cfond = j;
                        break;
                    }
                }
                return (Motif){.forme = forme, .config = (forme == 5)?0:1, .cfond = cfond, .cobjet = i};
            }
        }
    }

    return PAS_UN_MOTIF;
}

Motif identifieMotif(Image *img, Palette *palette, int depart_x, int depart_y, int largeur){

    if(largeur != TAILLE_MOTIF)
        return PAS_UN_MOTIF;
        
    Couleur couleurNO, couleurNE, couleurSO, couleurSE;

    couleurNO = estCouleurUniforme(img,depart_x,depart_y+16,largeur/2,largeur/2);
    couleurNE = estCouleurUniforme(img,depart_x+16,depart_y+16,largeur/2,largeur/2);
    couleurSO = estCouleurUniforme(img,depart_x,depart_y,largeur/2,largeur/2);
    couleurSE = estCouleurUniforme(img,depart_x+16,depart_y,largeur/2,largeur/2);

    //Si ce sont des couleurs
    if(sontDesCouleursVariadic(4,couleurNO, couleurNE, couleurSO, couleurSE)){

        //MOTIF 0
        if(compareCouleurVariadic(couleurSO,couleurNO,2,couleurNE,couleurSE)){
            Motif ret = determineConfig(palette,0, couleurNO, PAS_UNE_COULEUR);
            ret.forme = 0;
            return ret;
        }

        //MOTIF 1
        if(compareCouleur(couleurSO,couleurSE) && compareCouleur(couleurNO,couleurNE) && compareCouleur(couleurNO,couleurSO) == false){
            Motif ret = determineConfig(palette,1, couleurNO, couleurSO);
            ret.forme = 1;
            return ret;
        }
        //MOTIF 2
        if(compareCouleur(couleurSO,couleurNO) && compareCouleur(couleurSE,couleurNE) && compareCouleur(couleurSO,couleurSE) == false){
            Motif ret = determineConfig(palette,2, couleurNO, couleurNE);
            ret.forme = 2;
            return ret;
        }

        //MOTIF 3
        if (compareCouleurVariadic(couleurSO,couleurNO,1,couleurNE) && compareCouleur(couleurSO,couleurSE) == false){
            Motif ret = determineConfig(palette,3, couleurNO, couleurSE);
            ret.forme = 3;
            return ret;
        }

        //MOTIF4
        if(compareCouleurVariadic(couleurSO,couleurNO,1,couleurSE) && compareCouleur(couleurNO,couleurNE) == false){
            Motif ret = determineConfig(palette,4, couleurNO, couleurNE);
            ret.forme = 4;
            return ret;
        }

        //MOTIF 5
        if(compareCouleurVariadic(couleurSO,couleurNE,1,couleurSE) && compareCouleur(couleurNO,couleurNE) == false){
            Motif ret = determineConfig(palette,5, couleurNO, couleurNE);
            ret.forme = 5;
            return ret;
        }
        
        //MOTIF 6
        if(compareCouleurVariadic(couleurNO,couleurNE,1,couleurSE) && compareCouleur(couleurSO,couleurSE) == false){
            Motif ret = determineConfig(palette,6, couleurNO, couleurSO);
            ret.forme = 6;
            return ret;
        }

    }
    return PAS_UN_MOTIF;
}

noeudMotif* creeArbreMotifs(Image *img, Palette *palette, int largeur, int x, int y){
    noeudMotif *root = (noeudMotif*) malloc(sizeof(noeudMotif));
    
    //Remplis la structure noeudMotif
    root->largeur = largeur;
    root->x = x;
    root->y = y;
    root->motif = identifieMotif(img,palette,x,y,largeur);

    //Si ce n'est pas un motif relance la récursivité
    if(compareMotif(root->motif, PAS_UN_MOTIF)){
        root->NO = creeArbreMotifs( img,
                                    palette,
                                    largeur/2,
                                    x,
                                    y+largeur/2);

        root->NE = creeArbreMotifs( img,
                                    palette,
                                    largeur/2,
                                    x+largeur/2,
                                    y+largeur/2);
                                
        root->SO = creeArbreMotifs( img,
                                    palette,
                                    largeur/2,
                                    x,
                                    y);
                                
        root->SE = creeArbreMotifs( img,
                                    palette,
                                    largeur/2,
                                    x+largeur/2,
                                    y);
    }
    //Sinon c'est une feuille
    else{
        root->NO = NULL;
        root->NE = NULL;
        root->SE = NULL;
        root->SO = NULL;
        return root;
    }

    return root;
}

void libereArbreMotif(noeudMotif *ptrRcn)
{
    if(ptrRcn)
    {
        libereArbreMotif(ptrRcn->NE);
        libereArbreMotif(ptrRcn->NO);
        libereArbreMotif(ptrRcn->SE);
        libereArbreMotif(ptrRcn->SO);
        free(ptrRcn);
    }
}

/**
 * @brief Fonction auxiliaire de sauveDescriptionImage, parcours récursivement l'arbre pour print ses valeurs
 * 
 * @param fp 
 * @param ptrRcn 
 */
void fprintfArbre(FILE *fp, noeudMotif* ptrRcn){
    if(ptrRcn){
        if(!compareMotif(ptrRcn->motif,PAS_UN_MOTIF))
            fprintf(fp, "\n%d %d %d%d %d %d",ptrRcn->x,ptrRcn->y,ptrRcn->motif.config,ptrRcn->motif.forme,ptrRcn->motif.cfond,ptrRcn->motif.cobjet);
        fprintfArbre(fp, ptrRcn->NO);
        fprintfArbre(fp, ptrRcn->NE);
        fprintfArbre(fp, ptrRcn->SO);
        fprintfArbre(fp, ptrRcn->SE);
    }
}

void sauveDescriptionImage(noeudMotif *rcn, Palette* pal, char* path){

    FILE * fp;

    /* open the file for writing*/
    fp = fopen (path,"w");
    
    /* write 10 lines of text into the file stream*/
    fprintf (fp, "%d %d\n",rcn->largeur, TAILLE_MOTIF);
    fprintf (fp, "%d",pal->nb);
    for(int i = 0; i < pal->nb; i++)
        fprintf (fp, "\n%d %d %d",pal->pal[i].rouge,pal->pal[i].vert,pal->pal[i].bleu);
    
    fprintfArbre(fp, rcn);

    /* close the file*/  
    fclose (fp);
}