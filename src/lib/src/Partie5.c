#include "../include/Partie5.h"
#include <stdarg.h>
#include <stdlib.h>

bool comparePoints(Point p1, Point p2){
    if(p1.x!=p2.x||p1.y!=p2.y) return false;
    return true;
}

int positionVoisinage(Point p1, Point p2){
    Point haut_gauche = (Point) {p2.x-1,p2.y+1},
    haut = (Point) {p2.x,p2.y+1},
    haut_droite = (Point) {p2.x+1,p2.y+1},
    droite = (Point) {p2.x+1,p2.y},
    bas_droite = (Point) {p2.x+1,p2.y-1},
    bas = (Point) {p2.x,p2.y-1},
    bas_gauche = (Point) {p2.x-1,p2.y-1},
    gauche = (Point) {p2.x-1,p2.y};

    if(comparePoints(p1,haut_gauche)) return 0;
    if(comparePoints(p1,haut)) return 1;
    if(comparePoints(p1,haut_droite)) return 2;
    if(comparePoints(p1,droite)) return 3;
    if(comparePoints(p1,bas_droite)) return 4;
    if(comparePoints(p1,bas)) return 5;
    if(comparePoints(p1,bas_gauche)) return 6;
    if(comparePoints(p1,gauche)) return 7;
    if(comparePoints(p1,p2)) return 8;

    return -1;
}

bool comparePointsVariadic (Point p1, Point p2, int nbreArgumentsOptionnels,  ...){
    if(p1.x!=p2.x||p1.y!=p2.y) return false;
    
    va_list ap;
    va_start (ap, nbreArgumentsOptionnels);
    Point tmp;
    for (int i = 0; i < nbreArgumentsOptionnels; i++){
        tmp = va_arg (ap, Point);
        if(p1.x!=tmp.x||p1.y!=tmp.y) return false;
    }
    va_end (ap);
    return true;
}

MaillonTraj* creeMaillon(const Point position){
    MaillonTraj *ret = (MaillonTraj*) malloc(sizeof(MaillonTraj));
    ret->position = (Point){position.x,position.y};
    ret->next = NULL;
    return ret;
}

void insererMaillonAvant(MaillonTraj *ptrTete, const int index, MaillonTraj *maillonAInserer){
    MaillonTraj *maillonCourant = ptrTete;
    int i = 0;
    while(maillonCourant)
    {
        if(i+1 == index)
        {
            MaillonTraj *tmp = maillonCourant->next;
            maillonCourant->next = maillonAInserer;
            maillonAInserer->next = tmp;
            return;
        }

        i++;
        maillonCourant = maillonCourant->next;
    }
}

void ajouterMaillon(MaillonTraj *ptrTete, MaillonTraj *maillonAAjouter){
    MaillonTraj *maillonCourant = ptrTete;
    while(maillonCourant->next)
    {
        maillonCourant = maillonCourant->next;
    }
    maillonCourant->next = maillonAAjouter;
}

void libereMaillon(MaillonTraj **maillon){

    if(*maillon) free(*maillon);
    maillon = NULL; 
}

int trouverIndexParValeur(MaillonTraj *ptrTete, const Point position){
    MaillonTraj *maillonCourant = ptrTete;
    int i = 0;
    while(maillonCourant->next)
    {
        if(maillonCourant->position.x == position.x && maillonCourant->position.y == position.y) return i;

        i++;
        maillonCourant = maillonCourant->next;
    }

    return -1;
}

void supprimerMaillonParValeur(MaillonTraj *ptrTete, const Point position){
    MaillonTraj *maillonCourant = ptrTete;
    while(maillonCourant->next)
    {
        MaillonTraj *next = maillonCourant->next;
        if(next->position.x == position.x && next->position.y == position.y)
        {
            MaillonTraj *target = maillonCourant->next;
            maillonCourant->next = maillonCourant->next->next;
            libereMaillon(&target);
            return;
        }
        maillonCourant = maillonCourant->next;
    }
}

void supprimerMaillon(MaillonTraj *ptrTete, MaillonTraj **maillon){
    MaillonTraj *maillonCourant = ptrTete;

    while(maillonCourant->next != *maillon)
    {
        maillonCourant = maillonCourant->next;
    }
    MaillonTraj *tmp = maillonCourant->next->next;
    libereMaillon(maillon);
    maillonCourant->next = tmp;
}

void dessineCarre(const Image *img, const Couleur couleur, const Emplacement emplacement){
    for (int x = emplacement.origine.x; x < emplacement.origine.x+emplacement.largeurMotif; x++)
    {
        for (int y = emplacement.origine.y; y < emplacement.origine.y+emplacement.largeurMotif; y++)
        {
            img->rouge[x][y] = couleur.rouge;
            img->vert[x][y] = couleur.vert;
            img->bleu[x][y] = couleur.bleu;
            img->gris[x][y] = 0.2125*couleur.rouge +0.7154*couleur.vert + 0.0721*couleur.bleu;
        }
    }
}

Robot* calculeTrajectoire(Image *img, Image *imgSansTrajectoires, Palette *palette, Point depart, Point arrivee){
    
    //Si les positions depart_x et depart_y sont valides
    if( depart.x+3 > img->largeur-1 || depart.x-3 < 0 ||
        depart.y+3 > img->hauteur-1 || depart.y-3 < 0 ||
        arrivee.x+3 > img->largeur-1 || arrivee.x-3 <0 ||
        arrivee.y+3 > img->hauteur-1 || arrivee.y-3 <0 ||
        !compareCouleur(palette->pal[0], estCouleurUniforme(imgSansTrajectoires,depart.x-3,depart.y-3,7,7)) ||
        !compareCouleur(palette->pal[0], estCouleurUniforme(imgSansTrajectoires,arrivee.x-3,arrivee.y-3,7,7)))
        {
            return NULL;
        }

    //Init la structure robot
    Robot* robot = (Robot*) malloc(sizeof(Robot));
    robot->depart = (Point){depart.x,depart.y};
    robot->arrivee = (Point){arrivee.x,arrivee.y};
    robot->position = (Point) {depart.x,depart.y};
    robot->ptrTete = creeMaillon(robot->depart);

    //Si le départ et l'arrivée ont les mêmes abscisses, y=ax+b ne marche pas
    if(depart.x == arrivee.x)
    {
        Point positionCourante = depart;
        while (!comparePoints(positionCourante,arrivee))
        {
            if(positionCourante.x < arrivee.x){
                positionCourante = (Point){positionCourante.x+1,positionCourante.y};
                ajouterMaillon(robot->ptrTete,creeMaillon(positionCourante));
            
            }
            else if( positionCourante.x > arrivee.x){
                positionCourante = (Point){positionCourante.x-1,positionCourante.y};
                ajouterMaillon(robot->ptrTete,creeMaillon(positionCourante));
                
            }
            else if(positionCourante.y < arrivee.y){
                positionCourante = (Point){positionCourante.x,positionCourante.y+1};
                ajouterMaillon(robot->ptrTete,creeMaillon(positionCourante));
                
            }
            else if(positionCourante.y > arrivee.y){
                positionCourante = (Point){positionCourante.x,positionCourante.y-1};
                ajouterMaillon(robot->ptrTete,creeMaillon(positionCourante));
            }
            else break;
        }
        
    }
    //Sinon on crée une liste chainée en traçant une droite du départ vers l'arrivée
    else
    {
        float a = ((float) arrivee.y-(float) depart.y)/((float) arrivee.x-(float) depart.x);
        float b = (float) depart.y - (float) a* (float) depart.x;

        MaillonTraj *maillonPrecedent = robot->ptrTete;
        for (int x=depart.x;(depart.x>arrivee.x)?x>=arrivee.x:x<=arrivee.x;(depart.x>arrivee.x)?x--:x++)
        {
            int y = a*x+b;

            MaillonTraj *nouveauMaillon = creeMaillon((Point){x,y});

            Point positionPrecedente = (Point){maillonPrecedent->position.x,maillonPrecedent->position.y};
            Point positionCourante = (Point){x,y};

            if(positionVoisinage(positionPrecedente, positionCourante) != 8){
                
                while(  positionVoisinage(positionPrecedente, positionCourante) != 1 &&
                        positionVoisinage(positionPrecedente, positionCourante) != 3 &&
                        positionVoisinage(positionPrecedente, positionCourante) != 5 &&
                        positionVoisinage(positionPrecedente, positionCourante) != 7){
                    if(positionPrecedente.x < positionCourante.x){
                        positionPrecedente = (Point){positionPrecedente.x+1,positionPrecedente.y};
                        ajouterMaillon(robot->ptrTete,creeMaillon(positionPrecedente));
                        
                    }
                    else if( positionPrecedente.x > positionCourante.x){
                        positionPrecedente = (Point){positionPrecedente.x-1,positionPrecedente.y};
                        ajouterMaillon(robot->ptrTete,creeMaillon(positionPrecedente));
                        
                    }
                    else if(positionPrecedente.y < positionCourante.y){
                        positionPrecedente = (Point){positionPrecedente.x,positionPrecedente.y+1};
                        ajouterMaillon(robot->ptrTete,creeMaillon(positionPrecedente));
                        
                    }
                    else if( positionPrecedente.y > positionCourante.y){
                        positionPrecedente = (Point){positionPrecedente.x,positionPrecedente.y-1};
                        ajouterMaillon(robot->ptrTete,creeMaillon(positionPrecedente));
                    }
                    else break;
                }
            }

            ajouterMaillon(robot->ptrTete,nouveauMaillon);
            maillonPrecedent = nouveauMaillon;
            
        }
    }

    MaillonTraj *current = robot->ptrTete;

    reloop: //Illégal et pas vraiment nécessaire mais épargne des lignes de code

    //Nous parcourons la liste chainée crée
    while(current->next)
    {

        //Si ce n'est pas le fond, c'est une trajectoire
        if(!compareCouleur(palette->pal[0], estCouleurUniforme(imgSansTrajectoires,current->next->position.x-4,current->next->position.y-4,9,9))){
            //Sauvegarde la première position
            MaillonTraj *firstSeg = current->next;
            MaillonTraj *currentT = firstSeg;

            //Sauvegarde la dernière position avant de sortir de la trajectoire
            while (!compareCouleur(palette->pal[0], estCouleurUniforme(imgSansTrajectoires,currentT->next->position.x-4,currentT->next->position.y-4,9,9)))
            {
                currentT=currentT->next;
                if(currentT->next == NULL)
                    return NULL;
            }
            MaillonTraj *secondSeg= currentT;
            
            //On va lancer la recherche de chemin dans 4 directions différentes
            Point previousPosition1 = (Point){current->next->position.x,current->next->position.y},
            previousPosition2 = (Point){current->next->position.x,current->next->position.y},
            previousPosition3 = (Point){current->next->position.x,current->next->position.y},
            previousPosition4 = (Point){current->next->position.x,current->next->position.y};

            MaillonTraj *ptrTetePath1 = creeMaillon((Point){current->next->position.x,current->next->position.y}),
            *ptrTetePath2 = creeMaillon((Point){current->next->position.x,current->next->position.y}),
            *ptrTetePath3 = creeMaillon((Point){current->next->position.x,current->next->position.y}),
            *ptrTetePath4 = creeMaillon((Point){current->next->position.x,current->next->position.y}),
            
            *current1 = ptrTetePath1,
            *current2 = ptrTetePath2,
            *current3 = ptrTetePath3,
            *current4 = ptrTetePath4;
            
            int passage = 0;
            bool pathFind = false;

            //Tant que le chemin n'a pas été trouvé
            while(pathFind == false)
            {
                //On continue la recherche de chemin
                Point up1,right1,down1,left1,up2,right2,down2,left2,up3,right3,down3,left3,up4,right4,down4,left4;
                Couleur cUp1 ,cRight1,cDown1,cLeft1,cUp2 ,cRight2,cDown2,cLeft2,cUp3,cRight3,cDown3,cLeft3,cUp4 ,cRight4,cDown4,cLeft4;

                if(current1)
                {
                    up1=(Point){current1->position.x,current1->position.y+1},
                    right1=(Point){current1->position.x+1,current1->position.y},
                    down1=(Point){current1->position.x,current1->position.y-1},
                    left1=(Point){current1->position.x-1,current1->position.y};

                    cUp1 = (Couleur){img->rouge[up1.x][up1.y],img->vert[up1.x][up1.y],img->bleu[up1.x][up1.y]},
                    cRight1 = (Couleur){img->rouge[right1.x][right1.y],img->vert[right1.x][right1.y],img->bleu[right1.x][right1.y]},
                    cDown1 = (Couleur){img->rouge[down1.x][down1.y],img->vert[down1.x][down1.y],img->bleu[down1.x][down1.y]},
                    cLeft1 = (Couleur){img->rouge[left1.x][left1.y],img->vert[left1.x][left1.y],img->bleu[left1.x][left1.y]};
                }
                
                if(current2)
                {
                    up2=(Point){current2->position.x,current2->position.y+1},
                    right2=(Point){current2->position.x+1,current2->position.y},
                    down2=(Point){current2->position.x,current2->position.y-1},
                    left2=(Point){current2->position.x-1,current2->position.y};

                    cUp2 = (Couleur){img->rouge[up2.x][up2.y],img->vert[up2.x][up2.y],img->bleu[up2.x][up2.y]},
                    cRight2 = (Couleur){img->rouge[right2.x][right2.y],img->vert[right2.x][right2.y],img->bleu[right2.x][right2.y]},
                    cDown2 = (Couleur){img->rouge[down2.x][down2.y],img->vert[down2.x][down2.y],img->bleu[down2.x][down2.y]},
                    cLeft2 = (Couleur){img->rouge[left2.x][left2.y],img->vert[left2.x][left2.y],img->bleu[left2.x][left2.y]};
                }
                
                if(current3)
                {
                    up3=(Point){current3->position.x,current3->position.y+1},
                    right3=(Point){current3->position.x+1,current3->position.y},
                    down3=(Point){current3->position.x,current3->position.y-1},
                    left3=(Point){current3->position.x-1,current3->position.y};

                    cUp3 = (Couleur){img->rouge[up3.x][up3.y],img->vert[up3.x][up3.y],img->bleu[up3.x][up3.y]},
                    cRight3 = (Couleur){img->rouge[right3.x][right3.y],img->vert[right3.x][right3.y],img->bleu[right3.x][right3.y]},
                    cDown3 = (Couleur){img->rouge[down3.x][down3.y],img->vert[down3.x][down3.y],img->bleu[down3.x][down3.y]},
                    cLeft3 = (Couleur){img->rouge[left3.x][left3.y],img->vert[left3.x][left3.y],img->bleu[left3.x][left3.y]};
                }
                
                if(current4)
                {
                    up4=(Point){current4->position.x,current4->position.y+1},
                    right4=(Point){current4->position.x+1,current4->position.y},
                    down4=(Point){current4->position.x,current4->position.y-1},
                    left4=(Point){current4->position.x-1,current4->position.y};

                    cUp4 = (Couleur){img->rouge[up4.x][up4.y],img->vert[up4.x][up4.y],img->bleu[up4.x][up4.y]},
                    cRight4 = (Couleur){img->rouge[right4.x][right4.y],img->vert[right4.x][right4.y],img->bleu[right4.x][right4.y]},
                    cDown4 = (Couleur){img->rouge[down4.x][down4.y],img->vert[down4.x][down4.y],img->bleu[down4.x][down4.y]},
                    cLeft4 = (Couleur){img->rouge[left4.x][left4.y],img->vert[left4.x][left4.y],img->bleu[left4.x][left4.y]};
                }
                
                if(ptrTetePath1){
                    if(compareCouleur(cUp1,palette->pal[0]) == false && comparePoints(up1,previousPosition1) == false){
                        ajouterMaillon(ptrTetePath1,creeMaillon(up1));
                    }
                    else if(compareCouleur(cRight1,palette->pal[0]) == false && comparePoints(right1,previousPosition1) == false)
                    {
                        ajouterMaillon(ptrTetePath1,creeMaillon(right1));
                    }
                    else if(compareCouleur(cDown1,palette->pal[0]) == false && comparePoints(down1,previousPosition1) == false)
                    {
                        ajouterMaillon(ptrTetePath1,creeMaillon(down1));
                    }
                    else if(compareCouleur(cLeft1,palette->pal[0]) == false && comparePoints(left1,previousPosition1) == false)
                    {
                        ajouterMaillon(ptrTetePath1,creeMaillon(left1));
                    }

                    if(current1->next == NULL)
                    {
                        while(ptrTetePath1){
                            MaillonTraj *tmp = ptrTetePath1->next;
                            libereMaillon(&ptrTetePath1);
                            ptrTetePath1=tmp;
                        }
                        current1 = NULL;
                        previousPosition1 = (Point){0,0};
                    }
                    else
                    {
                        previousPosition1 = (Point){current1->position.x,current1->position.y};
                        current1 = current1->next;
                    }
                }
                
                if(ptrTetePath2)
                {
                    if(compareCouleur(cLeft2,palette->pal[0]) == false && comparePoints(left2,previousPosition2) == false)
                    {   
                        if (current1 == NULL || (comparePoints(left2,current1->position) == false))
                        {
                            ajouterMaillon(ptrTetePath2,creeMaillon(left2));
                        }
                    }
                    else if(compareCouleur(cDown2,palette->pal[0]) == false && comparePoints(down2,previousPosition2) == false)
                    {
                        if (current1 == NULL || (comparePoints(down2,current1->position) == false))
                        {
                            ajouterMaillon(ptrTetePath2,creeMaillon(down2));
                        }
                    }
                    else if(compareCouleur(cRight2,palette->pal[0]) == false && comparePoints(right2,previousPosition2) == false)
                    {
                        if (current1 == NULL || (comparePoints(right2,current1->position) == false))
                        {
                            ajouterMaillon(ptrTetePath2,creeMaillon(right2));
                        }
                    }
                    else if(compareCouleur(cUp2,palette->pal[0]) == false && comparePoints(up2,previousPosition2) == false)
                    {
                        if (current1 == NULL || (comparePoints(up2,current1->position) == false))
                        {
                            ajouterMaillon(ptrTetePath2,creeMaillon(up2));
                        }
                    }
                    
                    if(current2->next == NULL)
                    {
                        while(ptrTetePath2){
                            MaillonTraj *tmp = ptrTetePath2->next;
                            libereMaillon(&ptrTetePath2);
                            ptrTetePath2=tmp;
                        }
                        current2 = NULL;
                        previousPosition2 = (Point){0,0};
                    }
                    else
                    {
                        previousPosition2 = (Point){current2->position.x,current2->position.y};
                        current2 = current2->next;
                    }
                }
                
                if(ptrTetePath3)
                {
                    if(compareCouleur(cLeft3,palette->pal[0]) == false && comparePoints(left3,previousPosition3) == false)
                    {
                        if (current1 == NULL || (comparePoints(left3,current1->position) == false))
                        {
                            if (current2 == NULL || (comparePoints(left3,current2->position) == false))
                            {
                                ajouterMaillon(ptrTetePath3,creeMaillon(left3));
                            }
                        }

                        
                    }
                    else if(compareCouleur(cDown3,palette->pal[0]) == false && comparePoints(down3,previousPosition3) == false)
                    {
                        if (current1 == NULL || (comparePoints(down3,current1->position) == false))
                        {
                            if (current2 == NULL || (comparePoints(down3,current2->position) == false))
                            {
                                ajouterMaillon(ptrTetePath3,creeMaillon(down3));
                            }
                        }
                    }
                    else if(compareCouleur(cRight3,palette->pal[0]) == false && comparePoints(right3,previousPosition3) == false)
                    {
                        if (current1 == NULL || (comparePoints(right3,current1->position) == false))
                        {
                            if (current2 == NULL || (comparePoints(right3,current2->position) == false))
                            {
                                ajouterMaillon(ptrTetePath3,creeMaillon(right3));
                            }
                        }
                    }
                    else if(compareCouleur(cUp3,palette->pal[0]) == false && comparePoints(up3,previousPosition3) == false)
                    {
                        if (current1 == NULL || (comparePoints(up3,current1->position) == false))
                        {
                            if (current2 == NULL || (comparePoints(up3,current2->position) == false))
                            {
                                ajouterMaillon(ptrTetePath3,creeMaillon(up3));
                            }
                        }
                    }
                    if(current3->next == NULL){
                        while(ptrTetePath3){
                            MaillonTraj *tmp = ptrTetePath3->next;
                            libereMaillon(&ptrTetePath3);
                            ptrTetePath3=tmp;
                        }
                        current3 = NULL;
                        previousPosition3 = (Point){0,0};
                    }
                    else
                    {
                        previousPosition3 = (Point){current3->position.x,current3->position.y};
                        current3 = current3->next;
                    }
                    
                }

                if(ptrTetePath4)
                {
                   if(compareCouleur(cLeft4,palette->pal[0]) == false && comparePoints(left4,previousPosition4) == false)
                    {
                        if (current1 == NULL || (comparePoints(left4,current1->position) == false))
                        {
                            if (current2 == NULL || (comparePoints(left4,current2->position) == false))
                            {
                                if (current3 == NULL || (comparePoints(left4,current3->position) == false))
                                {
                                    ajouterMaillon(ptrTetePath4,creeMaillon(left4));
                                }
                            }
                        }
                    }
                    else if(compareCouleur(cDown4,palette->pal[0]) == false && comparePoints(down4,previousPosition4) == false)
                    {
                        if (current1 == NULL || (comparePoints(down4,current1->position) == false))
                        {
                            if (current2 == NULL || (comparePoints(down4,current2->position) == false))
                            {
                                if (current3 == NULL || (comparePoints(down4,current3->position) == false))
                                {
                                    ajouterMaillon(ptrTetePath4,creeMaillon(down4));
                                }
                            }
                        }
                    }
                    else if(compareCouleur(cRight4,palette->pal[0]) == false && comparePoints(right4,previousPosition4) == false)
                    {
                        if (current1 == NULL || (comparePoints(right4,current1->position) == false))
                        {
                            if (current2 == NULL || (comparePoints(right4,current2->position) == false))
                            {
                                if (current3 == NULL || (comparePoints(right4,current3->position) == false))
                                {
                                    ajouterMaillon(ptrTetePath4,creeMaillon(right4));
                                }
                            }
                        }
                    }
                    else if(compareCouleur(cUp4,palette->pal[0]) == false && comparePoints(up4,previousPosition4) == false)
                    {
                        if (current1 == NULL || (comparePoints(up4,current1->position) == false))
                        {
                            if (current2 == NULL || (comparePoints(up4,current2->position) == false))
                            {
                                if (current3 == NULL || (comparePoints(up4,current3->position) == false))
                                {
                                    ajouterMaillon(ptrTetePath4,creeMaillon(up4));
                                }
                            }
                        }
                    }
                    
                    if(current4->next == NULL)
                    {
                        while(ptrTetePath4){
                            MaillonTraj *tmp = ptrTetePath4->next;
                            libereMaillon(&ptrTetePath4);
                            ptrTetePath4=tmp;
                        }
                        current4 = NULL;
                        previousPosition4 = (Point){0,0};
                    }
                    else
                    {
                        previousPosition4 = (Point){current4->position.x,current4->position.y};
                        current4 = current4->next;
                    }
                    
                }

                if(current1 == NULL && current2 == NULL && current3 == NULL && current4 == NULL) return NULL;

                MaillonTraj *parcoursMainSegment = current->next;
                while (parcoursMainSegment)
                {
                    
                    if(current1 && comparePoints(current1->position,secondSeg->position))
                    {
                        //chemin trouvé path 1
                        current->next = ptrTetePath1;
                        current1->next = secondSeg->next;
                        secondSeg->next =NULL;
                        while(firstSeg){
                            MaillonTraj *tmp = firstSeg->next;
                            libereMaillon(&firstSeg);
                            firstSeg=tmp;
                        }
                        current = current1->next;
                        
                        while(ptrTetePath2){
                            MaillonTraj *tmp = ptrTetePath2->next;
                            libereMaillon(&ptrTetePath2);
                            ptrTetePath2=tmp;
                        }
                        while(ptrTetePath3){
                            MaillonTraj *tmp = ptrTetePath3->next;
                            libereMaillon(&ptrTetePath3);
                            ptrTetePath3=tmp;
                        }
                        while(ptrTetePath4){
                            MaillonTraj *tmp = ptrTetePath4->next;
                            libereMaillon(&ptrTetePath4);
                            ptrTetePath4=tmp;
                        }

                        goto reloop;
                    }
                    else if(current2 && comparePoints(current2->position,secondSeg->position))
                    {
                        //chemin trouvé path 2
                        current->next = ptrTetePath2;
                        current2->next = secondSeg->next;
                        secondSeg->next =NULL;
                        while(firstSeg){
                            MaillonTraj *tmp = firstSeg->next;
                            libereMaillon(&firstSeg);
                            firstSeg=tmp;
                        }
                        current = current2->next;

                        while(ptrTetePath1){
                            MaillonTraj *tmp = ptrTetePath1->next;
                            libereMaillon(&ptrTetePath1);
                            ptrTetePath1=tmp;
                        }
                        while(ptrTetePath3){
                            MaillonTraj *tmp = ptrTetePath3->next;
                            libereMaillon(&ptrTetePath3);
                            ptrTetePath3=tmp;
                        }
                        while(ptrTetePath4){
                            MaillonTraj *tmp = ptrTetePath4->next;
                            libereMaillon(&ptrTetePath4);
                            ptrTetePath4=tmp;
                        }
                        
                        
                        
                        goto reloop;
                    }
                    else if(current3 && comparePoints(current3->position,secondSeg->position))
                    {
                        //chemin trouvé path 3
                        current->next = ptrTetePath3;
                        current3->next = secondSeg->next;
                        
                        secondSeg->next =NULL;
                        while(firstSeg){
                            MaillonTraj *tmp = firstSeg->next;
                            libereMaillon(&firstSeg);
                            firstSeg=tmp;
                        }
                        current = current3->next;
                        
                        while(ptrTetePath1){
                            MaillonTraj *tmp = ptrTetePath1->next;
                            libereMaillon(&ptrTetePath1);
                            ptrTetePath1=tmp;
                        }
                        while(ptrTetePath2){
                            MaillonTraj *tmp = ptrTetePath2->next;
                            libereMaillon(&ptrTetePath2);
                            ptrTetePath2=tmp;
                        }
                        while(ptrTetePath4){
                            MaillonTraj *tmp = ptrTetePath4->next;
                            libereMaillon(&ptrTetePath4);
                            ptrTetePath4=tmp;
                        }

                        goto reloop;
                    }
                    else if(current4 && comparePoints(current4->position,secondSeg->position))
                    {
                        //chemin trouvé path 4
                        current->next = ptrTetePath4;
                        current4->next = secondSeg->next;
                        
                        secondSeg->next =NULL;
                        while(firstSeg){
                            MaillonTraj *tmp = firstSeg->next;
                            libereMaillon(&firstSeg);
                            firstSeg=tmp;
                        }
                        current = current4->next;
                        
                        while(ptrTetePath1){
                            MaillonTraj *tmp = ptrTetePath1->next;
                            libereMaillon(&ptrTetePath1);
                            ptrTetePath1=tmp;
                        }
                        while(ptrTetePath2){
                            MaillonTraj *tmp = ptrTetePath2->next;
                            libereMaillon(&ptrTetePath2);
                            ptrTetePath2=tmp;
                        }
                        while(ptrTetePath3){
                            MaillonTraj *tmp = ptrTetePath3->next;
                            libereMaillon(&ptrTetePath3);
                            ptrTetePath3=tmp;
                        }

                        goto reloop;
                    }
                    parcoursMainSegment=parcoursMainSegment->next;
                }
            }
        }
        current=current->next;
    }
    //Le chemin évitant les trajectoires a été trouvé mais n'est pas optimisé
    
    //Commence l'optimisation du chemin en supprimant les segments inutiles et traçant des nouveaux segments
    MaillonTraj *optiTraj = creeMaillon(robot->ptrTete->position);
    MaillonTraj *maillonPrecedent = optiTraj;
    current = robot->ptrTete;
    while (current)
    {
        
        MaillonTraj *currentBis = current->next;
        if(currentBis && currentBis->next)
        {
            MaillonTraj *lastAvailable = currentBis->next;
            while(currentBis->next)
            {
                if(compareCouleur(palette->pal[0],estCouleurUniformeDansCarre(imgSansTrajectoires,current->position.x,current->position.y,currentBis->next->position.x,currentBis->next->position.y)))
                {
                    lastAvailable = currentBis;
                }
                currentBis=currentBis->next;
            }
            if(lastAvailable || lastAvailable && comparePoints(arrivee,lastAvailable->position))
            {
                depart = current->position;
                arrivee = lastAvailable->position;

                if(depart.x == arrivee.x)
                {
                    Point positionCourante = depart;
                    while (!comparePoints(positionCourante,arrivee))
                    {
                        if(positionCourante.x < arrivee.x){
                            positionCourante = (Point){positionCourante.x+1,positionCourante.y};
                            ajouterMaillon(optiTraj,creeMaillon(positionCourante));
                        
                        }
                        else if( positionCourante.x > arrivee.x){
                            positionCourante = (Point){positionCourante.x-1,positionCourante.y};
                            ajouterMaillon(optiTraj,creeMaillon(positionCourante));
                            
                        }
                        else if(positionCourante.y < arrivee.y){
                            positionCourante = (Point){positionCourante.x,positionCourante.y+1};
                            ajouterMaillon(optiTraj,creeMaillon(positionCourante));
                            
                        }
                        else if(positionCourante.y > arrivee.y){
                            positionCourante = (Point){positionCourante.x,positionCourante.y-1};
                            ajouterMaillon(optiTraj,creeMaillon(positionCourante));
                        }
                        else break;
                    }
                    
                }
                else
                {
                    float a = ((float) arrivee.y-(float) depart.y)/((float) arrivee.x-(float) depart.x);
                    float b = (float) depart.y - (float) a* (float) depart.x;
                    for (int x=depart.x;(depart.x>arrivee.x)?x>=arrivee.x:x<=arrivee.x;(depart.x>arrivee.x)?x--:x++)
                    {
                        int y = a*x+b;
                        MaillonTraj *nouveauMaillon = creeMaillon((Point){x,y});

                        Point positionPrecedente = (Point){maillonPrecedent->position.x,maillonPrecedent->position.y};
                        Point positionCourante = (Point){x,y};
                        

                        if(positionVoisinage(positionPrecedente, positionCourante) != 8){
                            
                            while(  positionVoisinage(positionPrecedente, positionCourante) != 1 &&
                                    positionVoisinage(positionPrecedente, positionCourante) != 3 &&
                                    positionVoisinage(positionPrecedente, positionCourante) != 5 &&
                                    positionVoisinage(positionPrecedente, positionCourante) != 7){
                                
                                if(positionPrecedente.x < positionCourante.x){
                                    positionPrecedente = (Point){positionPrecedente.x+1,positionPrecedente.y};
                                    ajouterMaillon(optiTraj,creeMaillon(positionPrecedente));
                                    
                                }
                                else if( positionPrecedente.x > positionCourante.x){
                                    positionPrecedente = (Point){positionPrecedente.x-1,positionPrecedente.y};
                                    ajouterMaillon(optiTraj,creeMaillon(positionPrecedente));
                                    
                                }
                                else if(positionPrecedente.y < positionCourante.y){
                                    positionPrecedente = (Point){positionPrecedente.x,positionPrecedente.y+1};
                                    ajouterMaillon(optiTraj,creeMaillon(positionPrecedente));
                                    
                                }
                                else if( positionPrecedente.y > positionCourante.y){
                                    positionPrecedente = (Point){positionPrecedente.x,positionPrecedente.y-1};
                                    ajouterMaillon(optiTraj,creeMaillon(positionPrecedente));
                                }
                                else break;
                            }
                        }

                        ajouterMaillon(optiTraj,nouveauMaillon);
                        maillonPrecedent = nouveauMaillon;
                        
                    }
                }
                for(;maillonPrecedent->next;maillonPrecedent=maillonPrecedent->next);
                current = lastAvailable;
            }
        }
        current=current->next;
    }
    while (robot->ptrTete)
    {
        MaillonTraj *tmp = robot->ptrTete->next;
        libereMaillon(&robot->ptrTete);
        robot->ptrTete = tmp;
    }
    
    robot->ptrTete = optiTraj;
    return robot;
}

void dessineMotif0(const Image *img, const Palette palette, const  Motif motif, const Emplacement emplacement, const bool trajectoire){
    Emplacement carreNO = (Emplacement) {(Point){emplacement.origine.x,emplacement.origine.y+emplacement.largeurMotif/2},emplacement.largeurMotif/2},
    carreNE = (Emplacement) {(Point){emplacement.origine.x+emplacement.largeurMotif/2,emplacement.origine.y+emplacement.largeurMotif/2},emplacement.largeurMotif/2},
    carreSO = (Emplacement) {(Point){emplacement.origine.x,emplacement.origine.y},emplacement.largeurMotif/2},
    carreSE = (Emplacement) {(Point){emplacement.origine.x+emplacement.largeurMotif/2,emplacement.origine.y},emplacement.largeurMotif/2};

    if(motif.config == 0)
    {
        dessineCarre(img,palette.pal[motif.cfond], carreNO);
        dessineCarre(img,palette.pal[motif.cfond], carreNE);
        dessineCarre(img,palette.pal[motif.cfond], carreSO);
        dessineCarre(img,palette.pal[motif.cfond], carreSE);
        
    }
    else
    {
        dessineCarre(img,palette.pal[motif.cobjet], carreNO);
        dessineCarre(img,palette.pal[motif.cobjet], carreNE);
        dessineCarre(img,palette.pal[motif.cobjet], carreSO);
        dessineCarre(img,palette.pal[motif.cobjet], carreSE);
        
    }
}

void dessineMotif1(const Image *img, const Palette palette, const  Motif motif, const Emplacement emplacement, const bool trajectoire){
    Emplacement carreNO = (Emplacement) {(Point){emplacement.origine.x,emplacement.origine.y+emplacement.largeurMotif/2},emplacement.largeurMotif/2},
    carreNE = (Emplacement) {(Point){emplacement.origine.x+emplacement.largeurMotif/2,emplacement.origine.y+emplacement.largeurMotif/2},emplacement.largeurMotif/2},
    carreSO = (Emplacement) {(Point){emplacement.origine.x,emplacement.origine.y},emplacement.largeurMotif/2},
    carreSE = (Emplacement) {(Point){emplacement.origine.x+emplacement.largeurMotif/2,emplacement.origine.y},emplacement.largeurMotif/2};

    if(motif.config == 0)
    {   
        dessineCarre(img,palette.pal[motif.cfond], carreNO);
        dessineCarre(img,palette.pal[motif.cfond], carreNE);
        dessineCarre(img,palette.pal[motif.cobjet], carreSO);
        dessineCarre(img,palette.pal[motif.cobjet], carreSE);
        if(trajectoire)
        {
            int y = carreSO.origine.y+carreSO.largeurMotif+3;
            for (int x = carreSO.origine.x; x < carreSO.origine.x +2*carreSO.largeurMotif; x++)
            {
                img->rouge[x][y] = palette.pal[motif.cobjet].rouge;
                img->vert[x][y] = palette.pal[motif.cobjet].vert;
                img->bleu[x][y] = palette.pal[motif.cobjet].bleu;
                img->gris[x][y] = 0.2125*palette.pal[motif.cobjet].rouge +0.7154*palette.pal[motif.cobjet].vert + 0.0721*palette.pal[motif.cobjet].bleu;
            }
        }
    }
    else
    {
        dessineCarre(img,palette.pal[motif.cobjet], carreNO);
        dessineCarre(img,palette.pal[motif.cobjet], carreNE);
        dessineCarre(img,palette.pal[motif.cfond], carreSO);
        dessineCarre(img,palette.pal[motif.cfond], carreSE);
        if(trajectoire)
        {
            int y = carreSO.origine.y+carreSO.largeurMotif-4;
            for (int x = carreSO.origine.x; x < carreSO.origine.x+ 2*carreSO.largeurMotif; x++)
            {
                img->rouge[x][y] = palette.pal[motif.cobjet].rouge;
                img->vert[x][y] = palette.pal[motif.cobjet].vert;
                img->bleu[x][y] = palette.pal[motif.cobjet].bleu;
                img->gris[x][y] = 0.2125*palette.pal[motif.cobjet].rouge +0.7154*palette.pal[motif.cobjet].vert + 0.0721*palette.pal[motif.cobjet].bleu;
            }
        }
        
        
    }
}

void dessineMotif2(const Image *img, const Palette palette, const  Motif motif, const Emplacement emplacement, const bool trajectoire){
    Emplacement carreNO = (Emplacement) {(Point){emplacement.origine.x,emplacement.origine.y+emplacement.largeurMotif/2},emplacement.largeurMotif/2},
    carreNE = (Emplacement) {(Point){emplacement.origine.x+emplacement.largeurMotif/2,emplacement.origine.y+emplacement.largeurMotif/2},emplacement.largeurMotif/2},
    carreSO = (Emplacement) {(Point){emplacement.origine.x,emplacement.origine.y},emplacement.largeurMotif/2},
    carreSE = (Emplacement) {(Point){emplacement.origine.x+emplacement.largeurMotif/2,emplacement.origine.y},emplacement.largeurMotif/2};

    if(motif.config == 0)
    {
        dessineCarre(img,palette.pal[motif.cfond], carreNO);
        dessineCarre(img,palette.pal[motif.cobjet], carreNE);
        dessineCarre(img,palette.pal[motif.cfond], carreSO);
        dessineCarre(img,palette.pal[motif.cobjet], carreSE);
        if(trajectoire)
        {
            int x = carreSO.origine.x+carreSO.largeurMotif-4;
            for (int y = carreSO.origine.y; y < carreSO.origine.y+ 2*carreSO.largeurMotif; y++)
            {
                img->rouge[x][y] = palette.pal[motif.cobjet].rouge;
                img->vert[x][y] = palette.pal[motif.cobjet].vert;
                img->bleu[x][y] = palette.pal[motif.cobjet].bleu;
                img->gris[x][y] = 0.2125*palette.pal[motif.cobjet].rouge +0.7154*palette.pal[motif.cobjet].vert + 0.0721*palette.pal[motif.cobjet].bleu;
            }
        }
    }
    else
    {
        dessineCarre(img,palette.pal[motif.cobjet], carreNO);
        dessineCarre(img,palette.pal[motif.cfond], carreNE);
        dessineCarre(img,palette.pal[motif.cobjet], carreSO);
        dessineCarre(img,palette.pal[motif.cfond], carreSE);
        if(trajectoire)
        {
            int x = carreSO.origine.x+carreSO.largeurMotif+3;
            for (int y = carreSO.origine.y; y < carreSO.origine.y+ 2*carreSO.largeurMotif; y++)
            {
                img->rouge[x][y] = palette.pal[motif.cobjet].rouge;
                img->vert[x][y] = palette.pal[motif.cobjet].vert;
                img->bleu[x][y] = palette.pal[motif.cobjet].bleu;
                img->gris[x][y] = 0.2125*palette.pal[motif.cobjet].rouge +0.7154*palette.pal[motif.cobjet].vert + 0.0721*palette.pal[motif.cobjet].bleu;
            }
        }
    }
}

void dessineMotif3(const Image *img, const Palette palette, const  Motif motif, const Emplacement emplacement, const bool trajectoire){
    Emplacement carreNO = (Emplacement) {(Point){emplacement.origine.x,emplacement.origine.y+emplacement.largeurMotif/2},emplacement.largeurMotif/2},
    carreNE = (Emplacement) {(Point){emplacement.origine.x+emplacement.largeurMotif/2,emplacement.origine.y+emplacement.largeurMotif/2},emplacement.largeurMotif/2},
    carreSO = (Emplacement) {(Point){emplacement.origine.x,emplacement.origine.y},emplacement.largeurMotif/2},
    carreSE = (Emplacement) {(Point){emplacement.origine.x+emplacement.largeurMotif/2,emplacement.origine.y},emplacement.largeurMotif/2};

    if(motif.config == 0)
    {
        dessineCarre(img,palette.pal[motif.cfond], carreNO);
        dessineCarre(img,palette.pal[motif.cfond], carreNE);
        dessineCarre(img,palette.pal[motif.cfond], carreSO);
        dessineCarre(img,palette.pal[motif.cobjet], carreSE);
        if(trajectoire)
        {
            int x = carreSO.origine.x+carreSO.largeurMotif-4;
            for (int y = carreSO.origine.y; y < carreSO.origine.y+ carreSO.largeurMotif+3; y++)
            {
                img->rouge[x][y] = palette.pal[motif.cobjet].rouge;
                img->vert[x][y] = palette.pal[motif.cobjet].vert;
                img->bleu[x][y] = palette.pal[motif.cobjet].bleu;
                img->gris[x][y] = 0.2125*palette.pal[motif.cobjet].rouge +0.7154*palette.pal[motif.cobjet].vert + 0.0721*palette.pal[motif.cobjet].bleu;
            }
            
            int y = carreSO.origine.y+carreSO.largeurMotif+3;
            for (x = carreSO.origine.x+carreSO.largeurMotif-4; x < carreSO.origine.x+ 2*carreSO.largeurMotif; x++)
            {
                img->rouge[x][y] = palette.pal[motif.cobjet].rouge;
                img->vert[x][y] = palette.pal[motif.cobjet].vert;
                img->bleu[x][y] = palette.pal[motif.cobjet].bleu;
                img->gris[x][y] = 0.2125*palette.pal[motif.cobjet].rouge +0.7154*palette.pal[motif.cobjet].vert + 0.0721*palette.pal[motif.cobjet].bleu;
            }
        }
    }
    else
    {
        dessineCarre(img,palette.pal[motif.cobjet], carreNO);
        dessineCarre(img,palette.pal[motif.cobjet], carreNE);
        dessineCarre(img,palette.pal[motif.cobjet], carreSO);
        dessineCarre(img,palette.pal[motif.cfond], carreSE);
        if(trajectoire)
        {
            int x = carreSO.origine.x+carreSO.largeurMotif+3;
            for (int y = carreSO.origine.y; y < carreSO.origine.y+ carreSO.largeurMotif-4; y++)
            {
                img->rouge[x][y] = palette.pal[motif.cobjet].rouge;
                img->vert[x][y] = palette.pal[motif.cobjet].vert;
                img->bleu[x][y] = palette.pal[motif.cobjet].bleu;
                img->gris[x][y] = 0.2125*palette.pal[motif.cobjet].rouge +0.7154*palette.pal[motif.cobjet].vert + 0.0721*palette.pal[motif.cobjet].bleu;
            }
            
            int y = carreSO.origine.y+carreSO.largeurMotif-4;
            for (x = carreSO.origine.x+carreSO.largeurMotif+3; x < carreSO.origine.x+ 2*carreSO.largeurMotif; x++)
            {
                img->rouge[x][y] = palette.pal[motif.cobjet].rouge;
                img->vert[x][y] = palette.pal[motif.cobjet].vert;
                img->bleu[x][y] = palette.pal[motif.cobjet].bleu;
                img->gris[x][y] = 0.2125*palette.pal[motif.cobjet].rouge +0.7154*palette.pal[motif.cobjet].vert + 0.0721*palette.pal[motif.cobjet].bleu;
            }
        }
    }
}

void dessineMotif4(const Image *img, const Palette palette, const  Motif motif, const Emplacement emplacement, const bool trajectoire){
    Emplacement carreNO = (Emplacement) {(Point){emplacement.origine.x,emplacement.origine.y+emplacement.largeurMotif/2},emplacement.largeurMotif/2},
    carreNE = (Emplacement) {(Point){emplacement.origine.x+emplacement.largeurMotif/2,emplacement.origine.y+emplacement.largeurMotif/2},emplacement.largeurMotif/2},
    carreSO = (Emplacement) {(Point){emplacement.origine.x,emplacement.origine.y},emplacement.largeurMotif/2},
    carreSE = (Emplacement) {(Point){emplacement.origine.x+emplacement.largeurMotif/2,emplacement.origine.y},emplacement.largeurMotif/2};

    if(motif.config == 0)
    {
        dessineCarre(img,palette.pal[motif.cfond], carreNO);
        dessineCarre(img,palette.pal[motif.cobjet], carreNE);
        dessineCarre(img,palette.pal[motif.cfond], carreSO);
        dessineCarre(img,palette.pal[motif.cfond], carreSE);
        if(trajectoire)
        {
            int x = carreSO.origine.x+carreSO.largeurMotif-4;
            for (int y = carreSO.origine.y+2*carreSO.largeurMotif; y > carreSO.origine.y + carreSO.largeurMotif-4; y--)
            {
                img->rouge[x][y] = palette.pal[motif.cobjet].rouge;
                img->vert[x][y] = palette.pal[motif.cobjet].vert;
                img->bleu[x][y] = palette.pal[motif.cobjet].bleu;
                img->gris[x][y] = 0.2125*palette.pal[motif.cobjet].rouge +0.7154*palette.pal[motif.cobjet].vert + 0.0721*palette.pal[motif.cobjet].bleu;
            }
            
            int y = carreSO.origine.y + carreSO.largeurMotif-4;
            for (x = carreSO.origine.x+carreSO.largeurMotif-4; x < carreSO.origine.x+ 2*carreSO.largeurMotif; x++)
            {
                img->rouge[x][y] = palette.pal[motif.cobjet].rouge;
                img->vert[x][y] = palette.pal[motif.cobjet].vert;
                img->bleu[x][y] = palette.pal[motif.cobjet].bleu;
                img->gris[x][y] = 0.2125*palette.pal[motif.cobjet].rouge +0.7154*palette.pal[motif.cobjet].vert + 0.0721*palette.pal[motif.cobjet].bleu;
            }
        }
    }
    else
    {
        dessineCarre(img,palette.pal[motif.cobjet], carreNO);
        dessineCarre(img,palette.pal[motif.cfond], carreNE);
        dessineCarre(img,palette.pal[motif.cobjet], carreSO);
        dessineCarre(img,palette.pal[motif.cobjet], carreSE);
        if(trajectoire)
        {
            int x = carreSO.origine.x+carreSO.largeurMotif+3;
            for (int y = carreSO.origine.y+2*carreSO.largeurMotif; y > carreSO.origine.y+carreSO.largeurMotif+3; y--)
            {
                img->rouge[x][y] = palette.pal[motif.cobjet].rouge;
                img->vert[x][y] = palette.pal[motif.cobjet].vert;
                img->bleu[x][y] = palette.pal[motif.cobjet].bleu;
                img->gris[x][y] = 0.2125*palette.pal[motif.cobjet].rouge +0.7154*palette.pal[motif.cobjet].vert + 0.0721*palette.pal[motif.cobjet].bleu;
            }
            
            int y = carreSO.origine.y+carreSO.largeurMotif+3;
            for (x = carreSO.origine.x+carreSO.largeurMotif+3; x < carreSO.origine.x+ 2*carreSO.largeurMotif; x++)
            {
                img->rouge[x][y] = palette.pal[motif.cobjet].rouge;
                img->vert[x][y] = palette.pal[motif.cobjet].vert;
                img->bleu[x][y] = palette.pal[motif.cobjet].bleu;
                img->gris[x][y] = 0.2125*palette.pal[motif.cobjet].rouge +0.7154*palette.pal[motif.cobjet].vert + 0.0721*palette.pal[motif.cobjet].bleu;
            }
        }
    }
}

void dessineMotif5(const Image *img, const Palette palette, const  Motif motif, const Emplacement emplacement, const bool trajectoire){
    Emplacement carreNO = (Emplacement) {(Point){emplacement.origine.x,emplacement.origine.y+emplacement.largeurMotif/2},emplacement.largeurMotif/2},
    carreNE = (Emplacement) {(Point){emplacement.origine.x+emplacement.largeurMotif/2,emplacement.origine.y+emplacement.largeurMotif/2},emplacement.largeurMotif/2},
    carreSO = (Emplacement) {(Point){emplacement.origine.x,emplacement.origine.y},emplacement.largeurMotif/2},
    carreSE = (Emplacement) {(Point){emplacement.origine.x+emplacement.largeurMotif/2,emplacement.origine.y},emplacement.largeurMotif/2};

    if(motif.config == 0)
    {
        dessineCarre(img,palette.pal[motif.cobjet], carreNO);
        dessineCarre(img,palette.pal[motif.cfond], carreNE);
        dessineCarre(img,palette.pal[motif.cfond], carreSO);
        dessineCarre(img,palette.pal[motif.cfond], carreSE);
        if(trajectoire)
        {
            int y = carreSO.origine.y + carreSO.largeurMotif-4;
            for (int x = carreSO.origine.x; x < carreSO.origine.x+carreSO.largeurMotif+3; x++)
            {
                img->rouge[x][y] = palette.pal[motif.cobjet].rouge;
                img->vert[x][y] = palette.pal[motif.cobjet].vert;
                img->bleu[x][y] = palette.pal[motif.cobjet].bleu;
                img->gris[x][y] = 0.2125*palette.pal[motif.cobjet].rouge +0.7154*palette.pal[motif.cobjet].vert + 0.0721*palette.pal[motif.cobjet].bleu;
            }
            
            int x = carreSO.origine.x+carreSO.largeurMotif+3;
            for (y = carreSO.origine.y + carreSO.largeurMotif-4; y < carreSO.origine.y + 2*carreSO.largeurMotif; y++)
            {
                img->rouge[x][y] = palette.pal[motif.cobjet].rouge;
                img->vert[x][y] = palette.pal[motif.cobjet].vert;
                img->bleu[x][y] = palette.pal[motif.cobjet].bleu;
                img->gris[x][y] = 0.2125*palette.pal[motif.cobjet].rouge +0.7154*palette.pal[motif.cobjet].vert + 0.0721*palette.pal[motif.cobjet].bleu;
            }
            
        }
    }
    else
    {
        dessineCarre(img,palette.pal[motif.cfond], carreNO);
        dessineCarre(img,palette.pal[motif.cobjet], carreNE);
        dessineCarre(img,palette.pal[motif.cobjet], carreSO);
        dessineCarre(img,palette.pal[motif.cobjet], carreSE);
        if(trajectoire)
        {
            int y = carreSO.origine.y+carreSO.largeurMotif+3;
            for (int x = carreSO.origine.x; x < carreSO.origine.x+carreSO.largeurMotif-4; x++)
            {
                img->rouge[x][y] = palette.pal[motif.cobjet].rouge;
                img->vert[x][y] = palette.pal[motif.cobjet].vert;
                img->bleu[x][y] = palette.pal[motif.cobjet].bleu;
                img->gris[x][y] = 0.2125*palette.pal[motif.cobjet].rouge +0.7154*palette.pal[motif.cobjet].vert + 0.0721*palette.pal[motif.cobjet].bleu;
            }

            int x = carreSO.origine.x+carreSO.largeurMotif-4;
            for (int y = carreSO.origine.y+carreSO.largeurMotif+3; y < carreSO.origine.y+ 2*carreSO.largeurMotif; y++)
            {
                img->rouge[x][y] = palette.pal[motif.cobjet].rouge;
                img->vert[x][y] = palette.pal[motif.cobjet].vert;
                img->bleu[x][y] = palette.pal[motif.cobjet].bleu;
                img->gris[x][y] = 0.2125*palette.pal[motif.cobjet].rouge +0.7154*palette.pal[motif.cobjet].vert + 0.0721*palette.pal[motif.cobjet].bleu;
            }
        }
    }
}

void dessineMotif6(const Image *img, const Palette palette, const  Motif motif, const Emplacement emplacement, const bool trajectoire){
    Emplacement carreNO = (Emplacement) {(Point){emplacement.origine.x,emplacement.origine.y+emplacement.largeurMotif/2},emplacement.largeurMotif/2},
    carreNE = (Emplacement) {(Point){emplacement.origine.x+emplacement.largeurMotif/2,emplacement.origine.y+emplacement.largeurMotif/2},emplacement.largeurMotif/2},
    carreSO = (Emplacement) {(Point){emplacement.origine.x,emplacement.origine.y},emplacement.largeurMotif/2},
    carreSE = (Emplacement) {(Point){emplacement.origine.x+emplacement.largeurMotif/2,emplacement.origine.y},emplacement.largeurMotif/2};

    if(motif.config == 0)
    {
        dessineCarre(img,palette.pal[motif.cfond], carreNO);
        dessineCarre(img,palette.pal[motif.cfond], carreNE);
        dessineCarre(img,palette.pal[motif.cobjet], carreSO);
        dessineCarre(img,palette.pal[motif.cfond], carreSE);
        if(trajectoire)
        {
            int y = carreSO.origine.y+carreSO.largeurMotif+3;
            for (int x = carreSO.origine.x; x < carreSO.origine.x+carreSO.largeurMotif+3; x++)
            {
                img->rouge[x][y] = palette.pal[motif.cobjet].rouge;
                img->vert[x][y] = palette.pal[motif.cobjet].vert;
                img->bleu[x][y] = palette.pal[motif.cobjet].bleu;
                img->gris[x][y] = 0.2125*palette.pal[motif.cobjet].rouge +0.7154*palette.pal[motif.cobjet].vert + 0.0721*palette.pal[motif.cobjet].bleu;
            }

            int x = carreSO.origine.x+carreSO.largeurMotif+3;
            for (int y = carreSO.origine.y+carreSO.largeurMotif+3; y >= carreSO.origine.y; y--)
            {
                img->rouge[x][y] = palette.pal[motif.cobjet].rouge;
                img->vert[x][y] = palette.pal[motif.cobjet].vert;
                img->bleu[x][y] = palette.pal[motif.cobjet].bleu;
                img->gris[x][y] = 0.2125*palette.pal[motif.cobjet].rouge +0.7154*palette.pal[motif.cobjet].vert + 0.0721*palette.pal[motif.cobjet].bleu;
            }

        }
        
    }
    else
    {
        dessineCarre(img,palette.pal[motif.cobjet], carreNO);
        dessineCarre(img,palette.pal[motif.cobjet], carreNE);
        dessineCarre(img,palette.pal[motif.cfond], carreSO);
        dessineCarre(img,palette.pal[motif.cobjet], carreSE);
        if(trajectoire)
        {
            int y = carreSO.origine.y+carreSO.largeurMotif-4;
            for (int x = carreSO.origine.x; x < carreSO.origine.x+carreSO.largeurMotif-4; x++)
            {
                img->rouge[x][y] = palette.pal[motif.cobjet].rouge;
                img->vert[x][y] = palette.pal[motif.cobjet].vert;
                img->bleu[x][y] = palette.pal[motif.cobjet].bleu;
                img->gris[x][y] = 0.2125*palette.pal[motif.cobjet].rouge +0.7154*palette.pal[motif.cobjet].vert + 0.0721*palette.pal[motif.cobjet].bleu;
            }
            
            int x = carreSO.origine.x+carreSO.largeurMotif-4;
            for (int y = carreSO.origine.y+carreSO.largeurMotif-4; y >= carreSO.origine.y; y--)
            {
                img->rouge[x][y] = palette.pal[motif.cobjet].rouge;
                img->vert[x][y] = palette.pal[motif.cobjet].vert;
                img->bleu[x][y] = palette.pal[motif.cobjet].bleu;
                img->gris[x][y] = 0.2125*palette.pal[motif.cobjet].rouge +0.7154*palette.pal[motif.cobjet].vert + 0.0721*palette.pal[motif.cobjet].bleu;
            }
        }
    }
}

void dessineMotifNoeud(const Image *img,const Palette palette, const noeudMotif *noeud, const int formeId, const bool trajectoire){
    Emplacement emplacement = (Emplacement) {(Point){noeud->x,noeud->y},TAILLE_MOTIF};
    switch (formeId)
    {
    case 0:
        dessineMotif0(img,palette,noeud->motif,emplacement,trajectoire);
        break;
    case 1:
        dessineMotif1(img,palette,noeud->motif,emplacement,trajectoire);
        break;
    case 2:
        dessineMotif2(img,palette,noeud->motif,emplacement,trajectoire);
        break;
    case 3:
        dessineMotif3(img,palette,noeud->motif,emplacement,trajectoire);
        break;
    case 4:
        dessineMotif4(img,palette,noeud->motif,emplacement,trajectoire);
        break;
    case 5:
        dessineMotif5(img,palette,noeud->motif,emplacement,trajectoire);
        break;
    case 6:
        dessineMotif6(img,palette,noeud->motif,emplacement,trajectoire);
        break;
    default:
        break;
    }
}

void dessineCarte(const Image *img, const Palette palette, const noeudMotif *racine, const bool trajectoire){
    if(racine == NULL)
        return;
    dessineMotifNoeud(img,palette,racine,racine->motif.forme,trajectoire);
    dessineCarte(img,palette,racine->NO,trajectoire);
    dessineCarte(img,palette,racine->NE,trajectoire);
    dessineCarte(img,palette,racine->SO,trajectoire);
    dessineCarte(img,palette,racine->SE,trajectoire);
}

void sauveDescriptionChemin(Robot *robot,char* path){

    FILE * fp;

    /* open the file for writing*/
    fp = fopen (path,"w");
    int i = 0;
    MaillonTraj *current = robot->ptrTete;
    while (current)
    {
        i++;
        current=current->next;
    }
    
    /* write 10 lines of text into the file stream*/
    fprintf (fp, "%d mouvements\n",i);
    fprintf (fp, "Depart: x=%d y=%d\n",robot->depart.x,robot->depart.y);
    fprintf (fp, "Arrivee: x=%d y=%d",robot->arrivee.x,robot->arrivee.y);

    
    current = robot->ptrTete;
    char mouvementCode;
    while (current->next)
    {
        switch (positionVoisinage(current->next->position,current->position))
        {
        case 1:
            mouvementCode = '1';
            break;
        
        case 3:
            mouvementCode = '3';
            break;
        case 5:
            mouvementCode = '2';
            break;
        case 7:
            mouvementCode = '4';
            break;
        case 8:
            current=current->next;
            continue;
        default:
            mouvementCode = '?';
            break;
        }
        fprintf (fp, "\n%c",mouvementCode);
        current=current->next;
    }

    /* close the file*/  
    fclose (fp);
}