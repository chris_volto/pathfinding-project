#include "../include/Partie3.h"
#include <stdlib.h> // Pour pouvoir utiliser exit()
#include <stdio.h> // Pour pouvoir utiliser printf()
#include <string.h>

short int calculCarreNB(int start_x, int start_y, int size, short int **matrix){
    bool isBlack = false;
    bool isWhite = false;

    for(int x = start_x; x < size+start_x; x++){
        for(int y = start_y; y < size+start_y; y++){

            //Detecte du blanc
            if(matrix[x][y] == 255){

                isWhite = true;

                //Si on avait détecté du noir
                if(isBlack){
                     return -1;
                }
            }

            //Detecte du noir
            else if(matrix[x][y] == 0){

                isBlack = true;
                //Si on avait détecté du blanc
                if(isWhite){
                     return -1;
                }
                   
            }
            
        }
    }

    if(isBlack)
        return 0;
    else
        return 255;
}

/**
 * @brief Fonction auxiliaire à la fonction creeArbreNB
 * 
 * @param val 
 * @return int 
 */
int etiquetteFeuilleNB(int val){
    if(val+20 < 256)
        return val+20;
    else
        return val+20 - 256;
}


noeudBinaire* creeArbreNB(int start_x, int start_y, int imgSideSize, short int **img){
    static int compteurEtiquettes = 0; // compteur d'étiquette
    static int compteurnoeud = 0; //compteur de noeud

    noeudBinaire *root = (noeudBinaire*) malloc(sizeof(noeudBinaire));
    root->size = imgSideSize;
    root->start_x = start_x;
    root->start_y = start_y;
    root->value = calculCarreNB(start_x,start_y,imgSideSize,img);

    switch (root->value)
    {
    //Si c'est une feuille
    case 255:
    case 0:
        root->NW = NULL;
        root->NE = NULL;
        root->SE = NULL;
        root->SW = NULL;
        root->etiquette = compteurEtiquettes;
        compteurEtiquettes = etiquetteFeuilleNB(compteurEtiquettes);

        break;

    //Si c'est un noeud, on relance la récursivité
    case -1:
        root->etiquette = -1;
        compteurnoeud++;

        root->NW = creeArbreNB( start_x,
                                start_y+imgSideSize/2,
                                imgSideSize/2,
                                img);

        root->NE = creeArbreNB( start_x+imgSideSize/2,
                                start_y+imgSideSize/2,
                                imgSideSize/2,
                                img);
                                
        root->SW = creeArbreNB( start_x,
                                start_y,
                                imgSideSize/2,
                                img);
                                
        root->SE = creeArbreNB( start_x+imgSideSize/2,
                                start_y,
                                imgSideSize/2,
                                img);
        break;
    }

    return root;
}

int compteFeuille(noeudBinaire* racine)
{
    static int nbreEtiquettes = 0;
    if(racine)
    {
        if(racine->etiquette != -1)
        {
            nbreEtiquettes++;
        }
        compteFeuille(racine->NE);
        compteFeuille(racine->NW);
        compteFeuille(racine->SE);
        compteFeuille(racine->SW);
    }

    return nbreEtiquettes;
}

void creeImageArbreNB(Image* img, noeudBinaire* rcn, bool etiq){
    if(rcn != NULL){
        //Si c'est un noeud on relance la récursivité
        if(rcn->value == -1){
            creeImageArbreNB(img,rcn->NW,etiq);
            creeImageArbreNB(img,rcn->NE,etiq);
            creeImageArbreNB(img,rcn->SW,etiq);
            creeImageArbreNB(img,rcn->SE,etiq);
        }
        else{
            for(int y = rcn->start_y; y < rcn->size +rcn->start_y ; y++){
                for(int x = rcn->start_x; x < rcn->size+rcn->start_x; x++){

                    //Si etiq == true, on écrit l'étiquette sinon la valeur stockée
                    img->gris[x][y] = (etiq)?rcn->etiquette:rcn->value;
                    img->rouge[x][y] = (etiq)?rcn->etiquette:rcn->value;
                    img->bleu[x][y] = (etiq)?rcn->etiquette:rcn->value;
                    img->vert[x][y] = (etiq)?rcn->etiquette:rcn->value;
                }
            }
        }
    }
}

void libereArbreBinaire(noeudBinaire *rcn)
{
    if(rcn)
    {
        libereArbreBinaire(rcn->NW);
        libereArbreBinaire(rcn->NE);
        libereArbreBinaire(rcn->SW);
        libereArbreBinaire(rcn->SE);
        free(rcn);
    }
}