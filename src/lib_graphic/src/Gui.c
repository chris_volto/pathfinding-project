#include "../include/Gui.h"
#include "../include/AppState.h"
#include <stdlib.h>

extern AppState *appState;

void freeGui(Gui *gui)
{
    if(gui->window)
        SDL_DestroyWindow(gui->window);
    if(gui->renderer)
        SDL_DestroyRenderer(gui->renderer);
    if(gui->views)
        freeViews(gui->views);

    free(gui);
}

void freeGuis(Gui **guis)
{
    for(int i = 0; guis[i]; i++)
        freeGui(guis[i]);
    free(guis);
}

Gui* initGui(Gui_Settings gui_settings)
{
    Gui *gui =(Gui*) malloc(sizeof(Gui));
    
    if(gui == NULL) return NULL;

    if((gui->window = SDL_CreateWindow(gui_settings.title,gui_settings.x,gui_settings.y,gui_settings.w,gui_settings.h,gui_settings.win_flags)) == NULL ||
       (gui->renderer = SDL_CreateRenderer(gui->window,-1,gui_settings.ren_flags)) == NULL ||
       SDL_SetRenderDrawColor(gui->renderer,0,0,0,0) != 0 ||
       SDL_RenderClear(gui->renderer) != 0)
    {
        freeGui(gui);
        return NULL;
    }

    gui->views = gui_settings.views;
    gui->visible = SDL_TRUE;

    return gui;
}


int addGui(Gui_Settings gui_settings)
{
    Gui *gui = initGui(gui_settings);
    if(gui == NULL) return -1;

    int nbreGuis;
    for(nbreGuis=0;appState->guis[nbreGuis];nbreGuis++);
    Gui *tmpGuis[nbreGuis];
    for(int i=0;i<nbreGuis;i++) tmpGuis[i] = appState->guis[i];

    appState->guis = (Gui**) realloc(appState->guis,sizeof(Gui*)*(nbreGuis+2));
    if(appState->guis == NULL) return -1;

    for(int i=0;i<nbreGuis;i++) appState->guis[i] = tmpGuis[i];
    appState->guis[nbreGuis] = gui;
    appState->guis[nbreGuis+1] = NULL;

    return 0;
}

int removeGui(Gui* gui)
{
    int targetIndex = -1;
    int nbreGuis; 
    for(nbreGuis = 0; appState->guis[nbreGuis]; nbreGuis++)
        if(appState->guis[nbreGuis] == gui)
            targetIndex = nbreGuis;
    if(targetIndex == -1) return -1;
    else freeGui(appState->guis[targetIndex]);

    Gui *tmpGuis[nbreGuis-1];
    for(int i=0;i<nbreGuis;i++)
    {
        if(i<targetIndex)
            tmpGuis[i] = appState->guis[i];
        else if(i>targetIndex)
            tmpGuis[i-1] = appState->guis[i];
    }
    appState->guis = (Gui**) realloc(appState->guis,sizeof(Gui*)*nbreGuis);
    if(appState->guis == NULL) return -1;

    for(int i=0;i<nbreGuis-1;i++) appState->guis[i] = tmpGuis[i];
    appState->guis[nbreGuis-1] = NULL;

    return 0;
}