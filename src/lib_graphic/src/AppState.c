#include "../include/AppState.h"
#include "../include/App.h"
#include <stdlib.h>

extern AppState *appState;

int main(int argc, char *argv[])
{

    initialization(
        1,
        (Gui_Settings)
        {
            .title = "MiniProjet Algorithmie - Professeur Mme.ROBERT",
            .x = SDL_WINDOWPOS_CENTERED,
            .y = SDL_WINDOWPOS_CENTERED,
            .w = WINDOW_W,
            .h = WINDOW_H,
            .win_flags = 0,
            .ren_flags = SDL_RENDERER_PRESENTVSYNC,
            initViews(1,
                initView(
                    SDL_TRUE,
                    initFrames(
                        9,
                        initFrame(
                            SDL_TRUE,
                            0,
                            0,
                            WINDOW_W,
                            100,
                            (SDL_Color){240,240,240,SDL_ALPHA_OPAQUE},
                            (SDL_Color){0,0,0,SDL_ALPHA_OPAQUE},
                            2,
                            NULL,
                            initButtons(
                                1,
                                initButton(
                                    0,
                                    0,
                                    WINDOW_W,
                                    100,
                                    0,
                                    "Mini Projet - VOLTO Christophe",
                                    "./resources/font/sans.ttf",
                                    25,
                                    (SDL_Color){0,0,0,SDL_ALPHA_OPAQUE},
                                    (SDL_Color){0,0,0,SDL_ALPHA_TRANSPARENT},
                                    (SDL_Color){0,0,0,SDL_ALPHA_TRANSPARENT},
                                    SDL_TRUE,
                                    NULL
                                )
                            ),
                            NULL
                        ),
                        initFrame(
                            SDL_TRUE,
                            650,
                            98,
                            250,
                            WINDOW_H,
                            (SDL_Color){240,240,240,SDL_ALPHA_OPAQUE},
                            (SDL_Color){0,0,0,SDL_ALPHA_OPAQUE},
                            2,
                            NULL,
                            initButtons(
                                6,
                                initButton(
                                    25,
                                    33,
                                    200,
                                    70,
                                    2,
                                    "Basculer sur la partie 1",
                                    "./resources/font/sans.ttf",
                                    15,
                                    (SDL_Color){0,0,0,SDL_ALPHA_OPAQUE}, //Text Color
                                    (SDL_Color){240,240,240,SDL_ALPHA_OPAQUE}, //Background Color
                                    (SDL_Color){106,106,106,SDL_ALPHA_OPAQUE}, //OutLine Color
                                    SDL_TRUE,
                                    affiche_partie1_callback
                                ),
                                initButton(
                                    25,
                                    167,
                                    200,
                                    70,
                                    2,
                                    "Basculer sur la partie 2",
                                    "./resources/font/sans.ttf",
                                    15,
                                    (SDL_Color){0,0,0,SDL_ALPHA_OPAQUE}, //Text Color
                                    (SDL_Color){240,240,240,SDL_ALPHA_OPAQUE}, //Background Color
                                    (SDL_Color){106,106,106,SDL_ALPHA_OPAQUE}, //OutLine Color
                                    SDL_TRUE,
                                    affiche_partie2_callback
                                ),
                                initButton(
                                    25,
                                    301,
                                    200,
                                    70,
                                    2,
                                    "Basculer sur la partie 3",
                                    "./resources/font/sans.ttf",
                                    15,
                                    (SDL_Color){0,0,0,SDL_ALPHA_OPAQUE}, //Text Color
                                    (SDL_Color){240,240,240,SDL_ALPHA_OPAQUE}, //Background Color
                                    (SDL_Color){106,106,106,SDL_ALPHA_OPAQUE}, //OutLine Color
                                    SDL_TRUE,
                                    affiche_partie3_callback
                                ),
                                initButton(
                                    25,
                                    434,
                                    200,
                                    70,
                                    2,
                                    "Basculer sur la partie 4",
                                    "./resources/font/sans.ttf",
                                    15,
                                    (SDL_Color){0,0,0,SDL_ALPHA_OPAQUE}, //Text Color
                                    (SDL_Color){240,240,240,SDL_ALPHA_OPAQUE}, //Background Color
                                    (SDL_Color){106,106,106,SDL_ALPHA_OPAQUE}, //OutLine Color
                                    SDL_TRUE,
                                    affiche_partie4_callback
                                ),
                                initButton(
                                    25,
                                    568,
                                    200,
                                    70,
                                    2,
                                    "Basculer sur la partie 5",
                                    "./resources/font/sans.ttf",
                                    15,
                                    (SDL_Color){0,0,0,SDL_ALPHA_OPAQUE}, //Text Color
                                    (SDL_Color){240,240,240,SDL_ALPHA_OPAQUE}, //Background Color
                                    (SDL_Color){106,106,106,SDL_ALPHA_OPAQUE}, //OutLine Color
                                    SDL_TRUE,
                                    affiche_partie5_callback
                                ),
                                initButton(
                                    25,
                                    702,
                                    200,
                                    70,
                                    2,
                                    "Basculer sur la partie 6",
                                    "./resources/font/sans.ttf",
                                    15,
                                    (SDL_Color){0,0,0,SDL_ALPHA_OPAQUE}, //Text Color
                                    (SDL_Color){240,240,240,SDL_ALPHA_OPAQUE}, //Background Color
                                    (SDL_Color){106,106,106,SDL_ALPHA_OPAQUE}, //OutLine Color
                                    SDL_TRUE,
                                    affiche_partie6_callback
                                )
                            ),
                            NULL
                        ),
                        initFrame(
                            SDL_TRUE,
                            0,
                            98,
                            652,
                            802,
                            (SDL_Color){200,200,200,SDL_ALPHA_OPAQUE},
                            (SDL_Color){0,0,0,SDL_ALPHA_OPAQUE},
                            2,
                            NULL,
                            initButtons(
                                1,
                                initButton(
                                    0,
                                    0,
                                    652,
                                    802,
                                    0,
                                    "Pour lancer une simulation, cliquer sur une partie.",
                                    "./resources/font/sans.ttf",
                                    25,
                                    (SDL_Color){0,0,0,SDL_ALPHA_OPAQUE},
                                    (SDL_Color){0,0,0,SDL_ALPHA_TRANSPARENT},
                                    (SDL_Color){0,0,0,SDL_ALPHA_TRANSPARENT},
                                    SDL_TRUE,
                                    NULL
                                )
                            ),
                            NULL
                        ),
                        initFrame(
                            SDL_FALSE,
                            0,
                            98,
                            652,
                            802,
                            (SDL_Color){0,0,0,SDL_ALPHA_OPAQUE},
                            (SDL_Color){0,0,0,SDL_ALPHA_OPAQUE},
                            2,
                            NULL,
                            NULL,
                            initTextAreas(
                                1,
                                initTextArea(
                                    SDL_TRUE,
                                    0,
                                    0,
                                    646,
                                    802,
                                    0,
                                    NULL,
                                    "./resources/font/sans.ttf",
                                    18,
                                    (SDL_Color){0,0,0,SDL_ALPHA_OPAQUE},
                                    (SDL_Color){0,0,0,SDL_ALPHA_TRANSPARENT},
                                    (SDL_Color){255,255,255,SDL_ALPHA_OPAQUE}
                                )
                            )
                        ),
                        initFrame(
                            SDL_FALSE,
                            0,
                            98,
                            652,
                            802,
                            (SDL_Color){0,0,0,SDL_ALPHA_OPAQUE},
                            (SDL_Color){0,0,0,SDL_ALPHA_OPAQUE},
                            2,
                            NULL,
                            NULL,
                            initTextAreas(
                                1,
                                initTextArea(
                                    SDL_TRUE,
                                    0,
                                    0,
                                    646,
                                    802,
                                    0,
                                    NULL,
                                    "./resources/font/sans.ttf",
                                    18,
                                    (SDL_Color){0,0,0,SDL_ALPHA_OPAQUE},
                                    (SDL_Color){0,0,0,SDL_ALPHA_TRANSPARENT},
                                    (SDL_Color){255,255,255,SDL_ALPHA_OPAQUE}
                                )
                            )
                        ),
                        initFrame(
                            SDL_FALSE,
                            0,
                            98,
                            652,
                            802,
                            (SDL_Color){0,0,0,SDL_ALPHA_OPAQUE},
                            (SDL_Color){0,0,0,SDL_ALPHA_OPAQUE},
                            2,
                            NULL,
                            NULL,
                            initTextAreas(
                                1,
                                initTextArea(
                                    SDL_TRUE,
                                    0,
                                    0,
                                    646,
                                    802,
                                    0,
                                    NULL,
                                    "./resources/font/sans.ttf",
                                    18,
                                    (SDL_Color){0,0,0,SDL_ALPHA_OPAQUE},
                                    (SDL_Color){0,0,0,SDL_ALPHA_TRANSPARENT},
                                    (SDL_Color){255,255,255,SDL_ALPHA_OPAQUE}
                                )
                            )
                        ),
                        initFrame(
                            SDL_FALSE,
                            0,
                            98,
                            652,
                            802,
                            (SDL_Color){0,0,0,SDL_ALPHA_OPAQUE},
                            (SDL_Color){0,0,0,SDL_ALPHA_OPAQUE},
                            2,
                            NULL,
                            NULL,
                            initTextAreas(
                                1,
                                initTextArea(
                                    SDL_TRUE,
                                    0,
                                    0,
                                    646,
                                    802,
                                    0,
                                    NULL,
                                    "./resources/font/sans.ttf",
                                    18,
                                    (SDL_Color){0,0,0,SDL_ALPHA_OPAQUE},
                                    (SDL_Color){0,0,0,SDL_ALPHA_TRANSPARENT},
                                    (SDL_Color){255,255,255,SDL_ALPHA_OPAQUE}
                                )
                            )
                        ),
                        initFrame(
                            SDL_FALSE,
                            0,
                            98,
                            652,
                            802,
                            (SDL_Color){0,0,0,SDL_ALPHA_OPAQUE},
                            (SDL_Color){0,0,0,SDL_ALPHA_OPAQUE},
                            2,
                            NULL,
                            NULL,
                            initTextAreas(
                                1,
                                initTextArea(
                                    SDL_TRUE,
                                    0,
                                    0,
                                    646,
                                    802,
                                    0,
                                    NULL,
                                    "./resources/font/sans.ttf",
                                    18,
                                    (SDL_Color){0,0,0,SDL_ALPHA_OPAQUE},
                                    (SDL_Color){0,0,0,SDL_ALPHA_TRANSPARENT},
                                    (SDL_Color){255,255,255,SDL_ALPHA_OPAQUE}
                                )
                            )
                        ),
                        initFrame(
                            SDL_FALSE,
                            0,
                            98,
                            652,
                            802,
                            (SDL_Color){0,0,0,SDL_ALPHA_OPAQUE},
                            (SDL_Color){0,0,0,SDL_ALPHA_OPAQUE},
                            2,
                            NULL,
                            NULL,
                            initTextAreas(
                                1,
                                initTextArea(
                                    SDL_TRUE,
                                    0,
                                    0,
                                    646,
                                    802,
                                    0,
                                    NULL,
                                    "./resources/font/sans.ttf",
                                    18,
                                    (SDL_Color){0,0,0,SDL_ALPHA_OPAQUE},
                                    (SDL_Color){0,0,0,SDL_ALPHA_TRANSPARENT},
                                    (SDL_Color){255,255,255,SDL_ALPHA_OPAQUE}
                                )
                            )
                        )
                    )
                    
                )
            )
        }
    );

    if(appState == NULL) SDL_ExitWithError("cant init");
    
    while (appState->program_running)
    {
        if(SDL_WaitEvent(&appState->event)) eventHandler();

        update();
    }

    cleanEverything();
    SDL_Log("Clean exit :) .");
    return EXIT_SUCCESS;
}