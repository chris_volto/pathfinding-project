#include "../../include/containers/View.h"
#include <stdlib.h>

/**
 * @brief Args must be view*
 * 
 * @param nbreArguments 
 * @param ... 
 * @return View** 
 */
View* initView(SDL_bool visible, Frame **frames)
{
    View *view = (View*) malloc(sizeof(View));

    view->visible = visible;
    view->frames = frames;

    return view;
}

View ** initViews(int nbreArguments, ...){
    va_list ap;
    va_start (ap, nbreArguments);

    View **views = (View**) malloc(sizeof(View*)*(nbreArguments+1));

    for (int i = 0; i < nbreArguments; i++)
        views[i] = va_arg (ap, View*);
    va_end (ap);
    
    views[nbreArguments] = NULL;

    return views;
}

void freeView(View *view)
{
    if(view->frames)
        freeFrames(view->frames);
    free(view);
}

void freeViews(View **views)
{
    for (int i = 0; views[i] ; i++)
        freeView(views[i]);
    free(views);
}