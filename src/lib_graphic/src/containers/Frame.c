#include "../../include/containers/Frame.h"
#include <stdlib.h>

Frame* initFrame(SDL_bool visible,int x, int y,int w, int h, SDL_Color bgColor, SDL_Color outlineColor, int outlineWidth, DisplaySurface **displaySurfaces, Button **buttons,TextArea **textareas)
{
    Frame *frame = (Frame*) malloc(sizeof(Frame));

    frame->visible = visible;
    frame->displaySurfaces = displaySurfaces;
    frame->buttons = buttons;
    frame->x = x;
    frame->y = y;
    frame->h = h;
    frame->w = w;
    frame->outlineColor = outlineColor;
    frame->bgColor = bgColor;
    frame->outlineWidth = outlineWidth;
    frame->textareas = textareas;
    return frame;
}

Frame ** initFrames(int nbreArguments, ...){
    va_list ap;
    va_start (ap, nbreArguments);

    Frame **frames = (Frame**) malloc(sizeof(Frame*)*(nbreArguments+1));

    for (int i = 0; i < nbreArguments; i++)
        frames[i] = va_arg (ap, Frame*);
    va_end (ap);
    
    frames[nbreArguments] = NULL;

    return frames;
}

void freeFrame(Frame *frame)
{
    if(frame->displaySurfaces)
        freeDisplaySurfaces(frame->displaySurfaces);
    if(frame->buttons)
        freeButtons(frame->buttons);
    if(frame->textareas)
        freeTextAreas(frame->textareas);
    free(frame);
}

void freeFrames(Frame **frames)
{
    for(int i = 0; frames[i]; i++)
        freeFrame(frames[i]);
    free(frames);
}