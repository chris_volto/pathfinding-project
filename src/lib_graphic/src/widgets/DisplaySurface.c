#include "../../include/widgets/DisplaySurface.h"
#include "../../include/AppState.h"
#include <stdlib.h>

extern AppState *appState;

DisplaySurface * initDisplaySurface(SDL_bool visible,SDL_bool disabled, int x, int y, int w, int h, Image *bgImage, void (*callback)(void *parentGui, void *parentFrame)){
    DisplaySurface *ret = (DisplaySurface*) malloc(sizeof(DisplaySurface));
    ret->h = h;
    ret->w = w;
    ret->x = x;
    ret->y = y;
    ret->bgImage = bgImage;
    ret->callback = callback;
    ret->visible = visible;
    ret->disabled = disabled;
    return ret;
}

DisplaySurface ** initDisplaySurfaces(int nbreArguments, ...){
    va_list ap;
    va_start (ap, nbreArguments);

    DisplaySurface **displaySurface = (DisplaySurface**) malloc(sizeof(DisplaySurface*)*(nbreArguments+1));

    for (int i = 0; i < nbreArguments; i++)
        displaySurface[i] = va_arg (ap, DisplaySurface*);
    va_end (ap);
    
    displaySurface[nbreArguments] = NULL;

    return displaySurface;
}

void freeDisplaySurface(DisplaySurface *displaySurface)
{
    if(displaySurface->bgImage)
        libereImage(&(displaySurface->bgImage));
    free(displaySurface);
}

void freeDisplaySurfaces(DisplaySurface **displaySurfaces)
{
    for (int i = 0; displaySurfaces[i]; i++)
        freeDisplaySurface(displaySurfaces[i]);
    free(displaySurfaces);   
}

SDL_bool isOverDisplaySurface(DisplaySurface *displaySurface, void *parent)
{
    if(appState->event.button.x >= ((Frame*) parent)->x+displaySurface->x && appState->event.button.x <= ((Frame*) parent)->x+displaySurface->x+displaySurface->w && appState->event.button.y >= ((Frame*) parent)->y+displaySurface->y && appState->event.button.y <= ((Frame*) parent)->y+displaySurface->y+displaySurface->h)
        return SDL_TRUE;
    else
        return SDL_FALSE;
}