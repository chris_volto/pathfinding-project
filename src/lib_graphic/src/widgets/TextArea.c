#include "../../include/widgets/TextArea.h"
#include <stdlib.h>
#include <string.h>

TextArea* initTextArea(SDL_bool visible, int x, int y, int width, int heigth, int outlineWidth, char *text, char *fontName, int fontSize, SDL_Color bgColor, SDL_Color outlineColor, SDL_Color textColor)
{
    TextArea *textArea = (TextArea*) malloc(sizeof(TextArea));
    if(!textArea) return NULL;

    textArea->visible = visible;
    textArea->x = x;
    textArea->y = y;
    textArea->width = width;
    textArea->heigth = heigth;
    textArea->outlineWidth = outlineWidth;
    textArea->scrollPosition = 0;
    textArea->text = text;
    textArea->fontName = fontName;
    textArea->fontSize = fontSize;
    textArea->bgColor = bgColor;
    textArea->outlineColor = outlineColor;
    textArea->textColor = textColor;
    
    return textArea;
}

TextArea** initTextAreas(int nbreArguments, ...)
{
    va_list ap;
    va_start (ap, nbreArguments);

    TextArea **textAreas = (TextArea**) malloc(sizeof(TextArea*)*(nbreArguments+1));

    for (int i = 0; i < nbreArguments; i++)
        textAreas[i] = va_arg (ap, TextArea*);
    va_end (ap);
    
    textAreas[nbreArguments] = NULL;

    return textAreas;
}

void freeTextArea(TextArea *textArea)
{
    if(textArea->text)
        free(textArea->text);
    free(textArea);
}
void freeTextAreas(TextArea **textAreas)
{
    for (int i = 0; textAreas[i]; i++)
        freeTextArea(textAreas[i]);
    free(textAreas);
}

char* createString(char *string)
{
    char *ret = (char*) malloc(sizeof(char)*(strlen(string)+1));
    for (int i = 0; i < strlen(string); i++)
        ret[i] = string[i];
    ret[strlen(string)] = '\0';
    return ret;
}

char* extractSubString(int index1, int index2, char* string)
{
    char *substring = (char*) malloc(sizeof(char)*(index2-index1+2));
    int index = 0;
    for (int i = index1; i <= index2; i++)
    {
        substring[index] = string[i];
        index++;
    }
    substring[index] = '\0';
    return substring;
}

char* myStrCat(char* str1, char* str2)
{
    char* ret = (char*) malloc(sizeof(char)*(strlen(str1)+strlen(str2)+1));
    for(int i = 0; i < strlen(str1);i++)
        ret[i] = str1[i];
    int index = strlen(str1);
    for(int i = 0; i < strlen(str2);i++)
    {
        ret[index] = str2[i];
        index++;
    }
    ret[strlen(str1)+strlen(str2)] = '\0';

    return ret;
}

void addText(TextArea *textArea, char *textToAdd)
{
    
    char *newText = myStrCat(textArea->text, textToAdd);
    free(textArea->text);
    textArea->text = newText;
}