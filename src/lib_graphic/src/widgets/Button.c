#include "../../include/widgets/Button.h"
#include "../../include/AppState.h"
#include "../../include/App.h"

#include "../../../lib/include/Partie3.h"
#include "../../../lib/include/Partie4.h"
#include "../../../lib/include/Partie5.h"
#include "../../../lib/include/Partie6.h"
#include <stdlib.h>
#include <stdio.h>

extern AppState *appState;

void simple_callback(void *parentGui, void *parentFrame){
    SDL_Log("bouton appuye\n");
}

void trigger_error_callback(void *parentGui, void *parentFrame){
    SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR,
                         "Erreur.",
                         "Invalid pre-set robot positions.",
                         ((Gui*) parentGui)->window);
}

void affiche_partie1_callback(void *parentGui, void *parentFrame){
    while(appState->guis[1])
        removeGui(appState->guis[1]);
        
    Gui *gui = (Gui*) parentGui;
    TextArea *textArea = gui->views[0]->frames[3]->textareas[0];

    if(gui->views[0]->frames[3]->textareas[0]->text)
    {
        free(gui->views[0]->frames[3]->textareas[0]->text);
        gui->views[0]->frames[3]->textareas[0]->text = createString("RESET\n------ LANCEMENT PARTIE 1 ------");
    }
    else
    {
        gui->views[0]->frames[3]->textareas[0]->text = createString("------ LANCEMENT PARTIE 1 ------");
    }
    
    gui->views[0]->frames[2]->visible = SDL_FALSE;
    gui->views[0]->frames[3]->visible = SDL_TRUE;
    gui->views[0]->frames[4]->visible = SDL_FALSE;
    gui->views[0]->frames[5]->visible = SDL_FALSE;
    gui->views[0]->frames[6]->visible = SDL_FALSE;
    gui->views[0]->frames[7]->visible = SDL_FALSE;
    gui->views[0]->frames[8]->visible = SDL_FALSE;

    char textToAdd[500];

    sprintf(textToAdd,"\n> lancement fonction lisBMPRGB\narg1:'%s'","./resources/global_assets/partie1.bmp");
    addText(textArea,textToAdd);
    update();
    DonneesImageRGB* img = lisBMPRGB("./resources/global_assets/partie1.bmp");
    sprintf(textToAdd,"\n> OUTPUT: %p - %s",img,(img==NULL)?"ERREUR":"OK");
    addText(textArea,textToAdd);
    update();
   
    if(img == NULL) return;

    sprintf(textToAdd,"\n\n> Dimensions: %dx%d.",img->largeurImage,img->hauteurImage);
    addText(textArea,textToAdd);
    update();
    
    sprintf(textToAdd,"\n\n> lancement fonction ecrisBMPRGB_Dans\narg1:'%p' arg2:'%s'",img,"./resources/partie1/partie1_copie.bmp");
    addText(textArea,textToAdd);
    update();
    int retCode = ecrisBMPRGB_Dans(img,"./resources/partie1/partie1_copie.bmp");
    sprintf(textToAdd,"\n> OUTPUT: %d - %s",retCode,(retCode==1)?"OK":"ERREUR");
    addText(textArea,textToAdd);
    update();

    addGui(
        (Gui_Settings)
        {
            .title = "P1-Annexe 1",
            .x = SDL_WINDOWPOS_CENTERED,
            .y = SDL_WINDOWPOS_CENTERED,
            .w = 256,
            .h = 256,
            .win_flags = 0,
            .ren_flags = SDL_RENDERER_PRESENTVSYNC,
            initViews(1,
                initView(
                    SDL_TRUE,
                    initFrames(
                        1,
                        initFrame(
                            SDL_TRUE,
                            0,
                            0,
                            256,
                            256,
                            (SDL_Color){0,0,0,SDL_ALPHA_OPAQUE},
                            (SDL_Color){0,0,0,SDL_ALPHA_OPAQUE},
                            0,
                            initDisplaySurfaces(
                                1,
                                initDisplaySurface(
                                    SDL_TRUE,
                                    SDL_FALSE,
                                    0,
                                    0,
                                    256,
                                    256,
                                    chargeImage("./resources/partie1/partie1_copie.bmp"),
                                    NULL
                                )
                            ),
                            NULL,
                            NULL
                        )
                    )
                )
            )
        }
    );

    sprintf(textToAdd,"\n\n> lancement fonction libereDonneesImageRGB\narg1:'%p'",&img);
    addText(textArea,textToAdd);
    update();
    libereDonneesImageRGB(&img);
    sprintf(textToAdd,"\n> Retour main: 0 - OK.");
    addText(textArea,textToAdd);
    update();

    SDL_FlushEvent(SDL_MOUSEBUTTONDOWN);
}

void affiche_partie2_callback(void *parentGui, void *parentFrame){
    while(appState->guis[1])
        removeGui(appState->guis[1]);

    Gui *gui = (Gui*) parentGui;
    if(gui->views[0]->frames[4]->textareas[0]->text)
    {
        free(gui->views[0]->frames[4]->textareas[0]->text);
        gui->views[0]->frames[4]->textareas[0]->text = createString("RESET\n------ LANCEMENT PARTIE 2 ------");
    }
    else
    {
        gui->views[0]->frames[4]->textareas[0]->text = createString("------ LANCEMENT PARTIE 2 ------");
    }

    
    TextArea *textArea = gui->views[0]->frames[4]->textareas[0];
    
    gui->views[0]->frames[2]->visible = SDL_FALSE;
    gui->views[0]->frames[3]->visible = SDL_FALSE;
    gui->views[0]->frames[4]->visible = SDL_TRUE;
    gui->views[0]->frames[5]->visible = SDL_FALSE;
    gui->views[0]->frames[6]->visible = SDL_FALSE;
    gui->views[0]->frames[7]->visible = SDL_FALSE;
    gui->views[0]->frames[8]->visible = SDL_FALSE;

    char textToAdd[500];

    sprintf(textToAdd,"\n> lancement fonction chargeImage\narg1:'%s'","./resources/global_assets/partie2.bmp");
    addText(textArea,textToAdd);
    update();
    Image* imgHibou = chargeImage("./resources/global_assets/partie2.bmp");
    sprintf(textToAdd,"\n> OUTPUT: %p - %s",imgHibou,(imgHibou==NULL)?"ERREUR":"OK");
    addText(textArea,textToAdd);
    update();
   
    if(!imgHibou) return;

    sprintf(textToAdd,"\n\n> lancement fonction dupliqueImage\narg1:'%p'",imgHibou);
    addText(textArea,textToAdd);
    update();
    Image* hibouDuplique = dupliqueImage(imgHibou);
    sprintf(textToAdd,"\n> OUTPUT: 1 - OK");
    addText(textArea,textToAdd);
    update();
    
    sprintf(textToAdd,"\n\n> lancement fonction sauveImage\narg1:'%p' arg2:'%s'",hibouDuplique,"./resources/partie2/partie2_copie.bmp");
    addText(textArea,textToAdd);
    update();
    sauveImage(hibouDuplique,"./resources/partie2/partie2_copie.bmp");
    sprintf(textToAdd,"\n> OUTPUT: 1 - OK");
    addText(textArea,textToAdd);
    update();

    addGui(
        (Gui_Settings)
        {
            .title = "P2-Annexe 1",
            .x = SDL_WINDOWPOS_CENTERED,
            .y = SDL_WINDOWPOS_CENTERED,
            .w = 256,
            .h = 256,
            .win_flags = 0,
            .ren_flags = SDL_RENDERER_PRESENTVSYNC,
            initViews(1,
                initView(
                    SDL_TRUE,
                    initFrames(
                        1,
                        initFrame(
                            SDL_TRUE,
                            0,
                            0,
                            256,
                            256,
                            (SDL_Color){0,0,0,SDL_ALPHA_OPAQUE},
                            (SDL_Color){0,0,0,SDL_ALPHA_OPAQUE},
                            0,
                            initDisplaySurfaces(
                                1,
                                initDisplaySurface(
                                    SDL_TRUE,
                                    SDL_FALSE,
                                    0,
                                    0,
                                    256,
                                    256,
                                    chargeImage("./resources/partie2/partie2_copie.bmp"),
                                    NULL
                                )
                            ),
                            NULL,
                            NULL
                        )
                    )
                )
            )
        }
    );

    sprintf(textToAdd,"\n\n> lancement fonction sauveImageNG\narg1:'%p' arg2:'%s'",hibouDuplique,"./resources/partie2/partie2_ng.bmp");
    addText(textArea,textToAdd);
    update();
    sauveImageNG(hibouDuplique, "./resources/partie2/partie2_copie_ng.bmp");
    sprintf(textToAdd,"\n> OUTPUT: 1 - OK");
    addText(textArea,textToAdd);
    update();

    addGui(
        (Gui_Settings)
        {
            .title = "P2-Annexe 2",
            .x = SDL_WINDOWPOS_CENTERED,
            .y = SDL_WINDOWPOS_CENTERED,
            .w = 256,
            .h = 256,
            .win_flags = 0,
            .ren_flags = SDL_RENDERER_PRESENTVSYNC,
            initViews(1,
                initView(
                    SDL_TRUE,
                    initFrames(
                        1,
                        initFrame(
                            SDL_TRUE,
                            0,
                            0,
                            256,
                            256,
                            (SDL_Color){0,0,0,SDL_ALPHA_OPAQUE},
                            (SDL_Color){0,0,0,SDL_ALPHA_OPAQUE},
                            0,
                            initDisplaySurfaces(
                                1,
                                initDisplaySurface(
                                    SDL_TRUE,
                                    SDL_FALSE,
                                    0,
                                    0,
                                    256,
                                    256,
                                    chargeImage("./resources/partie2/partie2_copie_ng.bmp"),
                                    NULL
                                )
                            ),
                            NULL,
                            NULL
                        )
                    )
                )
            )
        }
    );

    sprintf(textToAdd,"\n> lancement fonction chargeImage\narg1:'%s'","./resources/partie2/partie2_copie_ng.bmp");
    addText(textArea,textToAdd);
    update();
    Image* imgHibouGris = chargeImage("./resources/partie2/partie2_copie_ng.bmp");
    sprintf(textToAdd,"\n> OUTPUT: %p - %s",imgHibouGris,(imgHibouGris==NULL)?"ERREUR":"OK");
    addText(textArea,textToAdd);
    update();

    if(!imgHibouGris) return;

    sprintf(textToAdd,"\n> lancement fonction differenceImage\narg1:'%p' arg2:'%p'",hibouDuplique,imgHibouGris);
    addText(textArea,textToAdd);
    update();
    Image* imgDiff = differenceImage(hibouDuplique,imgHibouGris);
    sprintf(textToAdd,"\n> OUTPUT: %p - %s",imgDiff,(imgDiff==NULL)?"ERREUR":"OK");
    addText(textArea,textToAdd);
    update();

    sprintf(textToAdd,"\n\n> lancement fonction sauveImage\narg1:'%p' arg2:'%s'",imgDiff,"./resources/partie2/partie2_diff.bmp");
    addText(textArea,textToAdd);
    update();
    sauveImage(imgDiff,"./resources/partie2/partie2_diff.bmp");
    sprintf(textToAdd,"\n> OUTPUT: 1 - OK");
    addText(textArea,textToAdd);
    update();

    addGui(
        (Gui_Settings)
        {
            .title = "P2-Annexe 3",
            .x = SDL_WINDOWPOS_CENTERED,
            .y = SDL_WINDOWPOS_CENTERED,
            .w = 256,
            .h = 256,
            .win_flags = 0,
            .ren_flags = SDL_RENDERER_PRESENTVSYNC,
            initViews(1,
                initView(
                    SDL_TRUE,
                    initFrames(
                        1,
                        initFrame(
                            SDL_TRUE,
                            0,
                            0,
                            256,
                            256,
                            (SDL_Color){0,0,0,SDL_ALPHA_OPAQUE},
                            (SDL_Color){0,0,0,SDL_ALPHA_OPAQUE},
                            0,
                            initDisplaySurfaces(
                                1,
                                initDisplaySurface(
                                    SDL_TRUE,
                                    SDL_FALSE,
                                    0,
                                    0,
                                    256,
                                    256,
                                    chargeImage("./resources/partie2/partie2_diff.bmp"),
                                    NULL
                                )
                            ),
                            NULL,
                            NULL
                        )
                    )
                )
            )
        }
    );

    sprintf(textToAdd,"\n\n> lancement fonction sauveImageNG\narg1:'%p' arg2:'%s'",imgDiff,"./resources/partie2/partie2_diff_ng.bmp");
    addText(textArea,textToAdd);
    update();
    sauveImageNG(imgDiff, "./resources/partie2/partie2_diff_ng.bmp");
    sprintf(textToAdd,"\n> OUTPUT: 1 - OK");
    addText(textArea,textToAdd);
    update();

    addGui(
        (Gui_Settings)
        {
            .title = "P2-Annexe 4",
            .x = SDL_WINDOWPOS_CENTERED,
            .y = SDL_WINDOWPOS_CENTERED,
            .w = 256,
            .h = 256,
            .win_flags = 0,
            .ren_flags = SDL_RENDERER_PRESENTVSYNC,
            initViews(1,
                initView(
                    SDL_TRUE,
                    initFrames(
                        1,
                        initFrame(
                            SDL_TRUE,
                            0,
                            0,
                            256,
                            256,
                            (SDL_Color){0,0,0,SDL_ALPHA_OPAQUE},
                            (SDL_Color){0,0,0,SDL_ALPHA_OPAQUE},
                            0,
                            initDisplaySurfaces(
                                1,
                                initDisplaySurface(
                                    SDL_TRUE,
                                    SDL_FALSE,
                                    0,
                                    0,
                                    256,
                                    256,
                                    chargeImage("./resources/partie2/partie2_diff_ng.bmp"),
                                    NULL
                                )
                            ),
                            NULL,
                            NULL
                        )
                    )
                )
            )
        }
    );

    sprintf(textToAdd,"\n\n> lancement fonction libereImage\narg1:'%p'",&imgHibou);
    addText(textArea,textToAdd);
    update();
    libereImage(&imgHibou);
    sprintf(textToAdd,"\n\n> lancement fonction libereImage\narg1:'%p'",&hibouDuplique);
    addText(textArea,textToAdd);
    update();
    libereImage(&hibouDuplique);
    sprintf(textToAdd,"\n\n> lancement fonction libereImage\narg1:'%p'",&imgHibouGris);
    addText(textArea,textToAdd);
    update();
    libereImage(&imgHibouGris);
    sprintf(textToAdd,"\n\n> lancement fonction libereImage\narg1:'%p'",&imgDiff);
    addText(textArea,textToAdd);
    update();
    libereImage(&imgDiff);
    sprintf(textToAdd,"\n> Retour main: 0 - OK.");
    addText(textArea,textToAdd);
    update();

    SDL_FlushEvent(SDL_MOUSEBUTTONDOWN);
}

void affiche_partie3_callback(void *parentGui, void *parentFrame){
    while(appState->guis[1])
        removeGui(appState->guis[1]);

    Gui *gui = (Gui*) parentGui;
    if(gui->views[0]->frames[5]->textareas[0]->text)
    {
        free(gui->views[0]->frames[5]->textareas[0]->text);
        gui->views[0]->frames[5]->textareas[0]->text = createString("RESET\n------ LANCEMENT PARTIE 3 ------");
    }
    else
    {
        gui->views[0]->frames[5]->textareas[0]->text = createString("------ LANCEMENT PARTIE 3 ------");
    }
    
    TextArea *textArea = gui->views[0]->frames[5]->textareas[0];
    
    gui->views[0]->frames[2]->visible = SDL_FALSE;
    gui->views[0]->frames[3]->visible = SDL_FALSE;
    gui->views[0]->frames[4]->visible = SDL_FALSE;
    gui->views[0]->frames[5]->visible = SDL_TRUE;
    gui->views[0]->frames[6]->visible = SDL_FALSE;
    gui->views[0]->frames[7]->visible = SDL_FALSE;
    gui->views[0]->frames[8]->visible = SDL_FALSE;
    char textToAdd[500];

    sprintf(textToAdd,"\n> lancement fonction chargeImage\narg1:'%s'","./resources/global_assets/partie3.bmp");
    addText(textArea,textToAdd);
    update();
    Image* img = chargeImage("./resources/global_assets/partie3.bmp");
    sprintf(textToAdd,"\n> OUTPUT: %p - %s",img,(img==NULL)?"ERREUR":"OK");
    addText(textArea,textToAdd);
    update();

    if(!img) return;

    sprintf(textToAdd,"\n> lancement fonction creeArbreNB\narg1:'%d' arg2:'%d' arg3:'%d' arg4:'%p'",0,0,img->largeur,img->gris);
    addText(textArea,textToAdd);
    update();
    noeudBinaire* rcn = creeArbreNB(0,0,img->largeur,img->gris);
    sprintf(textToAdd,"\n> OUTPUT: %p - %s",rcn,(rcn==NULL)?"ERREUR":"OK");
    addText(textArea,textToAdd);
    update();

    sprintf(textToAdd,"\n> lancement fonction compteFeuille\narg1:'%p'",rcn);
    addText(textArea,textToAdd);
    update();
    int nbreEtiquettes = compteFeuille(rcn);
    sprintf(textToAdd,"\n> OUTPUT: %d",nbreEtiquettes);
    addText(textArea,textToAdd);
    update();

    Image *imgArbre = alloueImage(rcn->size,rcn->size);
    
    sprintf(textToAdd,"\n> lancement fonction creeImageArbreNB\narg1:'%p' arg2:'%p' arg3:'%d'",imgArbre,rcn,true);
    addText(textArea,textToAdd);
    update();
    creeImageArbreNB(imgArbre,rcn,true);
    sprintf(textToAdd,"\n> OUTPUT: OK");
    addText(textArea,textToAdd);
    update();

    sprintf(textToAdd,"\n> lancement fonction sauveImage\narg1:'%p' arg2:'%s'",imgArbre,"./resources/partie3/partie3_reproduit_etiq.bmp");
    addText(textArea,textToAdd);
    update();
    sauveImage(imgArbre,"./resources/partie3/partie3_reproduit_etiq.bmp");
    sprintf(textToAdd,"\n> OUTPUT: OK");
    addText(textArea,textToAdd);
    update();

    addGui(
        (Gui_Settings)
        {
            .title = "P3-Annexe 1",
            .x = SDL_WINDOWPOS_CENTERED,
            .y = SDL_WINDOWPOS_CENTERED,
            .w = 256,
            .h = 256,
            .win_flags = 0,
            .ren_flags = SDL_RENDERER_PRESENTVSYNC,
            initViews(1,
                initView(
                    SDL_TRUE,
                    initFrames(
                        1,
                        initFrame(
                            SDL_TRUE,
                            0,
                            0,
                            256,
                            256,
                            (SDL_Color){0,0,0,SDL_ALPHA_OPAQUE},
                            (SDL_Color){0,0,0,SDL_ALPHA_OPAQUE},
                            0,
                            initDisplaySurfaces(
                                1,
                                initDisplaySurface(
                                    SDL_TRUE,
                                    SDL_FALSE,
                                    0,
                                    0,
                                    256,
                                    256,
                                    chargeImage("./resources/partie3/partie3_reproduit_etiq.bmp"),
                                    NULL
                                )
                            ),
                            NULL,
                            NULL
                        )
                    )
                )
            )
        }
    );

    sprintf(textToAdd,"\n> lancement fonction creeImageArbreNB\narg1:'%p' arg2:'%p' arg3:'%d'",imgArbre,rcn,false);
    addText(textArea,textToAdd);
    update();
    creeImageArbreNB(imgArbre,rcn,false);
    sprintf(textToAdd,"\n> OUTPUT: OK");
    addText(textArea,textToAdd);
    update();

    sprintf(textToAdd,"\n> lancement fonction sauveImage\narg1:'%p' arg2:'%s'",imgArbre,"./resources/partie3/partie3_reproduit.bmp");
    addText(textArea,textToAdd);
    update();
    sauveImage(imgArbre,"./resources/partie3/partie3_reproduit.bmp");
    sprintf(textToAdd,"\n> OUTPUT: OK");
    addText(textArea,textToAdd);
    update();

    addGui(
        (Gui_Settings)
        {
            .title = "P3-Annexe 2",
            .x = SDL_WINDOWPOS_CENTERED,
            .y = SDL_WINDOWPOS_CENTERED,
            .w = 256,
            .h = 256,
            .win_flags = 0,
            .ren_flags = SDL_RENDERER_PRESENTVSYNC,
            initViews(1,
                initView(
                    SDL_TRUE,
                    initFrames(
                        1,
                        initFrame(
                            SDL_TRUE,
                            0,
                            0,
                            256,
                            256,
                            (SDL_Color){0,0,0,SDL_ALPHA_OPAQUE},
                            (SDL_Color){0,0,0,SDL_ALPHA_OPAQUE},
                            0,
                            initDisplaySurfaces(
                                1,
                                initDisplaySurface(
                                    SDL_TRUE,
                                    SDL_FALSE,
                                    0,
                                    0,
                                    256,
                                    256,
                                    chargeImage("./resources/partie3/partie3_reproduit.bmp"),
                                    NULL
                                )
                            ),
                            NULL,
                            NULL
                        )
                    )
                )
            )
        }
    );

    libereImage(&img);
    libereImage(&imgArbre);
    libereArbreBinaire(rcn);

    sprintf(textToAdd,"\n> Main return 0 - OK");
    addText(textArea,textToAdd);
}

void affiche_partie4_callback(void *parentGui, void *parentFrame){
    while(appState->guis[1])
        removeGui(appState->guis[1]);

    Gui *gui = (Gui*) parentGui;
    if(gui->views[0]->frames[6]->textareas[0]->text)
    {
        free(gui->views[0]->frames[6]->textareas[0]->text);
        gui->views[0]->frames[6]->textareas[0]->text = createString("RESET\n------ LANCEMENT PARTIE 4 ------");
    }
    else
    {
        gui->views[0]->frames[6]->textareas[0]->text = createString("------ LANCEMENT PARTIE 4 ------");
    }
    
    TextArea *textArea = gui->views[0]->frames[6]->textareas[0];
    
    gui->views[0]->frames[2]->visible = SDL_FALSE;
    gui->views[0]->frames[3]->visible = SDL_FALSE;
    gui->views[0]->frames[4]->visible = SDL_FALSE;
    gui->views[0]->frames[5]->visible = SDL_FALSE;
    gui->views[0]->frames[6]->visible = SDL_TRUE;
    gui->views[0]->frames[7]->visible = SDL_FALSE;
    gui->views[0]->frames[8]->visible = SDL_FALSE;
    char textToAdd[500];
    /******* LOAD *******/
    sprintf(textToAdd,"\n> lancement fonction chargeImage\narg1:'%s'","./resources/global_assets/partie4.bmp");
    addText(textArea,textToAdd);
    update();
    Image* imgP4 = chargeImage("./resources/global_assets/partie4.bmp");
    sprintf(textToAdd,"\n> OUTPUT: %p - %s",imgP4,(imgP4==NULL)?"ERREUR":"OK");
    addText(textArea,textToAdd);
    update();

    sprintf(textToAdd,"\n> lancement fonction chargeImage\narg1:'%s'","./resources/global_assets/partie5_simple.bmp");
    addText(textArea,textToAdd);
    update();
    Image* imgP5_simple = chargeImage("./resources/global_assets/partie5_simple.bmp");
    sprintf(textToAdd,"\n> OUTPUT: %p - %s",imgP5_simple,(imgP5_simple==NULL)?"ERREUR":"OK");
    addText(textArea,textToAdd);
    update();

    sprintf(textToAdd,"\n> lancement fonction chargeImage\narg1:'%s'","./resources/global_assets/partie5_complexe.bmp");
    addText(textArea,textToAdd);
    update();
    Image* imgP5_complexe = chargeImage("./resources/global_assets/partie5_complexe.bmp");
    sprintf(textToAdd,"\n> OUTPUT: %p - %s",imgP5_complexe,(imgP5_complexe==NULL)?"ERREUR":"OK");
    addText(textArea,textToAdd);
    update();
    /******* PALETTES *******/
    sprintf(textToAdd,"\n> lancement fonction creePalette\narg1:'%p'",imgP4);
    addText(textArea,textToAdd);
    update();
    Palette* palP4 = creePalette(imgP4);
    sprintf(textToAdd,"\n> OUTPUT: %p - %s",palP4,(palP4==NULL)?"ERREUR":"OK");
    addText(textArea,textToAdd);
    update();

    sprintf(textToAdd,"\n> lancement fonction creePalette\narg1:'%p'",imgP5_simple);
    addText(textArea,textToAdd);
    update();
    Palette* palP5_simple = creePalette(imgP5_simple);
    sprintf(textToAdd,"\n> OUTPUT: %p - %s",palP5_simple,(palP5_simple==NULL)?"ERREUR":"OK");
    addText(textArea,textToAdd);
    update();

    sprintf(textToAdd,"\n> lancement fonction creePalette\narg1:'%p'",imgP5_complexe);
    addText(textArea,textToAdd);
    update();
    Palette* palP5_complexe = creePalette(imgP5_complexe);
    sprintf(textToAdd,"\n> OUTPUT: %p - %s",palP5_complexe,(palP5_complexe==NULL)?"ERREUR":"OK");
    addText(textArea,textToAdd);
    update();
    /******* CreeArbreMotif *******/
    sprintf(textToAdd,"\n> lancement fonction creeArbreMotifs\narg1:'%p' arg2:'%p' arg3:'%d' arg4:'%d' arg5:'%d'",imgP4,palP4,imgP4->largeur,0,0);
    addText(textArea,textToAdd);
    update();
    noeudMotif* ptrRcnP4 = creeArbreMotifs(imgP4,palP4,imgP4->largeur,0,0);
    sprintf(textToAdd,"\n> OUTPUT: %p - %s",ptrRcnP4,(ptrRcnP4==NULL)?"ERREUR":"OK");
    addText(textArea,textToAdd);
    update();

    sprintf(textToAdd,"\n> lancement fonction creeArbreMotifs\narg1:'%p' arg2:'%p' arg3:'%d' arg4:'%d' arg5:'%d'",imgP5_simple,palP5_simple,imgP5_simple->largeur,0,0);
    addText(textArea,textToAdd);
    update();
    noeudMotif* ptrRcnP5_simple = creeArbreMotifs(imgP5_simple,palP5_simple,imgP5_simple->largeur,0,0);
    sprintf(textToAdd,"\n> OUTPUT: %p - %s",ptrRcnP5_simple,(ptrRcnP5_simple==NULL)?"ERREUR":"OK");
    addText(textArea,textToAdd);
    update();

    sprintf(textToAdd,"\n> lancement fonction creeArbreMotifs\narg1:'%p' arg2:'%p' arg3:'%d' arg4:'%d' arg5:'%d'",imgP5_complexe,palP5_complexe,imgP5_complexe->largeur,0,0);
    addText(textArea,textToAdd);
    update();
    noeudMotif* ptrRcnP5_complexe = creeArbreMotifs(imgP5_complexe,palP5_complexe,imgP5_complexe->largeur,0,0);
    sprintf(textToAdd,"\n> OUTPUT: %p - %s",ptrRcnP5_complexe,(ptrRcnP5_complexe==NULL)?"ERREUR":"OK");
    addText(textArea,textToAdd);
    update();
    /******* sauveDescriptionImage *******/
    sprintf(textToAdd,"\n> lancement fonction sauveDescriptionImage\narg1:'%p' arg2:'%p'\n arg3:'%s'",ptrRcnP4,palP4, "./resources/partie4/description_imagePartie4.txt");
    addText(textArea,textToAdd);
    update();
    sauveDescriptionImage(ptrRcnP4,palP4, "./resources/partie4/description_imagePartie4.txt");
    sprintf(textToAdd,"\n> OUTPUT: OK");
    addText(textArea,textToAdd);
    update();
    
    FILE * fp;
    char * line = NULL;
    size_t len = 0;
    ssize_t read;

    read = 0;
    fp = fopen("./resources/partie4/description_imagePartie4.txt","r");
    if (fp == NULL)
        return;

    while ((read = getline(&line, &len, fp)) != -1) {
        sprintf(textToAdd,"\n%s",line);
        addText(textArea,textToAdd);
        update();
    }

    fclose(fp);
    if (line)
        free(line);

    sprintf(textToAdd,"\n> lancement fonction sauveDescriptionImage\narg1:'%p' arg2:'%p'\n arg3:'%s'",ptrRcnP5_simple,palP5_simple, "./resources/partie4/description_imagePartie5_simple.txt");
    addText(textArea,textToAdd);
    update();
    sauveDescriptionImage(ptrRcnP5_simple,palP5_simple, "./resources/partie4/description_imagePartie5_simple.txt");
    sprintf(textToAdd,"\n> OUTPUT: OK");
    addText(textArea,textToAdd);
    update();

    sprintf(textToAdd,"\nFICHIER TROP LOURD A AFFICHER");
    addText(textArea,textToAdd);
    update();

    sprintf(textToAdd,"\n> lancement fonction sauveDescriptionImage\narg1:'%p' arg2:'%p'\n arg3:'%s'",ptrRcnP5_complexe,palP5_complexe,"./resources/partie4/description_imagePartie5_complexe.txt");
    addText(textArea,textToAdd);
    update();
    sauveDescriptionImage(ptrRcnP5_complexe,palP5_complexe, "./resources/partie4/description_imagePartie5_complexe.txt");
    sprintf(textToAdd,"\n> OUTPUT: OK");
    addText(textArea,textToAdd);
    update();
    
    sprintf(textToAdd,"\nFICHIER TROP LOURD A AFFICHER");
    addText(textArea,textToAdd);
    update();

    /******* liberation *******/

    libereImage(&imgP4);
    libereImage(&imgP5_simple);
    libereImage(&imgP5_complexe);
    free(palP4->pal);
    free(palP5_simple->pal);
    free(palP5_complexe->pal);
    free(palP4);
    free(palP5_simple);
    free(palP5_complexe);
    libereArbreMotif(ptrRcnP4);
    libereArbreMotif(ptrRcnP5_simple);
    libereArbreMotif(ptrRcnP5_complexe);

    sprintf(textToAdd,"\n> Main return 0 - OK");
    addText(textArea,textToAdd);
}

void modfify_partie5_imgP4_callback(void *parentGui, void *parentFrame)
{
    
    Gui *gui = (Gui*) parentGui;
    Frame *frame = (Frame*) parentFrame;

    SDL_Point last_point1;
    SDL_Point last_point2;

    static SDL_Point point1P4 = (SDL_Point){67,15};
    static SDL_Point point2P4 = (SDL_Point){19,68};

    last_point1 = point1P4;
    last_point2 = point2P4;

    if(appState->event.button.button == SDL_BUTTON_LEFT)
    {
        point1P4 = (SDL_Point){appState->event.button.x-frame->x-frame->displaySurfaces[0]->x,128-(appState->event.button.y-frame->y-frame->displaySurfaces[0]->y)};
    }
    else if(appState->event.button.button == SDL_BUTTON_RIGHT){
        point2P4 = (SDL_Point){appState->event.button.x-frame->x-frame->displaySurfaces[0]->x,128-(appState->event.button.y-frame->y-frame->displaySurfaces[0]->y)};
    }
    
    if(appState->event.button.button == SDL_BUTTON_LEFT || appState->event.button.button == SDL_BUTTON_RIGHT)
    {
        Image* imgSansTrajectoires = chargeImage("./resources/global_assets/partie4.bmp");
        Palette* pal = creePalette(imgSansTrajectoires);
        noeudMotif* ptrRcn = creeArbreMotifs(imgSansTrajectoires,pal,imgSansTrajectoires->largeur,0,0);
        Image* imgAvecTrajectoires = alloueImage(imgSansTrajectoires->largeur,imgSansTrajectoires->hauteur);
        dessineCarte(imgAvecTrajectoires,*pal,ptrRcn,true);
        Robot *robot = calculeTrajectoire(imgAvecTrajectoires,imgSansTrajectoires,  pal,(Point){point1P4.x,point1P4.y}, (Point){point2P4.x,point2P4.y});
        if(robot != NULL){
            MaillonTraj *current = robot->ptrTete;
            while (current)
            {
                imgSansTrajectoires->rouge[current->position.x][current->position.y] = 0;
                imgSansTrajectoires->vert[current->position.x][current->position.y] = 0;
                imgSansTrajectoires->bleu[current->position.x][current->position.y] = 0;
                current=current->next;
            }
            sauveImage(imgSansTrajectoires,"./resources/partie5/partie4_robotpath.bmp");
            libereImage(&frame->displaySurfaces[0]->bgImage);
            frame->displaySurfaces[0]->bgImage = imgSansTrajectoires;
            sauveDescriptionChemin(robot,"./resources/partie5/partie4_robotpath.txt");
            
            while (robot->ptrTete)
            {
                MaillonTraj *tmp = robot->ptrTete->next;
                libereMaillon(&robot->ptrTete);
                robot->ptrTete = tmp;
            }
            free(robot);
            libereImage(&imgAvecTrajectoires);
            free(pal->pal);
            free(pal);
            libereArbreMotif(ptrRcn);
        }
        else{
            trigger_error_callback(parentGui,parentFrame);
            point1P4 = (SDL_Point){last_point1.x,last_point1.y};
            point2P4 = (SDL_Point){last_point2.x,last_point2.y};
            
            libereImage(&imgSansTrajectoires);
            libereImage(&imgAvecTrajectoires);
            free(pal->pal);
            free(pal);
            libereArbreMotif(ptrRcn);
        }
        
    }
}

void modfify_partie5_imgP5_simple_callback(void *parentGui, void *parentFrame)
{
    
    Gui *gui = (Gui*) parentGui;
    Frame *frame = (Frame*) parentFrame;

    SDL_Point last_point1;
    SDL_Point last_point2;

    static SDL_Point point1P5_simple = (SDL_Point){95,70};
    static SDL_Point point2P5_simple = (SDL_Point){501,503};

    last_point1 = point1P5_simple;
    last_point2 = point2P5_simple;

    if(appState->event.button.button == SDL_BUTTON_LEFT)
    {
        point1P5_simple = (SDL_Point){appState->event.button.x-frame->x-frame->displaySurfaces[0]->x,512-(appState->event.button.y-frame->y-frame->displaySurfaces[0]->y)};
    }
    else if(appState->event.button.button == SDL_BUTTON_RIGHT){
        point2P5_simple = (SDL_Point){appState->event.button.x-frame->x-frame->displaySurfaces[0]->x,512-(appState->event.button.y-frame->y-frame->displaySurfaces[0]->y)};
    }
    
    if(appState->event.button.button == SDL_BUTTON_LEFT || appState->event.button.button == SDL_BUTTON_RIGHT)
    {
        Image* imgSansTrajectoires = chargeImage("./resources/global_assets/partie5_simple.bmp");
        Palette* pal = creePalette(imgSansTrajectoires);
        noeudMotif* ptrRcn = creeArbreMotifs(imgSansTrajectoires,pal,imgSansTrajectoires->largeur,0,0);
        Image* imgAvecTrajectoires = alloueImage(imgSansTrajectoires->largeur,imgSansTrajectoires->hauteur);
        dessineCarte(imgAvecTrajectoires,*pal,ptrRcn,true);
        Robot *robot = calculeTrajectoire(imgAvecTrajectoires,imgSansTrajectoires,  pal,(Point){point1P5_simple.x,point1P5_simple.y}, (Point){point2P5_simple.x,point2P5_simple.y});
        if(robot != NULL){
            MaillonTraj *current = robot->ptrTete;
            while (current)
            {
                imgSansTrajectoires->rouge[current->position.x][current->position.y] = 0;
                imgSansTrajectoires->vert[current->position.x][current->position.y] = 0;
                imgSansTrajectoires->bleu[current->position.x][current->position.y] = 0;
                current=current->next;
            }
            sauveImage(imgSansTrajectoires,"./resources/partie5/partie5_simple_robotpath.bmp");
            libereImage(&frame->displaySurfaces[0]->bgImage);
            frame->displaySurfaces[0]->bgImage = imgSansTrajectoires;
            sauveDescriptionChemin(robot,"./resources/partie5/partie5_simple_robotpath.txt");
            
            while (robot->ptrTete)
            {
                MaillonTraj *tmp = robot->ptrTete->next;
                libereMaillon(&robot->ptrTete);
                robot->ptrTete = tmp;
            }
            free(robot);
            libereImage(&imgAvecTrajectoires);
            free(pal->pal);
            free(pal);
            libereArbreMotif(ptrRcn);
        }
        else{
            trigger_error_callback(parentGui,parentFrame);
            point1P5_simple = (SDL_Point){last_point1.x,last_point1.y};
            point2P5_simple = (SDL_Point){last_point2.x,last_point2.y};
            
            libereImage(&imgSansTrajectoires);
            libereImage(&imgAvecTrajectoires);
            free(pal->pal);
            free(pal);
            libereArbreMotif(ptrRcn);
        }
        
    }
}

void modfify_partie5_imgP5_complexe_callback(void *parentGui, void *parentFrame)
{
    
    Gui *gui = (Gui*) parentGui;
    Frame *frame = (Frame*) parentFrame;

    SDL_Point last_point1;
    SDL_Point last_point2;

    static SDL_Point point1P5_complexe = (SDL_Point){95,70};
    static SDL_Point point2P5_complexe = (SDL_Point){501,503};

    last_point1 = point1P5_complexe;
    last_point2 = point2P5_complexe;

    if(appState->event.button.button == SDL_BUTTON_LEFT)
    {
        point1P5_complexe = (SDL_Point){appState->event.button.x-frame->x-frame->displaySurfaces[0]->x,512-(appState->event.button.y-frame->y-frame->displaySurfaces[0]->y)};
    }
    else if(appState->event.button.button == SDL_BUTTON_RIGHT){
        point2P5_complexe = (SDL_Point){appState->event.button.x-frame->x-frame->displaySurfaces[0]->x,512-(appState->event.button.y-frame->y-frame->displaySurfaces[0]->y)};
    }
    
    if(appState->event.button.button == SDL_BUTTON_LEFT || appState->event.button.button == SDL_BUTTON_RIGHT)
    {
        Image* imgSansTrajectoires = chargeImage("./resources/global_assets/partie5_complexe.bmp");
        Palette* pal = creePalette(imgSansTrajectoires);
        noeudMotif* ptrRcn = creeArbreMotifs(imgSansTrajectoires,pal,imgSansTrajectoires->largeur,0,0);
        Image* imgAvecTrajectoires = alloueImage(imgSansTrajectoires->largeur,imgSansTrajectoires->hauteur);
        dessineCarte(imgAvecTrajectoires,*pal,ptrRcn,true);
        Robot *robot = calculeTrajectoire(imgAvecTrajectoires,imgSansTrajectoires,  pal,(Point){point1P5_complexe.x,point1P5_complexe.y}, (Point){point2P5_complexe.x,point2P5_complexe.y});
        if(robot != NULL){
            MaillonTraj *current = robot->ptrTete;
            while (current)
            {
                imgSansTrajectoires->rouge[current->position.x][current->position.y] = 0;
                imgSansTrajectoires->vert[current->position.x][current->position.y] = 0;
                imgSansTrajectoires->bleu[current->position.x][current->position.y] = 0;
                current=current->next;
            }
            sauveImage(imgSansTrajectoires,"./resources/partie5/partie5_complexe_robotpath.bmp");
            libereImage(&frame->displaySurfaces[0]->bgImage);
            frame->displaySurfaces[0]->bgImage = imgSansTrajectoires;
            sauveDescriptionChemin(robot,"./resources/partie5/partie5_complexe_robotpath.txt");
            
            while (robot->ptrTete)
            {
                MaillonTraj *tmp = robot->ptrTete->next;
                libereMaillon(&robot->ptrTete);
                robot->ptrTete = tmp;
            }
            free(robot);
            libereImage(&imgAvecTrajectoires);
            free(pal->pal);
            free(pal);
            libereArbreMotif(ptrRcn);
        }
        else{
            trigger_error_callback(parentGui,parentFrame);
            point1P5_complexe = (SDL_Point){last_point1.x,last_point1.y};
            point2P5_complexe = (SDL_Point){last_point2.x,last_point2.y};
            
            libereImage(&imgSansTrajectoires);
            libereImage(&imgAvecTrajectoires);
            free(pal->pal);
            free(pal);
            libereArbreMotif(ptrRcn);
        }
        
    }
}

void affiche_partie5_callback(void *parentGui, void *parentFrame)
{
    while(appState->guis[1])
        removeGui(appState->guis[1]);

    Gui *gui = (Gui*) parentGui;
    if(gui->views[0]->frames[7]->textareas[0]->text)
    {
        free(gui->views[0]->frames[7]->textareas[0]->text);
        gui->views[0]->frames[7]->textareas[0]->text = createString("RESET\n------ LANCEMENT PARTIE 5 ------");
    }
    else
    {
        gui->views[0]->frames[7]->textareas[0]->text = createString("------ LANCEMENT PARTIE 5 ------");
    }
    
    TextArea *textArea = gui->views[0]->frames[7]->textareas[0];
    
    gui->views[0]->frames[2]->visible = SDL_FALSE;
    gui->views[0]->frames[3]->visible = SDL_FALSE;
    gui->views[0]->frames[4]->visible = SDL_FALSE;
    gui->views[0]->frames[5]->visible = SDL_FALSE;
    gui->views[0]->frames[6]->visible = SDL_FALSE;
    gui->views[0]->frames[7]->visible = SDL_TRUE;
    gui->views[0]->frames[8]->visible = SDL_FALSE;
    char textToAdd[500];

    /******* LOAD *******/
    sprintf(textToAdd,"\n> lancement fonction chargeImage\narg1:'%s'","./resources/global_assets/partie4.bmp");
    addText(textArea,textToAdd);
    update();
    Image* imgP4 = chargeImage("./resources/global_assets/partie4.bmp");
    sprintf(textToAdd,"\n> OUTPUT: %p - %s",imgP4,(imgP4==NULL)?"ERREUR":"OK");
    addText(textArea,textToAdd);
    update();

    sprintf(textToAdd,"\n> lancement fonction chargeImage\narg1:'%s'","./resources/global_assets/partie5_simple.bmp");
    addText(textArea,textToAdd);
    update();
    Image* imgP5_simple = chargeImage("./resources/global_assets/partie5_simple.bmp");
    sprintf(textToAdd,"\n> OUTPUT: %p - %s",imgP5_simple,(imgP5_simple==NULL)?"ERREUR":"OK");
    addText(textArea,textToAdd);
    update();

    sprintf(textToAdd,"\n> lancement fonction chargeImage\narg1:'%s'","./resources/global_assets/partie5_complexe.bmp");
    addText(textArea,textToAdd);
    update();
    Image* imgP5_complexe = chargeImage("./resources/global_assets/partie5_complexe.bmp");
    sprintf(textToAdd,"\n> OUTPUT: %p - %s",imgP5_complexe,(imgP5_complexe==NULL)?"ERREUR":"OK");
    addText(textArea,textToAdd);
    update();
    /******* PALETTES *******/
    sprintf(textToAdd,"\n> lancement fonction creePalette\narg1:'%p'",imgP4);
    addText(textArea,textToAdd);
    update();
    Palette* palP4 = creePalette(imgP4);
    sprintf(textToAdd,"\n> OUTPUT: %p - %s",palP4,(palP4==NULL)?"ERREUR":"OK");
    addText(textArea,textToAdd);
    update();

    sprintf(textToAdd,"\n> lancement fonction creePalette\narg1:'%p'",imgP5_simple);
    addText(textArea,textToAdd);
    update();
    Palette* palP5_simple = creePalette(imgP5_simple);
    sprintf(textToAdd,"\n> OUTPUT: %p - %s",palP5_simple,(palP5_simple==NULL)?"ERREUR":"OK");
    addText(textArea,textToAdd);
    update();

    sprintf(textToAdd,"\n> lancement fonction creePalette\narg1:'%p'",imgP5_complexe);
    addText(textArea,textToAdd);
    update();
    Palette* palP5_complexe = creePalette(imgP5_complexe);
    sprintf(textToAdd,"\n> OUTPUT: %p - %s",palP5_complexe,(palP5_complexe==NULL)?"ERREUR":"OK");
    addText(textArea,textToAdd);
    update();
    /******* CreeArbreMotif *******/
    sprintf(textToAdd,"\n> lancement fonction creeArbreMotifs\narg1:'%p' arg2:'%p' arg3:'%d' arg4:'%d' arg5:'%d'",imgP4,palP4,imgP4->largeur,0,0);
    addText(textArea,textToAdd);
    update();
    noeudMotif* ptrRcnP4 = creeArbreMotifs(imgP4,palP4,imgP4->largeur,0,0);
    sprintf(textToAdd,"\n> OUTPUT: %p - %s",ptrRcnP4,(ptrRcnP4==NULL)?"ERREUR":"OK");
    addText(textArea,textToAdd);
    update();

    sprintf(textToAdd,"\n> lancement fonction creeArbreMotifs\narg1:'%p' arg2:'%p' arg3:'%d' arg4:'%d' arg5:'%d'",imgP5_simple,palP5_simple,imgP5_simple->largeur,0,0);
    addText(textArea,textToAdd);
    update();
    noeudMotif* ptrRcnP5_simple = creeArbreMotifs(imgP5_simple,palP5_simple,imgP5_simple->largeur,0,0);
    sprintf(textToAdd,"\n> OUTPUT: %p - %s",ptrRcnP5_simple,(ptrRcnP5_simple==NULL)?"ERREUR":"OK");
    addText(textArea,textToAdd);
    update();

    sprintf(textToAdd,"\n> lancement fonction creeArbreMotifs\narg1:'%p' arg2:'%p' arg3:'%d' arg4:'%d' arg5:'%d'",imgP5_complexe,palP5_complexe,imgP5_complexe->largeur,0,0);
    addText(textArea,textToAdd);
    update();
    noeudMotif* ptrRcnP5_complexe = creeArbreMotifs(imgP5_complexe,palP5_complexe,imgP5_complexe->largeur,0,0);
    sprintf(textToAdd,"\n> OUTPUT: %p - %s",ptrRcnP5_complexe,(ptrRcnP5_complexe==NULL)?"ERREUR":"OK");
    addText(textArea,textToAdd);
    update();


    Image* imgAvecTrajectoiresP4 = alloueImage(imgP4->largeur,imgP4->hauteur);
    Image* imgAvecTrajectoiresP5_simple = alloueImage(imgP5_simple->largeur,imgP5_simple->hauteur);
    Image* imgAvecTrajectoiresP5_complexe = alloueImage(imgP5_complexe->largeur,imgP5_complexe->hauteur);

    /******* dessinecarte *******/
    sprintf(textToAdd,"\n> lancement fonction dessineCarte\narg1:'%p' arg2:'%p' arg3:'%p' arg4:'%d'",imgAvecTrajectoiresP4,palP4,ptrRcnP4,true);
    addText(textArea,textToAdd);
    update();
    dessineCarte(imgAvecTrajectoiresP4,*palP4,ptrRcnP4,true);
    sprintf(textToAdd,"\n> OUTPUT: OK");
    addText(textArea,textToAdd);
    update();
    libereArbreMotif(ptrRcnP4);
    sprintf(textToAdd,"\n> lancement fonction dessineCarte\narg1:'%p' arg2:'%p' arg3:'%p' arg4:'%d'",imgAvecTrajectoiresP5_simple,palP5_simple,ptrRcnP5_simple,true);
    addText(textArea,textToAdd);
    update();
    dessineCarte(imgAvecTrajectoiresP5_simple,*palP5_simple,ptrRcnP5_simple,true);
    sprintf(textToAdd,"\n> OUTPUT: OK");
    addText(textArea,textToAdd);
    update();
    libereArbreMotif(ptrRcnP5_simple);
    sprintf(textToAdd,"\n> lancement fonction dessineCarte\narg1:'%p' arg2:'%p' arg3:'%p' arg4:'%d'",imgAvecTrajectoiresP5_complexe,palP5_complexe,ptrRcnP5_complexe,true);
    addText(textArea,textToAdd);
    update();
    dessineCarte(imgAvecTrajectoiresP5_complexe,*palP5_complexe,ptrRcnP5_complexe,true);
    sprintf(textToAdd,"\n> OUTPUT: OK");
    addText(textArea,textToAdd);
    update();
    libereArbreMotif(ptrRcnP5_complexe);
    /******* sauveImage *******/
    sauveImage(imgAvecTrajectoiresP4, "./resources/partie5/partie4_traj.bmp");
    sauveImage(imgAvecTrajectoiresP5_simple, "./resources/partie5/partie5_simple_traj.bmp");
    sauveImage(imgAvecTrajectoiresP5_complexe, "./resources/partie5/partie5_complexe_traj.bmp");
    /******* affichage *******/

    addGui(
        (Gui_Settings)
        {
            .title = "P5-Annexe 1",
            .x = SDL_WINDOWPOS_CENTERED,
            .y = SDL_WINDOWPOS_CENTERED,
            .w = 256,
            .h = 256,
            .win_flags = 0,
            .ren_flags = SDL_RENDERER_PRESENTVSYNC,
            initViews(1,
                initView(
                    SDL_TRUE,
                    initFrames(
                        1,
                        initFrame(
                            SDL_TRUE,
                            0,
                            0,
                            256,
                            256,
                            (SDL_Color){0,0,0,SDL_ALPHA_OPAQUE},
                            (SDL_Color){0,0,0,SDL_ALPHA_OPAQUE},
                            0,
                            initDisplaySurfaces(
                                1,
                                initDisplaySurface(
                                    SDL_TRUE,
                                    SDL_FALSE,
                                    0,
                                    0,
                                    128,
                                    128,
                                    chargeImage("./resources/partie5/partie4_traj.bmp"),
                                    NULL
                                )
                            ),
                            NULL,
                            NULL
                        )
                    )
                )
            )
        }
    );

    addGui(
        (Gui_Settings)
        {
            .title = "P5-Annexe 2",
            .x = SDL_WINDOWPOS_CENTERED,
            .y = SDL_WINDOWPOS_CENTERED,
            .w = 512,
            .h = 512,
            .win_flags = 0,
            .ren_flags = SDL_RENDERER_PRESENTVSYNC,
            initViews(1,
                initView(
                    SDL_TRUE,
                    initFrames(
                        1,
                        initFrame(
                            SDL_TRUE,
                            0,
                            0,
                            512,
                            512,
                            (SDL_Color){0,0,0,SDL_ALPHA_OPAQUE},
                            (SDL_Color){0,0,0,SDL_ALPHA_OPAQUE},
                            0,
                            initDisplaySurfaces(
                                1,
                                initDisplaySurface(
                                    SDL_TRUE,
                                    SDL_FALSE,
                                    0,
                                    0,
                                    512,
                                    512,
                                    chargeImage("./resources/partie5/partie5_simple_traj.bmp"),
                                    NULL
                                )
                            ),
                            NULL,
                            NULL
                        )
                    )
                )
            )
        }
    );

    addGui(
        (Gui_Settings)
        {
            .title = "P5-Annexe 3",
            .x = SDL_WINDOWPOS_CENTERED,
            .y = SDL_WINDOWPOS_CENTERED,
            .w = 512,
            .h = 512,
            .win_flags = 0,
            .ren_flags = SDL_RENDERER_PRESENTVSYNC,
            initViews(1,
                initView(
                    SDL_TRUE,
                    initFrames(
                        1,
                        initFrame(
                            SDL_TRUE,
                            0,
                            0,
                            512,
                            512,
                            (SDL_Color){0,0,0,SDL_ALPHA_OPAQUE},
                            (SDL_Color){0,0,0,SDL_ALPHA_OPAQUE},
                            0,
                            initDisplaySurfaces(
                                1,
                                initDisplaySurface(
                                    SDL_TRUE,
                                    SDL_FALSE,
                                    0,
                                    0,
                                    512,
                                    512,
                                    chargeImage("./resources/partie5/partie5_complexe_traj.bmp"),
                                    NULL
                                )
                            ),
                            NULL,
                            NULL
                        )
                    )
                )
            )
        }
    );

    /******* calcule trajectoire *******/
    sprintf(textToAdd,"\n> lancement fonction calculeTrajectoire\narg1:'%p' arg2:'%p' arg3:'%p' arg4:'%d %d' arg5:'%d %d'",   imgAvecTrajectoiresP4,
                                                                                                                                            imgP4,
                                                                                                                                            palP4,
                                                                                                                                            67,15,19,68);
    addText(textArea,textToAdd);
    Robot *robotP4 = calculeTrajectoire(imgAvecTrajectoiresP4,imgP4,  palP4,(Point){67,15}, (Point){19,68});
    sprintf(textToAdd,"\n> OUTPUT: %p - %s",robotP4,(robotP4==NULL)?"ERREUR":"OK");
    addText(textArea,textToAdd);
    update();

    if(robotP4 == NULL)
    {
        free(palP4->pal);
        free(palP4);
        free(palP5_simple->pal);
        free(palP5_simple);
        free(palP5_complexe->pal);
        free(palP5_complexe);
        libereImage(&imgAvecTrajectoiresP4);
        libereImage(&imgP4);
        libereImage(&imgAvecTrajectoiresP5_simple);
        libereImage(&imgP5_simple);
        libereImage(&imgAvecTrajectoiresP5_complexe);
        libereImage(&imgP5_complexe);
        return;
    }

    sprintf(textToAdd,"\n> lancement fonction calculeTrajectoire\narg1:'%p' arg2:'%p' arg3:'%p' arg4:'%d %d' arg5:'%d %d'",   imgAvecTrajectoiresP5_simple,
                                                                                                                                            imgP5_simple,
                                                                                                                                            palP5_simple,
                                                                                                                                            95,70,501,503);
    addText(textArea,textToAdd);
    Robot *robotP5_simple = calculeTrajectoire(imgAvecTrajectoiresP5_simple,imgP5_simple,  palP5_simple,(Point){95,70}, (Point){501,503});
    sprintf(textToAdd,"\n> OUTPUT: %p - %s",robotP5_simple,(robotP5_simple==NULL)?"ERREUR":"OK");
    addText(textArea,textToAdd);
    update();

    if(robotP5_simple == NULL)
    {
        free(palP4->pal);
        free(palP4);
        free(palP5_simple->pal);
        free(palP5_simple);
        free(palP5_complexe->pal);
        free(palP5_complexe);
        libereImage(&imgAvecTrajectoiresP4);
        libereImage(&imgP4);
        libereImage(&imgAvecTrajectoiresP5_simple);
        libereImage(&imgP5_simple);
        libereImage(&imgAvecTrajectoiresP5_complexe);
        libereImage(&imgP5_complexe);
        return;
    }

    sprintf(textToAdd,"\n> lancement fonction calculeTrajectoire\narg1:'%p' arg2:'%p' arg3:'%p' arg4:'%d %d' arg5:'%d %d'",   imgAvecTrajectoiresP5_complexe,
                                                                                                                                            imgP5_complexe,
                                                                                                                                            palP5_complexe,
                                                                                                                                            95,70,501,503);
    addText(textArea,textToAdd);
    Robot *robotP5_complexe = calculeTrajectoire(imgAvecTrajectoiresP5_complexe,imgP5_complexe,  palP5_complexe,(Point){95,70}, (Point){501,503});
    sprintf(textToAdd,"\n> OUTPUT: %p - %s",robotP5_complexe,(robotP5_complexe==NULL)?"ERREUR":"OK");
    addText(textArea,textToAdd);
    update();

    if(robotP5_complexe == NULL)
    {
        free(palP4->pal);
        free(palP4);
        free(palP5_simple->pal);
        free(palP5_simple);
        free(palP5_complexe->pal);
        free(palP5_complexe);
        libereImage(&imgAvecTrajectoiresP4);
        libereImage(&imgP4);
        libereImage(&imgAvecTrajectoiresP5_simple);
        libereImage(&imgP5_simple);
        libereImage(&imgAvecTrajectoiresP5_complexe);
        libereImage(&imgP5_complexe);
        return;
    }

    /******* sauveDescriptionChemin *******/
    
    sprintf(textToAdd,"\n> lancement fonction sauveDescriptionChemin\narg1:'%p %s'",robotP4,"./resources/partie5/partie4_robotpath.txt");
    addText(textArea,textToAdd);
    update();
    sauveDescriptionChemin(robotP4,"./resources/partie5/partie4_robotpath.txt");

    sprintf(textToAdd,"\n> lancement fonction sauveDescriptionChemin\narg1:'%p %s'",robotP5_simple,"./resources/partie5/partie5_simple_robotpath.txt");
    addText(textArea,textToAdd);
    update();
    sauveDescriptionChemin(robotP5_simple,"./resources/partie5/partie5_simple_robotpath.txt");

    sprintf(textToAdd,"\n> lancement fonction sauveDescriptionChemin\narg1:'%p %s'",robotP5_complexe,"./resources/partie5/partie5_complexe_robotpath.txt");
    addText(textArea,textToAdd);
    update();
    sauveDescriptionChemin(robotP5_complexe,"./resources/partie5/partie5_complexe_robotpath.txt");

    free(palP4->pal);
    free(palP4);
    free(palP5_simple->pal);
    free(palP5_simple);
    free(palP5_complexe->pal);
    free(palP5_complexe);
    libereImage(&imgAvecTrajectoiresP4);
    libereImage(&imgP4);
    libereImage(&imgAvecTrajectoiresP5_simple);
    libereImage(&imgP5_simple);
    libereImage(&imgAvecTrajectoiresP5_complexe);
    libereImage(&imgP5_complexe);
    
    sprintf(textToAdd,"\n> Main return 0 - OK");
    addText(textArea,textToAdd);

    Image *imgAnnexe4 = chargeImage("./resources/global_assets/partie4.bmp");
    addGui(
        (Gui_Settings)
        {
            .title = "P5-Annexe 4",
            .x = SDL_WINDOWPOS_CENTERED,
            .y = SDL_WINDOWPOS_CENTERED,
            .w = 256,
            .h = 256,
            .win_flags = 0,
            .ren_flags = SDL_RENDERER_PRESENTVSYNC,
            initViews(1,
                initView(
                    SDL_TRUE,
                    initFrames(
                        1,
                        initFrame(
                            SDL_TRUE,
                            0,
                            0,
                            256,
                            256,
                            (SDL_Color){0,0,0,SDL_ALPHA_OPAQUE},
                            (SDL_Color){0,0,0,SDL_ALPHA_OPAQUE},
                            0,
                            initDisplaySurfaces(
                                1,
                                initDisplaySurface(
                                    SDL_TRUE,
                                    SDL_FALSE,
                                    0,
                                    0,
                                    128,
                                    128,
                                    imgAnnexe4,
                                    modfify_partie5_imgP4_callback
                                )
                            ),
                            NULL,
                            NULL
                        )
                    )
                )
            )
        }
    );

    Image *imgAnnexe5 = chargeImage("./resources/global_assets/partie5_simple.bmp");
    addGui(
        (Gui_Settings)
        {
            .title = "P5-Annexe 5",
            .x = SDL_WINDOWPOS_CENTERED,
            .y = SDL_WINDOWPOS_CENTERED,
            .w = 512,
            .h = 512,
            .win_flags = 0,
            .ren_flags = SDL_RENDERER_PRESENTVSYNC,
            initViews(1,
                initView(
                    SDL_TRUE,
                    initFrames(
                        1,
                        initFrame(
                            SDL_TRUE,
                            0,
                            0,
                            512,
                            512,
                            (SDL_Color){0,0,0,SDL_ALPHA_OPAQUE},
                            (SDL_Color){0,0,0,SDL_ALPHA_OPAQUE},
                            0,
                            initDisplaySurfaces(
                                1,
                                initDisplaySurface(
                                    SDL_TRUE,
                                    SDL_FALSE,
                                    0,
                                    0,
                                    512,
                                    512,
                                    imgAnnexe5,
                                    modfify_partie5_imgP5_simple_callback
                                )
                            ),
                            NULL,
                            NULL
                        )
                    )
                )
            )
        }
    );

    Image *imgAnnexe6 = chargeImage("./resources/global_assets/partie5_complexe.bmp");
    addGui(
        (Gui_Settings)
        {
            .title = "P5-Annexe 6",
            .x = SDL_WINDOWPOS_CENTERED,
            .y = SDL_WINDOWPOS_CENTERED,
            .w = 512,
            .h = 512,
            .win_flags = 0,
            .ren_flags = SDL_RENDERER_PRESENTVSYNC,
            initViews(1,
                initView(
                    SDL_TRUE,
                    initFrames(
                        1,
                        initFrame(
                            SDL_TRUE,
                            0,
                            0,
                            512,
                            512,
                            (SDL_Color){0,0,0,SDL_ALPHA_OPAQUE},
                            (SDL_Color){0,0,0,SDL_ALPHA_OPAQUE},
                            0,
                            initDisplaySurfaces(
                                1,
                                initDisplaySurface(
                                    SDL_TRUE,
                                    SDL_FALSE,
                                    0,
                                    0,
                                    512,
                                    512,
                                    imgAnnexe6,
                                    modfify_partie5_imgP5_complexe_callback
                                )
                            ),
                            NULL,
                            NULL
                        )
                    )
                )
            )
        }
    );
    
    MaillonTraj *currentP4 = robotP4->ptrTete;
    while (currentP4)
    {
        imgAnnexe4->rouge[currentP4->position.x][currentP4->position.y] = 0;
        imgAnnexe4->vert[currentP4->position.x][currentP4->position.y] = 0;
        imgAnnexe4->bleu[currentP4->position.x][currentP4->position.y] = 0;
        currentP4=currentP4->next;
    }
    sauveImage(imgAnnexe4,"./resources/partie5/partie4_robotpath.bmp");
    MaillonTraj *currentP5_simple = robotP5_simple->ptrTete;
    while (currentP5_simple)
    {
        imgAnnexe5->rouge[currentP5_simple->position.x][currentP5_simple->position.y] = 0;
        imgAnnexe5->vert[currentP5_simple->position.x][currentP5_simple->position.y] = 0;
        imgAnnexe5->bleu[currentP5_simple->position.x][currentP5_simple->position.y] = 0;
        currentP5_simple=currentP5_simple->next;
    }
    sauveImage(imgAnnexe5,"./resources/partie5/partie5_simple_robotpath.bmp");
    MaillonTraj *currentP5_complexe = robotP5_complexe->ptrTete;
    while (currentP5_complexe)
    {
        imgAnnexe6->rouge[currentP5_complexe->position.x][currentP5_complexe->position.y] = 0;
        imgAnnexe6->vert[currentP5_complexe->position.x][currentP5_complexe->position.y] = 0;
        imgAnnexe6->bleu[currentP5_complexe->position.x][currentP5_complexe->position.y] = 0;
        currentP5_complexe=currentP5_complexe->next;
    }
    sauveImage(imgAnnexe6,"./resources/partie5/partie5_complexe_robotpath.bmp");

    while (robotP4->ptrTete)
    {
        MaillonTraj *tmp = robotP4->ptrTete->next;
        libereMaillon(&robotP4->ptrTete);
        robotP4->ptrTete = tmp;
    }
    free(robotP4);

    while (robotP5_simple->ptrTete)
    {
        MaillonTraj *tmp = robotP5_simple->ptrTete->next;
        libereMaillon(&robotP5_simple->ptrTete);
        robotP5_simple->ptrTete = tmp;
    }
    free(robotP5_simple);

    while (robotP5_complexe->ptrTete)
    {
        MaillonTraj *tmp = robotP5_complexe->ptrTete->next;
        libereMaillon(&robotP5_complexe->ptrTete);
        robotP5_complexe->ptrTete = tmp;
    }
    free(robotP5_complexe);
}


void affiche_partie6_callback(void *parentGui, void *parentFrame)
{
    while(appState->guis[1])
        removeGui(appState->guis[1]);

    Gui *gui = (Gui*) parentGui;
    if(gui->views[0]->frames[8]->textareas[0]->text)
    {
        free(gui->views[0]->frames[8]->textareas[0]->text);
        gui->views[0]->frames[8]->textareas[0]->text = createString("RESET\n------ LANCEMENT PARTIE 6 ------");
    }
    else
    {
        gui->views[0]->frames[8]->textareas[0]->text = createString("------ LANCEMENT PARTIE 6 ------");
    }
    
    TextArea *textArea = gui->views[0]->frames[8]->textareas[0];
    
    gui->views[0]->frames[2]->visible = SDL_FALSE;
    gui->views[0]->frames[3]->visible = SDL_FALSE;
    gui->views[0]->frames[4]->visible = SDL_FALSE;
    gui->views[0]->frames[5]->visible = SDL_FALSE;
    gui->views[0]->frames[6]->visible = SDL_FALSE;
    gui->views[0]->frames[7]->visible = SDL_FALSE;
    gui->views[0]->frames[8]->visible = SDL_TRUE;
    char textToAdd[500];

    sprintf(textToAdd,"\n> lancement fonction compteOccurrenceMotif\narg1:'%s'","./resources/global_assets/description_image.txt");
    addText(textArea,textToAdd);
    update();
    TabSymbOcc *tabSymbOcc = compteOccurrenceMotif("./resources/global_assets/description_image.txt");
    sprintf(textToAdd,"\n> OUTPUT: %p - %s",tabSymbOcc,(tabSymbOcc==NULL)?"ERREUR":"OK");
    addText(textArea,textToAdd);
    update();
    
    if(!tabSymbOcc) return;

    sprintf(textToAdd,"\n> lancement fonction creeArbreCodeOcc\narg1:'%p'",tabSymbOcc);
    addText(textArea,textToAdd);
    update();
    NoeudCode *racine = creeArbreCodeOcc(tabSymbOcc);
    sprintf(textToAdd,"\n> OUTPUT: %p - %s",racine,(racine==NULL)?"ERREUR":"OK");
    addText(textArea,textToAdd);
    update();

    if(!racine) return;

    sprintf(textToAdd,"\n> lancement fonction creeCodeAdaptatif\narg1:'%p'",racine);
    addText(textArea,textToAdd);
    update();
    TabSymbCode *tabSymbCode = creeCodeAdaptatif(racine);
    sprintf(textToAdd,"\n> OUTPUT: %p - %s",tabSymbCode,(tabSymbCode==NULL)?"ERREUR":"OK");
    addText(textArea,textToAdd);
    update();
    
    if(!tabSymbCode) return;

    sprintf(textToAdd,"\n> lancement fonction sauveCodeAdaptatif\narg1:'%s' \narg2;'%p'","./resources/partie6/codeAdaptatif.txt",tabSymbCode);
    addText(textArea,textToAdd);
    update();
    sauveCodeAdaptatif("./resources/partie6/codeAdaptatif.txt",tabSymbCode);

    FILE * fp;
    char * line = NULL;
    size_t len = 0;
    ssize_t read;
    
    fp = fopen("./resources/partie6/codeAdaptatif.txt","r");
    if (fp == NULL)
        return;

    while ((read = getline(&line, &len, fp)) != -1) {
        sprintf(textToAdd,"\n%s",line);
        addText(textArea,textToAdd);
        update();
    }

    fclose(fp);
    if (line)
        free(line);

    sprintf(textToAdd,"\n> lancement fonction litCodeAdaptatif\narg1:'%s'","./resources/partie6/codeAdaptatif.txt");
    addText(textArea,textToAdd);
    update();
    TabSymbCode* tabSymbCodeLoad = litCodeAdaptatif("./resources/partie6/codeAdaptatif.txt");
    sprintf(textToAdd,"\n> OUTPUT: %p - %s",tabSymbCodeLoad,(tabSymbCodeLoad==NULL)?"ERREUR":"OK");
    addText(textArea,textToAdd);
    update();

    if(!tabSymbCodeLoad) return;

    sprintf(textToAdd,"\n> lancement fonction creeArbreCodeAdaptatif\narg1:'%p'",tabSymbCodeLoad);
    addText(textArea,textToAdd);
    update();
    NoeudCode* racineLoad = creeArbreCodeAdaptatif(tabSymbCodeLoad);
    sprintf(textToAdd,"\n> OUTPUT: %p - %s",racineLoad,(racineLoad==NULL)?"ERREUR":"OK");
    addText(textArea,textToAdd);
    update();

    if(!racineLoad) return;

    sprintf(textToAdd,"\n> lancement fonction codeFichier\narg1:'%s' \narg2:'%s' \narg3:'%p'","./resources/global_assets/description_image.txt","./resources/partie6/codeAdaptatifCode.txt",tabSymbCode);
    addText(textArea,textToAdd);
    update();
    codeFichier("./resources/global_assets/description_image.txt","./resources/partie6/codeAdaptatifCode.txt",tabSymbCode);

    read = 0;
    len = 0;
    line = NULL;
    fp = fopen("./resources/partie6/codeAdaptatifCode.txt","r");
    if (fp == NULL)
        return;

    while ((read = getline(&line, &len, fp)) != -1) {
        sprintf(textToAdd,"\n%s",line);
        addText(textArea,textToAdd);
        update();
    }

    fclose(fp);
    if (line)
        free(line);

    sprintf(textToAdd,"\n> lancement fonction decodeFichier\narg1:'%s' \narg2:'%s' \narg3:'%p'","./resources/partie6/codeAdaptatifCode.txt","./resources/partie6/codeAdaptatifDecode.txt",tabSymbCode);
    addText(textArea,textToAdd);
    update();
    decodeFichier("./resources/partie6/codeAdaptatifCode.txt","./resources/partie6/codeAdaptatifDecode.txt",tabSymbCode);

    sprintf(textToAdd,"\n> lancement fonction transcritTexteEnBinaire\narg1:'%s' \narg2:'%s'","./resources/partie6/codeAdaptatifCode.txt","./resources/partie6/codeAdaptatifCodeBinaire.txt");
    addText(textArea,textToAdd);
    update();
    transcritTexteEnBinaire("./resources/partie6/codeAdaptatifCode.txt", "./resources/partie6/codeAdaptatifCodeBinaire.bin");

    sprintf(textToAdd,"\n> lancement fonction transcritBinaireEnTexte\narg1:'%s' \narg2:'%s'","./resources/partie6/codeAdaptatifCodeBinaire.txt","./resources/partie6/codeAdaptatifCodeBinaireATexte.txt");
    addText(textArea,textToAdd);
    update();
    transcritBinaireEnTexte("./resources/partie6/codeAdaptatifCodeBinaire.bin","./resources/partie6/codeAdaptatifCodeBinaireATexte.txt");

    sprintf(textToAdd,"\n> lancement fonction decodeFichier\narg1:'%s' \narg2:'%s' \narg3:'%p'","./resources/partie6/codeAdaptatifCodeBinaireATexte.txt","./resources/partie6/codeAdaptatifBinaireATexteDecode.txt",tabSymbCode);
    addText(textArea,textToAdd);
    update();
    decodeFichier("./resources/partie6/codeAdaptatifCodeBinaireATexte.txt","./resources/partie6/codeAdaptatifBinaireATexteDecode.txt",tabSymbCode);

    read = 0;
    len = 0;
    line = NULL;
    fp = fopen("./resources/partie6/codeAdaptatifBinaireATexteDecode.txt","r");
    if (fp == NULL)
        return;

    while ((read = getline(&line, &len, fp)) != -1) {
        sprintf(textToAdd,"\n%s",line);
        addText(textArea,textToAdd);
        update();
    }

    fclose(fp);
    if (line)
        free(line);
    
    Image *imgExemple = chargeImage("./resources/global_assets/partie3.bmp");

    if(!imgExemple) return;

    noeudBinaire* rcn = creeArbreNB(0,0,imgExemple->largeur,imgExemple->gris);
    
    if(!rcn) return;

    sprintf(textToAdd,"\n> lancement fonction sauveArbreNB_Texte\narg1:'%p'",rcn);
    addText(textArea,textToAdd);
    update();
    sauveArbreNB_Texte(rcn);

    libereArbreCodeAdaptatif(racine);
    libereArbreCodeAdaptatif(racineLoad);
    libereArbreBinaire(rcn);
    libereImage(&imgExemple);
    free(tabSymbOcc->tab);
    free(tabSymbOcc);
    free(tabSymbCode->tab);
    free(tabSymbCode);
    free(tabSymbCodeLoad->tab);
    free(tabSymbCodeLoad);

    sprintf(textToAdd,"\n> Main return 0 - OK");
    addText(textArea,textToAdd);
}
Button* initButton(int x, int y, int width, int heigth, int outlineWidth, char *text, char* fontName, int fontSize, SDL_Color textColor, SDL_Color bgColor, SDL_Color outlineColor, SDL_bool visible, void (*callback)(void *parentGui, void *parentFrame)){
    Button *button = (Button*) malloc(sizeof(Button));

    button->visible = visible;
    button->disabled = SDL_FALSE;
    button->x=x;
    button->y=y;
    button->text = text;
    button->textColor = textColor;
    button->fontName = fontName;
    button->fontSize = fontSize;
    button->width=width;
    button->heigth=heigth;
    button->outlineWidth=outlineWidth;
    button->bgColor=bgColor;
    button->outlineColor=outlineColor;
    button->callback = callback;

    return button;
}

/**
 * @brief Args must be Button*
 * 
 * @param nbreArguments 
 * @param ... 
 * @return Button** 
 */
Button** initButtons(int nbreArguments, ...){
    va_list ap;
    va_start (ap, nbreArguments);

    Button **buttons = (Button**) malloc(sizeof(Button*)*(nbreArguments+1));

    for (int i = 0; i < nbreArguments; i++)
        buttons[i] = va_arg (ap, Button*);
    va_end (ap);
    
    buttons[nbreArguments] = NULL;

    return buttons;
}

SDL_bool isOverButton(Button *button, void *parent)
{
    if(appState->event.button.x >= ((Frame*) parent)->x+button->x && appState->event.button.x <= ((Frame*) parent)->x+button->x+button->width && appState->event.button.y >= ((Frame*) parent)->y+button->y && appState->event.button.y <= ((Frame*) parent)->y+button->y+button->heigth)
        return SDL_TRUE;
    else
        return SDL_FALSE;
}

void freeButton(Button *button)
{
    free(button);
}

void freeButtons(Button **buttons)
{
    for (int i = 0; buttons[i]; i++)
        freeButton(buttons[i]);
    free(buttons);
    
}