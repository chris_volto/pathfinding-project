#ifndef _VOLTO_VIEW_H
#define _VOLTO_VIEW_H

    #include "Frame.h"
    #include <SDL.h>

    typedef struct View{
        SDL_bool visible;
        
        Frame **frames;
    }View;

    View* initView(SDL_bool visible, Frame **frames);
    View** initViews(int nbreArguments, ...);
    void freeView(View *view);
    void freeViews(View **views);

#endif
