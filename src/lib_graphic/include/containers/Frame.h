#ifndef _VOLTO_FRAME_H
#define _VOLTO_FRAME_H

#include "../widgets/DisplaySurface.h"
#include "../widgets/Button.h"
#include "../widgets/TextArea.h"
#include <SDL.h>

typedef struct Frame{
    SDL_bool visible;
    int x,y,w,h,outlineWidth;
    SDL_Color bgColor, outlineColor;

    //WIDGETS
    DisplaySurface **displaySurfaces;
    Button **buttons;
    TextArea **textareas;
}Frame;

Frame* initFrame(SDL_bool visible,int x, int y,int w, int h, SDL_Color bgColor, SDL_Color outlineColor, int outlineWidth, DisplaySurface **displaySurfaces, Button **buttons, TextArea **textareas);
Frame ** initFrames(int nbreArguments, ...);
void freeFrame(Frame *frame);
void freeFrames(Frame **frames);

#endif