#ifndef _VOLTO_APPSTATE_H
#define _VOLTO_APPSTATE_H

    #include "Gui.h"
    #include <SDL.h>

    typedef struct AppState{
        SDL_bool program_running;

        SDL_Event event;
        
        Gui **guis;
    }AppState;

#endif