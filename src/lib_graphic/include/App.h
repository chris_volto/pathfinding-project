#ifndef _VOLTO_APP_H
#define _VOLTO_APP_H

    #define WINDOW_W 900 
    #define WINDOW_H 900

    void initialization(int nbreArgs, ...);
    void update(void);
    void eventHandler(void);
    void cleanEverything(void);
    void SDL_ExitWithError(const char* message);

#endif