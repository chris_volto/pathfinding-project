#ifndef _VOLTO_BUTTON_H
#define _VOLTO_BUTTON_H

#include <SDL.h>

typedef struct Button{
    SDL_bool visible;
    SDL_bool disabled;

    int x,y,width,heigth,outlineWidth;

    char *text, *fontName;
    int fontSize;


    SDL_Color bgColor, outlineColor, textColor;

    void (*callback)(void *parentGui, void *parentFrame);
}Button;

//Callbacks
void simple_callback(void *parentGui, void *parentFrame);
//void modify_image_callback_button(void* parent);
void trigger_error_callback(void *parentGui, void *parentFrame);

void affiche_partie1_callback(void *parentGui, void *parentFrame);
void affiche_partie2_callback(void *parentGui, void *parentFrame);
void affiche_partie3_callback(void *parentGui, void *parentFrame);
void affiche_partie4_callback(void *parentGui, void *parentFrame);
void affiche_partie5_callback(void *parentGui, void *parentFrame);
void modfify_partie5_callback(void *parentGui, void *parentFrame);
void affiche_partie6_callback(void *parentGui, void *parentFrame);

SDL_bool isOverButton(Button *button, void *parent);
Button* initButton(int x, int y, int width, int heigth, int outlineWidth, char *text, char* fontName, int fontSize, SDL_Color textColor, SDL_Color bgColor, SDL_Color outlineColor, SDL_bool visible, void (*callback)(void *parentGui, void *parentFrame));
Button** initButtons(int nbreArguments, ...);
void freeButton(Button *button);
void freeButtons(Button **buttons);
#endif