#ifndef _VOLTO_TEXTAREA_H
#define _VOLTO_TEXTAREA_H

    #include <SDL.h>

    typedef struct TextArea{
        SDL_bool visible;

        int x,y,width,heigth,outlineWidth, scrollPosition;

        char *text, *fontName;
        int fontSize;


        SDL_Color bgColor, outlineColor, textColor;
    }TextArea;

    TextArea* initTextArea(SDL_bool visible, int x, int y, int width, int heigth, int outlineWidth, char *text, char *fontName, int fontSize, SDL_Color bgColor, SDL_Color outlineColor, SDL_Color textColor);
    TextArea** initTextAreas(int nbreArguments, ...);
    void freeTextArea(TextArea *textArea);
    void freeTextAreas(TextArea **textAreas);
    char* createString(char *string);
    char* extractSubString(int index1, int index2, char* string);
    char* myStrCat(char* str1, char* str2);
    void addText(TextArea *textArea, char *textToAdd);

#endif
