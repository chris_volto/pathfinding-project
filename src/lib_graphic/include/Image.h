#ifndef _VOLTO_MINIPROJET_PART2_H_
    #define _VOLTO_MINIPROJET_PART2_H_
    
    //Contient une image BMP 24pp
    typedef struct Image {
        int largeur, hauteur;

        short int **rouge, **vert, **bleu, **gris;
    } Image;
    
    /**
     * @brief Alloue une struct Image à partir de la largeur et hauteur
     * 
     * @param largeur 
     * @param hauteur 
     * @return Image* 
     */
    Image *alloueImage(int largeur, int hauteur);

    /**
     * @brief Libère une struct Image allouée
     * 
     * @param ptrImage 
     */
    void libereImage(Image **ptrImage);

    /**
     * @brief Charge une image bmp dans une struct Image allouée
     * 
     * @param nom 
     * @return Image* 
     */
    Image *chargeImage(char *nom);
    /**
     * @brief Sauve une struct Image
     * 
     * @param monImage 
     * @param nom 
     */
    void sauveImage(Image *monImage, char *nom);
    /**
     * @brief Sauve une struct Image en noir et blanc
     * 
     * @param monImage 
     * @param nom 
     */
    void sauveImageNG(Image *monImage, char *nom);
    /**
     * @brief duplique une structure Image
     * 
     * @param monImage 
     * @return Image* 
     */
    Image *dupliqueImage(Image *monImage);
    /**
     * @brief Fais la différence des couleurs rgb sur chaque pixel
     * 
     * @param image1 
     * @param image2 
     * @return Image* 
     */
    Image *differenceImage(Image *image1, Image *image2);

#endif