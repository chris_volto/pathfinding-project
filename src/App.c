/*
    GNU/Linux et MacOS
        > gcc main.c $(sdl2-config --cflags --libs) -o prog
        > gcc *.c $(sdl2-config --cflags --libs) -o prog
    Windows
        > gcc src/main.c -o bin/prog -I include -L lib -lmingw32 -lSDL2main -lSDL2
        > gcc src/main.c -o bin/prog -I include -L lib -lmingw32 -lSDL2main -lSDL2 -mwindows
*/
#include "./lib_graphic/include/App.h"
#include "./lib_graphic/include/AppState.h"
#include "./lib_graphic/include/widgets/Button.h"
#include "./lib_graphic/include/widgets/DisplaySurface.h"
#include <SDL_ttf.h>
#include <string.h>
#include <stdlib.h>

AppState *appState;

static SDL_bool SDL_Started = SDL_FALSE;
static SDL_bool TTF_STARTED = SDL_FALSE;

void cleanEverything(void)
{
    if(appState)
    {
        freeGuis(appState->guis);
        free(appState);
    }
    
    if(TTF_STARTED)
        TTF_Quit();
    if(SDL_Started)
        SDL_Quit();
}

void SDL_ExitWithError(const char* message)
{
    SDL_Log("ERREUR : %s > %s\n", message, SDL_GetError());
    cleanEverything();
    exit(EXIT_FAILURE);
}

void TTF_ExitWithError(const char* message)
{
    SDL_Log("ERREUR : %s > %s\n", message, TTF_GetError());
    cleanEverything();
    exit(EXIT_FAILURE);
}

void initialization(int nbreArgs, ...)
{
    //Lancement SDL
    if(SDL_Init(SDL_INIT_VIDEO) != 0) SDL_ExitWithError("Impossible d'init SDL");
    SDL_Started = SDL_TRUE;

    //Lancement TTF
    if(TTF_Init() != 0) TTF_ExitWithError("Impossible d'init TTF");
    TTF_STARTED = SDL_TRUE;

    appState = (AppState*) malloc(sizeof(AppState));
    if(appState == NULL) SDL_ExitWithError("Impossible de malloc appState");
    
    appState->guis = NULL;

    Gui **guis = (Gui**) malloc(sizeof(Gui*)*(nbreArgs+1));
    if(guis == NULL) SDL_ExitWithError("Impossible de malloc gui**");
    for(int i = 0; i <= nbreArgs; i++) guis[i] = NULL;
    appState->guis = guis;

    va_list ap;
    va_start (ap, nbreArgs);
    //Création fenêtre 
    for(int i = 0; i < nbreArgs; i++)
    {
        guis[i] = initGui(va_arg (ap, Gui_Settings));
        if(guis[i] == NULL) SDL_ExitWithError("Impossible d'init un gui");

        if(i==0) guis[i]->visible = SDL_TRUE;
    }
    va_end (ap);
    appState->program_running = SDL_TRUE;
}

void update(void)
{
    if(appState->guis)
    {
        for (int i = 0; appState->guis[i]; i++)
        {
            Gui* gui = appState->guis[i];
            if(gui->visible && gui->views)
            {
                for (int j = 0; gui->views[j] != NULL; j++)
                {
                    View *view = gui->views[j];
                    if(view->visible && view->frames)
                    {
                        for (int k = 0; view->frames[k] != NULL; k++)
                        {
                            Frame *frame = view->frames[k];
                            if(frame->visible)
                            {
                                SDL_Color outlineColor = frame->outlineColor;
                                SDL_Color bgColor = frame->bgColor;

                                if(SDL_SetRenderDrawBlendMode(gui->renderer, SDL_BLENDMODE_BLEND) != 0)
                                    SDL_ExitWithError("Impossible de charger la couleur de blend");

                                if(SDL_SetRenderDrawColor(gui->renderer,outlineColor.r,outlineColor.g,outlineColor.b,outlineColor.a) != 0)
                                    SDL_ExitWithError("Impossible de charger la couleur de rendu");

                                SDL_Rect rectFrame = {frame->x,frame->y,frame->w,frame->h};

                                if(SDL_RenderFillRect(gui->renderer,&rectFrame) != 0)
                                    SDL_ExitWithError("Impossible de dessiner.");

                                if(SDL_SetRenderDrawColor(gui->renderer,bgColor.r,bgColor.g,bgColor.b,bgColor.a) != 0)
                                    SDL_ExitWithError("Impossible de charger la couleur de rendu");
                                
                                SDL_Rect outlineFrame = { frame->x+frame->outlineWidth,
                                                                frame->y+frame->outlineWidth,
                                                                frame->w-2*frame->outlineWidth,
                                                                frame->h-2*frame->outlineWidth};

                                if(SDL_RenderFillRect(gui->renderer,&outlineFrame) != 0)
                                    SDL_ExitWithError("Impossible de dessiner.");
                                if(frame->displaySurfaces)
                                {
                                    for(int l = 0; frame->displaySurfaces[l]; l++)
                                    {
                                        DisplaySurface *displaySurface = frame->displaySurfaces[l];
                                        if(displaySurface->visible && displaySurface->bgImage)
                                        {
                                            Image *bgImage = displaySurface->bgImage;

                                            Uint32 rmask, gmask, bmask, amask;
                                            int shift = 8;
                                            rmask = 0xff000000 >> shift;
                                            gmask = 0x00ff0000 >> shift;
                                            bmask = 0x0000ff00 >> shift;
                                            amask = 0x000000ff >> shift;
                                            int depth, pitch;
                                            depth = 24;
                                            pitch = 3*bgImage->largeur; // 3 bytes per pixel * pixels per row
                                            
                                            unsigned char * datas = (unsigned char *)malloc((unsigned int)bgImage->largeur*(unsigned int)bgImage->hauteur*3*sizeof(unsigned char));

                                            int i = 0;
                                            for(int y = bgImage->hauteur-1; y >=0; y--){
                                                for(int x = 0; x < bgImage->largeur; x++){
                                                    datas[i] = bgImage->bleu[x][y];
                                                    datas[i+1] = bgImage->vert[x][y];
                                                    datas[i+2] = bgImage->rouge[x][y];
                                                    i +=3;
                                                }
                                            }

                                            SDL_Surface* surf = SDL_CreateRGBSurfaceFrom((void*)datas, bgImage->largeur, bgImage->hauteur, depth, pitch,
                                                            rmask, gmask, bmask, amask);

                                            if(surf == NULL) SDL_ExitWithError("Impossible de creer la surface");

                                            SDL_Texture *texture = SDL_CreateTextureFromSurface(gui->renderer,surf);
                                            SDL_FreeSurface(surf);

                                            if(texture == NULL) SDL_ExitWithError("Impossible de creer la texture");

                                            SDL_Rect rect = (SDL_Rect){ .x=frame->x+displaySurface->x,
                                                                        .y=frame->y+displaySurface->y,
                                                                        .w=displaySurface->w,
                                                                        .h=displaySurface->h};
                                            if(SDL_QueryTexture(texture,NULL,NULL,&rect.w,&rect.h) !=0){
                                                SDL_DestroyTexture(texture);
                                                SDL_ExitWithError("Impossible de charger la texture");
                                            }

                                            if(SDL_RenderCopy(gui->renderer,texture,NULL,&rect) !=0) {
                                                SDL_DestroyTexture(texture);
                                                SDL_ExitWithError("Impossible d'afficher la texture");
                                            }
                                            free(datas);
                                            SDL_DestroyTexture(texture);
                                        }
                                            
                                    }
                                }
                                
                                if(frame->buttons)
                                {
                                    for(int l = 0; frame->buttons[l]; l++)
                                    {
                                        Button *button = frame->buttons[l];
                                        if(button->visible)
                                        {
                                            if(button->disabled == SDL_FALSE)
                                            {
                                                outlineColor = button->outlineColor;
                                                bgColor = button->bgColor;

                                                if(SDL_SetRenderDrawBlendMode(gui->renderer, SDL_BLENDMODE_BLEND) != 0)
                                                    SDL_ExitWithError("Impossible de charger la couleur de blend");

                                                if(SDL_SetRenderDrawColor(gui->renderer,outlineColor.r,outlineColor.g,outlineColor.b,outlineColor.a) != 0)
                                                    SDL_ExitWithError("Impossible de charger la couleur de rendu");

                                                SDL_Rect buttonFrame = {frame->x+button->x,
                                                                        frame->y+button->y,
                                                                        button->width,
                                                                        button->heigth};

                                                if(SDL_RenderFillRect(gui->renderer,&buttonFrame) != 0)
                                                    SDL_ExitWithError("Impossible de dessiner.");

                                                if(SDL_SetRenderDrawColor(gui->renderer,bgColor.r,bgColor.g,bgColor.b,bgColor.a) != 0)
                                                    SDL_ExitWithError("Impossible de charger la couleur de rendu");
                                                
                                                SDL_Rect outLineButtonFrame = { buttonFrame.x+button->outlineWidth,
                                                                                buttonFrame.y+button->outlineWidth,
                                                                                buttonFrame.w-2*button->outlineWidth,
                                                                                buttonFrame.h-2*button->outlineWidth};

                                                if(SDL_RenderFillRect(gui->renderer,&outLineButtonFrame) != 0)
                                                    SDL_ExitWithError("Impossible de dessiner.");
                                                
                                                if(button->fontName){
                                                    TTF_Font *ttfFont;
                                                    ttfFont = TTF_OpenFont(button->fontName, button->fontSize); //this opens a font style and sets a size
                                                    if(ttfFont == NULL)
                                                        continue;
                                                        
                                                    
                                                    SDL_Surface* surfaceText = TTF_RenderText_Solid(ttfFont, button->text, button->textColor); // as TTF_RenderText_Solid could only be used on SDL_Surface then you have to create the surface first
                                                    if(surfaceText == NULL)
                                                        SDL_ExitWithError("Impossible de creer une surface a partir de la font.");
                                                    SDL_Texture* textureText = SDL_CreateTextureFromSurface(gui->renderer, surfaceText); //now you can convert it into a texture
                                                    if(textureText == NULL)
                                                    {
                                                        SDL_FreeSurface(surfaceText);
                                                        SDL_ExitWithError("Impossible de creer une texture a partir de la surface.");
                                                    }

                                                    SDL_Rect messageRect = (SDL_Rect){  .x=buttonFrame.x+(buttonFrame.w/2)-(surfaceText->w/2),
                                                                                        .y=buttonFrame.y+(buttonFrame.h/2)-(surfaceText->h/2),
                                                                                        .w=surfaceText->w,
                                                                                        .h=surfaceText->h};
                                                    
                                                    //Mind you that (0,0) is on the top left of the window/screen, think a rect as the text's box, that way it would be very simple to understance
                                                    if(SDL_QueryTexture(textureText,NULL,NULL,&messageRect.w,&messageRect.h) !=0)
                                                        SDL_ExitWithError("Impossible de charger la texture");
                                                    //Now since it's a texture, you have to put RenderCopy in your game loop area, the area where the whole code executes

                                                    if(SDL_RenderCopy(gui->renderer, textureText, NULL, &messageRect) != 0) //you put the renderer's name first, the Message, the crop size(you can ignore this if you don't want to dabble with cropping), and the rect which is the size and coordinate of your texture
                                                    {
                                                        SDL_FreeSurface(surfaceText);
                                                        SDL_DestroyTexture(textureText);
                                                        SDL_ExitWithError("Impossible de render la texture.");
                                                    }
                                                    //Don't forget to free your surface and texture
                                                    SDL_FreeSurface(surfaceText);
                                                    SDL_DestroyTexture(textureText);
                                                    TTF_CloseFont(ttfFont);
                                                }
                                            }
                                            else
                                            {
                                                /* code */
                                            }
                                            
                                        }

                                    }

                                }

                                if(frame->textareas)
                                {
                                    for(int l = 0; frame->textareas[l]; l++)
                                    {
                                        TextArea *textArea = frame->textareas[l];
                                        if(textArea->visible)
                                        {
                                            outlineColor = textArea->outlineColor;
                                            bgColor = textArea->bgColor;

                                            if(SDL_SetRenderDrawBlendMode(gui->renderer, SDL_BLENDMODE_BLEND) != 0)
                                                SDL_ExitWithError("Impossible de charger la couleur de blend");

                                            if(SDL_SetRenderDrawColor(gui->renderer,outlineColor.r,outlineColor.g,outlineColor.b,outlineColor.a) != 0)
                                                SDL_ExitWithError("Impossible de charger la couleur de rendu");

                                            SDL_Rect textAreaFrame = {frame->x+textArea->x+5,
                                                                    frame->y+textArea->y+5,
                                                                    textArea->width,
                                                                    textArea->heigth};

                                            if(SDL_RenderFillRect(gui->renderer,&textAreaFrame) != 0)
                                                SDL_ExitWithError("Impossible de dessiner.");

                                            if(SDL_SetRenderDrawColor(gui->renderer,bgColor.r,bgColor.g,bgColor.b,bgColor.a) != 0)
                                                SDL_ExitWithError("Impossible de charger la couleur de rendu");
                                            
                                            SDL_Rect outLineTextAreaFrame = { textAreaFrame.x+textArea->outlineWidth,
                                                                            textAreaFrame.y+textArea->outlineWidth,
                                                                            textAreaFrame.w-2*textArea->outlineWidth,
                                                                            textAreaFrame.h-2*textArea->outlineWidth};

                                            if(SDL_RenderFillRect(gui->renderer,&outLineTextAreaFrame) != 0)
                                                SDL_ExitWithError("Impossible de dessiner.");
                                                
                                            if(textArea->fontName && textArea->text){
                                                TTF_Font *ttfFont;
                                                ttfFont = TTF_OpenFont(textArea->fontName, textArea->fontSize); //this opens a font style and sets a size
                                                if(ttfFont == NULL) continue;
                                                    
                                                int index1 = 0;
                                                int index2 = 0;
                                                int linespace = 0;
                                                for(int m = 0; textArea->text[m] != '\0';m++)
                                                {
                                                    if(textArea->text[m] == '\n' || textArea->text[m+1] == '\0' || (index2-index1+1)*textArea->fontSize/1.7>textArea->width)
                                                    {
                                                        if(textArea->text[m] == '\n')
                                                            index2 = m-1;
                                                        char *line = extractSubString(index1,index2,textArea->text);
                                                        if(textArea->text[m] == '\n' || (index2-index1+1)*textArea->fontSize/1.5>textArea->width)
                                                        {
                                                            index1 = m+1;
                                                            index2 = m+1;
                                                        }

                                                        if(strlen(line) == 0)
                                                        {
                                                            linespace++;
                                                            continue;
                                                        }
                                                        
                                                        if(textArea->scrollPosition > linespace)
                                                        {
                                                            linespace++;
                                                            free(line);
                                                            continue;
                                                        }
                                                        SDL_Surface* surfaceText = TTF_RenderText_Solid(ttfFont, line, textArea->textColor); // as TTF_RenderText_Solid could only be used on SDL_Surface then you have to create the surface first
                                            
                                                        if(surfaceText == NULL)
                                                            SDL_ExitWithError("Impossible de creer une surface a partir de la font.");
                                                        if(textArea->fontSize*(linespace+1-textArea->scrollPosition) < textAreaFrame.h)
                                                        {
                                                            SDL_Texture* textureText = SDL_CreateTextureFromSurface(gui->renderer, surfaceText); //now you can convert it into a texture
                                                            if(textureText == NULL)
                                                            {
                                                                SDL_FreeSurface(surfaceText);
                                                                SDL_ExitWithError("Impossible de creer une texture a partir de la surface.");
                                                            }

                                                            SDL_Rect messageRect = (SDL_Rect){  .x=textAreaFrame.x,
                                                                                                .y=textAreaFrame.y+textArea->fontSize*(linespace-textArea->scrollPosition),
                                                                                                .w=surfaceText->w,
                                                                                                .h=surfaceText->h};
                                                            
                                                            //Mind you that (0,0) is on the top left of the window/screen, think a rect as the text's box, that way it would be very simple to understance
                                                            if(SDL_QueryTexture(textureText,NULL,NULL,&messageRect.w,&messageRect.h) !=0)
                                                                SDL_ExitWithError("Impossible de charger la texture");
                                                            //Now since it's a texture, you have to put RenderCopy in your game loop area, the area where the whole code executes

                                                            if(SDL_RenderCopy(gui->renderer, textureText, NULL, &messageRect) != 0) //you put the renderer's name first, the Message, the crop size(you can ignore this if you don't want to dabble with cropping), and the rect which is the size and coordinate of your texture
                                                            {
                                                                SDL_FreeSurface(surfaceText);
                                                                SDL_DestroyTexture(textureText);
                                                                SDL_ExitWithError("Impossible de render la texture.");
                                                            }
                                                            //Don't forget to free your surface and texture
                                                            SDL_DestroyTexture(textureText);
                                                        }
                                                        SDL_FreeSurface(surfaceText);
                                                        linespace++;
                                                        free(line);
                                                    }
                                                    else
                                                    {
                                                        index2++;
                                                    }
                                                }
                                                    
                                                TTF_CloseFont(ttfFont);
                                            }
                                            
                                        }

                                    }

                                }
                                
                            }
                        }   
                    }
                }
            }
            SDL_RenderPresent(gui->renderer);
        }
    }
}

void eventHandler(void)
{
    SDL_Event event = appState->event;
    switch (event.type)
    {
    case SDL_KEYDOWN:
        switch (event.key.keysym.sym)
        {
        
        case SDLK_q:
            appState->program_running = SDL_FALSE;
            break;
        
        default:
            break;
        }
        break;
    case SDL_MOUSEWHEEL:
        if(appState->guis)
        {
            for (int i = 0; appState->guis[i]; i++)
            {
                Gui *gui = appState->guis[i];
                if(gui->visible && gui->views)
                {
                    for (int j = 0; gui->views[j] != NULL; j++)
                    {
                        View *view = gui->views[j];
                        if(view->visible && view->frames)
                        {
                            for (int k = 0; view->frames[k] != NULL; k++)
                            {
                                Frame *frame = view->frames[k];
                                if(frame->visible)
                                {
                                    if(frame->textareas)
                                    {
                                        for (int l = 0; frame->textareas[l]; l++)
                                        {
                                            TextArea *textArea = frame->textareas[l];
                                            int x,y;
                                            SDL_GetMouseState(&x,&y);
                                            if(textArea->visible == SDL_TRUE && x >= frame->x+textArea->x && x <= frame->x+textArea->x+textArea->width && y >= frame->y+textArea->y && y <= frame->y+textArea->y+textArea->heigth && SDL_GetWindowID(gui->window) == event.window.windowID)
                                            {
                                                if(event.wheel.y > 0 && textArea->scrollPosition > 0)
                                                {
                                                    textArea->scrollPosition--;
                                                }
                                                else if(event.wheel.y < 0)
                                                {
                                                    textArea->scrollPosition++;
                                                }
                                            }
                                        }
                                    }
                                }
                                
                            }   
                        }
                    }
                }
            }
        }
        break;
    case SDL_MOUSEBUTTONUP:
        if(appState->guis)
        {
            for (int i = 0; appState->guis[i]; i++)
            {
                Gui *gui = appState->guis[i];
                if(gui->visible && gui->views)
                {
                    for (int j = 0; gui->views[j] != NULL; j++)
                    {
                        View *view = gui->views[j];
                        if(view->visible && view->frames)
                        {
                            for (int k = 0; view->frames[k] != NULL; k++)
                            {
                                Frame *frame = view->frames[k];
                                if(frame->visible)
                                {
                                    if(frame->displaySurfaces)
                                    {
                                        for(int l = 0; frame->displaySurfaces[l]; l++)
                                        {
                                            DisplaySurface *displaySurface = frame->displaySurfaces[l];
                                            if(displaySurface->callback && displaySurface->visible == SDL_TRUE && displaySurface->disabled == SDL_FALSE && isOverDisplaySurface(displaySurface,frame) && SDL_GetWindowID(gui->window) == event.window.windowID)
                                            {
                                                displaySurface->callback(gui,frame);
                                            }
                                        }
                                    }
                                    if(frame->buttons)
                                    {
                                        for (int l = 0; frame->buttons[l]; l++)
                                        {
                                            Button *button = frame->buttons[l];
                                            if(button->callback && button->visible == SDL_TRUE && button->disabled == SDL_FALSE && isOverButton(button,frame) && SDL_GetWindowID(gui->window) == event.window.windowID)
                                            {
                                                button->callback(gui,frame);
                                            }
                                        }
                                    }
                                }
                                
                            }   
                        }
                    }
                }
            }
        }
        
        break;

    case SDL_WINDOWEVENT:
        if(event.window.event == SDL_WINDOWEVENT_CLOSE)
        {
            for (int i = 0; appState->guis[i]; i++)
            {
                if (SDL_GetWindowID(appState->guis[i]->window) == event.window.windowID)
                {
                    if(i==0)
                    {
                        appState->program_running = SDL_FALSE;
                    }
                    else{
                        removeGui(appState->guis[i]);
                    }
                    break;
                }
            }
        }
        break;
    }
}