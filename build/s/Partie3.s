	.file	"Partie3.c"
	.text
	.globl	_calculCarreNB
	.def	_calculCarreNB;	.scl	2;	.type	32;	.endef
_calculCarreNB:
LFB20:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$16, %esp
	movb	$0, -1(%ebp)
	movb	$0, -2(%ebp)
	movl	8(%ebp), %eax
	movl	%eax, -8(%ebp)
	jmp	L2
L8:
	movl	12(%ebp), %eax
	movl	%eax, -12(%ebp)
	jmp	L3
L7:
	movl	-8(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	20(%ebp), %eax
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-12(%ebp), %edx
	addl	%edx, %edx
	addl	%edx, %eax
	movzwl	(%eax), %eax
	cmpw	$255, %ax
	jne	L4
	movb	$1, -2(%ebp)
	cmpb	$0, -1(%ebp)
	je	L5
	movl	$-1, %eax
	jmp	L6
L4:
	movl	-8(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	20(%ebp), %eax
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-12(%ebp), %edx
	addl	%edx, %edx
	addl	%edx, %eax
	movzwl	(%eax), %eax
	testw	%ax, %ax
	jne	L5
	movb	$1, -1(%ebp)
	cmpb	$0, -2(%ebp)
	je	L5
	movl	$-1, %eax
	jmp	L6
L5:
	addl	$1, -12(%ebp)
L3:
	movl	16(%ebp), %edx
	movl	12(%ebp), %eax
	addl	%edx, %eax
	cmpl	%eax, -12(%ebp)
	jl	L7
	addl	$1, -8(%ebp)
L2:
	movl	16(%ebp), %edx
	movl	8(%ebp), %eax
	addl	%edx, %eax
	cmpl	%eax, -8(%ebp)
	jl	L8
	cmpb	$0, -1(%ebp)
	je	L9
	movl	$0, %eax
	jmp	L6
L9:
	movl	$255, %eax
L6:
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE20:
	.globl	_etiquetteFeuilleNB
	.def	_etiquetteFeuilleNB;	.scl	2;	.type	32;	.endef
_etiquetteFeuilleNB:
LFB21:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	cmpl	$235, 8(%ebp)
	jg	L11
	movl	8(%ebp), %eax
	addl	$20, %eax
	jmp	L12
L11:
	movl	8(%ebp), %eax
	subl	$236, %eax
L12:
	popl	%ebp
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE21:
	.globl	_creeArbreNB
	.def	_creeArbreNB;	.scl	2;	.type	32;	.endef
_creeArbreNB:
LFB22:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	pushl	%ebx
	subl	$36, %esp
	.cfi_offset 3, -12
	movl	$36, (%esp)
	call	_malloc
	movl	%eax, -12(%ebp)
	movl	-12(%ebp), %eax
	movl	16(%ebp), %edx
	movl	%edx, 16(%eax)
	movl	-12(%ebp), %eax
	movl	8(%ebp), %edx
	movl	%edx, 8(%eax)
	movl	-12(%ebp), %eax
	movl	12(%ebp), %edx
	movl	%edx, 12(%eax)
	movl	20(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	16(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	12(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_calculCarreNB
	movl	%eax, %edx
	movl	-12(%ebp), %eax
	movw	%dx, (%eax)
	movl	-12(%ebp), %eax
	movzwl	(%eax), %eax
	cwtl
	testl	%eax, %eax
	je	L14
	cmpl	$255, %eax
	je	L14
	cmpl	$-1, %eax
	je	L15
	jmp	L16
L14:
	movl	-12(%ebp), %eax
	movl	$0, 20(%eax)
	movl	-12(%ebp), %eax
	movl	$0, 24(%eax)
	movl	-12(%ebp), %eax
	movl	$0, 28(%eax)
	movl	-12(%ebp), %eax
	movl	$0, 32(%eax)
	movl	_compteurEtiquettes.2861, %edx
	movl	-12(%ebp), %eax
	movl	%edx, 4(%eax)
	movl	_compteurEtiquettes.2861, %eax
	movl	%eax, (%esp)
	call	_etiquetteFeuilleNB
	movl	%eax, _compteurEtiquettes.2861
	jmp	L16
L15:
	movl	-12(%ebp), %eax
	movl	$-1, 4(%eax)
	movl	_compteurnoeud.2862, %eax
	addl	$1, %eax
	movl	%eax, _compteurnoeud.2862
	movl	16(%ebp), %eax
	movl	%eax, %edx
	shrl	$31, %edx
	addl	%edx, %eax
	sarl	%eax
	movl	%eax, %ecx
	movl	16(%ebp), %eax
	movl	%eax, %edx
	shrl	$31, %edx
	addl	%edx, %eax
	sarl	%eax
	movl	%eax, %edx
	movl	12(%ebp), %eax
	addl	%eax, %edx
	movl	20(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	%ecx, 8(%esp)
	movl	%edx, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_creeArbreNB
	movl	%eax, %edx
	movl	-12(%ebp), %eax
	movl	%edx, 20(%eax)
	movl	16(%ebp), %eax
	movl	%eax, %edx
	shrl	$31, %edx
	addl	%edx, %eax
	sarl	%eax
	movl	%eax, %ebx
	movl	16(%ebp), %eax
	movl	%eax, %edx
	shrl	$31, %edx
	addl	%edx, %eax
	sarl	%eax
	movl	%eax, %edx
	movl	12(%ebp), %eax
	leal	(%edx,%eax), %ecx
	movl	16(%ebp), %eax
	movl	%eax, %edx
	shrl	$31, %edx
	addl	%edx, %eax
	sarl	%eax
	movl	%eax, %edx
	movl	8(%ebp), %eax
	addl	%eax, %edx
	movl	20(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	%ebx, 8(%esp)
	movl	%ecx, 4(%esp)
	movl	%edx, (%esp)
	call	_creeArbreNB
	movl	%eax, %edx
	movl	-12(%ebp), %eax
	movl	%edx, 24(%eax)
	movl	16(%ebp), %eax
	movl	%eax, %edx
	shrl	$31, %edx
	addl	%edx, %eax
	sarl	%eax
	movl	%eax, %edx
	movl	20(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	%edx, 8(%esp)
	movl	12(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_creeArbreNB
	movl	%eax, %edx
	movl	-12(%ebp), %eax
	movl	%edx, 32(%eax)
	movl	16(%ebp), %eax
	movl	%eax, %edx
	shrl	$31, %edx
	addl	%edx, %eax
	sarl	%eax
	movl	%eax, %ecx
	movl	16(%ebp), %eax
	movl	%eax, %edx
	shrl	$31, %edx
	addl	%edx, %eax
	sarl	%eax
	movl	%eax, %edx
	movl	8(%ebp), %eax
	addl	%eax, %edx
	movl	20(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	%ecx, 8(%esp)
	movl	12(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	%edx, (%esp)
	call	_creeArbreNB
	movl	%eax, %edx
	movl	-12(%ebp), %eax
	movl	%edx, 28(%eax)
	nop
L16:
	movl	-12(%ebp), %eax
	addl	$36, %esp
	popl	%ebx
	.cfi_restore 3
	popl	%ebp
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE22:
	.globl	_compteFeuille
	.def	_compteFeuille;	.scl	2;	.type	32;	.endef
_compteFeuille:
LFB23:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$24, %esp
	cmpl	$0, 8(%ebp)
	je	L19
	movl	8(%ebp), %eax
	movl	4(%eax), %eax
	cmpl	$-1, %eax
	je	L20
	movl	_nbreEtiquettes.2871, %eax
	addl	$1, %eax
	movl	%eax, _nbreEtiquettes.2871
L20:
	movl	8(%ebp), %eax
	movl	24(%eax), %eax
	movl	%eax, (%esp)
	call	_compteFeuille
	movl	8(%ebp), %eax
	movl	20(%eax), %eax
	movl	%eax, (%esp)
	call	_compteFeuille
	movl	8(%ebp), %eax
	movl	28(%eax), %eax
	movl	%eax, (%esp)
	call	_compteFeuille
	movl	8(%ebp), %eax
	movl	32(%eax), %eax
	movl	%eax, (%esp)
	call	_compteFeuille
L19:
	movl	_nbreEtiquettes.2871, %eax
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE23:
	.globl	_creeImageArbreNB
	.def	_creeImageArbreNB;	.scl	2;	.type	32;	.endef
_creeImageArbreNB:
LFB24:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$56, %esp
	movl	16(%ebp), %eax
	movb	%al, -28(%ebp)
	cmpl	$0, 12(%ebp)
	je	L37
	movl	12(%ebp), %eax
	movzwl	(%eax), %eax
	cmpw	$-1, %ax
	jne	L24
	movzbl	-28(%ebp), %edx
	movl	12(%ebp), %eax
	movl	20(%eax), %eax
	movl	%edx, 8(%esp)
	movl	%eax, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_creeImageArbreNB
	movzbl	-28(%ebp), %edx
	movl	12(%ebp), %eax
	movl	24(%eax), %eax
	movl	%edx, 8(%esp)
	movl	%eax, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_creeImageArbreNB
	movzbl	-28(%ebp), %edx
	movl	12(%ebp), %eax
	movl	32(%eax), %eax
	movl	%edx, 8(%esp)
	movl	%eax, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_creeImageArbreNB
	movzbl	-28(%ebp), %edx
	movl	12(%ebp), %eax
	movl	28(%eax), %eax
	movl	%edx, 8(%esp)
	movl	%eax, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_creeImageArbreNB
	jmp	L37
L24:
	movl	12(%ebp), %eax
	movl	12(%eax), %eax
	movl	%eax, -12(%ebp)
	jmp	L25
L36:
	movl	12(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -16(%ebp)
	jmp	L26
L35:
	cmpb	$0, -28(%ebp)
	je	L27
	movl	12(%ebp), %eax
	movl	4(%eax), %eax
	jmp	L28
L27:
	movl	12(%ebp), %eax
	movzwl	(%eax), %eax
L28:
	movl	8(%ebp), %edx
	movl	20(%edx), %edx
	movl	-16(%ebp), %ecx
	sall	$2, %ecx
	addl	%ecx, %edx
	movl	(%edx), %edx
	movl	-12(%ebp), %ecx
	addl	%ecx, %ecx
	addl	%ecx, %edx
	movw	%ax, (%edx)
	cmpb	$0, -28(%ebp)
	je	L29
	movl	12(%ebp), %eax
	movl	4(%eax), %eax
	jmp	L30
L29:
	movl	12(%ebp), %eax
	movzwl	(%eax), %eax
L30:
	movl	8(%ebp), %edx
	movl	8(%edx), %edx
	movl	-16(%ebp), %ecx
	sall	$2, %ecx
	addl	%ecx, %edx
	movl	(%edx), %edx
	movl	-12(%ebp), %ecx
	addl	%ecx, %ecx
	addl	%ecx, %edx
	movw	%ax, (%edx)
	cmpb	$0, -28(%ebp)
	je	L31
	movl	12(%ebp), %eax
	movl	4(%eax), %eax
	jmp	L32
L31:
	movl	12(%ebp), %eax
	movzwl	(%eax), %eax
L32:
	movl	8(%ebp), %edx
	movl	16(%edx), %edx
	movl	-16(%ebp), %ecx
	sall	$2, %ecx
	addl	%ecx, %edx
	movl	(%edx), %edx
	movl	-12(%ebp), %ecx
	addl	%ecx, %ecx
	addl	%ecx, %edx
	movw	%ax, (%edx)
	cmpb	$0, -28(%ebp)
	je	L33
	movl	12(%ebp), %eax
	movl	4(%eax), %eax
	jmp	L34
L33:
	movl	12(%ebp), %eax
	movzwl	(%eax), %eax
L34:
	movl	8(%ebp), %edx
	movl	12(%edx), %edx
	movl	-16(%ebp), %ecx
	sall	$2, %ecx
	addl	%ecx, %edx
	movl	(%edx), %edx
	movl	-12(%ebp), %ecx
	addl	%ecx, %ecx
	addl	%ecx, %edx
	movw	%ax, (%edx)
	addl	$1, -16(%ebp)
L26:
	movl	12(%ebp), %eax
	movl	16(%eax), %edx
	movl	12(%ebp), %eax
	movl	8(%eax), %eax
	addl	%edx, %eax
	cmpl	%eax, -16(%ebp)
	jl	L35
	addl	$1, -12(%ebp)
L25:
	movl	12(%ebp), %eax
	movl	16(%eax), %edx
	movl	12(%ebp), %eax
	movl	12(%eax), %eax
	addl	%edx, %eax
	cmpl	%eax, -12(%ebp)
	jl	L36
L37:
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE24:
	.globl	_libereArbreBinaire
	.def	_libereArbreBinaire;	.scl	2;	.type	32;	.endef
_libereArbreBinaire:
LFB25:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$24, %esp
	cmpl	$0, 8(%ebp)
	je	L40
	movl	8(%ebp), %eax
	movl	20(%eax), %eax
	movl	%eax, (%esp)
	call	_libereArbreBinaire
	movl	8(%ebp), %eax
	movl	24(%eax), %eax
	movl	%eax, (%esp)
	call	_libereArbreBinaire
	movl	8(%ebp), %eax
	movl	32(%eax), %eax
	movl	%eax, (%esp)
	call	_libereArbreBinaire
	movl	8(%ebp), %eax
	movl	28(%eax), %eax
	movl	%eax, (%esp)
	call	_libereArbreBinaire
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
L40:
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE25:
.lcomm _compteurEtiquettes.2861,4,4
.lcomm _compteurnoeud.2862,4,4
.lcomm _nbreEtiquettes.2871,4,4
	.ident	"GCC: (MinGW.org GCC-8.2.0-5) 8.2.0"
	.def	_malloc;	.scl	2;	.type	32;	.endef
	.def	_free;	.scl	2;	.type	32;	.endef
