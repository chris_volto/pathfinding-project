	.file	"Partie6.c"
	.text
	.globl	_triTabSymbOcc
	.def	_triTabSymbOcc;	.scl	2;	.type	32;	.endef
_triTabSymbOcc:
LFB20:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$48, %esp
	movl	$0, -4(%ebp)
	jmp	L2
L9:
	movl	8(%ebp), %eax
	movl	4(%eax), %eax
	movl	-4(%ebp), %edx
	sall	$3, %edx
	addl	%edx, %eax
	movl	4(%eax), %eax
	movl	%eax, -8(%ebp)
	movl	$-1, -12(%ebp)
	movl	-4(%ebp), %eax
	movl	%eax, -16(%ebp)
	jmp	L3
L5:
	movl	8(%ebp), %eax
	movl	4(%eax), %eax
	movl	-16(%ebp), %edx
	sall	$3, %edx
	addl	%edx, %eax
	movl	4(%eax), %eax
	cmpl	%eax, -8(%ebp)
	jle	L4
	movl	8(%ebp), %eax
	movl	4(%eax), %eax
	movl	-16(%ebp), %edx
	sall	$3, %edx
	addl	%edx, %eax
	movl	4(%eax), %eax
	movl	%eax, -8(%ebp)
	movl	-16(%ebp), %eax
	movl	%eax, -12(%ebp)
L4:
	addl	$1, -16(%ebp)
L3:
	movl	8(%ebp), %eax
	movl	(%eax), %eax
	cmpl	%eax, -16(%ebp)
	jl	L5
	cmpl	$0, -12(%ebp)
	js	L6
	movl	8(%ebp), %eax
	movl	4(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$3, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	%eax, -28(%ebp)
	movl	8(%ebp), %eax
	movl	4(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$3, %edx
	addl	%edx, %eax
	movl	4(%eax), %eax
	movl	%eax, -24(%ebp)
	movl	8(%ebp), %eax
	movl	4(%eax), %eax
	movl	-4(%ebp), %edx
	sall	$3, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	%eax, -36(%ebp)
	movl	8(%ebp), %eax
	movl	4(%eax), %eax
	movl	-4(%ebp), %edx
	sall	$3, %edx
	addl	%edx, %eax
	movl	4(%eax), %eax
	movl	%eax, -32(%ebp)
	movl	-4(%ebp), %eax
	movl	%eax, -20(%ebp)
	jmp	L7
L8:
	movl	8(%ebp), %eax
	movl	4(%eax), %eax
	movl	-20(%ebp), %edx
	addl	$1, %edx
	sall	$3, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	%eax, -44(%ebp)
	movl	8(%ebp), %eax
	movl	4(%eax), %eax
	movl	-20(%ebp), %edx
	addl	$1, %edx
	sall	$3, %edx
	addl	%edx, %eax
	movl	4(%eax), %eax
	movl	%eax, -40(%ebp)
	movl	8(%ebp), %eax
	movl	4(%eax), %eax
	movl	-20(%ebp), %edx
	addl	$1, %edx
	sall	$3, %edx
	addl	%eax, %edx
	movl	-36(%ebp), %eax
	movl	%eax, (%edx)
	movl	8(%ebp), %eax
	movl	4(%eax), %eax
	movl	-20(%ebp), %edx
	addl	$1, %edx
	sall	$3, %edx
	addl	%eax, %edx
	movl	-32(%ebp), %eax
	movl	%eax, 4(%edx)
	movl	-44(%ebp), %eax
	movl	-40(%ebp), %edx
	movl	%eax, -36(%ebp)
	movl	%edx, -32(%ebp)
	addl	$1, -20(%ebp)
L7:
	movl	-20(%ebp), %eax
	cmpl	-12(%ebp), %eax
	jne	L8
	movl	8(%ebp), %eax
	movl	4(%eax), %eax
	movl	-4(%ebp), %edx
	sall	$3, %edx
	leal	(%eax,%edx), %ecx
	movl	-28(%ebp), %eax
	movl	-24(%ebp), %edx
	movl	%eax, (%ecx)
	movl	%edx, 4(%ecx)
L6:
	addl	$1, -4(%ebp)
L2:
	movl	8(%ebp), %eax
	movl	(%eax), %eax
	cmpl	%eax, -4(%ebp)
	jl	L9
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE20:
	.section .rdata,"dr"
LC0:
	.ascii "r\0"
LC1:
	.ascii "%s%c\0"
	.text
	.globl	_compteOccurrence
	.def	_compteOccurrence;	.scl	2;	.type	32;	.endef
_compteOccurrence:
LFB21:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	pushl	%esi
	pushl	%ebx
	subl	$64, %esp
	.cfi_offset 6, -12
	.cfi_offset 3, -16
	movl	$LC0, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_fopen
	movl	%eax, -32(%ebp)
	cmpl	$0, -32(%ebp)
	jne	L11
	movl	$0, %eax
	jmp	L12
L11:
	movl	$2, 8(%esp)
	movl	$0, 4(%esp)
	movl	-32(%ebp), %eax
	movl	%eax, (%esp)
	call	_fseek
	movl	-32(%ebp), %eax
	movl	%eax, (%esp)
	call	_ftell
	movl	%eax, -36(%ebp)
	movl	-32(%ebp), %eax
	movl	%eax, (%esp)
	call	_rewind
	movl	$8, (%esp)
	call	_malloc
	movl	%eax, -40(%ebp)
	movl	-40(%ebp), %eax
	movl	$0, (%eax)
	movl	-40(%ebp), %eax
	movl	$0, 4(%eax)
	movl	-36(%ebp), %eax
	movl	%eax, (%esp)
	call	_malloc
	movl	%eax, -28(%ebp)
	movl	-28(%ebp), %eax
	movb	$0, (%eax)
	movl	$0, -24(%ebp)
	jmp	L13
L25:
	movl	-32(%ebp), %eax
	movl	%eax, (%esp)
	call	_fgetc
	movl	%eax, -24(%ebp)
	cmpl	$47, -24(%ebp)
	jle	L14
	cmpl	$57, -24(%ebp)
	jle	L15
L14:
	cmpl	$45, -24(%ebp)
	jne	L16
L15:
	movl	-24(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	-28(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC1, 4(%esp)
	movl	-28(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	jmp	L13
L16:
	movl	%esp, %eax
	movl	%eax, %esi
	movl	$0, -20(%ebp)
	movl	-28(%ebp), %eax
	movl	%eax, (%esp)
	call	_atoi
	movl	%eax, -44(%ebp)
	movl	-40(%ebp), %eax
	movl	(%eax), %eax
	leal	-1(%eax), %edx
	movl	%edx, -48(%ebp)
	leal	0(,%eax,8), %edx
	movl	$16, %eax
	subl	$1, %eax
	addl	%edx, %eax
	movl	$16, %ebx
	movl	$0, %edx
	divl	%ebx
	imull	$16, %eax, %eax
	call	___chkstk_ms
	subl	%eax, %esp
	leal	16(%esp), %eax
	addl	$3, %eax
	shrl	$2, %eax
	sall	$2, %eax
	movl	%eax, -52(%ebp)
	movl	-40(%ebp), %eax
	movl	(%eax), %eax
	testl	%eax, %eax
	je	L17
	movl	$0, -16(%ebp)
	jmp	L18
L20:
	movl	-40(%ebp), %eax
	movl	4(%eax), %eax
	movl	-16(%ebp), %edx
	sall	$3, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	cmpl	%eax, -44(%ebp)
	jne	L19
	movl	-40(%ebp), %eax
	movl	4(%eax), %eax
	movl	-16(%ebp), %edx
	sall	$3, %edx
	addl	%edx, %eax
	movl	4(%eax), %edx
	addl	$1, %edx
	movl	%edx, 4(%eax)
	movl	$1, -20(%ebp)
	jmp	L17
L19:
	movl	-40(%ebp), %eax
	movl	4(%eax), %eax
	movl	-16(%ebp), %edx
	sall	$3, %edx
	addl	%edx, %eax
	movl	-52(%ebp), %ecx
	movl	-16(%ebp), %ebx
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, (%ecx,%ebx,8)
	movl	%edx, 4(%ecx,%ebx,8)
	addl	$1, -16(%ebp)
L18:
	movl	-40(%ebp), %eax
	movl	(%eax), %eax
	cmpl	%eax, -16(%ebp)
	jl	L20
L17:
	cmpl	$0, -20(%ebp)
	jne	L21
	movl	-40(%ebp), %eax
	movl	(%eax), %eax
	leal	1(%eax), %edx
	movl	-40(%ebp), %eax
	movl	%edx, (%eax)
	movl	-40(%ebp), %eax
	movl	(%eax), %eax
	leal	0(,%eax,8), %edx
	movl	-40(%ebp), %eax
	movl	4(%eax), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_realloc
	movl	%eax, %edx
	movl	-40(%ebp), %eax
	movl	%edx, 4(%eax)
	movl	-40(%ebp), %eax
	movl	(%eax), %eax
	cmpl	$1, %eax
	je	L22
	movl	$0, -12(%ebp)
	jmp	L23
L24:
	movl	-40(%ebp), %eax
	movl	4(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$3, %edx
	leal	(%eax,%edx), %ecx
	movl	-52(%ebp), %eax
	movl	-12(%ebp), %edx
	leal	(%eax,%edx,8), %edx
	movl	(%edx), %eax
	movl	4(%edx), %edx
	movl	%eax, (%ecx)
	movl	%edx, 4(%ecx)
	addl	$1, -12(%ebp)
L23:
	movl	-40(%ebp), %eax
	movl	(%eax), %eax
	cmpl	%eax, -12(%ebp)
	jl	L24
L22:
	movl	-40(%ebp), %eax
	movl	4(%eax), %edx
	movl	-40(%ebp), %eax
	movl	(%eax), %eax
	addl	$536870911, %eax
	sall	$3, %eax
	addl	%edx, %eax
	movl	-44(%ebp), %edx
	movl	%edx, (%eax)
	movl	$1, 4(%eax)
L21:
	movl	-28(%ebp), %eax
	movb	$0, (%eax)
	movl	%eax, -28(%ebp)
	movl	%esi, %esp
L13:
	cmpl	$-1, -24(%ebp)
	jne	L25
	movl	-28(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-32(%ebp), %eax
	movl	%eax, (%esp)
	call	_fclose
	movl	-40(%ebp), %eax
	movl	%eax, (%esp)
	call	_triTabSymbOcc
	movl	-40(%ebp), %eax
L12:
	leal	-8(%ebp), %esp
	popl	%ebx
	.cfi_restore 3
	popl	%esi
	.cfi_restore 6
	popl	%ebp
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE21:
	.globl	_triNoeudArbreCodeOcc
	.def	_triNoeudArbreCodeOcc;	.scl	2;	.type	32;	.endef
_triNoeudArbreCodeOcc:
LFB22:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$32, %esp
	movl	$0, -4(%ebp)
	jmp	L27
L34:
	movl	-4(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	8(%ebp), %eax
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	movl	%eax, -8(%ebp)
	movl	$-1, -12(%ebp)
	movl	-4(%ebp), %eax
	movl	%eax, -16(%ebp)
	jmp	L28
L30:
	movl	-16(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	8(%ebp), %eax
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	cmpl	%eax, -8(%ebp)
	jle	L29
	movl	-16(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	8(%ebp), %eax
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	movl	%eax, -8(%ebp)
	movl	-16(%ebp), %eax
	movl	%eax, -12(%ebp)
L29:
	addl	$1, -16(%ebp)
L28:
	movl	-16(%ebp), %eax
	cmpl	12(%ebp), %eax
	jl	L30
	cmpl	$0, -12(%ebp)
	js	L31
	movl	-12(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	8(%ebp), %eax
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	%eax, -28(%ebp)
	movl	-4(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	8(%ebp), %eax
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	%eax, -20(%ebp)
	movl	-4(%ebp), %eax
	movl	%eax, -24(%ebp)
	jmp	L32
L33:
	movl	-24(%ebp), %eax
	addl	$1, %eax
	leal	0(,%eax,4), %edx
	movl	8(%ebp), %eax
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	%eax, -32(%ebp)
	movl	-24(%ebp), %eax
	addl	$1, %eax
	leal	0(,%eax,4), %edx
	movl	8(%ebp), %eax
	addl	%eax, %edx
	movl	-20(%ebp), %eax
	movl	%eax, (%edx)
	movl	-32(%ebp), %eax
	movl	%eax, -20(%ebp)
	addl	$1, -24(%ebp)
L32:
	movl	-24(%ebp), %eax
	cmpl	-12(%ebp), %eax
	jne	L33
	movl	-4(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	8(%ebp), %eax
	addl	%eax, %edx
	movl	-28(%ebp), %eax
	movl	%eax, (%edx)
L31:
	addl	$1, -4(%ebp)
L27:
	movl	-4(%ebp), %eax
	cmpl	12(%ebp), %eax
	jl	L34
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE22:
	.globl	_deplaceNoeudArbreCodeOcc
	.def	_deplaceNoeudArbreCodeOcc;	.scl	2;	.type	32;	.endef
_deplaceNoeudArbreCodeOcc:
LFB23:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$32, %esp
	movl	$0, -4(%ebp)
	jmp	L36
L43:
	movl	-4(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	8(%ebp), %eax
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	(%eax), %eax
	cmpl	$-666, %eax
	jne	L37
	movl	-4(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	8(%ebp), %eax
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	movl	%eax, -8(%ebp)
	movl	$-1, -12(%ebp)
	movl	-4(%ebp), %eax
	movl	%eax, -16(%ebp)
	jmp	L38
L40:
	movl	-16(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	8(%ebp), %eax
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	cmpl	%eax, -8(%ebp)
	jl	L39
	movl	-16(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	8(%ebp), %eax
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	movl	%eax, -8(%ebp)
	movl	-16(%ebp), %eax
	movl	%eax, -12(%ebp)
L39:
	addl	$1, -16(%ebp)
L38:
	movl	-16(%ebp), %eax
	cmpl	12(%ebp), %eax
	jl	L40
	cmpl	$0, -12(%ebp)
	js	L37
	movl	-4(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	8(%ebp), %eax
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	(%eax), %eax
	movl	%eax, -28(%ebp)
	movl	-4(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	8(%ebp), %eax
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	movl	%eax, -24(%ebp)
	movl	-4(%ebp), %eax
	movl	%eax, -20(%ebp)
	jmp	L41
L42:
	movl	-20(%ebp), %eax
	addl	$1, %eax
	leal	0(,%eax,4), %edx
	movl	8(%ebp), %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-20(%ebp), %eax
	leal	0(,%eax,4), %ecx
	movl	8(%ebp), %eax
	addl	%ecx, %eax
	movl	(%eax), %eax
	movl	(%edx), %edx
	movl	%edx, (%eax)
	movl	-20(%ebp), %eax
	addl	$1, %eax
	leal	0(,%eax,4), %edx
	movl	8(%ebp), %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-20(%ebp), %eax
	leal	0(,%eax,4), %ecx
	movl	8(%ebp), %eax
	addl	%ecx, %eax
	movl	(%eax), %eax
	movl	4(%edx), %edx
	movl	%edx, 4(%eax)
	addl	$1, -20(%ebp)
L41:
	movl	-20(%ebp), %eax
	cmpl	-12(%ebp), %eax
	jne	L42
	movl	-12(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	8(%ebp), %eax
	addl	%edx, %eax
	movl	(%eax), %ecx
	movl	-28(%ebp), %eax
	movl	-24(%ebp), %edx
	movl	%eax, (%ecx)
	movl	%edx, 4(%ecx)
L37:
	addl	$1, -4(%ebp)
L36:
	movl	-4(%ebp), %eax
	cmpl	12(%ebp), %eax
	jl	L43
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE23:
	.globl	_creeArbreCodeOcc
	.def	_creeArbreCodeOcc;	.scl	2;	.type	32;	.endef
_creeArbreCodeOcc:
LFB24:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	pushl	%ebx
	subl	$68, %esp
	.cfi_offset 3, -12
	movl	8(%ebp), %eax
	movl	(%eax), %eax
	sall	$2, %eax
	movl	%eax, (%esp)
	call	_malloc
	movl	%eax, -24(%ebp)
	movl	8(%ebp), %eax
	movl	(%eax), %eax
	movl	%eax, -20(%ebp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_triTabSymbOcc
	movl	$0, -16(%ebp)
	jmp	L45
L46:
	movl	-16(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	-24(%ebp), %eax
	leal	(%edx,%eax), %ebx
	movl	$16, (%esp)
	call	_malloc
	movl	%eax, (%ebx)
	movl	-16(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	-24(%ebp), %eax
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	$0, 8(%eax)
	movl	-16(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	-24(%ebp), %eax
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	$0, 12(%eax)
	movl	8(%ebp), %eax
	movl	4(%eax), %eax
	movl	-16(%ebp), %edx
	sall	$3, %edx
	addl	%edx, %eax
	movl	-16(%ebp), %edx
	leal	0(,%edx,4), %ecx
	movl	-24(%ebp), %edx
	addl	%ecx, %edx
	movl	(%edx), %ecx
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, (%ecx)
	movl	%edx, 4(%ecx)
	addl	$1, -16(%ebp)
L45:
	movl	8(%ebp), %eax
	movl	(%eax), %eax
	cmpl	%eax, -16(%ebp)
	jl	L46
	jmp	L47
L53:
	movl	$0, -36(%ebp)
	movl	-36(%ebp), %eax
	cmpl	-20(%ebp), %eax
	jge	L47
	movl	%esp, %eax
	movl	%eax, %ebx
	movl	$16, (%esp)
	call	_malloc
	movl	%eax, -40(%ebp)
	movl	-36(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	-24(%ebp), %eax
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	4(%eax), %edx
	movl	-36(%ebp), %eax
	addl	$1, %eax
	leal	0(,%eax,4), %ecx
	movl	-24(%ebp), %eax
	addl	%ecx, %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	%eax, %edx
	movl	-40(%ebp), %eax
	movl	%edx, 4(%eax)
	movl	-40(%ebp), %eax
	movl	$-666, (%eax)
	movl	-36(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	-24(%ebp), %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-40(%ebp), %eax
	movl	%edx, 8(%eax)
	movl	-36(%ebp), %eax
	addl	$1, %eax
	leal	0(,%eax,4), %edx
	movl	-24(%ebp), %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-40(%ebp), %eax
	movl	%edx, 12(%eax)
	movl	-20(%ebp), %eax
	subl	$2, %eax
	leal	-1(%eax), %edx
	movl	%edx, -44(%ebp)
	leal	0(,%eax,4), %edx
	movl	$16, %eax
	subl	$1, %eax
	addl	%edx, %eax
	movl	$16, %ecx
	movl	$0, %edx
	divl	%ecx
	imull	$16, %eax, %eax
	call	___chkstk_ms
	subl	%eax, %esp
	leal	8(%esp), %eax
	addl	$3, %eax
	shrl	$2, %eax
	sall	$2, %eax
	movl	%eax, -48(%ebp)
	movl	$0, -12(%ebp)
	movl	$0, -28(%ebp)
	jmp	L48
L50:
	movl	-28(%ebp), %eax
	cmpl	-36(%ebp), %eax
	je	L49
	movl	-36(%ebp), %eax
	addl	$1, %eax
	cmpl	%eax, -28(%ebp)
	je	L49
	movl	-28(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	-24(%ebp), %eax
	addl	%edx, %eax
	movl	(%eax), %ecx
	movl	-48(%ebp), %eax
	movl	-12(%ebp), %edx
	movl	%ecx, (%eax,%edx,4)
	addl	$1, -12(%ebp)
L49:
	addl	$1, -28(%ebp)
L48:
	movl	-28(%ebp), %eax
	cmpl	-20(%ebp), %eax
	jl	L50
	subl	$1, -20(%ebp)
	movl	-24(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-20(%ebp), %eax
	sall	$2, %eax
	movl	%eax, (%esp)
	call	_malloc
	movl	%eax, -24(%ebp)
	movl	$0, -32(%ebp)
	jmp	L51
L52:
	movl	-32(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	-24(%ebp), %eax
	leal	(%edx,%eax), %ecx
	movl	-48(%ebp), %eax
	movl	-32(%ebp), %edx
	movl	(%eax,%edx,4), %eax
	movl	%eax, (%ecx)
	addl	$1, -32(%ebp)
L51:
	movl	-20(%ebp), %eax
	subl	$1, %eax
	cmpl	%eax, -32(%ebp)
	jl	L52
	movl	-20(%ebp), %eax
	addl	$1073741823, %eax
	leal	0(,%eax,4), %edx
	movl	-24(%ebp), %eax
	addl	%eax, %edx
	movl	-40(%ebp), %eax
	movl	%eax, (%edx)
	movl	-20(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-24(%ebp), %eax
	movl	%eax, (%esp)
	call	_triNoeudArbreCodeOcc
	movl	-20(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-24(%ebp), %eax
	movl	%eax, (%esp)
	call	_deplaceNoeudArbreCodeOcc
	nop
	movl	%ebx, %esp
L47:
	cmpl	$1, -20(%ebp)
	jg	L53
	movl	-24(%ebp), %eax
	movl	(%eax), %eax
	movl	%eax, -52(%ebp)
	movl	-24(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-52(%ebp), %eax
	movl	-4(%ebp), %ebx
	leave
	.cfi_restore 5
	.cfi_restore 3
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE24:
	.section .rdata,"dr"
LC2:
	.ascii "symb=%d occ=%d %p\12\0"
	.text
	.globl	_printArbreCodeAdaptatif
	.def	_printArbreCodeAdaptatif;	.scl	2;	.type	32;	.endef
_printArbreCodeAdaptatif:
LFB25:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$24, %esp
	cmpl	$0, 8(%ebp)
	je	L57
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, (%esp)
	call	_printArbreCodeAdaptatif
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	movl	%eax, (%esp)
	call	_printArbreCodeAdaptatif
	movl	8(%ebp), %eax
	movl	(%eax), %eax
	cmpl	$-666, %eax
	je	L57
	movl	8(%ebp), %eax
	movl	4(%eax), %edx
	movl	8(%ebp), %eax
	movl	(%eax), %eax
	movl	8(%ebp), %ecx
	movl	%ecx, 12(%esp)
	movl	%edx, 8(%esp)
	movl	%eax, 4(%esp)
	movl	$LC2, (%esp)
	call	_printf
L57:
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE25:
	.globl	_libereArbreCodeAdaptatif
	.def	_libereArbreCodeAdaptatif;	.scl	2;	.type	32;	.endef
_libereArbreCodeAdaptatif:
LFB26:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$24, %esp
	cmpl	$0, 8(%ebp)
	je	L60
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, (%esp)
	call	_libereArbreCodeAdaptatif
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	movl	%eax, (%esp)
	call	_libereArbreCodeAdaptatif
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
L60:
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE26:
	.globl	_creeCodeAdaptatif
	.def	_creeCodeAdaptatif;	.scl	2;	.type	32;	.endef
_creeCodeAdaptatif:
LFB27:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	pushl	%edi
	pushl	%ebx
	subl	$64, %esp
	.cfi_offset 7, -12
	.cfi_offset 3, -16
	cmpl	$0, 8(%ebp)
	je	L62
	movl	$8, (%esp)
	call	_malloc
	movl	%eax, -32(%ebp)
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	testl	%eax, %eax
	jne	L63
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	testl	%eax, %eax
	jne	L63
	movl	-32(%ebp), %eax
	movl	$1, (%eax)
	movl	$8, (%esp)
	call	_malloc
	movl	%eax, %edx
	movl	-32(%ebp), %eax
	movl	%edx, 4(%eax)
	movl	$1, (%esp)
	call	_malloc
	movl	%eax, -36(%ebp)
	movl	-36(%ebp), %eax
	movb	$0, (%eax)
	movl	-32(%ebp), %eax
	movl	4(%eax), %eax
	movl	8(%ebp), %edx
	movl	(%edx), %edx
	movl	%edx, (%eax)
	movl	-36(%ebp), %edx
	movl	%edx, 4(%eax)
	jmp	L64
L63:
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, (%esp)
	call	_creeCodeAdaptatif
	movl	%eax, -40(%ebp)
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	movl	%eax, (%esp)
	call	_creeCodeAdaptatif
	movl	%eax, -44(%ebp)
	cmpl	$0, -40(%ebp)
	je	L65
	movl	-40(%ebp), %eax
	movl	(%eax), %edx
	jmp	L66
L65:
	movl	$0, %edx
L66:
	cmpl	$0, -44(%ebp)
	je	L67
	movl	-44(%ebp), %eax
	movl	(%eax), %eax
	jmp	L68
L67:
	movl	$0, %eax
L68:
	addl	%edx, %eax
	sall	$3, %eax
	movl	%eax, (%esp)
	call	_malloc
	movl	%eax, %edx
	movl	-32(%ebp), %eax
	movl	%edx, 4(%eax)
	cmpl	$0, -40(%ebp)
	je	L69
	movl	-40(%ebp), %eax
	movl	(%eax), %edx
	jmp	L70
L69:
	movl	$0, %edx
L70:
	cmpl	$0, -44(%ebp)
	je	L71
	movl	-44(%ebp), %eax
	movl	(%eax), %eax
	jmp	L72
L71:
	movl	$0, %eax
L72:
	addl	%eax, %edx
	movl	-32(%ebp), %eax
	movl	%edx, (%eax)
	movl	$0, -12(%ebp)
	cmpl	$0, -40(%ebp)
	je	L73
	movl	$0, -16(%ebp)
	jmp	L74
L75:
	movl	-40(%ebp), %eax
	movl	4(%eax), %eax
	movl	-16(%ebp), %edx
	sall	$3, %edx
	addl	%edx, %eax
	movl	4(%eax), %eax
	movl	%eax, (%esp)
	call	_strlen
	addl	$1, %eax
	movl	%eax, (%esp)
	call	_malloc
	movl	%eax, -48(%ebp)
	movl	-48(%ebp), %eax
	movb	$0, (%eax)
	movl	-48(%ebp), %eax
	movl	$-1, %ecx
	movl	%eax, %edx
	movl	$0, %eax
	movl	%edx, %edi
	repnz scasb
	movl	%ecx, %eax
	notl	%eax
	leal	-1(%eax), %edx
	movl	-48(%ebp), %eax
	addl	%edx, %eax
	movw	$48, (%eax)
	movl	-40(%ebp), %eax
	movl	4(%eax), %eax
	movl	-16(%ebp), %edx
	sall	$3, %edx
	addl	%edx, %eax
	movl	4(%eax), %eax
	movl	%eax, 4(%esp)
	movl	-48(%ebp), %eax
	movl	%eax, (%esp)
	call	_strcat
	movl	-40(%ebp), %eax
	movl	4(%eax), %eax
	movl	-16(%ebp), %edx
	sall	$3, %edx
	addl	%edx, %eax
	movl	4(%eax), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-40(%ebp), %eax
	movl	4(%eax), %eax
	movl	-16(%ebp), %edx
	sall	$3, %edx
	addl	%eax, %edx
	movl	-48(%ebp), %eax
	movl	%eax, 4(%edx)
	addl	$1, -16(%ebp)
L74:
	movl	-40(%ebp), %eax
	movl	(%eax), %eax
	cmpl	%eax, -16(%ebp)
	jl	L75
	movl	$0, -20(%ebp)
	jmp	L76
L77:
	movl	-40(%ebp), %eax
	movl	4(%eax), %eax
	movl	-20(%ebp), %edx
	sall	$3, %edx
	addl	%eax, %edx
	movl	-32(%ebp), %eax
	movl	4(%eax), %eax
	movl	-20(%ebp), %ecx
	sall	$3, %ecx
	addl	%eax, %ecx
	movl	(%edx), %eax
	movl	4(%edx), %edx
	movl	%eax, (%ecx)
	movl	%edx, 4(%ecx)
	addl	$1, -12(%ebp)
	addl	$1, -20(%ebp)
L76:
	movl	-40(%ebp), %eax
	movl	(%eax), %eax
	cmpl	%eax, -20(%ebp)
	jl	L77
L73:
	cmpl	$0, -44(%ebp)
	je	L64
	movl	$0, -24(%ebp)
	jmp	L78
L79:
	movl	-44(%ebp), %eax
	movl	4(%eax), %eax
	movl	-24(%ebp), %edx
	sall	$3, %edx
	addl	%edx, %eax
	movl	4(%eax), %eax
	movl	%eax, (%esp)
	call	_strlen
	addl	$1, %eax
	movl	%eax, (%esp)
	call	_malloc
	movl	%eax, -52(%ebp)
	movl	-52(%ebp), %eax
	movb	$0, (%eax)
	movl	-52(%ebp), %eax
	movl	$-1, %ecx
	movl	%eax, %edx
	movl	$0, %eax
	movl	%edx, %edi
	repnz scasb
	movl	%ecx, %eax
	notl	%eax
	leal	-1(%eax), %edx
	movl	-52(%ebp), %eax
	addl	%edx, %eax
	movw	$49, (%eax)
	movl	-44(%ebp), %eax
	movl	4(%eax), %eax
	movl	-24(%ebp), %edx
	sall	$3, %edx
	addl	%edx, %eax
	movl	4(%eax), %eax
	movl	%eax, 4(%esp)
	movl	-52(%ebp), %eax
	movl	%eax, (%esp)
	call	_strcat
	movl	-44(%ebp), %eax
	movl	4(%eax), %eax
	movl	-24(%ebp), %edx
	sall	$3, %edx
	addl	%edx, %eax
	movl	4(%eax), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-44(%ebp), %eax
	movl	4(%eax), %eax
	movl	-24(%ebp), %edx
	sall	$3, %edx
	addl	%eax, %edx
	movl	-52(%ebp), %eax
	movl	%eax, 4(%edx)
	addl	$1, -24(%ebp)
L78:
	movl	-44(%ebp), %eax
	movl	(%eax), %eax
	cmpl	%eax, -24(%ebp)
	jl	L79
	movl	$0, -28(%ebp)
	jmp	L80
L81:
	movl	-44(%ebp), %eax
	movl	4(%eax), %eax
	movl	-28(%ebp), %edx
	sall	$3, %edx
	addl	%eax, %edx
	movl	-32(%ebp), %eax
	movl	4(%eax), %eax
	movl	-12(%ebp), %ebx
	movl	-28(%ebp), %ecx
	addl	%ebx, %ecx
	sall	$3, %ecx
	addl	%eax, %ecx
	movl	(%edx), %eax
	movl	4(%edx), %edx
	movl	%eax, (%ecx)
	movl	%edx, 4(%ecx)
	addl	$1, -28(%ebp)
L80:
	movl	-44(%ebp), %eax
	movl	(%eax), %eax
	cmpl	%eax, -28(%ebp)
	jl	L81
L64:
	movl	-32(%ebp), %eax
	jmp	L82
L62:
	movl	$0, %eax
L82:
	addl	$64, %esp
	popl	%ebx
	.cfi_restore 3
	popl	%edi
	.cfi_restore 7
	popl	%ebp
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE27:
	.section .rdata,"dr"
LC3:
	.ascii "w\0"
LC4:
	.ascii "%d:%s\12\0"
	.text
	.globl	_sauveCodeAdaptatif
	.def	_sauveCodeAdaptatif;	.scl	2;	.type	32;	.endef
_sauveCodeAdaptatif:
LFB28:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$40, %esp
	movl	$0, -16(%ebp)
	movl	$0, -20(%ebp)
	movl	$LC3, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_fopen
	movl	%eax, -16(%ebp)
	cmpl	$0, -16(%ebp)
	je	L88
	movl	$0, -12(%ebp)
	jmp	L86
L87:
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_fflush
	movl	12(%ebp), %eax
	movl	4(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$3, %edx
	addl	%edx, %eax
	movl	4(%eax), %edx
	movl	12(%ebp), %eax
	movl	4(%eax), %eax
	movl	-12(%ebp), %ecx
	sall	$3, %ecx
	addl	%ecx, %eax
	movl	(%eax), %eax
	movl	%edx, 12(%esp)
	movl	%eax, 8(%esp)
	movl	$LC4, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_fprintf
	addl	$1, -12(%ebp)
L86:
	movl	12(%ebp), %eax
	movl	(%eax), %eax
	cmpl	%eax, -12(%ebp)
	jl	L87
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_fclose
	jmp	L83
L88:
	nop
L83:
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE28:
	.globl	_litCodeAdaptatif
	.def	_litCodeAdaptatif;	.scl	2;	.type	32;	.endef
_litCodeAdaptatif:
LFB29:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	pushl	%esi
	pushl	%ebx
	subl	$112, %esp
	.cfi_offset 6, -12
	.cfi_offset 3, -16
	movl	$0, -20(%ebp)
	movl	$LC0, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_fopen
	movl	%eax, -20(%ebp)
	cmpl	$0, -20(%ebp)
	jne	L90
	movl	$0, %eax
	jmp	L100
L90:
	movl	$8, (%esp)
	call	_malloc
	movl	%eax, -24(%ebp)
	movl	-24(%ebp), %eax
	movl	$0, (%eax)
	movl	-24(%ebp), %eax
	movl	$0, 4(%eax)
	jmp	L92
L99:
	movl	%esp, %eax
	movl	%eax, %esi
	movl	-24(%ebp), %eax
	movl	(%eax), %eax
	leal	-1(%eax), %edx
	movl	%edx, -28(%ebp)
	leal	0(,%eax,8), %edx
	movl	$16, %eax
	subl	$1, %eax
	addl	%edx, %eax
	movl	$16, %ebx
	movl	$0, %edx
	divl	%ebx
	imull	$16, %eax, %eax
	call	___chkstk_ms
	subl	%eax, %esp
	leal	16(%esp), %eax
	addl	$3, %eax
	shrl	$2, %eax
	sall	$2, %eax
	movl	%eax, -32(%ebp)
	movl	-24(%ebp), %eax
	movl	(%eax), %eax
	testl	%eax, %eax
	jle	L93
	movl	$0, -16(%ebp)
	jmp	L94
L95:
	movl	-24(%ebp), %eax
	movl	4(%eax), %eax
	movl	-16(%ebp), %edx
	sall	$3, %edx
	addl	%edx, %eax
	movl	-32(%ebp), %ecx
	movl	-16(%ebp), %ebx
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, (%ecx,%ebx,8)
	movl	%edx, 4(%ecx,%ebx,8)
	addl	$1, -16(%ebp)
L94:
	movl	-24(%ebp), %eax
	movl	(%eax), %eax
	cmpl	%eax, -16(%ebp)
	jl	L95
L93:
	movl	-24(%ebp), %eax
	movl	(%eax), %eax
	addl	$1, %eax
	leal	0(,%eax,8), %edx
	movl	-24(%ebp), %eax
	movl	4(%eax), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_realloc
	movl	%eax, %edx
	movl	-24(%ebp), %eax
	movl	%edx, 4(%eax)
	movl	-24(%ebp), %eax
	movl	(%eax), %eax
	testl	%eax, %eax
	jle	L96
	movl	$0, -12(%ebp)
	jmp	L97
L98:
	movl	-24(%ebp), %eax
	movl	4(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$3, %edx
	leal	(%eax,%edx), %ecx
	movl	-32(%ebp), %eax
	movl	-12(%ebp), %edx
	leal	(%eax,%edx,8), %edx
	movl	(%edx), %eax
	movl	4(%edx), %edx
	movl	%eax, (%ecx)
	movl	%edx, 4(%ecx)
	addl	$1, -12(%ebp)
L97:
	movl	-24(%ebp), %eax
	movl	(%eax), %eax
	cmpl	%eax, -12(%ebp)
	jl	L98
L96:
	leal	-90(%ebp), %eax
	movl	%eax, (%esp)
	call	_strlen
	movl	%eax, (%esp)
	call	_malloc
	movl	%eax, -36(%ebp)
	leal	-90(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-36(%ebp), %eax
	movl	%eax, (%esp)
	call	_strcpy
	movl	%eax, -36(%ebp)
	movl	-24(%ebp), %eax
	movl	4(%eax), %edx
	movl	-24(%ebp), %eax
	movl	(%eax), %eax
	sall	$3, %eax
	addl	%edx, %eax
	movl	-40(%ebp), %edx
	movl	%edx, (%eax)
	movl	-36(%ebp), %edx
	movl	%edx, 4(%eax)
	movl	-24(%ebp), %eax
	movl	(%eax), %eax
	leal	1(%eax), %edx
	movl	-24(%ebp), %eax
	movl	%edx, (%eax)
	movl	%esi, %esp
L92:
	leal	-90(%ebp), %eax
	movl	%eax, 12(%esp)
	leal	-40(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC4, 4(%esp)
	movl	-20(%ebp), %eax
	movl	%eax, (%esp)
	call	_fscanf
	cmpl	$-1, %eax
	jne	L99
	movl	-20(%ebp), %eax
	movl	%eax, (%esp)
	call	_fflush
	movl	-20(%ebp), %eax
	movl	%eax, (%esp)
	call	_fclose
	movl	-24(%ebp), %eax
L100:
	leal	-8(%ebp), %esp
	popl	%ebx
	.cfi_restore 3
	popl	%esi
	.cfi_restore 6
	popl	%ebp
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE29:
	.globl	_creeArbreCodeAdaptatif
	.def	_creeArbreCodeAdaptatif;	.scl	2;	.type	32;	.endef
_creeArbreCodeAdaptatif:
LFB30:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$40, %esp
	movl	$16, (%esp)
	call	_malloc
	movl	%eax, -24(%ebp)
	movl	-24(%ebp), %eax
	movl	$0, 8(%eax)
	movl	-24(%ebp), %eax
	movl	$0, 12(%eax)
	movl	-24(%ebp), %eax
	movl	$-666, (%eax)
	movl	-24(%ebp), %eax
	movl	$0, 4(%eax)
	movl	$0, -12(%ebp)
	jmp	L102
L112:
	movl	-24(%ebp), %eax
	movl	%eax, -16(%ebp)
	movl	$0, -20(%ebp)
	jmp	L103
L111:
	movl	8(%ebp), %eax
	movl	4(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$3, %edx
	addl	%edx, %eax
	movl	4(%eax), %edx
	movl	-20(%ebp), %eax
	addl	%edx, %eax
	movzbl	(%eax), %eax
	cmpb	$48, %al
	jne	L104
	movl	-16(%ebp), %eax
	movl	8(%eax), %eax
	testl	%eax, %eax
	jne	L105
	movl	$16, (%esp)
	call	_malloc
	movl	%eax, %edx
	movl	-16(%ebp), %eax
	movl	%edx, 8(%eax)
	movl	-16(%ebp), %eax
	movl	8(%eax), %eax
	movl	$0, 8(%eax)
	movl	-16(%ebp), %eax
	movl	8(%eax), %eax
	movl	$0, 12(%eax)
L105:
	movl	-16(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -16(%ebp)
	movl	8(%ebp), %eax
	movl	4(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$3, %edx
	addl	%edx, %eax
	movl	4(%eax), %eax
	movl	%eax, (%esp)
	call	_strlen
	leal	-1(%eax), %edx
	movl	-20(%ebp), %eax
	cmpl	%eax, %edx
	jne	L106
	movl	8(%ebp), %eax
	movl	4(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$3, %edx
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-16(%ebp), %eax
	movl	%edx, (%eax)
	movl	-16(%ebp), %eax
	movl	$0, 4(%eax)
	jmp	L108
L106:
	movl	-16(%ebp), %eax
	movl	$-666, (%eax)
	movl	-16(%ebp), %eax
	movl	$0, 4(%eax)
	jmp	L108
L104:
	movl	8(%ebp), %eax
	movl	4(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$3, %edx
	addl	%edx, %eax
	movl	4(%eax), %edx
	movl	-20(%ebp), %eax
	addl	%edx, %eax
	movzbl	(%eax), %eax
	cmpb	$49, %al
	jne	L108
	movl	-16(%ebp), %eax
	movl	12(%eax), %eax
	testl	%eax, %eax
	jne	L109
	movl	$16, (%esp)
	call	_malloc
	movl	%eax, %edx
	movl	-16(%ebp), %eax
	movl	%edx, 12(%eax)
	movl	-16(%ebp), %eax
	movl	12(%eax), %eax
	movl	$0, 8(%eax)
	movl	-16(%ebp), %eax
	movl	12(%eax), %eax
	movl	$0, 12(%eax)
L109:
	movl	-16(%ebp), %eax
	movl	12(%eax), %eax
	movl	%eax, -16(%ebp)
	movl	8(%ebp), %eax
	movl	4(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$3, %edx
	addl	%edx, %eax
	movl	4(%eax), %eax
	movl	%eax, (%esp)
	call	_strlen
	leal	-1(%eax), %edx
	movl	-20(%ebp), %eax
	cmpl	%eax, %edx
	jne	L110
	movl	8(%ebp), %eax
	movl	4(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$3, %edx
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-16(%ebp), %eax
	movl	%edx, (%eax)
	movl	-16(%ebp), %eax
	movl	$0, 4(%eax)
	jmp	L108
L110:
	movl	-16(%ebp), %eax
	movl	$-666, (%eax)
	movl	-16(%ebp), %eax
	movl	$0, 4(%eax)
L108:
	addl	$1, -20(%ebp)
L103:
	movl	8(%ebp), %eax
	movl	4(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$3, %edx
	addl	%edx, %eax
	movl	4(%eax), %eax
	movl	%eax, (%esp)
	call	_strlen
	movl	%eax, %edx
	movl	-20(%ebp), %eax
	cmpl	%eax, %edx
	ja	L111
	addl	$1, -12(%ebp)
L102:
	movl	8(%ebp), %eax
	movl	(%eax), %eax
	cmpl	%eax, -12(%ebp)
	jl	L112
	movl	-24(%ebp), %eax
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE30:
	.globl	_codeFichier
	.def	_codeFichier;	.scl	2;	.type	32;	.endef
_codeFichier:
LFB31:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$56, %esp
	movl	$0, -24(%ebp)
	movl	$0, -28(%ebp)
	movl	$LC0, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_fopen
	movl	%eax, -24(%ebp)
	cmpl	$0, -24(%ebp)
	je	L127
	movl	$LC3, 4(%esp)
	movl	12(%ebp), %eax
	movl	%eax, (%esp)
	call	_fopen
	movl	%eax, -28(%ebp)
	cmpl	$0, -24(%ebp)
	jne	L117
	movl	-24(%ebp), %eax
	movl	%eax, (%esp)
	call	_fclose
	jmp	L114
L117:
	movl	$2, 8(%esp)
	movl	$0, 4(%esp)
	movl	-24(%ebp), %eax
	movl	%eax, (%esp)
	call	_fseek
	movl	-24(%ebp), %eax
	movl	%eax, (%esp)
	call	_ftell
	movl	%eax, -32(%ebp)
	movl	-24(%ebp), %eax
	movl	%eax, (%esp)
	call	_rewind
	movl	-32(%ebp), %eax
	movl	%eax, (%esp)
	call	_malloc
	movl	%eax, -12(%ebp)
	movl	-12(%ebp), %eax
	movb	$0, (%eax)
	movl	$0, -16(%ebp)
	jmp	L118
L126:
	movl	-24(%ebp), %eax
	movl	%eax, (%esp)
	call	_fgetc
	movl	%eax, -16(%ebp)
	cmpl	$47, -16(%ebp)
	jle	L119
	cmpl	$57, -16(%ebp)
	jle	L120
L119:
	cmpl	$45, -16(%ebp)
	jne	L121
L120:
	movl	-16(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	-12(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC1, 4(%esp)
	movl	-12(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	jmp	L118
L121:
	movl	-12(%ebp), %eax
	movl	%eax, (%esp)
	call	_atoi
	movl	%eax, -36(%ebp)
	movl	-12(%ebp), %eax
	movzbl	(%eax), %eax
	testb	%al, %al
	je	L122
	movl	$0, -20(%ebp)
	jmp	L123
L125:
	movl	16(%ebp), %eax
	movl	4(%eax), %eax
	movl	-20(%ebp), %edx
	sall	$3, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	cmpl	%eax, -36(%ebp)
	jne	L124
	movl	-28(%ebp), %eax
	movl	%eax, (%esp)
	call	_fflush
	movl	16(%ebp), %eax
	movl	4(%eax), %eax
	movl	-20(%ebp), %edx
	sall	$3, %edx
	addl	%edx, %eax
	movl	4(%eax), %eax
	movl	-28(%ebp), %edx
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_fputs
L124:
	addl	$1, -20(%ebp)
L123:
	movl	16(%ebp), %eax
	movl	(%eax), %eax
	cmpl	%eax, -20(%ebp)
	jl	L125
L122:
	movl	-12(%ebp), %eax
	movb	$0, (%eax)
	movl	%eax, -12(%ebp)
L118:
	cmpl	$-1, -16(%ebp)
	jne	L126
	movl	-12(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-24(%ebp), %eax
	movl	%eax, (%esp)
	call	_fclose
	movl	-28(%ebp), %eax
	movl	%eax, (%esp)
	call	_fclose
	jmp	L114
L127:
	nop
L114:
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE31:
	.globl	_isValueIn
	.def	_isValueIn;	.scl	2;	.type	32;	.endef
_isValueIn:
LFB32:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$16, %esp
	movl	$0, -4(%ebp)
	jmp	L129
L132:
	movl	-4(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	8(%ebp), %eax
	addl	%edx, %eax
	movl	(%eax), %eax
	cmpl	%eax, 16(%ebp)
	jne	L130
	movl	$1, %eax
	jmp	L131
L130:
	addl	$1, -4(%ebp)
L129:
	movl	-4(%ebp), %eax
	cmpl	12(%ebp), %eax
	jl	L132
	movl	$0, %eax
L131:
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE32:
	.globl	_decodeFichier
	.def	_decodeFichier;	.scl	2;	.type	32;	.endef
_decodeFichier:
LFB33:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	pushl	%ebx
	subl	$68, %esp
	.cfi_offset 3, -12
	movl	$0, -40(%ebp)
	movl	$0, -44(%ebp)
	movl	$LC0, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_fopen
	movl	%eax, -40(%ebp)
	cmpl	$0, -40(%ebp)
	je	L151
	movl	$LC3, 4(%esp)
	movl	12(%ebp), %eax
	movl	%eax, (%esp)
	call	_fopen
	movl	%eax, -44(%ebp)
	cmpl	$0, -40(%ebp)
	jne	L136
	movl	-40(%ebp), %eax
	movl	%eax, (%esp)
	call	_fclose
	jmp	L133
L136:
	movl	$2, 8(%esp)
	movl	$0, 4(%esp)
	movl	-40(%ebp), %eax
	movl	%eax, (%esp)
	call	_fseek
	movl	-40(%ebp), %eax
	movl	%eax, (%esp)
	call	_ftell
	movl	%eax, -48(%ebp)
	movl	-40(%ebp), %eax
	movl	%eax, (%esp)
	call	_rewind
	movl	$0, -16(%ebp)
	movl	$0, -12(%ebp)
	movl	-48(%ebp), %eax
	movl	%eax, (%esp)
	call	_malloc
	movl	%eax, -20(%ebp)
	movl	-20(%ebp), %eax
	movb	$0, (%eax)
	movl	$0, -24(%ebp)
	jmp	L137
L150:
	movl	-40(%ebp), %eax
	movl	%eax, (%esp)
	call	_fgetc
	movl	%eax, -24(%ebp)
	movl	-20(%ebp), %eax
	movzbl	(%eax), %eax
	testb	%al, %al
	je	L138
	movl	$0, -28(%ebp)
	jmp	L139
L148:
	movl	16(%ebp), %eax
	movl	4(%eax), %eax
	movl	-28(%ebp), %edx
	sall	$3, %edx
	addl	%edx, %eax
	movl	4(%eax), %eax
	movl	-20(%ebp), %edx
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_strcmp
	testl	%eax, %eax
	jne	L140
	movl	16(%ebp), %eax
	movl	4(%eax), %eax
	movl	-28(%ebp), %edx
	sall	$3, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	-12(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_isValueIn
	testl	%eax, %eax
	jne	L141
	cmpl	$0, -12(%ebp)
	je	L142
	movl	%esp, %eax
	movl	%eax, %ebx
	movl	-12(%ebp), %eax
	leal	-1(%eax), %edx
	movl	%edx, -52(%ebp)
	leal	0(,%eax,4), %edx
	movl	$16, %eax
	subl	$1, %eax
	addl	%edx, %eax
	movl	$16, %ecx
	movl	$0, %edx
	divl	%ecx
	imull	$16, %eax, %eax
	call	___chkstk_ms
	subl	%eax, %esp
	leal	16(%esp), %eax
	addl	$3, %eax
	shrl	$2, %eax
	sall	$2, %eax
	movl	%eax, -56(%ebp)
	movl	$0, -32(%ebp)
	jmp	L143
L144:
	movl	-32(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	-16(%ebp), %eax
	addl	%edx, %eax
	movl	(%eax), %ecx
	movl	-56(%ebp), %eax
	movl	-32(%ebp), %edx
	movl	%ecx, (%eax,%edx,4)
	addl	$1, -32(%ebp)
L143:
	movl	-32(%ebp), %eax
	cmpl	-12(%ebp), %eax
	jl	L144
	addl	$1, -12(%ebp)
	movl	-12(%ebp), %eax
	sall	$2, %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_realloc
	movl	%eax, -16(%ebp)
	movl	$0, -36(%ebp)
	jmp	L145
L146:
	movl	-36(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	-16(%ebp), %eax
	leal	(%edx,%eax), %ecx
	movl	-56(%ebp), %eax
	movl	-36(%ebp), %edx
	movl	(%eax,%edx,4), %eax
	movl	%eax, (%ecx)
	addl	$1, -36(%ebp)
L145:
	movl	-36(%ebp), %eax
	cmpl	-12(%ebp), %eax
	jl	L146
	movl	%ebx, %esp
	jmp	L147
L142:
	addl	$1, -12(%ebp)
	movl	-12(%ebp), %eax
	sall	$2, %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_realloc
	movl	%eax, -16(%ebp)
L147:
	movl	16(%ebp), %eax
	movl	4(%eax), %eax
	movl	-28(%ebp), %edx
	sall	$3, %edx
	addl	%edx, %eax
	movl	-12(%ebp), %edx
	addl	$1073741823, %edx
	leal	0(,%edx,4), %ecx
	movl	-16(%ebp), %edx
	addl	%ecx, %edx
	movl	(%eax), %eax
	movl	%eax, (%edx)
	movl	16(%ebp), %eax
	movl	4(%eax), %eax
	movl	-28(%ebp), %edx
	sall	$3, %edx
	addl	%edx, %eax
	movl	4(%eax), %edx
	movl	16(%ebp), %eax
	movl	4(%eax), %eax
	movl	-28(%ebp), %ecx
	sall	$3, %ecx
	addl	%ecx, %eax
	movl	(%eax), %eax
	movl	%edx, 12(%esp)
	movl	%eax, 8(%esp)
	movl	$LC4, 4(%esp)
	movl	-44(%ebp), %eax
	movl	%eax, (%esp)
	call	_fprintf
L141:
	movl	-20(%ebp), %eax
	movb	$0, (%eax)
	movl	%eax, -20(%ebp)
	jmp	L138
L140:
	addl	$1, -28(%ebp)
L139:
	movl	16(%ebp), %eax
	movl	(%eax), %eax
	cmpl	%eax, -28(%ebp)
	jl	L148
L138:
	cmpl	$48, -24(%ebp)
	je	L149
	cmpl	$49, -24(%ebp)
	jne	L137
L149:
	movl	-24(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	-20(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC1, 4(%esp)
	movl	-20(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
L137:
	cmpl	$-1, -24(%ebp)
	jne	L150
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-20(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-40(%ebp), %eax
	movl	%eax, (%esp)
	call	_fclose
	movl	-44(%ebp), %eax
	movl	%eax, (%esp)
	call	_fclose
	jmp	L133
L151:
	nop
L133:
	movl	-4(%ebp), %ebx
	leave
	.cfi_restore 5
	.cfi_restore 3
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE33:
	.globl	_interpretStringToBinary
	.def	_interpretStringToBinary;	.scl	2;	.type	32;	.endef
_interpretStringToBinary:
LFB34:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$40, %esp
	movl	$0, -12(%ebp)
	movl	$0, -16(%ebp)
	jmp	L153
L155:
	movl	-16(%ebp), %edx
	movl	8(%ebp), %eax
	addl	%edx, %eax
	movzbl	(%eax), %eax
	cmpb	$49, %al
	jne	L154
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_strlen
	movl	%eax, %edx
	movl	-16(%ebp), %eax
	subl	%eax, %edx
	movl	%edx, %eax
	subl	$1, %eax
	movl	$1, %edx
	movl	%eax, %ecx
	sall	%cl, %edx
	movl	%edx, %eax
	addl	%eax, -12(%ebp)
L154:
	addl	$1, -16(%ebp)
L153:
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_strlen
	movl	%eax, %edx
	movl	-16(%ebp), %eax
	cmpl	%eax, %edx
	ja	L155
	movl	-12(%ebp), %eax
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE34:
	.section .rdata,"dr"
LC5:
	.ascii "wb\0"
	.text
	.globl	_transcritTexteEnBinaire
	.def	_transcritTexteEnBinaire;	.scl	2;	.type	32;	.endef
_transcritTexteEnBinaire:
LFB35:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	pushl	%ebx
	subl	$52, %esp
	.cfi_offset 3, -12
	movl	$0, -20(%ebp)
	movl	$0, -24(%ebp)
	movl	$LC0, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_fopen
	movl	%eax, -20(%ebp)
	cmpl	$0, -20(%ebp)
	je	L165
	movl	$LC5, 4(%esp)
	movl	12(%ebp), %eax
	movl	%eax, (%esp)
	call	_fopen
	movl	%eax, -24(%ebp)
	cmpl	$0, -20(%ebp)
	jne	L160
	movl	-20(%ebp), %eax
	movl	%eax, (%esp)
	call	_fclose
	jmp	L157
L160:
	movl	$2, 8(%esp)
	movl	$0, 4(%esp)
	movl	-20(%ebp), %eax
	movl	%eax, (%esp)
	call	_fseek
	movl	-20(%ebp), %eax
	movl	%eax, (%esp)
	call	_ftell
	movl	%eax, -28(%ebp)
	movl	-20(%ebp), %eax
	movl	%eax, (%esp)
	call	_rewind
	movl	-28(%ebp), %eax
	movl	%eax, (%esp)
	call	_malloc
	movl	%eax, -12(%ebp)
	movl	-12(%ebp), %eax
	movb	$0, (%eax)
	movl	$0, -16(%ebp)
	jmp	L161
L164:
	movl	-20(%ebp), %eax
	movl	%eax, (%esp)
	call	_fgetc
	movl	%eax, -16(%ebp)
	movl	-12(%ebp), %eax
	movl	%eax, (%esp)
	call	_strlen
	cmpl	$6, %eax
	ja	L162
	cmpl	$-1, -16(%ebp)
	je	L162
	movl	-16(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	-12(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC1, 4(%esp)
	movl	-12(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	jmp	L161
L162:
	movl	-16(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	-12(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC1, 4(%esp)
	movl	-12(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	movl	-12(%ebp), %eax
	movl	%eax, (%esp)
	call	_interpretStringToBinary
	movb	%al, -29(%ebp)
	movl	-12(%ebp), %eax
	movl	%eax, (%esp)
	call	_strlen
	movl	$8, %edx
	subl	%eax, %edx
	movl	%edx, %eax
	cmpl	$7, %eax
	ja	L163
	movzbl	-29(%ebp), %eax
	movzbl	%al, %ebx
	movl	-12(%ebp), %eax
	movl	%eax, (%esp)
	call	_strlen
	movl	$8, %edx
	subl	%eax, %edx
	movl	%edx, %eax
	movl	%eax, %ecx
	sall	%cl, %ebx
	movl	%ebx, %eax
	movb	%al, -29(%ebp)
L163:
	movl	-24(%ebp), %eax
	movl	%eax, (%esp)
	call	_fflush
	movl	-24(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	$1, 8(%esp)
	movl	$1, 4(%esp)
	leal	-29(%ebp), %eax
	movl	%eax, (%esp)
	call	_fwrite
	movl	-12(%ebp), %eax
	movb	$0, (%eax)
	movl	%eax, -12(%ebp)
L161:
	cmpl	$-1, -16(%ebp)
	jne	L164
	movl	-12(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-20(%ebp), %eax
	movl	%eax, (%esp)
	call	_fclose
	movl	-24(%ebp), %eax
	movl	%eax, (%esp)
	call	_fclose
	jmp	L157
L165:
	nop
L157:
	addl	$52, %esp
	popl	%ebx
	.cfi_restore 3
	popl	%ebp
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE35:
	.section .rdata,"dr"
LC6:
	.ascii "rb\0"
	.text
	.globl	_transcritBinaireEnTexte
	.def	_transcritBinaireEnTexte;	.scl	2;	.type	32;	.endef
_transcritBinaireEnTexte:
LFB36:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$56, %esp
	movl	$0, -28(%ebp)
	movl	$0, -32(%ebp)
	movl	$LC6, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_fopen
	movl	%eax, -28(%ebp)
	cmpl	$0, -28(%ebp)
	je	L182
	movl	$LC3, 4(%esp)
	movl	12(%ebp), %eax
	movl	%eax, (%esp)
	call	_fopen
	movl	%eax, -32(%ebp)
	cmpl	$0, -28(%ebp)
	jne	L169
	movl	-28(%ebp), %eax
	movl	%eax, (%esp)
	call	_fclose
	jmp	L166
L169:
	movl	$0, -12(%ebp)
	movl	-28(%ebp), %eax
	movl	%eax, (%esp)
	call	_fgetc
	movl	%eax, -12(%ebp)
	jmp	L170
L181:
	cmpl	$-1, -12(%ebp)
	je	L170
	movl	-12(%ebp), %eax
	movl	%eax, -36(%ebp)
	movl	-28(%ebp), %eax
	movl	%eax, (%esp)
	call	_fgetc
	movl	%eax, -12(%ebp)
	movl	$-1, -16(%ebp)
	movl	$7, -20(%ebp)
	jmp	L171
L173:
	movl	-20(%ebp), %eax
	movl	-36(%ebp), %edx
	movl	%eax, %ecx
	sarl	%cl, %edx
	movl	%edx, %eax
	andl	$1, %eax
	testl	%eax, %eax
	je	L172
	movl	-20(%ebp), %eax
	movl	%eax, -16(%ebp)
L172:
	subl	$1, -20(%ebp)
L171:
	cmpl	$0, -20(%ebp)
	jns	L173
	movl	$7, -24(%ebp)
	jmp	L174
L180:
	cmpl	$-1, -12(%ebp)
	jne	L175
	cmpl	$-1, -16(%ebp)
	je	L178
	movl	-24(%ebp), %eax
	cmpl	-16(%ebp), %eax
	jl	L178
	movl	-24(%ebp), %eax
	movl	-36(%ebp), %edx
	movl	%eax, %ecx
	sarl	%cl, %edx
	movl	%edx, %eax
	andl	$1, %eax
	testl	%eax, %eax
	je	L177
	movl	-32(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	$49, (%esp)
	call	_fputc
	jmp	L178
L177:
	movl	-32(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	$48, (%esp)
	call	_fputc
	jmp	L178
L175:
	movl	-24(%ebp), %eax
	movl	-36(%ebp), %edx
	movl	%eax, %ecx
	sarl	%cl, %edx
	movl	%edx, %eax
	andl	$1, %eax
	testl	%eax, %eax
	je	L179
	movl	-32(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	$49, (%esp)
	call	_fputc
	jmp	L178
L179:
	movl	-32(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	$48, (%esp)
	call	_fputc
L178:
	subl	$1, -24(%ebp)
L174:
	cmpl	$0, -24(%ebp)
	jns	L180
L170:
	cmpl	$-1, -12(%ebp)
	jne	L181
	movl	-28(%ebp), %eax
	movl	%eax, (%esp)
	call	_fclose
	movl	-32(%ebp), %eax
	movl	%eax, (%esp)
	call	_fclose
	jmp	L166
L182:
	nop
L166:
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE36:
	.globl	_compteOccurrenceMotif
	.def	_compteOccurrenceMotif;	.scl	2;	.type	32;	.endef
_compteOccurrenceMotif:
LFB37:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$24, %esp
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_compteOccurrence
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE37:
	.globl	_codeFichierMotif
	.def	_codeFichierMotif;	.scl	2;	.type	32;	.endef
_codeFichierMotif:
LFB38:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$24, %esp
	movl	16(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	12(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_codeFichier
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE38:
	.globl	_decodeFichierMotif
	.def	_decodeFichierMotif;	.scl	2;	.type	32;	.endef
_decodeFichierMotif:
LFB39:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$24, %esp
	movl	16(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	12(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_decodeFichier
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE39:
	.globl	_compteOccurrenceTraj
	.def	_compteOccurrenceTraj;	.scl	2;	.type	32;	.endef
_compteOccurrenceTraj:
LFB40:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$24, %esp
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_compteOccurrence
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE40:
	.globl	_codeFichierTraj
	.def	_codeFichierTraj;	.scl	2;	.type	32;	.endef
_codeFichierTraj:
LFB41:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$24, %esp
	movl	16(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	12(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_codeFichier
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE41:
	.globl	_decodeFichierTraj
	.def	_decodeFichierTraj;	.scl	2;	.type	32;	.endef
_decodeFichierTraj:
LFB42:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$24, %esp
	movl	16(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	12(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_decodeFichier
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE42:
	.section .rdata,"dr"
LC7:
	.ascii "\12%d %d %d %d\0"
	.text
	.globl	_fprintfArbreBinaire
	.def	_fprintfArbreBinaire;	.scl	2;	.type	32;	.endef
_fprintfArbreBinaire:
LFB43:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	pushl	%ebx
	subl	$36, %esp
	.cfi_offset 3, -12
	cmpl	$0, 12(%ebp)
	je	L194
	movl	12(%ebp), %eax
	movl	4(%eax), %eax
	cmpl	$-1, %eax
	je	L193
	movl	12(%ebp), %eax
	movzwl	(%eax), %eax
	movswl	%ax, %ebx
	movl	12(%ebp), %eax
	movl	16(%eax), %ecx
	movl	12(%ebp), %eax
	movl	12(%eax), %edx
	movl	12(%ebp), %eax
	movl	8(%eax), %eax
	movl	%ebx, 20(%esp)
	movl	%ecx, 16(%esp)
	movl	%edx, 12(%esp)
	movl	%eax, 8(%esp)
	movl	$LC7, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_fprintf
L193:
	movl	12(%ebp), %eax
	movl	20(%eax), %eax
	movl	%eax, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_fprintfArbreBinaire
	movl	12(%ebp), %eax
	movl	24(%eax), %eax
	movl	%eax, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_fprintfArbreBinaire
	movl	12(%ebp), %eax
	movl	32(%eax), %eax
	movl	%eax, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_fprintfArbreBinaire
	movl	12(%ebp), %eax
	movl	28(%eax), %eax
	movl	%eax, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_fprintfArbreBinaire
L194:
	nop
	addl	$36, %esp
	popl	%ebx
	.cfi_restore 3
	popl	%ebp
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE43:
	.section .rdata,"dr"
	.align 4
LC8:
	.ascii "./resources/partie6/arbreNB_Texte.txt\0"
LC9:
	.ascii "%d\0"
	.text
	.globl	_sauveArbreNB_Texte
	.def	_sauveArbreNB_Texte;	.scl	2;	.type	32;	.endef
_sauveArbreNB_Texte:
LFB44:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$40, %esp
	movl	$LC3, 4(%esp)
	movl	$LC8, (%esp)
	call	_fopen
	movl	%eax, -12(%ebp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_compteFeuille
	movl	%eax, 8(%esp)
	movl	$LC9, 4(%esp)
	movl	-12(%ebp), %eax
	movl	%eax, (%esp)
	call	_fprintf
	movl	8(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-12(%ebp), %eax
	movl	%eax, (%esp)
	call	_fprintfArbreBinaire
	movl	-12(%ebp), %eax
	movl	%eax, (%esp)
	call	_fclose
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE44:
	.globl	_compteOccurrenceNB
	.def	_compteOccurrenceNB;	.scl	2;	.type	32;	.endef
_compteOccurrenceNB:
LFB45:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$24, %esp
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_compteOccurrence
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE45:
	.globl	_codeFichierNB
	.def	_codeFichierNB;	.scl	2;	.type	32;	.endef
_codeFichierNB:
LFB46:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$24, %esp
	movl	16(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	12(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_codeFichier
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE46:
	.globl	_decodeFichierNB
	.def	_decodeFichierNB;	.scl	2;	.type	32;	.endef
_decodeFichierNB:
LFB47:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$24, %esp
	movl	16(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	12(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_decodeFichier
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE47:
	.globl	_transcritTexteEnBinaireMotif
	.def	_transcritTexteEnBinaireMotif;	.scl	2;	.type	32;	.endef
_transcritTexteEnBinaireMotif:
LFB48:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$24, %esp
	movl	12(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_transcritTexteEnBinaire
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE48:
	.globl	_transcritTexteEnBinaireTraj
	.def	_transcritTexteEnBinaireTraj;	.scl	2;	.type	32;	.endef
_transcritTexteEnBinaireTraj:
LFB49:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$24, %esp
	movl	12(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_transcritTexteEnBinaireMotif
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE49:
	.globl	_transcritTexteEnBinaireNB
	.def	_transcritTexteEnBinaireNB;	.scl	2;	.type	32;	.endef
_transcritTexteEnBinaireNB:
LFB50:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$24, %esp
	movl	12(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_transcritTexteEnBinaire
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE50:
	.globl	_transcritBinaireEnTexteMotif
	.def	_transcritBinaireEnTexteMotif;	.scl	2;	.type	32;	.endef
_transcritBinaireEnTexteMotif:
LFB51:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$24, %esp
	movl	12(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_transcritBinaireEnTexte
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE51:
	.globl	_transcritBinaireEnTexteTraj
	.def	_transcritBinaireEnTexteTraj;	.scl	2;	.type	32;	.endef
_transcritBinaireEnTexteTraj:
LFB52:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$24, %esp
	movl	12(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_transcritBinaireEnTexte
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE52:
	.globl	_transcritBinaireEnTexteNB
	.def	_transcritBinaireEnTexteNB;	.scl	2;	.type	32;	.endef
_transcritBinaireEnTexteNB:
LFB53:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$24, %esp
	movl	12(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_transcritBinaireEnTexte
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE53:
	.ident	"GCC: (MinGW.org GCC-8.2.0-5) 8.2.0"
	.def	_fopen;	.scl	2;	.type	32;	.endef
	.def	_fseek;	.scl	2;	.type	32;	.endef
	.def	_ftell;	.scl	2;	.type	32;	.endef
	.def	_rewind;	.scl	2;	.type	32;	.endef
	.def	_malloc;	.scl	2;	.type	32;	.endef
	.def	_fgetc;	.scl	2;	.type	32;	.endef
	.def	_sprintf;	.scl	2;	.type	32;	.endef
	.def	_atoi;	.scl	2;	.type	32;	.endef
	.def	_realloc;	.scl	2;	.type	32;	.endef
	.def	_free;	.scl	2;	.type	32;	.endef
	.def	_fclose;	.scl	2;	.type	32;	.endef
	.def	_printf;	.scl	2;	.type	32;	.endef
	.def	_strlen;	.scl	2;	.type	32;	.endef
	.def	_strcat;	.scl	2;	.type	32;	.endef
	.def	_fflush;	.scl	2;	.type	32;	.endef
	.def	_fprintf;	.scl	2;	.type	32;	.endef
	.def	_strcpy;	.scl	2;	.type	32;	.endef
	.def	_fscanf;	.scl	2;	.type	32;	.endef
	.def	_fputs;	.scl	2;	.type	32;	.endef
	.def	_strcmp;	.scl	2;	.type	32;	.endef
	.def	_fwrite;	.scl	2;	.type	32;	.endef
	.def	_fputc;	.scl	2;	.type	32;	.endef
	.def	_compteFeuille;	.scl	2;	.type	32;	.endef
