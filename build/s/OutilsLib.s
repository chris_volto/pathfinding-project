	.file	"OutilsLib.c"
	.text
	.def	_systemeBigEndian;	.scl	3;	.type	32;	.endef
_systemeBigEndian:
LFB0:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$16, %esp
	movw	$15, -2(%ebp)
	leal	-2(%ebp), %eax
	movzbl	(%eax), %eax
	testb	%al, %al
	sete	%al
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE0:
	.def	_barriereNatif32;	.scl	3;	.type	32;	.endef
_barriereNatif32:
LFB1:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$16, %esp
	movl	8(%ebp), %eax
	shrl	$24, %eax
	movl	%eax, -4(%ebp)
	movl	8(%ebp), %eax
	sarl	$8, %eax
	andl	$65280, %eax
	orl	%eax, -4(%ebp)
	movl	8(%ebp), %eax
	sall	$8, %eax
	andl	$16711680, %eax
	orl	%eax, -4(%ebp)
	movl	8(%ebp), %eax
	sall	$24, %eax
	orl	%eax, -4(%ebp)
	movl	-4(%ebp), %eax
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE1:
	.def	_barriereNatif16;	.scl	3;	.type	32;	.endef
_barriereNatif16:
LFB2:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$4, %esp
	movl	8(%ebp), %eax
	movw	%ax, -4(%ebp)
	movzwl	-4(%ebp), %eax
	shrw	$8, %ax
	movl	%eax, %edx
	movswl	-4(%ebp), %eax
	sall	$8, %eax
	orl	%edx, %eax
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE2:
	.globl	_little32VersNatif
	.def	_little32VersNatif;	.scl	2;	.type	32;	.endef
_little32VersNatif:
LFB3:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$4, %esp
	call	_systemeBigEndian
	testb	%al, %al
	je	L8
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_barriereNatif32
	jmp	L9
L8:
	movl	8(%ebp), %eax
L9:
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE3:
	.globl	_natif32VersLittle
	.def	_natif32VersLittle;	.scl	2;	.type	32;	.endef
_natif32VersLittle:
LFB4:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$4, %esp
	call	_systemeBigEndian
	testb	%al, %al
	je	L11
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_barriereNatif32
	jmp	L12
L11:
	movl	8(%ebp), %eax
L12:
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE4:
	.globl	_little16VersNatif
	.def	_little16VersNatif;	.scl	2;	.type	32;	.endef
_little16VersNatif:
LFB5:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$8, %esp
	movl	8(%ebp), %eax
	movw	%ax, -4(%ebp)
	call	_systemeBigEndian
	testb	%al, %al
	je	L14
	movswl	-4(%ebp), %eax
	movl	%eax, (%esp)
	call	_barriereNatif16
	jmp	L15
L14:
	movzwl	-4(%ebp), %eax
L15:
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE5:
	.globl	_natif16VersLittle
	.def	_natif16VersLittle;	.scl	2;	.type	32;	.endef
_natif16VersLittle:
LFB6:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$8, %esp
	movl	8(%ebp), %eax
	movw	%ax, -4(%ebp)
	call	_systemeBigEndian
	testb	%al, %al
	je	L17
	movswl	-4(%ebp), %eax
	movl	%eax, (%esp)
	call	_barriereNatif16
	jmp	L18
L17:
	movzwl	-4(%ebp), %eax
L18:
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE6:
	.globl	_big32VersNatif
	.def	_big32VersNatif;	.scl	2;	.type	32;	.endef
_big32VersNatif:
LFB7:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$4, %esp
	call	_systemeBigEndian
	testb	%al, %al
	je	L20
	movl	8(%ebp), %eax
	jmp	L21
L20:
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_barriereNatif32
L21:
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE7:
	.globl	_natif32VersBig
	.def	_natif32VersBig;	.scl	2;	.type	32;	.endef
_natif32VersBig:
LFB8:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$4, %esp
	call	_systemeBigEndian
	testb	%al, %al
	je	L23
	movl	8(%ebp), %eax
	jmp	L24
L23:
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_barriereNatif32
L24:
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE8:
	.globl	_big16VersNatif
	.def	_big16VersNatif;	.scl	2;	.type	32;	.endef
_big16VersNatif:
LFB9:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$8, %esp
	movl	8(%ebp), %eax
	movw	%ax, -4(%ebp)
	call	_systemeBigEndian
	testb	%al, %al
	je	L26
	movzwl	-4(%ebp), %eax
	jmp	L27
L26:
	movswl	-4(%ebp), %eax
	movl	%eax, (%esp)
	call	_barriereNatif16
L27:
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE9:
	.globl	_natif16VersBig
	.def	_natif16VersBig;	.scl	2;	.type	32;	.endef
_natif16VersBig:
LFB10:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$8, %esp
	movl	8(%ebp), %eax
	movw	%ax, -4(%ebp)
	call	_systemeBigEndian
	testb	%al, %al
	je	L29
	movzwl	-4(%ebp), %eax
	jmp	L30
L29:
	movswl	-4(%ebp), %eax
	movl	%eax, (%esp)
	call	_barriereNatif32
L30:
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE10:
	.ident	"GCC: (MinGW.org GCC-8.2.0-5) 8.2.0"
