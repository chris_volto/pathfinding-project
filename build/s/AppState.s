	.file	"AppState.c"
	.text
	.section .rdata,"dr"
	.align 4
LC0:
	.ascii "MiniProjet Algorithmie - Professeur Mme.ROBERT\0"
LC1:
	.ascii "./resources/font/sans.ttf\0"
	.align 4
LC2:
	.ascii "Pour lancer une simulation, cliquer sur une partie.\0"
LC3:
	.ascii "Basculer sur la partie 6\0"
LC4:
	.ascii "Basculer sur la partie 5\0"
LC5:
	.ascii "Basculer sur la partie 4\0"
LC6:
	.ascii "Basculer sur la partie 3\0"
LC7:
	.ascii "Basculer sur la partie 2\0"
LC8:
	.ascii "Basculer sur la partie 1\0"
	.align 4
LC9:
	.ascii "Mini Projet - VOLTO Christophe\0"
LC10:
	.ascii "cant init\0"
LC11:
	.ascii "Clean exit :) .\0"
	.text
	.globl	_SDL_main
	.def	_SDL_main;	.scl	2;	.type	32;	.endef
_SDL_main:
LFB13:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	pushl	%edi
	pushl	%esi
	pushl	%ebx
	subl	$348, %esp
	.cfi_offset 7, -12
	.cfi_offset 6, -16
	.cfi_offset 3, -20
	movl	$LC0, -56(%ebp)
	movl	$805240832, -52(%ebp)
	movl	$805240832, -48(%ebp)
	movl	$900, -44(%ebp)
	movl	$900, -40(%ebp)
	movl	$0, -36(%ebp)
	movl	$4, -32(%ebp)
	movb	$-1, %dl
	movl	$-1, %eax
	movb	%al, %dh
	movl	%edx, %eax
	orl	$16711680, %eax
	movl	%eax, %edx
	movl	%edx, %eax
	orl	$-16777216, %eax
	movl	%eax, %edx
	movb	$0, %cl
	movl	$0, %eax
	movb	%al, %ch
	movl	%ecx, %eax
	andl	$-16711681, %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	andl	$16777215, %eax
	movl	%eax, %ecx
	movb	$0, %bl
	movl	$0, %eax
	movb	%al, %bh
	movl	%ebx, %eax
	andl	$-16711681, %eax
	movl	%eax, %ebx
	movl	%ebx, %eax
	orl	$-16777216, %eax
	movl	%eax, %ebx
	movl	%edx, 44(%esp)
	movl	%ecx, 40(%esp)
	movl	%ebx, 36(%esp)
	movl	$18, 32(%esp)
	movl	$LC1, 28(%esp)
	movl	$0, 24(%esp)
	movl	$0, 20(%esp)
	movl	$802, 16(%esp)
	movl	$646, 12(%esp)
	movl	$0, 8(%esp)
	movl	$0, 4(%esp)
	movl	$1, (%esp)
	call	_initTextArea
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initTextAreas
	movl	%eax, %edx
	movb	$0, -284(%ebp)
	movl	$0, %eax
	movl	-284(%ebp), %ecx
	movb	%al, %ch
	andl	$-16711681, %ecx
	movl	%ecx, %eax
	orl	$-16777216, %eax
	movl	%eax, %ecx
	movb	$0, -144(%ebp)
	movl	$0, %eax
	movl	-144(%ebp), %ebx
	movb	%al, %bh
	andl	$-16711681, %ebx
	movl	%ebx, %eax
	orl	$-16777216, %eax
	movl	%edx, 40(%esp)
	movl	$0, 36(%esp)
	movl	$0, 32(%esp)
	movl	$2, 28(%esp)
	movl	%ecx, 24(%esp)
	movl	%eax, 20(%esp)
	movl	$802, 16(%esp)
	movl	$652, 12(%esp)
	movl	$98, 8(%esp)
	movl	$0, 4(%esp)
	movl	$0, (%esp)
	call	_initFrame
	movl	%eax, -144(%ebp)
	movb	$-1, -140(%ebp)
	movl	$-1, %eax
	movl	-140(%ebp), %ecx
	movb	%al, %ch
	orl	$16711680, %ecx
	movl	%ecx, %eax
	orl	$-16777216, %eax
	movl	%eax, -140(%ebp)
	movb	$0, -280(%ebp)
	movl	$0, %eax
	movl	-280(%ebp), %ebx
	movb	%al, %bh
	andl	$-16711681, %ebx
	movl	%ebx, %eax
	movl	%eax, %edx
	andl	$16777215, %edx
	movl	%edx, %eax
	movl	%eax, %ebx
	movb	$0, -276(%ebp)
	movl	$0, %eax
	movl	-276(%ebp), %ecx
	movb	%al, %ch
	andl	$-16711681, %ecx
	movl	%ecx, %eax
	movl	%eax, %ecx
	orl	$-16777216, %ecx
	movl	%ecx, %eax
	movl	%eax, %edx
	movl	-140(%ebp), %esi
	movl	%esi, 44(%esp)
	movl	%ebx, 40(%esp)
	movl	%edx, 36(%esp)
	movl	$18, 32(%esp)
	movl	$LC1, 28(%esp)
	movl	$0, 24(%esp)
	movl	$0, 20(%esp)
	movl	$802, 16(%esp)
	movl	$646, 12(%esp)
	movl	$0, 8(%esp)
	movl	$0, 4(%esp)
	movl	$1, (%esp)
	call	_initTextArea
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initTextAreas
	movl	%eax, %edx
	movb	$0, -136(%ebp)
	movl	$0, %eax
	movl	-136(%ebp), %ebx
	movb	%al, %bh
	andl	$-16711681, %ebx
	movl	%ebx, %eax
	orl	$-16777216, %eax
	movl	%eax, -136(%ebp)
	movb	$0, -272(%ebp)
	movl	$0, %eax
	movl	-272(%ebp), %ecx
	movb	%al, %ch
	andl	$-16711681, %ecx
	movl	%ecx, %eax
	movl	%eax, %ebx
	orl	$-16777216, %ebx
	movl	%ebx, %eax
	movl	%eax, %ecx
	movl	%edx, 40(%esp)
	movl	$0, 36(%esp)
	movl	$0, 32(%esp)
	movl	$2, 28(%esp)
	movl	-136(%ebp), %edi
	movl	%edi, 24(%esp)
	movl	%ecx, 20(%esp)
	movl	$802, 16(%esp)
	movl	$652, 12(%esp)
	movl	$98, 8(%esp)
	movl	$0, 4(%esp)
	movl	$0, (%esp)
	call	_initFrame
	movl	%eax, -136(%ebp)
	movb	$-1, -132(%ebp)
	movl	$-1, %eax
	movl	-132(%ebp), %edx
	movb	%al, %dh
	orl	$16711680, %edx
	movl	%edx, %eax
	movl	%eax, %edx
	orl	$-16777216, %edx
	movl	%edx, %eax
	movl	%eax, -132(%ebp)
	movb	$0, -268(%ebp)
	movl	$0, %eax
	movl	-268(%ebp), %ebx
	movb	%al, %bh
	andl	$-16711681, %ebx
	movl	%ebx, %eax
	movl	%eax, %ecx
	andl	$16777215, %ecx
	movl	%ecx, %eax
	movl	%eax, %ebx
	movb	$0, -264(%ebp)
	movl	$0, %eax
	movl	-264(%ebp), %ecx
	movb	%al, %ch
	andl	$-16711681, %ecx
	movl	%ecx, %eax
	movl	%eax, %edx
	orl	$-16777216, %edx
	movl	%edx, %eax
	movl	%eax, %edx
	movl	-132(%ebp), %esi
	movl	%esi, 44(%esp)
	movl	%ebx, 40(%esp)
	movl	%edx, 36(%esp)
	movl	$18, 32(%esp)
	movl	$LC1, 28(%esp)
	movl	$0, 24(%esp)
	movl	$0, 20(%esp)
	movl	$802, 16(%esp)
	movl	$646, 12(%esp)
	movl	$0, 8(%esp)
	movl	$0, 4(%esp)
	movl	$1, (%esp)
	call	_initTextArea
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initTextAreas
	movl	%eax, %edx
	movb	$0, -128(%ebp)
	movl	$0, %eax
	movl	-128(%ebp), %ebx
	movb	%al, %bh
	andl	$-16711681, %ebx
	movl	%ebx, %eax
	movl	%eax, %ebx
	orl	$-16777216, %ebx
	movl	%ebx, %eax
	movl	%eax, -128(%ebp)
	movb	$0, -260(%ebp)
	movl	$0, %eax
	movl	-260(%ebp), %ecx
	movb	%al, %ch
	andl	$-16711681, %ecx
	movl	%ecx, %eax
	movl	%eax, %ecx
	orl	$-16777216, %ecx
	movl	%ecx, %eax
	movl	%eax, %ebx
	movl	%edx, 40(%esp)
	movl	$0, 36(%esp)
	movl	$0, 32(%esp)
	movl	$2, 28(%esp)
	movl	-128(%ebp), %edi
	movl	%edi, 24(%esp)
	movl	%ebx, 20(%esp)
	movl	$802, 16(%esp)
	movl	$652, 12(%esp)
	movl	$98, 8(%esp)
	movl	$0, 4(%esp)
	movl	$0, (%esp)
	call	_initFrame
	movl	%eax, -128(%ebp)
	movb	$-1, -124(%ebp)
	movl	$-1, %eax
	movl	-124(%ebp), %edx
	movb	%al, %dh
	orl	$16711680, %edx
	movl	%edx, %eax
	movl	%eax, %edx
	orl	$-16777216, %edx
	movl	%edx, %eax
	movl	%eax, -124(%ebp)
	movb	$0, -256(%ebp)
	movl	$0, %eax
	movl	-256(%ebp), %edx
	movb	%al, %dh
	andl	$-16711681, %edx
	movl	%edx, %eax
	movl	%eax, %ebx
	andl	$16777215, %ebx
	movl	%ebx, %eax
	movl	%eax, %ecx
	movb	$0, -252(%ebp)
	movl	$0, %eax
	movl	-252(%ebp), %ebx
	movb	%al, %bh
	andl	$-16711681, %ebx
	movl	%ebx, %eax
	movl	%eax, %edx
	orl	$-16777216, %edx
	movl	%edx, %eax
	movl	%eax, %edx
	movl	-124(%ebp), %esi
	movl	%esi, 44(%esp)
	movl	%ecx, 40(%esp)
	movl	%edx, 36(%esp)
	movl	$18, 32(%esp)
	movl	$LC1, 28(%esp)
	movl	$0, 24(%esp)
	movl	$0, 20(%esp)
	movl	$802, 16(%esp)
	movl	$646, 12(%esp)
	movl	$0, 8(%esp)
	movl	$0, 4(%esp)
	movl	$1, (%esp)
	call	_initTextArea
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initTextAreas
	movl	%eax, %edx
	movb	$0, -120(%ebp)
	movl	$0, %eax
	movl	-120(%ebp), %ecx
	movb	%al, %ch
	andl	$-16711681, %ecx
	movl	%ecx, %eax
	movl	%eax, %ecx
	orl	$-16777216, %ecx
	movl	%ecx, %eax
	movl	%eax, -120(%ebp)
	movb	$0, -248(%ebp)
	movl	$0, %eax
	movl	-248(%ebp), %ebx
	movb	%al, %bh
	andl	$-16711681, %ebx
	movl	%ebx, %eax
	movl	%eax, %ebx
	orl	$-16777216, %ebx
	movl	%ebx, %eax
	movl	%eax, %ecx
	movl	%edx, 40(%esp)
	movl	$0, 36(%esp)
	movl	$0, 32(%esp)
	movl	$2, 28(%esp)
	movl	-120(%ebp), %edi
	movl	%edi, 24(%esp)
	movl	%ecx, 20(%esp)
	movl	$802, 16(%esp)
	movl	$652, 12(%esp)
	movl	$98, 8(%esp)
	movl	$0, 4(%esp)
	movl	$0, (%esp)
	call	_initFrame
	movl	%eax, -120(%ebp)
	movb	$-1, -116(%ebp)
	movl	$-1, %eax
	movl	-116(%ebp), %edx
	movb	%al, %dh
	orl	$16711680, %edx
	movl	%edx, %eax
	movl	%eax, %ebx
	orl	$-16777216, %ebx
	movl	%ebx, %eax
	movl	%eax, -116(%ebp)
	movb	$0, -244(%ebp)
	movl	$0, %eax
	movl	-244(%ebp), %ecx
	movb	%al, %ch
	andl	$-16711681, %ecx
	movl	%ecx, %eax
	movl	%eax, %edx
	andl	$16777215, %edx
	movl	%edx, %eax
	movl	%eax, %ecx
	movb	$0, -240(%ebp)
	movl	$0, %eax
	movl	-240(%ebp), %edx
	movb	%al, %dh
	andl	$-16711681, %edx
	movl	%edx, %eax
	movl	%eax, %ebx
	orl	$-16777216, %ebx
	movl	%ebx, %eax
	movl	%eax, %edx
	movl	-116(%ebp), %esi
	movl	%esi, 44(%esp)
	movl	%ecx, 40(%esp)
	movl	%edx, 36(%esp)
	movl	$18, 32(%esp)
	movl	$LC1, 28(%esp)
	movl	$0, 24(%esp)
	movl	$0, 20(%esp)
	movl	$802, 16(%esp)
	movl	$646, 12(%esp)
	movl	$0, 8(%esp)
	movl	$0, 4(%esp)
	movl	$1, (%esp)
	call	_initTextArea
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initTextAreas
	movl	%eax, %edx
	movb	$0, -112(%ebp)
	movl	$0, %eax
	movl	-112(%ebp), %ebx
	movb	%al, %bh
	andl	$-16711681, %ebx
	movl	%ebx, %eax
	movl	%eax, %ecx
	orl	$-16777216, %ecx
	movl	%ecx, %eax
	movl	%eax, -112(%ebp)
	movb	$0, -236(%ebp)
	movl	$0, %eax
	movl	-236(%ebp), %ecx
	movb	%al, %ch
	andl	$-16711681, %ecx
	movl	%ecx, %eax
	movl	%eax, %ebx
	orl	$-16777216, %ebx
	movl	%ebx, %eax
	movl	%eax, %ecx
	movl	%edx, 40(%esp)
	movl	$0, 36(%esp)
	movl	$0, 32(%esp)
	movl	$2, 28(%esp)
	movl	-112(%ebp), %edi
	movl	%edi, 24(%esp)
	movl	%ecx, 20(%esp)
	movl	$802, 16(%esp)
	movl	$652, 12(%esp)
	movl	$98, 8(%esp)
	movl	$0, 4(%esp)
	movl	$0, (%esp)
	call	_initFrame
	movl	%eax, -112(%ebp)
	movb	$-1, -108(%ebp)
	movl	$-1, %eax
	movl	-108(%ebp), %edx
	movb	%al, %dh
	orl	$16711680, %edx
	movl	%edx, %eax
	movl	%eax, %edx
	orl	$-16777216, %edx
	movl	%edx, %eax
	movl	%eax, -108(%ebp)
	movb	$0, -232(%ebp)
	movl	$0, %eax
	movl	-232(%ebp), %ebx
	movb	%al, %bh
	andl	$-16711681, %ebx
	movl	%ebx, %eax
	movl	%eax, %ecx
	andl	$16777215, %ecx
	movl	%ecx, %eax
	movl	%eax, %ebx
	movb	$0, -228(%ebp)
	movl	$0, %eax
	movl	-228(%ebp), %ecx
	movb	%al, %ch
	andl	$-16711681, %ecx
	movl	%ecx, %eax
	movl	%eax, %edx
	orl	$-16777216, %edx
	movl	%edx, %eax
	movl	%eax, %edx
	movl	-108(%ebp), %esi
	movl	%esi, 44(%esp)
	movl	%ebx, 40(%esp)
	movl	%edx, 36(%esp)
	movl	$18, 32(%esp)
	movl	$LC1, 28(%esp)
	movl	$0, 24(%esp)
	movl	$0, 20(%esp)
	movl	$802, 16(%esp)
	movl	$646, 12(%esp)
	movl	$0, 8(%esp)
	movl	$0, 4(%esp)
	movl	$1, (%esp)
	call	_initTextArea
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initTextAreas
	movl	%eax, %edx
	movb	$0, -104(%ebp)
	movl	$0, %eax
	movl	-104(%ebp), %ebx
	movb	%al, %bh
	andl	$-16711681, %ebx
	movl	%ebx, %eax
	movl	%eax, %ebx
	orl	$-16777216, %ebx
	movl	%ebx, %eax
	movl	%eax, -104(%ebp)
	movb	$0, -224(%ebp)
	movl	$0, %eax
	movl	-224(%ebp), %ecx
	movb	%al, %ch
	andl	$-16711681, %ecx
	movl	%ecx, %eax
	movl	%eax, %ecx
	orl	$-16777216, %ecx
	movl	%ecx, %eax
	movl	%eax, %ebx
	movl	%edx, 40(%esp)
	movl	$0, 36(%esp)
	movl	$0, 32(%esp)
	movl	$2, 28(%esp)
	movl	-104(%ebp), %edi
	movl	%edi, 24(%esp)
	movl	%ebx, 20(%esp)
	movl	$802, 16(%esp)
	movl	$652, 12(%esp)
	movl	$98, 8(%esp)
	movl	$0, 4(%esp)
	movl	$0, (%esp)
	call	_initFrame
	movl	%eax, -104(%ebp)
	movb	$0, -100(%ebp)
	movl	$0, %eax
	movl	-100(%ebp), %edx
	movb	%al, %dh
	andl	$-16711681, %edx
	movl	%edx, %eax
	movl	%eax, %edx
	andl	$16777215, %edx
	movl	%edx, %eax
	movl	%eax, -100(%ebp)
	movb	$0, -220(%ebp)
	movl	$0, %eax
	movl	-220(%ebp), %edx
	movb	%al, %dh
	andl	$-16711681, %edx
	movl	%edx, %eax
	movl	%eax, %ebx
	andl	$16777215, %ebx
	movl	%ebx, %eax
	movl	%eax, %ecx
	movb	$0, -216(%ebp)
	movl	$0, %eax
	movl	-216(%ebp), %ebx
	movb	%al, %bh
	andl	$-16711681, %ebx
	movl	%ebx, %eax
	movl	%eax, %edx
	orl	$-16777216, %edx
	movl	%edx, %eax
	movl	%eax, %edx
	movl	$0, 48(%esp)
	movl	$1, 44(%esp)
	movl	-100(%ebp), %esi
	movl	%esi, 40(%esp)
	movl	%ecx, 36(%esp)
	movl	%edx, 32(%esp)
	movl	$25, 28(%esp)
	movl	$LC1, 24(%esp)
	movl	$LC2, 20(%esp)
	movl	$0, 16(%esp)
	movl	$802, 12(%esp)
	movl	$652, 8(%esp)
	movl	$0, 4(%esp)
	movl	$0, (%esp)
	call	_initButton
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initButtons
	movl	%eax, %edx
	movb	$0, -96(%ebp)
	movl	$0, %eax
	movl	-96(%ebp), %ecx
	movb	%al, %ch
	andl	$-16711681, %ecx
	movl	%ecx, %eax
	movl	%eax, %ecx
	orl	$-16777216, %ecx
	movl	%ecx, %eax
	movl	%eax, -96(%ebp)
	movb	$-56, -212(%ebp)
	movl	$-56, %eax
	movl	-212(%ebp), %ebx
	movb	%al, %bh
	andl	$-16711681, %ebx
	movl	%ebx, %eax
	orl	$13107200, %eax
	movl	%eax, %ebx
	orl	$-16777216, %ebx
	movl	%ebx, %eax
	movl	%eax, %ecx
	movl	$0, 40(%esp)
	movl	%edx, 36(%esp)
	movl	$0, 32(%esp)
	movl	$2, 28(%esp)
	movl	-96(%ebp), %edi
	movl	%edi, 24(%esp)
	movl	%ecx, 20(%esp)
	movl	$802, 16(%esp)
	movl	$652, 12(%esp)
	movl	$98, 8(%esp)
	movl	$0, 4(%esp)
	movl	$1, (%esp)
	call	_initFrame
	movl	%eax, -96(%ebp)
	movb	$106, -92(%ebp)
	movl	$106, %eax
	movl	-92(%ebp), %edx
	movb	%al, %dh
	andl	$-16711681, %edx
	movl	%edx, %eax
	orl	$6946816, %eax
	movl	%eax, %ebx
	orl	$-16777216, %ebx
	movl	%ebx, %eax
	movl	%eax, -92(%ebp)
	movb	$-16, -208(%ebp)
	movl	$-16, %eax
	movl	-208(%ebp), %ecx
	movb	%al, %ch
	andl	$-16711681, %ecx
	movl	%ecx, %eax
	orl	$15728640, %eax
	movl	%eax, %edx
	orl	$-16777216, %edx
	movl	%edx, %eax
	movl	%eax, %ecx
	movb	$0, -204(%ebp)
	movl	$0, %eax
	movl	-204(%ebp), %edx
	movb	%al, %dh
	andl	$-16711681, %edx
	movl	%edx, %eax
	movl	%eax, %ebx
	orl	$-16777216, %ebx
	movl	%ebx, %eax
	movl	%eax, %edx
	movl	$_affiche_partie6_callback, 48(%esp)
	movl	$1, 44(%esp)
	movl	-92(%ebp), %esi
	movl	%esi, 40(%esp)
	movl	%ecx, 36(%esp)
	movl	%edx, 32(%esp)
	movl	$15, 28(%esp)
	movl	$LC1, 24(%esp)
	movl	$LC3, 20(%esp)
	movl	$2, 16(%esp)
	movl	$70, 12(%esp)
	movl	$200, 8(%esp)
	movl	$702, 4(%esp)
	movl	$25, (%esp)
	call	_initButton
	movl	%eax, -92(%ebp)
	movb	$106, -88(%ebp)
	movl	$106, %eax
	movl	-88(%ebp), %ebx
	movb	%al, %bh
	andl	$-16711681, %ebx
	movl	%ebx, %eax
	orl	$6946816, %eax
	movl	%eax, %ebx
	orl	$-16777216, %ebx
	movl	%ebx, %eax
	movl	%eax, -88(%ebp)
	movb	$-16, -200(%ebp)
	movl	$-16, %eax
	movl	-200(%ebp), %ecx
	movb	%al, %ch
	andl	$-16711681, %ecx
	movl	%ecx, %eax
	orl	$15728640, %eax
	movl	%eax, %edx
	orl	$-16777216, %edx
	movl	%edx, %eax
	movl	%eax, %ecx
	movb	$0, -196(%ebp)
	movl	$0, %eax
	movl	-196(%ebp), %edx
	movb	%al, %dh
	andl	$-16711681, %edx
	movl	%edx, %eax
	movl	%eax, %ebx
	orl	$-16777216, %ebx
	movl	%ebx, %eax
	movl	%eax, %edx
	movl	$_affiche_partie5_callback, 48(%esp)
	movl	$1, 44(%esp)
	movl	-88(%ebp), %edi
	movl	%edi, 40(%esp)
	movl	%ecx, 36(%esp)
	movl	%edx, 32(%esp)
	movl	$15, 28(%esp)
	movl	$LC1, 24(%esp)
	movl	$LC4, 20(%esp)
	movl	$2, 16(%esp)
	movl	$70, 12(%esp)
	movl	$200, 8(%esp)
	movl	$568, 4(%esp)
	movl	$25, (%esp)
	call	_initButton
	movl	%eax, -88(%ebp)
	movb	$106, -84(%ebp)
	movl	$106, %eax
	movl	-84(%ebp), %ebx
	movb	%al, %bh
	andl	$-16711681, %ebx
	movl	%ebx, %eax
	orl	$6946816, %eax
	movl	%eax, %ebx
	orl	$-16777216, %ebx
	movl	%ebx, %eax
	movl	%eax, -84(%ebp)
	movb	$-16, -192(%ebp)
	movl	$-16, %eax
	movl	-192(%ebp), %ecx
	movb	%al, %ch
	andl	$-16711681, %ecx
	movl	%ecx, %eax
	orl	$15728640, %eax
	movl	%eax, %edx
	orl	$-16777216, %edx
	movl	%edx, %eax
	movl	%eax, %ecx
	movb	$0, -188(%ebp)
	movl	$0, %eax
	movl	-188(%ebp), %edx
	movb	%al, %dh
	andl	$-16711681, %edx
	movl	%edx, %eax
	movl	%eax, %ebx
	orl	$-16777216, %ebx
	movl	%ebx, %eax
	movl	%eax, %ebx
	movl	$_affiche_partie4_callback, 48(%esp)
	movl	$1, 44(%esp)
	movl	-84(%ebp), %esi
	movl	%esi, 40(%esp)
	movl	%ecx, 36(%esp)
	movl	%ebx, 32(%esp)
	movl	$15, 28(%esp)
	movl	$LC1, 24(%esp)
	movl	$LC5, 20(%esp)
	movl	$2, 16(%esp)
	movl	$70, 12(%esp)
	movl	$200, 8(%esp)
	movl	$434, 4(%esp)
	movl	$25, (%esp)
	call	_initButton
	movl	%eax, %edi
	movb	$106, -80(%ebp)
	movl	$106, %eax
	movl	-80(%ebp), %ebx
	movb	%al, %bh
	andl	$-16711681, %ebx
	movl	%ebx, %eax
	orl	$6946816, %eax
	movl	%eax, %ebx
	orl	$-16777216, %ebx
	movl	%ebx, %eax
	movl	%eax, -80(%ebp)
	movb	$-16, -184(%ebp)
	movl	$-16, %eax
	movl	-184(%ebp), %ecx
	movb	%al, %ch
	andl	$-16711681, %ecx
	movl	%ecx, %eax
	orl	$15728640, %eax
	movl	%eax, %ebx
	orl	$-16777216, %ebx
	movl	%ebx, %eax
	movl	%eax, %ecx
	movb	$0, -180(%ebp)
	movl	$0, %eax
	movl	-180(%ebp), %edx
	movb	%al, %dh
	andl	$-16711681, %edx
	movl	%edx, %eax
	movl	%eax, %ebx
	orl	$-16777216, %ebx
	movl	%ebx, %eax
	movl	%eax, %ebx
	movl	$_affiche_partie3_callback, 48(%esp)
	movl	$1, 44(%esp)
	movl	-80(%ebp), %esi
	movl	%esi, 40(%esp)
	movl	%ecx, 36(%esp)
	movl	%ebx, 32(%esp)
	movl	$15, 28(%esp)
	movl	$LC1, 24(%esp)
	movl	$LC6, 20(%esp)
	movl	$2, 16(%esp)
	movl	$70, 12(%esp)
	movl	$200, 8(%esp)
	movl	$301, 4(%esp)
	movl	$25, (%esp)
	call	_initButton
	movl	%eax, %esi
	movb	$106, -76(%ebp)
	movl	$106, %eax
	movl	-76(%ebp), %ebx
	movb	%al, %bh
	andl	$-16711681, %ebx
	movl	%ebx, %eax
	orl	$6946816, %eax
	movl	%eax, %ebx
	orl	$-16777216, %ebx
	movl	%ebx, %eax
	movl	%eax, -76(%ebp)
	movb	$-16, -176(%ebp)
	movl	$-16, %eax
	movl	-176(%ebp), %ecx
	movb	%al, %ch
	andl	$-16711681, %ecx
	movl	%ecx, %eax
	orl	$15728640, %eax
	movl	%eax, %ebx
	orl	$-16777216, %ebx
	movl	%ebx, %eax
	movl	%eax, %ecx
	movb	$0, -172(%ebp)
	movl	$0, %eax
	movl	-172(%ebp), %edx
	movb	%al, %dh
	andl	$-16711681, %edx
	movl	%edx, %eax
	movl	%eax, %ebx
	orl	$-16777216, %ebx
	movl	%ebx, %eax
	movl	%eax, %ebx
	movl	$_affiche_partie2_callback, 48(%esp)
	movl	$1, 44(%esp)
	movl	-76(%ebp), %edx
	movl	%edx, 40(%esp)
	movl	%ecx, 36(%esp)
	movl	%ebx, 32(%esp)
	movl	$15, 28(%esp)
	movl	$LC1, 24(%esp)
	movl	$LC7, 20(%esp)
	movl	$2, 16(%esp)
	movl	$70, 12(%esp)
	movl	$200, 8(%esp)
	movl	$167, 4(%esp)
	movl	$25, (%esp)
	call	_initButton
	movl	%eax, %ebx
	movb	$106, -72(%ebp)
	movl	$106, %eax
	movl	-72(%ebp), %ecx
	movb	%al, %ch
	andl	$-16711681, %ecx
	movl	%ecx, %eax
	orl	$6946816, %eax
	movl	%eax, %edx
	orl	$-16777216, %edx
	movl	%edx, %eax
	movl	%eax, -72(%ebp)
	movb	$-16, -68(%ebp)
	movl	$-16, %eax
	movl	-68(%ebp), %edx
	movb	%al, %dh
	andl	$-16711681, %edx
	movl	%edx, %eax
	orl	$15728640, %eax
	movl	%eax, %edx
	orl	$-16777216, %edx
	movl	%edx, %eax
	movl	%eax, -68(%ebp)
	movb	$0, -168(%ebp)
	movl	$0, %eax
	movl	-168(%ebp), %ecx
	movb	%al, %ch
	andl	$-16711681, %ecx
	movl	%ecx, %eax
	movl	%eax, %edx
	orl	$-16777216, %edx
	movl	%edx, %eax
	movl	%eax, %edx
	movl	$_affiche_partie1_callback, 48(%esp)
	movl	$1, 44(%esp)
	movl	-72(%ebp), %ecx
	movl	%ecx, 40(%esp)
	movl	-68(%ebp), %ecx
	movl	%ecx, 36(%esp)
	movl	%edx, 32(%esp)
	movl	$15, 28(%esp)
	movl	$LC1, 24(%esp)
	movl	$LC8, 20(%esp)
	movl	$2, 16(%esp)
	movl	$70, 12(%esp)
	movl	$200, 8(%esp)
	movl	$33, 4(%esp)
	movl	$25, (%esp)
	call	_initButton
	movl	-92(%ebp), %ecx
	movl	%ecx, 24(%esp)
	movl	-88(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	%edi, 16(%esp)
	movl	%esi, 12(%esp)
	movl	%ebx, 8(%esp)
	movl	%eax, 4(%esp)
	movl	$6, (%esp)
	call	_initButtons
	movl	%eax, %edx
	movb	$0, -164(%ebp)
	movl	$0, %eax
	movl	-164(%ebp), %ebx
	movb	%al, %bh
	andl	$-16711681, %ebx
	movl	%ebx, %eax
	movl	%eax, %ebx
	orl	$-16777216, %ebx
	movl	%ebx, %eax
	movl	%eax, %ecx
	movb	$-16, -160(%ebp)
	movl	$-16, %eax
	movl	-160(%ebp), %ebx
	movb	%al, %bh
	andl	$-16711681, %ebx
	movl	%ebx, %eax
	orl	$15728640, %eax
	movl	%eax, %ebx
	orl	$-16777216, %ebx
	movl	%ebx, %eax
	movl	%eax, %ebx
	movl	$0, 40(%esp)
	movl	%edx, 36(%esp)
	movl	$0, 32(%esp)
	movl	$2, 28(%esp)
	movl	%ecx, 24(%esp)
	movl	%ebx, 20(%esp)
	movl	$900, 16(%esp)
	movl	$250, 12(%esp)
	movl	$98, 8(%esp)
	movl	$650, 4(%esp)
	movl	$1, (%esp)
	call	_initFrame
	movl	%eax, %ebx
	movb	$0, -64(%ebp)
	movl	$0, %eax
	movl	-64(%ebp), %edx
	movb	%al, %dh
	andl	$-16711681, %edx
	movl	%edx, %eax
	movl	%eax, %edx
	andl	$16777215, %edx
	movl	%edx, %eax
	movl	%eax, -64(%ebp)
	movb	$0, -156(%ebp)
	movl	$0, %eax
	movl	-156(%ebp), %ecx
	movb	%al, %ch
	andl	$-16711681, %ecx
	movl	%ecx, %eax
	movl	%eax, %edx
	andl	$16777215, %edx
	movl	%edx, %eax
	movl	%eax, %ecx
	movb	$0, -152(%ebp)
	movl	$0, %eax
	movl	-152(%ebp), %edx
	movb	%al, %dh
	andl	$-16711681, %edx
	movl	%edx, %eax
	movl	%eax, %edx
	orl	$-16777216, %edx
	movl	%edx, %eax
	movl	%eax, %edx
	movl	$0, 48(%esp)
	movl	$1, 44(%esp)
	movl	-64(%ebp), %edi
	movl	%edi, 40(%esp)
	movl	%ecx, 36(%esp)
	movl	%edx, 32(%esp)
	movl	$25, 28(%esp)
	movl	$LC1, 24(%esp)
	movl	$LC9, 20(%esp)
	movl	$0, 16(%esp)
	movl	$100, 12(%esp)
	movl	$900, 8(%esp)
	movl	$0, 4(%esp)
	movl	$0, (%esp)
	call	_initButton
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initButtons
	movl	%eax, %edx
	movb	$0, -60(%ebp)
	movl	$0, %eax
	movl	-60(%ebp), %ecx
	movb	%al, %ch
	andl	$-16711681, %ecx
	movl	%ecx, %eax
	movl	%eax, %ecx
	orl	$-16777216, %ecx
	movl	%ecx, %eax
	movl	%eax, -60(%ebp)
	movb	$-16, -148(%ebp)
	movl	$-16, %eax
	movl	-148(%ebp), %ecx
	movb	%al, %ch
	andl	$-16711681, %ecx
	movl	%ecx, %eax
	orl	$15728640, %eax
	movl	%eax, %ecx
	orl	$-16777216, %ecx
	movl	%ecx, %eax
	movl	%eax, %ecx
	movl	$0, 40(%esp)
	movl	%edx, 36(%esp)
	movl	$0, 32(%esp)
	movl	$2, 28(%esp)
	movl	-60(%ebp), %edi
	movl	%edi, 24(%esp)
	movl	%ecx, 20(%esp)
	movl	$100, 16(%esp)
	movl	$900, 12(%esp)
	movl	$0, 8(%esp)
	movl	$0, 4(%esp)
	movl	$1, (%esp)
	call	_initFrame
	movl	-144(%ebp), %esi
	movl	%esi, 36(%esp)
	movl	-136(%ebp), %edi
	movl	%edi, 32(%esp)
	movl	-128(%ebp), %esi
	movl	%esi, 28(%esp)
	movl	-120(%ebp), %edi
	movl	%edi, 24(%esp)
	movl	-112(%ebp), %esi
	movl	%esi, 20(%esp)
	movl	-104(%ebp), %edi
	movl	%edi, 16(%esp)
	movl	-96(%ebp), %esi
	movl	%esi, 12(%esp)
	movl	%ebx, 8(%esp)
	movl	%eax, 4(%esp)
	movl	$9, (%esp)
	call	_initFrames
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initView
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initViews
	movl	%eax, -28(%ebp)
	movl	-56(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-52(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	-48(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	-44(%ebp), %eax
	movl	%eax, 16(%esp)
	movl	-40(%ebp), %eax
	movl	%eax, 20(%esp)
	movl	-36(%ebp), %eax
	movl	%eax, 24(%esp)
	movl	-32(%ebp), %eax
	movl	%eax, 28(%esp)
	movl	-28(%ebp), %eax
	movl	%eax, 32(%esp)
	movl	$1, (%esp)
	call	_initialization
	movl	_appState, %eax
	testl	%eax, %eax
	jne	L3
	movl	$LC10, (%esp)
	call	_SDL_ExitWithError
	jmp	L3
L5:
	movl	_appState, %eax
	addl	$8, %eax
	movl	%eax, (%esp)
	call	_SDL_WaitEvent
	testl	%eax, %eax
	je	L4
	call	_eventHandler
L4:
	call	_update
L3:
	movl	_appState, %eax
	movl	(%eax), %eax
	testl	%eax, %eax
	jne	L5
	call	_cleanEverything
	movl	$LC11, (%esp)
	call	_SDL_Log
	movl	$0, %eax
	addl	$348, %esp
	popl	%ebx
	.cfi_restore 3
	popl	%esi
	.cfi_restore 6
	popl	%edi
	.cfi_restore 7
	popl	%ebp
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE13:
	.ident	"GCC: (MinGW.org GCC-8.2.0-5) 8.2.0"
	.def	_initTextArea;	.scl	2;	.type	32;	.endef
	.def	_initTextAreas;	.scl	2;	.type	32;	.endef
	.def	_initFrame;	.scl	2;	.type	32;	.endef
	.def	_initButton;	.scl	2;	.type	32;	.endef
	.def	_initButtons;	.scl	2;	.type	32;	.endef
	.def	_affiche_partie6_callback;	.scl	2;	.type	32;	.endef
	.def	_affiche_partie5_callback;	.scl	2;	.type	32;	.endef
	.def	_affiche_partie4_callback;	.scl	2;	.type	32;	.endef
	.def	_affiche_partie3_callback;	.scl	2;	.type	32;	.endef
	.def	_affiche_partie2_callback;	.scl	2;	.type	32;	.endef
	.def	_affiche_partie1_callback;	.scl	2;	.type	32;	.endef
	.def	_initFrames;	.scl	2;	.type	32;	.endef
	.def	_initView;	.scl	2;	.type	32;	.endef
	.def	_initViews;	.scl	2;	.type	32;	.endef
	.def	_initialization;	.scl	2;	.type	32;	.endef
	.def	_SDL_ExitWithError;	.scl	2;	.type	32;	.endef
	.def	_SDL_WaitEvent;	.scl	2;	.type	32;	.endef
	.def	_eventHandler;	.scl	2;	.type	32;	.endef
	.def	_update;	.scl	2;	.type	32;	.endef
	.def	_cleanEverything;	.scl	2;	.type	32;	.endef
	.def	_SDL_Log;	.scl	2;	.type	32;	.endef
