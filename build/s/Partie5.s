	.file	"Partie5.c"
	.text
	.globl	_comparePoints
	.def	_comparePoints;	.scl	2;	.type	32;	.endef
_comparePoints:
LFB17:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	movl	8(%ebp), %edx
	movl	16(%ebp), %eax
	cmpl	%eax, %edx
	jne	L2
	movl	12(%ebp), %edx
	movl	20(%ebp), %eax
	cmpl	%eax, %edx
	je	L3
L2:
	movl	$0, %eax
	jmp	L4
L3:
	movl	$1, %eax
L4:
	popl	%ebp
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE17:
	.globl	_positionVoisinage
	.def	_positionVoisinage;	.scl	2;	.type	32;	.endef
_positionVoisinage:
LFB18:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$80, %esp
	movl	16(%ebp), %eax
	subl	$1, %eax
	movl	%eax, -8(%ebp)
	movl	20(%ebp), %eax
	addl	$1, %eax
	movl	%eax, -4(%ebp)
	movl	16(%ebp), %eax
	movl	%eax, -16(%ebp)
	movl	20(%ebp), %eax
	addl	$1, %eax
	movl	%eax, -12(%ebp)
	movl	16(%ebp), %eax
	addl	$1, %eax
	movl	%eax, -24(%ebp)
	movl	20(%ebp), %eax
	addl	$1, %eax
	movl	%eax, -20(%ebp)
	movl	16(%ebp), %eax
	addl	$1, %eax
	movl	%eax, -32(%ebp)
	movl	20(%ebp), %eax
	movl	%eax, -28(%ebp)
	movl	16(%ebp), %eax
	addl	$1, %eax
	movl	%eax, -40(%ebp)
	movl	20(%ebp), %eax
	subl	$1, %eax
	movl	%eax, -36(%ebp)
	movl	16(%ebp), %eax
	movl	%eax, -48(%ebp)
	movl	20(%ebp), %eax
	subl	$1, %eax
	movl	%eax, -44(%ebp)
	movl	16(%ebp), %eax
	subl	$1, %eax
	movl	%eax, -56(%ebp)
	movl	20(%ebp), %eax
	subl	$1, %eax
	movl	%eax, -52(%ebp)
	movl	16(%ebp), %eax
	subl	$1, %eax
	movl	%eax, -64(%ebp)
	movl	20(%ebp), %eax
	movl	%eax, -60(%ebp)
	movl	-8(%ebp), %eax
	movl	-4(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	8(%ebp), %eax
	movl	12(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	testb	%al, %al
	je	L6
	movl	$0, %eax
	jmp	L16
L6:
	movl	-16(%ebp), %eax
	movl	-12(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	8(%ebp), %eax
	movl	12(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	testb	%al, %al
	je	L8
	movl	$1, %eax
	jmp	L16
L8:
	movl	-24(%ebp), %eax
	movl	-20(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	8(%ebp), %eax
	movl	12(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	testb	%al, %al
	je	L9
	movl	$2, %eax
	jmp	L16
L9:
	movl	-32(%ebp), %eax
	movl	-28(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	8(%ebp), %eax
	movl	12(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	testb	%al, %al
	je	L10
	movl	$3, %eax
	jmp	L16
L10:
	movl	-40(%ebp), %eax
	movl	-36(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	8(%ebp), %eax
	movl	12(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	testb	%al, %al
	je	L11
	movl	$4, %eax
	jmp	L16
L11:
	movl	-48(%ebp), %eax
	movl	-44(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	8(%ebp), %eax
	movl	12(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	testb	%al, %al
	je	L12
	movl	$5, %eax
	jmp	L16
L12:
	movl	-56(%ebp), %eax
	movl	-52(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	8(%ebp), %eax
	movl	12(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	testb	%al, %al
	je	L13
	movl	$6, %eax
	jmp	L16
L13:
	movl	-64(%ebp), %eax
	movl	-60(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	8(%ebp), %eax
	movl	12(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	testb	%al, %al
	je	L14
	movl	$7, %eax
	jmp	L16
L14:
	movl	16(%ebp), %eax
	movl	20(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	8(%ebp), %eax
	movl	12(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	testb	%al, %al
	je	L15
	movl	$8, %eax
	jmp	L16
L15:
	movl	$-1, %eax
L16:
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE18:
	.globl	_comparePointsVariadic
	.def	_comparePointsVariadic;	.scl	2;	.type	32;	.endef
_comparePointsVariadic:
LFB19:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$16, %esp
	movl	8(%ebp), %edx
	movl	16(%ebp), %eax
	cmpl	%eax, %edx
	jne	L18
	movl	12(%ebp), %edx
	movl	20(%ebp), %eax
	cmpl	%eax, %edx
	je	L19
L18:
	movl	$0, %eax
	jmp	L25
L19:
	leal	28(%ebp), %eax
	movl	%eax, -8(%ebp)
	movl	$0, -4(%ebp)
	jmp	L21
L24:
	movl	-8(%ebp), %eax
	leal	8(%eax), %edx
	movl	%edx, -8(%ebp)
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, -16(%ebp)
	movl	%edx, -12(%ebp)
	movl	8(%ebp), %edx
	movl	-16(%ebp), %eax
	cmpl	%eax, %edx
	jne	L22
	movl	12(%ebp), %edx
	movl	-12(%ebp), %eax
	cmpl	%eax, %edx
	je	L23
L22:
	movl	$0, %eax
	jmp	L25
L23:
	addl	$1, -4(%ebp)
L21:
	movl	-4(%ebp), %eax
	cmpl	24(%ebp), %eax
	jl	L24
	movl	$1, %eax
L25:
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE19:
	.globl	_creeMaillon
	.def	_creeMaillon;	.scl	2;	.type	32;	.endef
_creeMaillon:
LFB20:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$40, %esp
	movl	$12, (%esp)
	call	_malloc
	movl	%eax, -12(%ebp)
	movl	8(%ebp), %ecx
	movl	12(%ebp), %edx
	movl	-12(%ebp), %eax
	movl	%ecx, (%eax)
	movl	-12(%ebp), %eax
	movl	%edx, 4(%eax)
	movl	-12(%ebp), %eax
	movl	$0, 8(%eax)
	movl	-12(%ebp), %eax
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE20:
	.globl	_insererMaillonAvant
	.def	_insererMaillonAvant;	.scl	2;	.type	32;	.endef
_insererMaillonAvant:
LFB21:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$16, %esp
	movl	8(%ebp), %eax
	movl	%eax, -4(%ebp)
	movl	$0, -8(%ebp)
	jmp	L29
L32:
	movl	-8(%ebp), %eax
	addl	$1, %eax
	cmpl	%eax, 12(%ebp)
	jne	L30
	movl	-4(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -12(%ebp)
	movl	-4(%ebp), %eax
	movl	16(%ebp), %edx
	movl	%edx, 8(%eax)
	movl	16(%ebp), %eax
	movl	-12(%ebp), %edx
	movl	%edx, 8(%eax)
	jmp	L28
L30:
	addl	$1, -8(%ebp)
	movl	-4(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -4(%ebp)
L29:
	cmpl	$0, -4(%ebp)
	jne	L32
L28:
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE21:
	.globl	_ajouterMaillon
	.def	_ajouterMaillon;	.scl	2;	.type	32;	.endef
_ajouterMaillon:
LFB22:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$16, %esp
	movl	8(%ebp), %eax
	movl	%eax, -4(%ebp)
	jmp	L34
L35:
	movl	-4(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -4(%ebp)
L34:
	movl	-4(%ebp), %eax
	movl	8(%eax), %eax
	testl	%eax, %eax
	jne	L35
	movl	-4(%ebp), %eax
	movl	12(%ebp), %edx
	movl	%edx, 8(%eax)
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE22:
	.globl	_libereMaillon
	.def	_libereMaillon;	.scl	2;	.type	32;	.endef
_libereMaillon:
LFB23:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$24, %esp
	movl	8(%ebp), %eax
	movl	(%eax), %eax
	testl	%eax, %eax
	je	L37
	movl	8(%ebp), %eax
	movl	(%eax), %eax
	movl	%eax, (%esp)
	call	_free
L37:
	movl	$0, 8(%ebp)
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE23:
	.globl	_trouverIndexParValeur
	.def	_trouverIndexParValeur;	.scl	2;	.type	32;	.endef
_trouverIndexParValeur:
LFB24:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$16, %esp
	movl	8(%ebp), %eax
	movl	%eax, -4(%ebp)
	movl	$0, -8(%ebp)
	jmp	L39
L42:
	movl	-4(%ebp), %eax
	movl	(%eax), %edx
	movl	12(%ebp), %eax
	cmpl	%eax, %edx
	jne	L40
	movl	-4(%ebp), %eax
	movl	4(%eax), %edx
	movl	16(%ebp), %eax
	cmpl	%eax, %edx
	jne	L40
	movl	-8(%ebp), %eax
	jmp	L41
L40:
	addl	$1, -8(%ebp)
	movl	-4(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -4(%ebp)
L39:
	movl	-4(%ebp), %eax
	movl	8(%eax), %eax
	testl	%eax, %eax
	jne	L42
	movl	$-1, %eax
L41:
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE24:
	.globl	_supprimerMaillonParValeur
	.def	_supprimerMaillonParValeur;	.scl	2;	.type	32;	.endef
_supprimerMaillonParValeur:
LFB25:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$40, %esp
	movl	8(%ebp), %eax
	movl	%eax, -12(%ebp)
	jmp	L44
L47:
	movl	-12(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -16(%ebp)
	movl	-16(%ebp), %eax
	movl	(%eax), %edx
	movl	12(%ebp), %eax
	cmpl	%eax, %edx
	jne	L45
	movl	-16(%ebp), %eax
	movl	4(%eax), %edx
	movl	16(%ebp), %eax
	cmpl	%eax, %edx
	jne	L45
	movl	-12(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -20(%ebp)
	movl	-12(%ebp), %eax
	movl	8(%eax), %eax
	movl	8(%eax), %edx
	movl	-12(%ebp), %eax
	movl	%edx, 8(%eax)
	leal	-20(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereMaillon
	jmp	L43
L45:
	movl	-12(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -12(%ebp)
L44:
	movl	-12(%ebp), %eax
	movl	8(%eax), %eax
	testl	%eax, %eax
	jne	L47
L43:
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE25:
	.globl	_supprimerMaillon
	.def	_supprimerMaillon;	.scl	2;	.type	32;	.endef
_supprimerMaillon:
LFB26:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$40, %esp
	movl	8(%ebp), %eax
	movl	%eax, -12(%ebp)
	jmp	L49
L50:
	movl	-12(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -12(%ebp)
L49:
	movl	-12(%ebp), %eax
	movl	8(%eax), %edx
	movl	12(%ebp), %eax
	movl	(%eax), %eax
	cmpl	%eax, %edx
	jne	L50
	movl	-12(%ebp), %eax
	movl	8(%eax), %eax
	movl	8(%eax), %eax
	movl	%eax, -16(%ebp)
	movl	12(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereMaillon
	movl	-12(%ebp), %eax
	movl	-16(%ebp), %edx
	movl	%edx, 8(%eax)
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE26:
	.globl	_dessineCarre
	.def	_dessineCarre;	.scl	2;	.type	32;	.endef
_dessineCarre:
LFB27:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$24, %esp
	movl	20(%ebp), %eax
	movl	%eax, -4(%ebp)
	jmp	L52
L55:
	movl	24(%ebp), %eax
	movl	%eax, -8(%ebp)
	jmp	L53
L54:
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	movl	-4(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-8(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	12(%ebp), %eax
	movw	%ax, (%edx)
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	movl	-4(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-8(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	14(%ebp), %eax
	movw	%ax, (%edx)
	movl	8(%ebp), %eax
	movl	16(%eax), %eax
	movl	-4(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-8(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	16(%ebp), %eax
	movw	%ax, (%edx)
	movzwl	12(%ebp), %eax
	movw	%ax, -24(%ebp)
	filds	-24(%ebp)
	fldl	LC0
	fmulp	%st, %st(1)
	movzwl	14(%ebp), %eax
	movw	%ax, -24(%ebp)
	filds	-24(%ebp)
	fldl	LC1
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movzwl	16(%ebp), %eax
	movw	%ax, -24(%ebp)
	filds	-24(%ebp)
	fldl	LC2
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	8(%ebp), %eax
	movl	20(%eax), %eax
	movl	-4(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-8(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	fnstcw	-18(%ebp)
	movzwl	-18(%ebp), %eax
	orb	$12, %ah
	movw	%ax, -20(%ebp)
	fldcw	-20(%ebp)
	fistps	-22(%ebp)
	fldcw	-18(%ebp)
	movzwl	-22(%ebp), %eax
	movw	%ax, (%edx)
	addl	$1, -8(%ebp)
L53:
	movl	24(%ebp), %edx
	movl	28(%ebp), %eax
	addl	%eax, %edx
	movl	-8(%ebp), %eax
	cmpl	%eax, %edx
	ja	L54
	addl	$1, -4(%ebp)
L52:
	movl	20(%ebp), %edx
	movl	28(%ebp), %eax
	addl	%eax, %edx
	movl	-4(%ebp), %eax
	cmpl	%eax, %edx
	ja	L55
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE27:
	.globl	_calculeTrajectoire
	.def	_calculeTrajectoire;	.scl	2;	.type	32;	.endef
_calculeTrajectoire:
LFB28:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	pushl	%esi
	pushl	%ebx
	subl	$640, %esp
	.cfi_offset 6, -12
	.cfi_offset 3, -16
	movl	20(%ebp), %eax
	leal	3(%eax), %edx
	movl	8(%ebp), %eax
	movl	(%eax), %eax
	subl	$1, %eax
	cmpl	%eax, %edx
	ja	L57
	movl	24(%ebp), %eax
	leal	3(%eax), %edx
	movl	8(%ebp), %eax
	movl	4(%eax), %eax
	subl	$1, %eax
	cmpl	%eax, %edx
	ja	L57
	movl	28(%ebp), %eax
	leal	3(%eax), %edx
	movl	8(%ebp), %eax
	movl	(%eax), %eax
	subl	$1, %eax
	cmpl	%eax, %edx
	ja	L57
	movl	32(%ebp), %eax
	leal	3(%eax), %edx
	movl	8(%ebp), %eax
	movl	4(%eax), %eax
	subl	$1, %eax
	cmpl	%eax, %edx
	ja	L57
	movl	24(%ebp), %eax
	subl	$3, %eax
	movl	%eax, %ecx
	movl	20(%ebp), %eax
	subl	$3, %eax
	leal	-226(%ebp), %edx
	movl	$7, 20(%esp)
	movl	$7, 16(%esp)
	movl	%ecx, 12(%esp)
	movl	%eax, 8(%esp)
	movl	12(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	%edx, (%esp)
	call	_estCouleurUniforme
	movl	16(%ebp), %eax
	movl	4(%eax), %edx
	movl	-226(%ebp), %eax
	movl	%eax, 8(%esp)
	movzwl	-222(%ebp), %eax
	movw	%ax, 12(%esp)
	movl	(%edx), %eax
	movl	%eax, (%esp)
	movzwl	4(%edx), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleur
	xorl	$1, %eax
	testb	%al, %al
	jne	L57
	movl	32(%ebp), %eax
	subl	$3, %eax
	movl	%eax, %ecx
	movl	28(%ebp), %eax
	subl	$3, %eax
	leal	-220(%ebp), %edx
	movl	$7, 20(%esp)
	movl	$7, 16(%esp)
	movl	%ecx, 12(%esp)
	movl	%eax, 8(%esp)
	movl	12(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	%edx, (%esp)
	call	_estCouleurUniforme
	movl	16(%ebp), %eax
	movl	4(%eax), %edx
	movl	-220(%ebp), %eax
	movl	%eax, 8(%esp)
	movzwl	-216(%ebp), %eax
	movw	%ax, 12(%esp)
	movl	(%edx), %eax
	movl	%eax, (%esp)
	movzwl	4(%edx), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleur
	xorl	$1, %eax
	testb	%al, %al
	je	L58
L57:
	movl	$0, %eax
	jmp	L59
L58:
	movl	$28, (%esp)
	call	_malloc
	movl	%eax, -64(%ebp)
	movl	20(%ebp), %edx
	movl	24(%ebp), %ecx
	movl	-64(%ebp), %eax
	movl	%edx, 12(%eax)
	movl	-64(%ebp), %eax
	movl	%ecx, 16(%eax)
	movl	28(%ebp), %edx
	movl	32(%ebp), %ecx
	movl	-64(%ebp), %eax
	movl	%edx, 20(%eax)
	movl	-64(%ebp), %eax
	movl	%ecx, 24(%eax)
	movl	20(%ebp), %edx
	movl	24(%ebp), %ecx
	movl	-64(%ebp), %eax
	movl	%edx, 4(%eax)
	movl	-64(%ebp), %eax
	movl	%ecx, 8(%eax)
	movl	-64(%ebp), %eax
	movl	16(%eax), %edx
	movl	12(%eax), %eax
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, %edx
	movl	-64(%ebp), %eax
	movl	%edx, (%eax)
	movl	20(%ebp), %edx
	movl	28(%ebp), %eax
	cmpl	%eax, %edx
	jne	L60
	movl	20(%ebp), %eax
	movl	24(%ebp), %edx
	movl	%eax, -236(%ebp)
	movl	%edx, -232(%ebp)
	jmp	L61
L67:
	movl	-236(%ebp), %edx
	movl	28(%ebp), %eax
	cmpl	%eax, %edx
	jnb	L62
	movl	-236(%ebp), %eax
	leal	1(%eax), %edx
	movl	-232(%ebp), %eax
	movl	%edx, -236(%ebp)
	movl	%eax, -232(%ebp)
	movl	-236(%ebp), %eax
	movl	-232(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, %edx
	movl	-64(%ebp), %eax
	movl	(%eax), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_ajouterMaillon
	jmp	L61
L62:
	movl	-236(%ebp), %edx
	movl	28(%ebp), %eax
	cmpl	%eax, %edx
	jbe	L63
	movl	-236(%ebp), %eax
	leal	-1(%eax), %edx
	movl	-232(%ebp), %eax
	movl	%edx, -236(%ebp)
	movl	%eax, -232(%ebp)
	movl	-236(%ebp), %eax
	movl	-232(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, %edx
	movl	-64(%ebp), %eax
	movl	(%eax), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_ajouterMaillon
	jmp	L61
L63:
	movl	-232(%ebp), %edx
	movl	32(%ebp), %eax
	cmpl	%eax, %edx
	jnb	L64
	movl	-236(%ebp), %edx
	movl	-232(%ebp), %eax
	addl	$1, %eax
	movl	%edx, -236(%ebp)
	movl	%eax, -232(%ebp)
	movl	-236(%ebp), %eax
	movl	-232(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, %edx
	movl	-64(%ebp), %eax
	movl	(%eax), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_ajouterMaillon
	jmp	L61
L64:
	movl	-232(%ebp), %edx
	movl	32(%ebp), %eax
	cmpl	%eax, %edx
	jbe	L231
	movl	-236(%ebp), %edx
	movl	-232(%ebp), %eax
	subl	$1, %eax
	movl	%edx, -236(%ebp)
	movl	%eax, -232(%ebp)
	movl	-236(%ebp), %eax
	movl	-232(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, %edx
	movl	-64(%ebp), %eax
	movl	(%eax), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_ajouterMaillon
L61:
	movl	28(%ebp), %eax
	movl	32(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-236(%ebp), %eax
	movl	-232(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	jne	L67
	jmp	L68
L60:
	movl	32(%ebp), %eax
	movl	%eax, -608(%ebp)
	movl	$0, -604(%ebp)
	fildq	-608(%ebp)
	fstps	-556(%ebp)
	flds	-556(%ebp)
	movl	24(%ebp), %eax
	movl	%eax, -608(%ebp)
	movl	$0, -604(%ebp)
	fildq	-608(%ebp)
	fstps	-556(%ebp)
	flds	-556(%ebp)
	fsubrp	%st, %st(1)
	movl	28(%ebp), %eax
	movl	%eax, -608(%ebp)
	movl	$0, -604(%ebp)
	fildq	-608(%ebp)
	fstps	-556(%ebp)
	flds	-556(%ebp)
	movl	20(%ebp), %eax
	movl	%eax, -608(%ebp)
	movl	$0, -604(%ebp)
	fildq	-608(%ebp)
	fstps	-556(%ebp)
	flds	-556(%ebp)
	fsubrp	%st, %st(1)
	fdivrp	%st, %st(1)
	fstps	-68(%ebp)
	movl	24(%ebp), %eax
	movl	%eax, -608(%ebp)
	movl	$0, -604(%ebp)
	fildq	-608(%ebp)
	fstps	-556(%ebp)
	flds	-556(%ebp)
	movl	20(%ebp), %eax
	movl	%eax, -608(%ebp)
	movl	$0, -604(%ebp)
	fildq	-608(%ebp)
	fstps	-556(%ebp)
	flds	-556(%ebp)
	fmuls	-68(%ebp)
	fsubrp	%st, %st(1)
	fstps	-72(%ebp)
	movl	-64(%ebp), %eax
	movl	(%eax), %eax
	movl	%eax, -12(%ebp)
	movl	20(%ebp), %eax
	movl	%eax, -16(%ebp)
	jmp	L69
L80:
	fildl	-16(%ebp)
	fmuls	-68(%ebp)
	fadds	-72(%ebp)
	fnstcw	-558(%ebp)
	movzwl	-558(%ebp), %eax
	orb	$12, %ah
	movw	%ax, -560(%ebp)
	fldcw	-560(%ebp)
	fistpl	-76(%ebp)
	fldcw	-558(%ebp)
	movl	-16(%ebp), %eax
	movl	%eax, %ebx
	movl	-76(%ebp), %eax
	movl	%eax, %esi
	movl	%ebx, (%esp)
	movl	%esi, 4(%esp)
	call	_creeMaillon
	movl	%eax, -80(%ebp)
	movl	-12(%ebp), %eax
	movl	(%eax), %eax
	movl	%eax, -244(%ebp)
	movl	-12(%ebp), %eax
	movl	4(%eax), %eax
	movl	%eax, -240(%ebp)
	movl	-16(%ebp), %eax
	movl	%eax, -252(%ebp)
	movl	-76(%ebp), %eax
	movl	%eax, -248(%ebp)
	movl	-252(%ebp), %eax
	movl	-248(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-244(%ebp), %eax
	movl	-240(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_positionVoisinage
	cmpl	$8, %eax
	je	L70
	jmp	L71
L76:
	movl	-244(%ebp), %edx
	movl	-252(%ebp), %eax
	cmpl	%eax, %edx
	jnb	L72
	movl	-244(%ebp), %eax
	leal	1(%eax), %edx
	movl	-240(%ebp), %eax
	movl	%edx, -244(%ebp)
	movl	%eax, -240(%ebp)
	movl	-244(%ebp), %eax
	movl	-240(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, %edx
	movl	-64(%ebp), %eax
	movl	(%eax), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_ajouterMaillon
	jmp	L71
L72:
	movl	-244(%ebp), %edx
	movl	-252(%ebp), %eax
	cmpl	%eax, %edx
	jbe	L73
	movl	-244(%ebp), %eax
	leal	-1(%eax), %edx
	movl	-240(%ebp), %eax
	movl	%edx, -244(%ebp)
	movl	%eax, -240(%ebp)
	movl	-244(%ebp), %eax
	movl	-240(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, %edx
	movl	-64(%ebp), %eax
	movl	(%eax), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_ajouterMaillon
	jmp	L71
L73:
	movl	-240(%ebp), %edx
	movl	-248(%ebp), %eax
	cmpl	%eax, %edx
	jnb	L74
	movl	-244(%ebp), %edx
	movl	-240(%ebp), %eax
	addl	$1, %eax
	movl	%edx, -244(%ebp)
	movl	%eax, -240(%ebp)
	movl	-244(%ebp), %eax
	movl	-240(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, %edx
	movl	-64(%ebp), %eax
	movl	(%eax), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_ajouterMaillon
	jmp	L71
L74:
	movl	-240(%ebp), %edx
	movl	-248(%ebp), %eax
	cmpl	%eax, %edx
	jbe	L232
	movl	-244(%ebp), %edx
	movl	-240(%ebp), %eax
	subl	$1, %eax
	movl	%edx, -244(%ebp)
	movl	%eax, -240(%ebp)
	movl	-244(%ebp), %eax
	movl	-240(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, %edx
	movl	-64(%ebp), %eax
	movl	(%eax), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_ajouterMaillon
L71:
	movl	-252(%ebp), %eax
	movl	-248(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-244(%ebp), %eax
	movl	-240(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_positionVoisinage
	cmpl	$1, %eax
	je	L70
	movl	-252(%ebp), %eax
	movl	-248(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-244(%ebp), %eax
	movl	-240(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_positionVoisinage
	cmpl	$3, %eax
	je	L70
	movl	-252(%ebp), %eax
	movl	-248(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-244(%ebp), %eax
	movl	-240(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_positionVoisinage
	cmpl	$5, %eax
	je	L70
	movl	-252(%ebp), %eax
	movl	-248(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-244(%ebp), %eax
	movl	-240(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_positionVoisinage
	cmpl	$7, %eax
	jne	L76
	jmp	L70
L232:
	nop
L70:
	movl	-64(%ebp), %eax
	movl	(%eax), %edx
	movl	-80(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	%edx, (%esp)
	call	_ajouterMaillon
	movl	-80(%ebp), %eax
	movl	%eax, -12(%ebp)
	movl	20(%ebp), %edx
	movl	28(%ebp), %eax
	cmpl	%eax, %edx
	jbe	L77
	movl	-16(%ebp), %eax
	subl	$1, %eax
	movl	%eax, -16(%ebp)
	jmp	L69
L77:
	movl	-16(%ebp), %eax
	addl	$1, %eax
	movl	%eax, -16(%ebp)
L69:
	movl	20(%ebp), %edx
	movl	28(%ebp), %eax
	cmpl	%eax, %edx
	jbe	L78
	movl	28(%ebp), %edx
	movl	-16(%ebp), %eax
	cmpl	%eax, %edx
	setbe	%al
	jmp	L79
L78:
	movl	28(%ebp), %edx
	movl	-16(%ebp), %eax
	cmpl	%eax, %edx
	setnb	%al
L79:
	testb	%al, %al
	jne	L80
	jmp	L68
L231:
	nop
L68:
	movl	-64(%ebp), %eax
	movl	(%eax), %eax
	movl	%eax, -20(%ebp)
L81:
	jmp	L82
L198:
	movl	-20(%ebp), %eax
	movl	8(%eax), %eax
	movl	4(%eax), %eax
	subl	$4, %eax
	movl	%eax, %ecx
	movl	-20(%ebp), %eax
	movl	8(%eax), %eax
	movl	(%eax), %eax
	subl	$4, %eax
	leal	-214(%ebp), %edx
	movl	$9, 20(%esp)
	movl	$9, 16(%esp)
	movl	%ecx, 12(%esp)
	movl	%eax, 8(%esp)
	movl	12(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	%edx, (%esp)
	call	_estCouleurUniforme
	movl	16(%ebp), %eax
	movl	4(%eax), %edx
	movl	-214(%ebp), %eax
	movl	%eax, 8(%esp)
	movzwl	-210(%ebp), %eax
	movw	%ax, 12(%esp)
	movl	(%edx), %eax
	movl	%eax, (%esp)
	movzwl	4(%edx), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleur
	xorl	$1, %eax
	testb	%al, %al
	je	L83
	movl	-20(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -256(%ebp)
	movl	-256(%ebp), %eax
	movl	%eax, -24(%ebp)
	jmp	L84
L86:
	movl	-24(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -24(%ebp)
	movl	-24(%ebp), %eax
	movl	8(%eax), %eax
	testl	%eax, %eax
	jne	L84
	movl	$0, %eax
	jmp	L59
L84:
	movl	-24(%ebp), %eax
	movl	8(%eax), %eax
	movl	4(%eax), %eax
	subl	$4, %eax
	movl	%eax, %ecx
	movl	-24(%ebp), %eax
	movl	8(%eax), %eax
	movl	(%eax), %eax
	subl	$4, %eax
	leal	-208(%ebp), %edx
	movl	$9, 20(%esp)
	movl	$9, 16(%esp)
	movl	%ecx, 12(%esp)
	movl	%eax, 8(%esp)
	movl	12(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	%edx, (%esp)
	call	_estCouleurUniforme
	movl	16(%ebp), %eax
	movl	4(%eax), %edx
	movl	-208(%ebp), %eax
	movl	%eax, 8(%esp)
	movzwl	-204(%ebp), %eax
	movw	%ax, 12(%esp)
	movl	(%edx), %eax
	movl	%eax, (%esp)
	movzwl	4(%edx), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleur
	xorl	$1, %eax
	testb	%al, %al
	jne	L86
	movl	-24(%ebp), %eax
	movl	%eax, -84(%ebp)
	movl	-20(%ebp), %eax
	movl	8(%eax), %eax
	movl	(%eax), %eax
	movl	%eax, -264(%ebp)
	movl	-20(%ebp), %eax
	movl	8(%eax), %eax
	movl	4(%eax), %eax
	movl	%eax, -260(%ebp)
	movl	-20(%ebp), %eax
	movl	8(%eax), %eax
	movl	(%eax), %eax
	movl	%eax, -272(%ebp)
	movl	-20(%ebp), %eax
	movl	8(%eax), %eax
	movl	4(%eax), %eax
	movl	%eax, -268(%ebp)
	movl	-20(%ebp), %eax
	movl	8(%eax), %eax
	movl	(%eax), %eax
	movl	%eax, -280(%ebp)
	movl	-20(%ebp), %eax
	movl	8(%eax), %eax
	movl	4(%eax), %eax
	movl	%eax, -276(%ebp)
	movl	-20(%ebp), %eax
	movl	8(%eax), %eax
	movl	(%eax), %eax
	movl	%eax, -288(%ebp)
	movl	-20(%ebp), %eax
	movl	8(%eax), %eax
	movl	4(%eax), %eax
	movl	%eax, -284(%ebp)
	movl	-20(%ebp), %eax
	movl	8(%eax), %eax
	movl	(%eax), %eax
	movl	%eax, -600(%ebp)
	movl	-20(%ebp), %eax
	movl	8(%eax), %eax
	movl	4(%eax), %eax
	movl	%eax, -596(%ebp)
	movl	-600(%ebp), %eax
	movl	-596(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, -292(%ebp)
	movl	-20(%ebp), %eax
	movl	8(%eax), %eax
	movl	(%eax), %eax
	movl	%eax, -592(%ebp)
	movl	-20(%ebp), %eax
	movl	8(%eax), %eax
	movl	4(%eax), %eax
	movl	%eax, -588(%ebp)
	movl	-592(%ebp), %eax
	movl	-588(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, -296(%ebp)
	movl	-20(%ebp), %eax
	movl	8(%eax), %eax
	movl	(%eax), %eax
	movl	%eax, -584(%ebp)
	movl	-20(%ebp), %eax
	movl	8(%eax), %eax
	movl	4(%eax), %eax
	movl	%eax, -580(%ebp)
	movl	-584(%ebp), %eax
	movl	-580(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, -300(%ebp)
	movl	-20(%ebp), %eax
	movl	8(%eax), %eax
	movl	(%eax), %eax
	movl	%eax, -576(%ebp)
	movl	-20(%ebp), %eax
	movl	8(%eax), %eax
	movl	4(%eax), %eax
	movl	%eax, -572(%ebp)
	movl	-576(%ebp), %eax
	movl	-572(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, -304(%ebp)
	movl	-292(%ebp), %eax
	movl	%eax, -28(%ebp)
	movl	-296(%ebp), %eax
	movl	%eax, -32(%ebp)
	movl	-300(%ebp), %eax
	movl	%eax, -36(%ebp)
	movl	-304(%ebp), %eax
	movl	%eax, -40(%ebp)
	movl	$0, -88(%ebp)
	movb	$0, -89(%ebp)
	jmp	L87
L197:
	cmpl	$0, -28(%ebp)
	je	L88
	movl	-28(%ebp), %eax
	movl	(%eax), %edx
	movl	-28(%ebp), %eax
	movl	4(%eax), %eax
	addl	$1, %eax
	movl	%edx, -312(%ebp)
	movl	%eax, -308(%ebp)
	movl	-28(%ebp), %eax
	movl	(%eax), %eax
	leal	1(%eax), %edx
	movl	-28(%ebp), %eax
	movl	4(%eax), %eax
	movl	%edx, -320(%ebp)
	movl	%eax, -316(%ebp)
	movl	-28(%ebp), %eax
	movl	(%eax), %edx
	movl	-28(%ebp), %eax
	movl	4(%eax), %eax
	subl	$1, %eax
	movl	%edx, -328(%ebp)
	movl	%eax, -324(%ebp)
	movl	-28(%ebp), %eax
	movl	(%eax), %eax
	leal	-1(%eax), %edx
	movl	-28(%ebp), %eax
	movl	4(%eax), %eax
	movl	%edx, -336(%ebp)
	movl	%eax, -332(%ebp)
	movl	8(%ebp), %eax
	movl	8(%eax), %edx
	movl	-312(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-308(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ebx
	movl	8(%ebp), %eax
	movl	12(%eax), %edx
	movl	-312(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-308(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %edx
	movl	-312(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-308(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%bx, -438(%ebp)
	movw	%cx, -436(%ebp)
	movw	%ax, -434(%ebp)
	movl	8(%ebp), %eax
	movl	8(%eax), %edx
	movl	-320(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-316(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ebx
	movl	8(%ebp), %eax
	movl	12(%eax), %edx
	movl	-320(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-316(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %edx
	movl	-320(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-316(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%bx, -444(%ebp)
	movw	%cx, -442(%ebp)
	movw	%ax, -440(%ebp)
	movl	8(%ebp), %eax
	movl	8(%eax), %edx
	movl	-328(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-324(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ebx
	movl	8(%ebp), %eax
	movl	12(%eax), %edx
	movl	-328(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-324(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %edx
	movl	-328(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-324(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%bx, -450(%ebp)
	movw	%cx, -448(%ebp)
	movw	%ax, -446(%ebp)
	movl	8(%ebp), %eax
	movl	8(%eax), %edx
	movl	-336(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-332(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ebx
	movl	8(%ebp), %eax
	movl	12(%eax), %edx
	movl	-336(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-332(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %edx
	movl	-336(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-332(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%bx, -456(%ebp)
	movw	%cx, -454(%ebp)
	movw	%ax, -452(%ebp)
L88:
	cmpl	$0, -32(%ebp)
	je	L89
	movl	-32(%ebp), %eax
	movl	(%eax), %edx
	movl	-32(%ebp), %eax
	movl	4(%eax), %eax
	addl	$1, %eax
	movl	%edx, -344(%ebp)
	movl	%eax, -340(%ebp)
	movl	-32(%ebp), %eax
	movl	(%eax), %eax
	leal	1(%eax), %edx
	movl	-32(%ebp), %eax
	movl	4(%eax), %eax
	movl	%edx, -352(%ebp)
	movl	%eax, -348(%ebp)
	movl	-32(%ebp), %eax
	movl	(%eax), %edx
	movl	-32(%ebp), %eax
	movl	4(%eax), %eax
	subl	$1, %eax
	movl	%edx, -360(%ebp)
	movl	%eax, -356(%ebp)
	movl	-32(%ebp), %eax
	movl	(%eax), %eax
	leal	-1(%eax), %edx
	movl	-32(%ebp), %eax
	movl	4(%eax), %eax
	movl	%edx, -368(%ebp)
	movl	%eax, -364(%ebp)
	movl	8(%ebp), %eax
	movl	8(%eax), %edx
	movl	-344(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-340(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ebx
	movl	8(%ebp), %eax
	movl	12(%eax), %edx
	movl	-344(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-340(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %edx
	movl	-344(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-340(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%bx, -462(%ebp)
	movw	%cx, -460(%ebp)
	movw	%ax, -458(%ebp)
	movl	8(%ebp), %eax
	movl	8(%eax), %edx
	movl	-352(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-348(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ebx
	movl	8(%ebp), %eax
	movl	12(%eax), %edx
	movl	-352(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-348(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %edx
	movl	-352(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-348(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%bx, -468(%ebp)
	movw	%cx, -466(%ebp)
	movw	%ax, -464(%ebp)
	movl	8(%ebp), %eax
	movl	8(%eax), %edx
	movl	-360(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-356(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ebx
	movl	8(%ebp), %eax
	movl	12(%eax), %edx
	movl	-360(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-356(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %edx
	movl	-360(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-356(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%bx, -474(%ebp)
	movw	%cx, -472(%ebp)
	movw	%ax, -470(%ebp)
	movl	8(%ebp), %eax
	movl	8(%eax), %edx
	movl	-368(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-364(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ebx
	movl	8(%ebp), %eax
	movl	12(%eax), %edx
	movl	-368(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-364(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %edx
	movl	-368(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-364(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%bx, -480(%ebp)
	movw	%cx, -478(%ebp)
	movw	%ax, -476(%ebp)
L89:
	cmpl	$0, -36(%ebp)
	je	L90
	movl	-36(%ebp), %eax
	movl	(%eax), %edx
	movl	-36(%ebp), %eax
	movl	4(%eax), %eax
	addl	$1, %eax
	movl	%edx, -376(%ebp)
	movl	%eax, -372(%ebp)
	movl	-36(%ebp), %eax
	movl	(%eax), %eax
	leal	1(%eax), %edx
	movl	-36(%ebp), %eax
	movl	4(%eax), %eax
	movl	%edx, -384(%ebp)
	movl	%eax, -380(%ebp)
	movl	-36(%ebp), %eax
	movl	(%eax), %edx
	movl	-36(%ebp), %eax
	movl	4(%eax), %eax
	subl	$1, %eax
	movl	%edx, -392(%ebp)
	movl	%eax, -388(%ebp)
	movl	-36(%ebp), %eax
	movl	(%eax), %eax
	leal	-1(%eax), %edx
	movl	-36(%ebp), %eax
	movl	4(%eax), %eax
	movl	%edx, -400(%ebp)
	movl	%eax, -396(%ebp)
	movl	8(%ebp), %eax
	movl	8(%eax), %edx
	movl	-376(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-372(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ebx
	movl	8(%ebp), %eax
	movl	12(%eax), %edx
	movl	-376(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-372(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %edx
	movl	-376(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-372(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%bx, -486(%ebp)
	movw	%cx, -484(%ebp)
	movw	%ax, -482(%ebp)
	movl	8(%ebp), %eax
	movl	8(%eax), %edx
	movl	-384(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-380(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ebx
	movl	8(%ebp), %eax
	movl	12(%eax), %edx
	movl	-384(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-380(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %edx
	movl	-384(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-380(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%bx, -492(%ebp)
	movw	%cx, -490(%ebp)
	movw	%ax, -488(%ebp)
	movl	8(%ebp), %eax
	movl	8(%eax), %edx
	movl	-392(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-388(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ebx
	movl	8(%ebp), %eax
	movl	12(%eax), %edx
	movl	-392(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-388(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %edx
	movl	-392(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-388(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%bx, -498(%ebp)
	movw	%cx, -496(%ebp)
	movw	%ax, -494(%ebp)
	movl	8(%ebp), %eax
	movl	8(%eax), %edx
	movl	-400(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-396(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ebx
	movl	8(%ebp), %eax
	movl	12(%eax), %edx
	movl	-400(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-396(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %edx
	movl	-400(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-396(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%bx, -504(%ebp)
	movw	%cx, -502(%ebp)
	movw	%ax, -500(%ebp)
L90:
	cmpl	$0, -40(%ebp)
	je	L91
	movl	-40(%ebp), %eax
	movl	(%eax), %edx
	movl	-40(%ebp), %eax
	movl	4(%eax), %eax
	addl	$1, %eax
	movl	%edx, -408(%ebp)
	movl	%eax, -404(%ebp)
	movl	-40(%ebp), %eax
	movl	(%eax), %eax
	leal	1(%eax), %edx
	movl	-40(%ebp), %eax
	movl	4(%eax), %eax
	movl	%edx, -416(%ebp)
	movl	%eax, -412(%ebp)
	movl	-40(%ebp), %eax
	movl	(%eax), %edx
	movl	-40(%ebp), %eax
	movl	4(%eax), %eax
	subl	$1, %eax
	movl	%edx, -424(%ebp)
	movl	%eax, -420(%ebp)
	movl	-40(%ebp), %eax
	movl	(%eax), %eax
	leal	-1(%eax), %edx
	movl	-40(%ebp), %eax
	movl	4(%eax), %eax
	movl	%edx, -432(%ebp)
	movl	%eax, -428(%ebp)
	movl	8(%ebp), %eax
	movl	8(%eax), %edx
	movl	-408(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-404(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ebx
	movl	8(%ebp), %eax
	movl	12(%eax), %edx
	movl	-408(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-404(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %edx
	movl	-408(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-404(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%bx, -510(%ebp)
	movw	%cx, -508(%ebp)
	movw	%ax, -506(%ebp)
	movl	8(%ebp), %eax
	movl	8(%eax), %edx
	movl	-416(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-412(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ebx
	movl	8(%ebp), %eax
	movl	12(%eax), %edx
	movl	-416(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-412(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %edx
	movl	-416(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-412(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%bx, -516(%ebp)
	movw	%cx, -514(%ebp)
	movw	%ax, -512(%ebp)
	movl	8(%ebp), %eax
	movl	8(%eax), %edx
	movl	-424(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-420(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ebx
	movl	8(%ebp), %eax
	movl	12(%eax), %edx
	movl	-424(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-420(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %edx
	movl	-424(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-420(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%bx, -522(%ebp)
	movw	%cx, -520(%ebp)
	movw	%ax, -518(%ebp)
	movl	8(%ebp), %eax
	movl	8(%eax), %edx
	movl	-432(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-428(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ebx
	movl	8(%ebp), %eax
	movl	12(%eax), %edx
	movl	-432(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-428(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %edx
	movl	-432(%ebp), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-428(%ebp), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%bx, -528(%ebp)
	movw	%cx, -526(%ebp)
	movw	%ax, -524(%ebp)
L91:
	movl	-292(%ebp), %eax
	testl	%eax, %eax
	je	L92
	movl	16(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%edx), %eax
	movl	%eax, 8(%esp)
	movzwl	4(%edx), %eax
	movw	%ax, 12(%esp)
	movl	-438(%ebp), %eax
	movl	%eax, (%esp)
	movzwl	-434(%ebp), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleur
	xorl	$1, %eax
	testb	%al, %al
	je	L93
	movl	-264(%ebp), %eax
	movl	-260(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-312(%ebp), %eax
	movl	-308(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L93
	movl	-312(%ebp), %eax
	movl	-308(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, %edx
	movl	-292(%ebp), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_ajouterMaillon
	jmp	L94
L93:
	movl	16(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%edx), %eax
	movl	%eax, 8(%esp)
	movzwl	4(%edx), %eax
	movw	%ax, 12(%esp)
	movl	-444(%ebp), %eax
	movl	%eax, (%esp)
	movzwl	-440(%ebp), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleur
	xorl	$1, %eax
	testb	%al, %al
	je	L95
	movl	-264(%ebp), %eax
	movl	-260(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-320(%ebp), %eax
	movl	-316(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L95
	movl	-320(%ebp), %eax
	movl	-316(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, %edx
	movl	-292(%ebp), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_ajouterMaillon
	jmp	L94
L95:
	movl	16(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%edx), %eax
	movl	%eax, 8(%esp)
	movzwl	4(%edx), %eax
	movw	%ax, 12(%esp)
	movl	-450(%ebp), %eax
	movl	%eax, (%esp)
	movzwl	-446(%ebp), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleur
	xorl	$1, %eax
	testb	%al, %al
	je	L96
	movl	-264(%ebp), %eax
	movl	-260(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-328(%ebp), %eax
	movl	-324(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L96
	movl	-328(%ebp), %eax
	movl	-324(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, %edx
	movl	-292(%ebp), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_ajouterMaillon
	jmp	L94
L96:
	movl	16(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%edx), %eax
	movl	%eax, 8(%esp)
	movzwl	4(%edx), %eax
	movw	%ax, 12(%esp)
	movl	-456(%ebp), %eax
	movl	%eax, (%esp)
	movzwl	-452(%ebp), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleur
	xorl	$1, %eax
	testb	%al, %al
	je	L94
	movl	-264(%ebp), %eax
	movl	-260(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-336(%ebp), %eax
	movl	-332(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L94
	movl	-336(%ebp), %eax
	movl	-332(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, %edx
	movl	-292(%ebp), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_ajouterMaillon
L94:
	movl	-28(%ebp), %eax
	movl	8(%eax), %eax
	testl	%eax, %eax
	jne	L97
	jmp	L98
L99:
	movl	-292(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -96(%ebp)
	leal	-292(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereMaillon
	movl	-96(%ebp), %eax
	movl	%eax, -292(%ebp)
L98:
	movl	-292(%ebp), %eax
	testl	%eax, %eax
	jne	L99
	movl	$0, -28(%ebp)
	movl	$0, -264(%ebp)
	movl	$0, -260(%ebp)
	jmp	L92
L97:
	movl	-28(%ebp), %eax
	movl	(%eax), %edx
	movl	-28(%ebp), %eax
	movl	4(%eax), %eax
	movl	%edx, -264(%ebp)
	movl	%eax, -260(%ebp)
	movl	-28(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -28(%ebp)
L92:
	movl	-296(%ebp), %eax
	testl	%eax, %eax
	je	L100
	movl	16(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%edx), %eax
	movl	%eax, 8(%esp)
	movzwl	4(%edx), %eax
	movw	%ax, 12(%esp)
	movl	-480(%ebp), %eax
	movl	%eax, (%esp)
	movzwl	-476(%ebp), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleur
	xorl	$1, %eax
	testb	%al, %al
	je	L101
	movl	-272(%ebp), %eax
	movl	-268(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-368(%ebp), %eax
	movl	-364(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L101
	cmpl	$0, -28(%ebp)
	je	L102
	movl	-28(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-368(%ebp), %eax
	movl	-364(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L233
L102:
	movl	-368(%ebp), %eax
	movl	-364(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, %edx
	movl	-296(%ebp), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_ajouterMaillon
	jmp	L233
L101:
	movl	16(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%edx), %eax
	movl	%eax, 8(%esp)
	movzwl	4(%edx), %eax
	movw	%ax, 12(%esp)
	movl	-474(%ebp), %eax
	movl	%eax, (%esp)
	movzwl	-470(%ebp), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleur
	xorl	$1, %eax
	testb	%al, %al
	je	L105
	movl	-272(%ebp), %eax
	movl	-268(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-360(%ebp), %eax
	movl	-356(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L105
	cmpl	$0, -28(%ebp)
	je	L106
	movl	-28(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-360(%ebp), %eax
	movl	-356(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L234
L106:
	movl	-360(%ebp), %eax
	movl	-356(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, %edx
	movl	-296(%ebp), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_ajouterMaillon
	jmp	L234
L105:
	movl	16(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%edx), %eax
	movl	%eax, 8(%esp)
	movzwl	4(%edx), %eax
	movw	%ax, 12(%esp)
	movl	-468(%ebp), %eax
	movl	%eax, (%esp)
	movzwl	-464(%ebp), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleur
	xorl	$1, %eax
	testb	%al, %al
	je	L108
	movl	-272(%ebp), %eax
	movl	-268(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-352(%ebp), %eax
	movl	-348(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L108
	cmpl	$0, -28(%ebp)
	je	L109
	movl	-28(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-352(%ebp), %eax
	movl	-348(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L235
L109:
	movl	-352(%ebp), %eax
	movl	-348(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, %edx
	movl	-296(%ebp), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_ajouterMaillon
	jmp	L235
L108:
	movl	16(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%edx), %eax
	movl	%eax, 8(%esp)
	movzwl	4(%edx), %eax
	movw	%ax, 12(%esp)
	movl	-462(%ebp), %eax
	movl	%eax, (%esp)
	movzwl	-458(%ebp), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleur
	xorl	$1, %eax
	testb	%al, %al
	je	L104
	movl	-272(%ebp), %eax
	movl	-268(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-344(%ebp), %eax
	movl	-340(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L104
	cmpl	$0, -28(%ebp)
	je	L111
	movl	-28(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-344(%ebp), %eax
	movl	-340(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L104
L111:
	movl	-344(%ebp), %eax
	movl	-340(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, %edx
	movl	-296(%ebp), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_ajouterMaillon
	jmp	L104
L233:
	nop
	jmp	L104
L234:
	nop
	jmp	L104
L235:
	nop
L104:
	movl	-32(%ebp), %eax
	movl	8(%eax), %eax
	testl	%eax, %eax
	jne	L112
	jmp	L113
L114:
	movl	-296(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -100(%ebp)
	leal	-296(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereMaillon
	movl	-100(%ebp), %eax
	movl	%eax, -296(%ebp)
L113:
	movl	-296(%ebp), %eax
	testl	%eax, %eax
	jne	L114
	movl	$0, -32(%ebp)
	movl	$0, -272(%ebp)
	movl	$0, -268(%ebp)
	jmp	L100
L112:
	movl	-32(%ebp), %eax
	movl	(%eax), %edx
	movl	-32(%ebp), %eax
	movl	4(%eax), %eax
	movl	%edx, -272(%ebp)
	movl	%eax, -268(%ebp)
	movl	-32(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -32(%ebp)
L100:
	movl	-300(%ebp), %eax
	testl	%eax, %eax
	je	L115
	movl	16(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%edx), %eax
	movl	%eax, 8(%esp)
	movzwl	4(%edx), %eax
	movw	%ax, 12(%esp)
	movl	-504(%ebp), %eax
	movl	%eax, (%esp)
	movzwl	-500(%ebp), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleur
	xorl	$1, %eax
	testb	%al, %al
	je	L116
	movl	-280(%ebp), %eax
	movl	-276(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-400(%ebp), %eax
	movl	-396(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L116
	cmpl	$0, -28(%ebp)
	je	L117
	movl	-28(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-400(%ebp), %eax
	movl	-396(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L236
L117:
	cmpl	$0, -32(%ebp)
	je	L119
	movl	-32(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-400(%ebp), %eax
	movl	-396(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L236
L119:
	movl	-400(%ebp), %eax
	movl	-396(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, %edx
	movl	-300(%ebp), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_ajouterMaillon
	jmp	L236
L116:
	movl	16(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%edx), %eax
	movl	%eax, 8(%esp)
	movzwl	4(%edx), %eax
	movw	%ax, 12(%esp)
	movl	-498(%ebp), %eax
	movl	%eax, (%esp)
	movzwl	-494(%ebp), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleur
	xorl	$1, %eax
	testb	%al, %al
	je	L121
	movl	-280(%ebp), %eax
	movl	-276(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-392(%ebp), %eax
	movl	-388(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L121
	cmpl	$0, -28(%ebp)
	je	L122
	movl	-28(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-392(%ebp), %eax
	movl	-388(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L237
L122:
	cmpl	$0, -32(%ebp)
	je	L124
	movl	-32(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-392(%ebp), %eax
	movl	-388(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L237
L124:
	movl	-392(%ebp), %eax
	movl	-388(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, %edx
	movl	-300(%ebp), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_ajouterMaillon
	jmp	L237
L121:
	movl	16(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%edx), %eax
	movl	%eax, 8(%esp)
	movzwl	4(%edx), %eax
	movw	%ax, 12(%esp)
	movl	-492(%ebp), %eax
	movl	%eax, (%esp)
	movzwl	-488(%ebp), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleur
	xorl	$1, %eax
	testb	%al, %al
	je	L125
	movl	-280(%ebp), %eax
	movl	-276(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-384(%ebp), %eax
	movl	-380(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L125
	cmpl	$0, -28(%ebp)
	je	L126
	movl	-28(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-384(%ebp), %eax
	movl	-380(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L238
L126:
	cmpl	$0, -32(%ebp)
	je	L128
	movl	-32(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-384(%ebp), %eax
	movl	-380(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L238
L128:
	movl	-384(%ebp), %eax
	movl	-380(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, %edx
	movl	-300(%ebp), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_ajouterMaillon
	jmp	L238
L125:
	movl	16(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%edx), %eax
	movl	%eax, 8(%esp)
	movzwl	4(%edx), %eax
	movw	%ax, 12(%esp)
	movl	-486(%ebp), %eax
	movl	%eax, (%esp)
	movzwl	-482(%ebp), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleur
	xorl	$1, %eax
	testb	%al, %al
	je	L120
	movl	-280(%ebp), %eax
	movl	-276(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-376(%ebp), %eax
	movl	-372(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L120
	cmpl	$0, -28(%ebp)
	je	L129
	movl	-28(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-376(%ebp), %eax
	movl	-372(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L120
L129:
	cmpl	$0, -32(%ebp)
	je	L130
	movl	-32(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-376(%ebp), %eax
	movl	-372(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L120
L130:
	movl	-376(%ebp), %eax
	movl	-372(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, %edx
	movl	-300(%ebp), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_ajouterMaillon
	jmp	L120
L236:
	nop
	jmp	L120
L237:
	nop
	jmp	L120
L238:
	nop
L120:
	movl	-36(%ebp), %eax
	movl	8(%eax), %eax
	testl	%eax, %eax
	jne	L131
	jmp	L132
L133:
	movl	-300(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -104(%ebp)
	leal	-300(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereMaillon
	movl	-104(%ebp), %eax
	movl	%eax, -300(%ebp)
L132:
	movl	-300(%ebp), %eax
	testl	%eax, %eax
	jne	L133
	movl	$0, -36(%ebp)
	movl	$0, -280(%ebp)
	movl	$0, -276(%ebp)
	jmp	L115
L131:
	movl	-36(%ebp), %eax
	movl	(%eax), %edx
	movl	-36(%ebp), %eax
	movl	4(%eax), %eax
	movl	%edx, -280(%ebp)
	movl	%eax, -276(%ebp)
	movl	-36(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -36(%ebp)
L115:
	movl	-304(%ebp), %eax
	testl	%eax, %eax
	je	L134
	movl	16(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%edx), %eax
	movl	%eax, 8(%esp)
	movzwl	4(%edx), %eax
	movw	%ax, 12(%esp)
	movl	-528(%ebp), %eax
	movl	%eax, (%esp)
	movzwl	-524(%ebp), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleur
	xorl	$1, %eax
	testb	%al, %al
	je	L135
	movl	-288(%ebp), %eax
	movl	-284(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-432(%ebp), %eax
	movl	-428(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L135
	cmpl	$0, -28(%ebp)
	je	L136
	movl	-28(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-432(%ebp), %eax
	movl	-428(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L239
L136:
	cmpl	$0, -32(%ebp)
	je	L138
	movl	-32(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-432(%ebp), %eax
	movl	-428(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L239
L138:
	cmpl	$0, -36(%ebp)
	je	L139
	movl	-36(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-432(%ebp), %eax
	movl	-428(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L239
L139:
	movl	-432(%ebp), %eax
	movl	-428(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, %edx
	movl	-304(%ebp), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_ajouterMaillon
	jmp	L239
L135:
	movl	16(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%edx), %eax
	movl	%eax, 8(%esp)
	movzwl	4(%edx), %eax
	movw	%ax, 12(%esp)
	movl	-522(%ebp), %eax
	movl	%eax, (%esp)
	movzwl	-518(%ebp), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleur
	xorl	$1, %eax
	testb	%al, %al
	je	L141
	movl	-288(%ebp), %eax
	movl	-284(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-424(%ebp), %eax
	movl	-420(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L141
	cmpl	$0, -28(%ebp)
	je	L142
	movl	-28(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-424(%ebp), %eax
	movl	-420(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L240
L142:
	cmpl	$0, -32(%ebp)
	je	L144
	movl	-32(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-424(%ebp), %eax
	movl	-420(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L240
L144:
	cmpl	$0, -36(%ebp)
	je	L145
	movl	-36(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-424(%ebp), %eax
	movl	-420(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L240
L145:
	movl	-424(%ebp), %eax
	movl	-420(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, %edx
	movl	-304(%ebp), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_ajouterMaillon
	jmp	L240
L141:
	movl	16(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%edx), %eax
	movl	%eax, 8(%esp)
	movzwl	4(%edx), %eax
	movw	%ax, 12(%esp)
	movl	-516(%ebp), %eax
	movl	%eax, (%esp)
	movzwl	-512(%ebp), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleur
	xorl	$1, %eax
	testb	%al, %al
	je	L146
	movl	-288(%ebp), %eax
	movl	-284(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-416(%ebp), %eax
	movl	-412(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L146
	cmpl	$0, -28(%ebp)
	je	L147
	movl	-28(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-416(%ebp), %eax
	movl	-412(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L241
L147:
	cmpl	$0, -32(%ebp)
	je	L149
	movl	-32(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-416(%ebp), %eax
	movl	-412(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L241
L149:
	cmpl	$0, -36(%ebp)
	je	L150
	movl	-36(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-416(%ebp), %eax
	movl	-412(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L241
L150:
	movl	-416(%ebp), %eax
	movl	-412(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, %edx
	movl	-304(%ebp), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_ajouterMaillon
	jmp	L241
L146:
	movl	16(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%edx), %eax
	movl	%eax, 8(%esp)
	movzwl	4(%edx), %eax
	movw	%ax, 12(%esp)
	movl	-510(%ebp), %eax
	movl	%eax, (%esp)
	movzwl	-506(%ebp), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleur
	xorl	$1, %eax
	testb	%al, %al
	je	L140
	movl	-288(%ebp), %eax
	movl	-284(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-408(%ebp), %eax
	movl	-404(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L140
	cmpl	$0, -28(%ebp)
	je	L151
	movl	-28(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-408(%ebp), %eax
	movl	-404(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L140
L151:
	cmpl	$0, -32(%ebp)
	je	L152
	movl	-32(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-408(%ebp), %eax
	movl	-404(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L140
L152:
	cmpl	$0, -36(%ebp)
	je	L153
	movl	-36(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-408(%ebp), %eax
	movl	-404(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	je	L140
L153:
	movl	-408(%ebp), %eax
	movl	-404(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, %edx
	movl	-304(%ebp), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_ajouterMaillon
	jmp	L140
L239:
	nop
	jmp	L140
L240:
	nop
	jmp	L140
L241:
	nop
L140:
	movl	-40(%ebp), %eax
	movl	8(%eax), %eax
	testl	%eax, %eax
	jne	L154
	jmp	L155
L156:
	movl	-304(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -108(%ebp)
	leal	-304(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereMaillon
	movl	-108(%ebp), %eax
	movl	%eax, -304(%ebp)
L155:
	movl	-304(%ebp), %eax
	testl	%eax, %eax
	jne	L156
	movl	$0, -40(%ebp)
	movl	$0, -288(%ebp)
	movl	$0, -284(%ebp)
	jmp	L134
L154:
	movl	-40(%ebp), %eax
	movl	(%eax), %edx
	movl	-40(%ebp), %eax
	movl	4(%eax), %eax
	movl	%edx, -288(%ebp)
	movl	%eax, -284(%ebp)
	movl	-40(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -40(%ebp)
L134:
	cmpl	$0, -28(%ebp)
	jne	L157
	cmpl	$0, -32(%ebp)
	jne	L157
	cmpl	$0, -36(%ebp)
	jne	L157
	cmpl	$0, -40(%ebp)
	jne	L157
	movl	$0, %eax
	jmp	L59
L157:
	movl	-20(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -44(%ebp)
	jmp	L158
L196:
	cmpl	$0, -28(%ebp)
	je	L159
	movl	-84(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-28(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	testb	%al, %al
	je	L159
	movl	-292(%ebp), %edx
	movl	-20(%ebp), %eax
	movl	%edx, 8(%eax)
	movl	-84(%ebp), %eax
	movl	8(%eax), %edx
	movl	-28(%ebp), %eax
	movl	%edx, 8(%eax)
	movl	-84(%ebp), %eax
	movl	$0, 8(%eax)
	jmp	L160
L161:
	movl	-256(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -112(%ebp)
	leal	-256(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereMaillon
	movl	-112(%ebp), %eax
	movl	%eax, -256(%ebp)
L160:
	movl	-256(%ebp), %eax
	testl	%eax, %eax
	jne	L161
	movl	-28(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -20(%ebp)
	jmp	L162
L163:
	movl	-296(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -116(%ebp)
	leal	-296(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereMaillon
	movl	-116(%ebp), %eax
	movl	%eax, -296(%ebp)
L162:
	movl	-296(%ebp), %eax
	testl	%eax, %eax
	jne	L163
	jmp	L164
L165:
	movl	-300(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -120(%ebp)
	leal	-300(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereMaillon
	movl	-120(%ebp), %eax
	movl	%eax, -300(%ebp)
L164:
	movl	-300(%ebp), %eax
	testl	%eax, %eax
	jne	L165
	jmp	L166
L167:
	movl	-304(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -124(%ebp)
	leal	-304(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereMaillon
	movl	-124(%ebp), %eax
	movl	%eax, -304(%ebp)
L166:
	movl	-304(%ebp), %eax
	testl	%eax, %eax
	jne	L167
	jmp	L81
L159:
	cmpl	$0, -32(%ebp)
	je	L169
	movl	-84(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-32(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	testb	%al, %al
	je	L169
	movl	-296(%ebp), %edx
	movl	-20(%ebp), %eax
	movl	%edx, 8(%eax)
	movl	-84(%ebp), %eax
	movl	8(%eax), %edx
	movl	-32(%ebp), %eax
	movl	%edx, 8(%eax)
	movl	-84(%ebp), %eax
	movl	$0, 8(%eax)
	jmp	L170
L171:
	movl	-256(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -128(%ebp)
	leal	-256(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereMaillon
	movl	-128(%ebp), %eax
	movl	%eax, -256(%ebp)
L170:
	movl	-256(%ebp), %eax
	testl	%eax, %eax
	jne	L171
	movl	-32(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -20(%ebp)
	jmp	L172
L173:
	movl	-292(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -132(%ebp)
	leal	-292(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereMaillon
	movl	-132(%ebp), %eax
	movl	%eax, -292(%ebp)
L172:
	movl	-292(%ebp), %eax
	testl	%eax, %eax
	jne	L173
	jmp	L174
L175:
	movl	-300(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -136(%ebp)
	leal	-300(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereMaillon
	movl	-136(%ebp), %eax
	movl	%eax, -300(%ebp)
L174:
	movl	-300(%ebp), %eax
	testl	%eax, %eax
	jne	L175
	jmp	L176
L177:
	movl	-304(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -140(%ebp)
	leal	-304(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereMaillon
	movl	-140(%ebp), %eax
	movl	%eax, -304(%ebp)
L176:
	movl	-304(%ebp), %eax
	testl	%eax, %eax
	jne	L177
	jmp	L81
L169:
	cmpl	$0, -36(%ebp)
	je	L178
	movl	-84(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-36(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	testb	%al, %al
	je	L178
	movl	-300(%ebp), %edx
	movl	-20(%ebp), %eax
	movl	%edx, 8(%eax)
	movl	-84(%ebp), %eax
	movl	8(%eax), %edx
	movl	-36(%ebp), %eax
	movl	%edx, 8(%eax)
	movl	-84(%ebp), %eax
	movl	$0, 8(%eax)
	jmp	L179
L180:
	movl	-256(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -144(%ebp)
	leal	-256(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereMaillon
	movl	-144(%ebp), %eax
	movl	%eax, -256(%ebp)
L179:
	movl	-256(%ebp), %eax
	testl	%eax, %eax
	jne	L180
	movl	-36(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -20(%ebp)
	jmp	L181
L182:
	movl	-292(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -148(%ebp)
	leal	-292(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereMaillon
	movl	-148(%ebp), %eax
	movl	%eax, -292(%ebp)
L181:
	movl	-292(%ebp), %eax
	testl	%eax, %eax
	jne	L182
	jmp	L183
L184:
	movl	-296(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -152(%ebp)
	leal	-296(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereMaillon
	movl	-152(%ebp), %eax
	movl	%eax, -296(%ebp)
L183:
	movl	-296(%ebp), %eax
	testl	%eax, %eax
	jne	L184
	jmp	L185
L186:
	movl	-304(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -156(%ebp)
	leal	-304(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereMaillon
	movl	-156(%ebp), %eax
	movl	%eax, -304(%ebp)
L185:
	movl	-304(%ebp), %eax
	testl	%eax, %eax
	jne	L186
	jmp	L81
L178:
	cmpl	$0, -40(%ebp)
	je	L187
	movl	-84(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-40(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	testb	%al, %al
	je	L187
	movl	-304(%ebp), %edx
	movl	-20(%ebp), %eax
	movl	%edx, 8(%eax)
	movl	-84(%ebp), %eax
	movl	8(%eax), %edx
	movl	-40(%ebp), %eax
	movl	%edx, 8(%eax)
	movl	-84(%ebp), %eax
	movl	$0, 8(%eax)
	jmp	L188
L189:
	movl	-256(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -160(%ebp)
	leal	-256(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereMaillon
	movl	-160(%ebp), %eax
	movl	%eax, -256(%ebp)
L188:
	movl	-256(%ebp), %eax
	testl	%eax, %eax
	jne	L189
	movl	-40(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -20(%ebp)
	jmp	L190
L191:
	movl	-292(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -164(%ebp)
	leal	-292(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereMaillon
	movl	-164(%ebp), %eax
	movl	%eax, -292(%ebp)
L190:
	movl	-292(%ebp), %eax
	testl	%eax, %eax
	jne	L191
	jmp	L192
L193:
	movl	-296(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -168(%ebp)
	leal	-296(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereMaillon
	movl	-168(%ebp), %eax
	movl	%eax, -296(%ebp)
L192:
	movl	-296(%ebp), %eax
	testl	%eax, %eax
	jne	L193
	jmp	L194
L195:
	movl	-300(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -172(%ebp)
	leal	-300(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereMaillon
	movl	-172(%ebp), %eax
	movl	%eax, -300(%ebp)
L194:
	movl	-300(%ebp), %eax
	testl	%eax, %eax
	jne	L195
	jmp	L81
L187:
	movl	-44(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -44(%ebp)
L158:
	cmpl	$0, -44(%ebp)
	jne	L196
L87:
	movzbl	-89(%ebp), %eax
	xorl	$1, %eax
	testb	%al, %al
	jne	L197
L83:
	movl	-20(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -20(%ebp)
L82:
	movl	-20(%ebp), %eax
	movl	8(%eax), %eax
	testl	%eax, %eax
	jne	L198
	movl	-64(%ebp), %eax
	movl	(%eax), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, -176(%ebp)
	movl	-176(%ebp), %eax
	movl	%eax, -48(%ebp)
	movl	-64(%ebp), %eax
	movl	(%eax), %eax
	movl	%eax, -20(%ebp)
	jmp	L199
L228:
	movl	-20(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -52(%ebp)
	cmpl	$0, -52(%ebp)
	je	L200
	movl	-52(%ebp), %eax
	movl	8(%eax), %eax
	testl	%eax, %eax
	je	L200
	movl	-52(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -56(%ebp)
	jmp	L201
L203:
	movl	-52(%ebp), %eax
	movl	8(%eax), %eax
	movl	4(%eax), %eax
	movl	%eax, %esi
	movl	-52(%ebp), %eax
	movl	8(%eax), %eax
	movl	(%eax), %eax
	movl	%eax, %ebx
	movl	-20(%ebp), %eax
	movl	4(%eax), %eax
	movl	%eax, %ecx
	movl	-20(%ebp), %eax
	movl	(%eax), %eax
	leal	-202(%ebp), %edx
	movl	%esi, 20(%esp)
	movl	%ebx, 16(%esp)
	movl	%ecx, 12(%esp)
	movl	%eax, 8(%esp)
	movl	12(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	%edx, (%esp)
	call	_estCouleurUniformeDansCarre
	movl	16(%ebp), %eax
	movl	4(%eax), %eax
	movl	-202(%ebp), %edx
	movl	%edx, 8(%esp)
	movzwl	-198(%ebp), %edx
	movw	%dx, 12(%esp)
	movl	(%eax), %edx
	movl	%edx, (%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 4(%esp)
	call	_compareCouleur
	testb	%al, %al
	je	L202
	movl	-52(%ebp), %eax
	movl	%eax, -56(%ebp)
L202:
	movl	-52(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -52(%ebp)
L201:
	movl	-52(%ebp), %eax
	movl	8(%eax), %eax
	testl	%eax, %eax
	jne	L203
	cmpl	$0, -56(%ebp)
	jne	L204
	cmpl	$0, -56(%ebp)
	je	L200
	movl	-56(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	28(%ebp), %eax
	movl	32(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	testb	%al, %al
	je	L200
L204:
	movl	-20(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 20(%ebp)
	movl	%edx, 24(%ebp)
	movl	-56(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 28(%ebp)
	movl	%edx, 32(%ebp)
	movl	20(%ebp), %edx
	movl	28(%ebp), %eax
	cmpl	%eax, %edx
	jne	L205
	movl	20(%ebp), %eax
	movl	24(%ebp), %edx
	movl	%eax, -536(%ebp)
	movl	%edx, -532(%ebp)
	jmp	L206
L212:
	movl	-536(%ebp), %edx
	movl	28(%ebp), %eax
	cmpl	%eax, %edx
	jnb	L207
	movl	-536(%ebp), %eax
	leal	1(%eax), %edx
	movl	-532(%ebp), %eax
	movl	%edx, -536(%ebp)
	movl	%eax, -532(%ebp)
	movl	-536(%ebp), %eax
	movl	-532(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, 4(%esp)
	movl	-176(%ebp), %eax
	movl	%eax, (%esp)
	call	_ajouterMaillon
	jmp	L206
L207:
	movl	-536(%ebp), %edx
	movl	28(%ebp), %eax
	cmpl	%eax, %edx
	jbe	L208
	movl	-536(%ebp), %eax
	leal	-1(%eax), %edx
	movl	-532(%ebp), %eax
	movl	%edx, -536(%ebp)
	movl	%eax, -532(%ebp)
	movl	-536(%ebp), %eax
	movl	-532(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, 4(%esp)
	movl	-176(%ebp), %eax
	movl	%eax, (%esp)
	call	_ajouterMaillon
	jmp	L206
L208:
	movl	-532(%ebp), %edx
	movl	32(%ebp), %eax
	cmpl	%eax, %edx
	jnb	L209
	movl	-536(%ebp), %eax
	movl	-532(%ebp), %edx
	addl	$1, %edx
	movl	%eax, -536(%ebp)
	movl	%edx, -532(%ebp)
	movl	-536(%ebp), %eax
	movl	-532(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, 4(%esp)
	movl	-176(%ebp), %eax
	movl	%eax, (%esp)
	call	_ajouterMaillon
	jmp	L206
L209:
	movl	-532(%ebp), %edx
	movl	32(%ebp), %eax
	cmpl	%eax, %edx
	jbe	L242
	movl	-536(%ebp), %eax
	movl	-532(%ebp), %edx
	subl	$1, %edx
	movl	%eax, -536(%ebp)
	movl	%edx, -532(%ebp)
	movl	-536(%ebp), %eax
	movl	-532(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, 4(%esp)
	movl	-176(%ebp), %eax
	movl	%eax, (%esp)
	call	_ajouterMaillon
L206:
	movl	28(%ebp), %eax
	movl	32(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-536(%ebp), %eax
	movl	-532(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_comparePoints
	xorl	$1, %eax
	testb	%al, %al
	jne	L212
	jmp	L226
L205:
	movl	32(%ebp), %eax
	movl	%eax, -576(%ebp)
	movl	$0, -572(%ebp)
	fildq	-576(%ebp)
	fstps	-556(%ebp)
	flds	-556(%ebp)
	movl	24(%ebp), %eax
	movl	%eax, -576(%ebp)
	movl	$0, -572(%ebp)
	fildq	-576(%ebp)
	fstps	-556(%ebp)
	flds	-556(%ebp)
	fsubrp	%st, %st(1)
	movl	28(%ebp), %eax
	movl	%eax, -576(%ebp)
	movl	$0, -572(%ebp)
	fildq	-576(%ebp)
	fstps	-556(%ebp)
	flds	-556(%ebp)
	movl	20(%ebp), %eax
	movl	%eax, -576(%ebp)
	movl	$0, -572(%ebp)
	fildq	-576(%ebp)
	fstps	-556(%ebp)
	flds	-556(%ebp)
	fsubrp	%st, %st(1)
	fdivrp	%st, %st(1)
	fstps	-180(%ebp)
	movl	24(%ebp), %eax
	movl	%eax, -576(%ebp)
	movl	$0, -572(%ebp)
	fildq	-576(%ebp)
	fstps	-556(%ebp)
	flds	-556(%ebp)
	movl	20(%ebp), %eax
	movl	%eax, -576(%ebp)
	movl	$0, -572(%ebp)
	fildq	-576(%ebp)
	fstps	-556(%ebp)
	flds	-556(%ebp)
	fmuls	-180(%ebp)
	fsubrp	%st, %st(1)
	fstps	-184(%ebp)
	movl	20(%ebp), %eax
	movl	%eax, -60(%ebp)
	jmp	L214
L225:
	fildl	-60(%ebp)
	fmuls	-180(%ebp)
	fadds	-184(%ebp)
	fnstcw	-558(%ebp)
	movzwl	-558(%ebp), %eax
	orb	$12, %ah
	movw	%ax, -560(%ebp)
	fldcw	-560(%ebp)
	fistpl	-188(%ebp)
	fldcw	-558(%ebp)
	movl	-60(%ebp), %eax
	movl	%eax, -568(%ebp)
	movl	-188(%ebp), %eax
	movl	%eax, -564(%ebp)
	movl	-568(%ebp), %eax
	movl	-564(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, -192(%ebp)
	movl	-48(%ebp), %eax
	movl	(%eax), %eax
	movl	%eax, -544(%ebp)
	movl	-48(%ebp), %eax
	movl	4(%eax), %eax
	movl	%eax, -540(%ebp)
	movl	-60(%ebp), %eax
	movl	%eax, -552(%ebp)
	movl	-188(%ebp), %eax
	movl	%eax, -548(%ebp)
	movl	-552(%ebp), %eax
	movl	-548(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-544(%ebp), %eax
	movl	-540(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_positionVoisinage
	cmpl	$8, %eax
	je	L215
	jmp	L216
L221:
	movl	-544(%ebp), %edx
	movl	-552(%ebp), %eax
	cmpl	%eax, %edx
	jnb	L217
	movl	-544(%ebp), %eax
	leal	1(%eax), %edx
	movl	-540(%ebp), %eax
	movl	%edx, -544(%ebp)
	movl	%eax, -540(%ebp)
	movl	-544(%ebp), %eax
	movl	-540(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, 4(%esp)
	movl	-176(%ebp), %eax
	movl	%eax, (%esp)
	call	_ajouterMaillon
	jmp	L216
L217:
	movl	-544(%ebp), %edx
	movl	-552(%ebp), %eax
	cmpl	%eax, %edx
	jbe	L218
	movl	-544(%ebp), %eax
	leal	-1(%eax), %edx
	movl	-540(%ebp), %eax
	movl	%edx, -544(%ebp)
	movl	%eax, -540(%ebp)
	movl	-544(%ebp), %eax
	movl	-540(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, 4(%esp)
	movl	-176(%ebp), %eax
	movl	%eax, (%esp)
	call	_ajouterMaillon
	jmp	L216
L218:
	movl	-540(%ebp), %edx
	movl	-548(%ebp), %eax
	cmpl	%eax, %edx
	jnb	L219
	movl	-544(%ebp), %eax
	movl	-540(%ebp), %edx
	addl	$1, %edx
	movl	%eax, -544(%ebp)
	movl	%edx, -540(%ebp)
	movl	-544(%ebp), %eax
	movl	-540(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, 4(%esp)
	movl	-176(%ebp), %eax
	movl	%eax, (%esp)
	call	_ajouterMaillon
	jmp	L216
L219:
	movl	-540(%ebp), %edx
	movl	-548(%ebp), %eax
	cmpl	%eax, %edx
	jbe	L243
	movl	-544(%ebp), %eax
	movl	-540(%ebp), %edx
	subl	$1, %edx
	movl	%eax, -544(%ebp)
	movl	%edx, -540(%ebp)
	movl	-544(%ebp), %eax
	movl	-540(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_creeMaillon
	movl	%eax, 4(%esp)
	movl	-176(%ebp), %eax
	movl	%eax, (%esp)
	call	_ajouterMaillon
L216:
	movl	-552(%ebp), %eax
	movl	-548(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-544(%ebp), %eax
	movl	-540(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_positionVoisinage
	cmpl	$1, %eax
	je	L215
	movl	-552(%ebp), %eax
	movl	-548(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-544(%ebp), %eax
	movl	-540(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_positionVoisinage
	cmpl	$3, %eax
	je	L215
	movl	-552(%ebp), %eax
	movl	-548(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-544(%ebp), %eax
	movl	-540(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_positionVoisinage
	cmpl	$5, %eax
	je	L215
	movl	-552(%ebp), %eax
	movl	-548(%ebp), %edx
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	-544(%ebp), %eax
	movl	-540(%ebp), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_positionVoisinage
	cmpl	$7, %eax
	jne	L221
	jmp	L215
L243:
	nop
L215:
	movl	-192(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-176(%ebp), %eax
	movl	%eax, (%esp)
	call	_ajouterMaillon
	movl	-192(%ebp), %eax
	movl	%eax, -48(%ebp)
	movl	20(%ebp), %edx
	movl	28(%ebp), %eax
	cmpl	%eax, %edx
	jbe	L222
	movl	-60(%ebp), %eax
	subl	$1, %eax
	movl	%eax, -60(%ebp)
	jmp	L214
L222:
	movl	-60(%ebp), %eax
	addl	$1, %eax
	movl	%eax, -60(%ebp)
L214:
	movl	20(%ebp), %edx
	movl	28(%ebp), %eax
	cmpl	%eax, %edx
	jbe	L223
	movl	28(%ebp), %edx
	movl	-60(%ebp), %eax
	cmpl	%eax, %edx
	setbe	%al
	jmp	L224
L223:
	movl	28(%ebp), %edx
	movl	-60(%ebp), %eax
	cmpl	%eax, %edx
	setnb	%al
L224:
	testb	%al, %al
	jne	L225
	jmp	L226
L242:
	nop
	jmp	L226
L227:
	movl	-48(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -48(%ebp)
L226:
	movl	-48(%ebp), %eax
	movl	8(%eax), %eax
	testl	%eax, %eax
	jne	L227
	movl	-56(%ebp), %eax
	movl	%eax, -20(%ebp)
L200:
	movl	-20(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -20(%ebp)
L199:
	cmpl	$0, -20(%ebp)
	jne	L228
	jmp	L229
L230:
	movl	-64(%ebp), %eax
	movl	(%eax), %eax
	movl	8(%eax), %eax
	movl	%eax, -196(%ebp)
	movl	-64(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereMaillon
	movl	-64(%ebp), %eax
	movl	-196(%ebp), %edx
	movl	%edx, (%eax)
L229:
	movl	-64(%ebp), %eax
	movl	(%eax), %eax
	testl	%eax, %eax
	jne	L230
	movl	-64(%ebp), %eax
	movl	-176(%ebp), %edx
	movl	%edx, (%eax)
	movl	-64(%ebp), %eax
L59:
	addl	$640, %esp
	popl	%ebx
	.cfi_restore 3
	popl	%esi
	.cfi_restore 6
	popl	%ebp
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE28:
	.globl	_dessineMotif0
	.def	_dessineMotif0;	.scl	2;	.type	32;	.endef
_dessineMotif0:
LFB29:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$80, %esp
	movl	44(%ebp), %eax
	movb	%al, -52(%ebp)
	movl	32(%ebp), %eax
	movl	%eax, -12(%ebp)
	movl	36(%ebp), %eax
	movl	40(%ebp), %edx
	shrl	%edx
	addl	%edx, %eax
	movl	%eax, -8(%ebp)
	movl	40(%ebp), %eax
	shrl	%eax
	movl	%eax, -4(%ebp)
	movl	32(%ebp), %eax
	movl	40(%ebp), %edx
	shrl	%edx
	addl	%edx, %eax
	movl	%eax, -24(%ebp)
	movl	36(%ebp), %eax
	movl	40(%ebp), %edx
	shrl	%edx
	addl	%edx, %eax
	movl	%eax, -20(%ebp)
	movl	40(%ebp), %eax
	shrl	%eax
	movl	%eax, -16(%ebp)
	movl	32(%ebp), %eax
	movl	%eax, -36(%ebp)
	movl	36(%ebp), %eax
	movl	%eax, -32(%ebp)
	movl	40(%ebp), %eax
	shrl	%eax
	movl	%eax, -28(%ebp)
	movl	32(%ebp), %eax
	movl	40(%ebp), %edx
	shrl	%edx
	addl	%edx, %eax
	movl	%eax, -48(%ebp)
	movl	36(%ebp), %eax
	movl	%eax, -44(%ebp)
	movl	40(%ebp), %eax
	shrl	%eax
	movl	%eax, -40(%ebp)
	movzwl	22(%ebp), %eax
	testw	%ax, %ax
	jne	L245
	movl	16(%ebp), %edx
	movl	24(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-12(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-8(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-4(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	24(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-24(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-20(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-16(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	24(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-36(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-32(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-28(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	24(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-48(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-44(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-40(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	jmp	L247
L245:
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-12(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-8(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-4(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-24(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-20(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-16(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-36(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-32(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-28(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-48(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-44(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-40(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
L247:
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE29:
	.globl	_dessineMotif1
	.def	_dessineMotif1;	.scl	2;	.type	32;	.endef
_dessineMotif1:
LFB30:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$104, %esp
	movl	44(%ebp), %eax
	movb	%al, -68(%ebp)
	movl	32(%ebp), %eax
	movl	%eax, -28(%ebp)
	movl	36(%ebp), %eax
	movl	40(%ebp), %edx
	shrl	%edx
	addl	%edx, %eax
	movl	%eax, -24(%ebp)
	movl	40(%ebp), %eax
	shrl	%eax
	movl	%eax, -20(%ebp)
	movl	32(%ebp), %eax
	movl	40(%ebp), %edx
	shrl	%edx
	addl	%edx, %eax
	movl	%eax, -40(%ebp)
	movl	36(%ebp), %eax
	movl	40(%ebp), %edx
	shrl	%edx
	addl	%edx, %eax
	movl	%eax, -36(%ebp)
	movl	40(%ebp), %eax
	shrl	%eax
	movl	%eax, -32(%ebp)
	movl	32(%ebp), %eax
	movl	%eax, -52(%ebp)
	movl	36(%ebp), %eax
	movl	%eax, -48(%ebp)
	movl	40(%ebp), %eax
	shrl	%eax
	movl	%eax, -44(%ebp)
	movl	32(%ebp), %eax
	movl	40(%ebp), %edx
	shrl	%edx
	addl	%edx, %eax
	movl	%eax, -64(%ebp)
	movl	36(%ebp), %eax
	movl	%eax, -60(%ebp)
	movl	40(%ebp), %eax
	shrl	%eax
	movl	%eax, -56(%ebp)
	movzwl	22(%ebp), %eax
	testw	%ax, %ax
	jne	L249
	movl	16(%ebp), %edx
	movl	24(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-28(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-24(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-20(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	24(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-40(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-36(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-32(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-52(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-48(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-44(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-64(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-60(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-56(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	cmpb	$0, -68(%ebp)
	je	L256
	movl	-48(%ebp), %edx
	movl	-44(%ebp), %eax
	addl	%edx, %eax
	addl	$3, %eax
	movl	%eax, -12(%ebp)
	movl	-52(%ebp), %eax
	movl	%eax, -4(%ebp)
	jmp	L251
L252:
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	movl	-4(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-12(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	movl	-4(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-12(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	2(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %eax
	movl	-4(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-12(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	4(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -74(%ebp)
	filds	-74(%ebp)
	fldl	LC0
	fmulp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	2(%eax), %eax
	movw	%ax, -74(%ebp)
	filds	-74(%ebp)
	fldl	LC1
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	4(%eax), %eax
	movw	%ax, -74(%ebp)
	filds	-74(%ebp)
	fldl	LC2
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	8(%ebp), %eax
	movl	20(%eax), %eax
	movl	-4(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-12(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	fnstcw	-66(%ebp)
	movzwl	-66(%ebp), %eax
	orb	$12, %ah
	movw	%ax, -70(%ebp)
	fldcw	-70(%ebp)
	fistps	-72(%ebp)
	fldcw	-66(%ebp)
	movzwl	-72(%ebp), %eax
	movw	%ax, (%edx)
	addl	$1, -4(%ebp)
L251:
	movl	-52(%ebp), %eax
	movl	-44(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movl	-4(%ebp), %eax
	cmpl	%eax, %edx
	ja	L252
	jmp	L256
L249:
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-28(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-24(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-20(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-40(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-36(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-32(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	24(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-52(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-48(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-44(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	24(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-64(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-60(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-56(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	cmpb	$0, -68(%ebp)
	je	L256
	movl	-48(%ebp), %edx
	movl	-44(%ebp), %eax
	addl	%edx, %eax
	subl	$4, %eax
	movl	%eax, -16(%ebp)
	movl	-52(%ebp), %eax
	movl	%eax, -8(%ebp)
	jmp	L254
L255:
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	movl	-8(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-16(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	movl	-8(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-16(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	2(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %eax
	movl	-8(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-16(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	4(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -74(%ebp)
	filds	-74(%ebp)
	fldl	LC0
	fmulp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	2(%eax), %eax
	movw	%ax, -74(%ebp)
	filds	-74(%ebp)
	fldl	LC1
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	4(%eax), %eax
	movw	%ax, -74(%ebp)
	filds	-74(%ebp)
	fldl	LC2
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	8(%ebp), %eax
	movl	20(%eax), %eax
	movl	-8(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-16(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	fnstcw	-66(%ebp)
	movzwl	-66(%ebp), %eax
	orb	$12, %ah
	movw	%ax, -70(%ebp)
	fldcw	-70(%ebp)
	fistps	-72(%ebp)
	fldcw	-66(%ebp)
	movzwl	-72(%ebp), %eax
	movw	%ax, (%edx)
	addl	$1, -8(%ebp)
L254:
	movl	-52(%ebp), %eax
	movl	-44(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movl	-8(%ebp), %eax
	cmpl	%eax, %edx
	ja	L255
L256:
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE30:
	.globl	_dessineMotif2
	.def	_dessineMotif2;	.scl	2;	.type	32;	.endef
_dessineMotif2:
LFB31:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$104, %esp
	movl	44(%ebp), %eax
	movb	%al, -68(%ebp)
	movl	32(%ebp), %eax
	movl	%eax, -28(%ebp)
	movl	36(%ebp), %eax
	movl	40(%ebp), %edx
	shrl	%edx
	addl	%edx, %eax
	movl	%eax, -24(%ebp)
	movl	40(%ebp), %eax
	shrl	%eax
	movl	%eax, -20(%ebp)
	movl	32(%ebp), %eax
	movl	40(%ebp), %edx
	shrl	%edx
	addl	%edx, %eax
	movl	%eax, -40(%ebp)
	movl	36(%ebp), %eax
	movl	40(%ebp), %edx
	shrl	%edx
	addl	%edx, %eax
	movl	%eax, -36(%ebp)
	movl	40(%ebp), %eax
	shrl	%eax
	movl	%eax, -32(%ebp)
	movl	32(%ebp), %eax
	movl	%eax, -52(%ebp)
	movl	36(%ebp), %eax
	movl	%eax, -48(%ebp)
	movl	40(%ebp), %eax
	shrl	%eax
	movl	%eax, -44(%ebp)
	movl	32(%ebp), %eax
	movl	40(%ebp), %edx
	shrl	%edx
	addl	%edx, %eax
	movl	%eax, -64(%ebp)
	movl	36(%ebp), %eax
	movl	%eax, -60(%ebp)
	movl	40(%ebp), %eax
	shrl	%eax
	movl	%eax, -56(%ebp)
	movzwl	22(%ebp), %eax
	testw	%ax, %ax
	jne	L258
	movl	16(%ebp), %edx
	movl	24(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-28(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-24(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-20(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-40(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-36(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-32(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	24(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-52(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-48(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-44(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-64(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-60(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-56(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	cmpb	$0, -68(%ebp)
	je	L265
	movl	-52(%ebp), %edx
	movl	-44(%ebp), %eax
	addl	%edx, %eax
	subl	$4, %eax
	movl	%eax, -12(%ebp)
	movl	-48(%ebp), %eax
	movl	%eax, -4(%ebp)
	jmp	L260
L261:
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-4(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-4(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	2(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-4(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	4(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -74(%ebp)
	filds	-74(%ebp)
	fldl	LC0
	fmulp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	2(%eax), %eax
	movw	%ax, -74(%ebp)
	filds	-74(%ebp)
	fldl	LC1
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	4(%eax), %eax
	movw	%ax, -74(%ebp)
	filds	-74(%ebp)
	fldl	LC2
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	8(%ebp), %eax
	movl	20(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-4(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	fnstcw	-66(%ebp)
	movzwl	-66(%ebp), %eax
	orb	$12, %ah
	movw	%ax, -70(%ebp)
	fldcw	-70(%ebp)
	fistps	-72(%ebp)
	fldcw	-66(%ebp)
	movzwl	-72(%ebp), %eax
	movw	%ax, (%edx)
	addl	$1, -4(%ebp)
L260:
	movl	-48(%ebp), %eax
	movl	-44(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movl	-4(%ebp), %eax
	cmpl	%eax, %edx
	ja	L261
	jmp	L265
L258:
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-28(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-24(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-20(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	24(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-40(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-36(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-32(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-52(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-48(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-44(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	24(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-64(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-60(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-56(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	cmpb	$0, -68(%ebp)
	je	L265
	movl	-52(%ebp), %edx
	movl	-44(%ebp), %eax
	addl	%edx, %eax
	addl	$3, %eax
	movl	%eax, -16(%ebp)
	movl	-48(%ebp), %eax
	movl	%eax, -8(%ebp)
	jmp	L263
L264:
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	movl	-16(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-8(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	movl	-16(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-8(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	2(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %eax
	movl	-16(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-8(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	4(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -74(%ebp)
	filds	-74(%ebp)
	fldl	LC0
	fmulp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	2(%eax), %eax
	movw	%ax, -74(%ebp)
	filds	-74(%ebp)
	fldl	LC1
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	4(%eax), %eax
	movw	%ax, -74(%ebp)
	filds	-74(%ebp)
	fldl	LC2
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	8(%ebp), %eax
	movl	20(%eax), %eax
	movl	-16(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-8(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	fnstcw	-66(%ebp)
	movzwl	-66(%ebp), %eax
	orb	$12, %ah
	movw	%ax, -70(%ebp)
	fldcw	-70(%ebp)
	fistps	-72(%ebp)
	fldcw	-66(%ebp)
	movzwl	-72(%ebp), %eax
	movw	%ax, (%edx)
	addl	$1, -8(%ebp)
L263:
	movl	-48(%ebp), %eax
	movl	-44(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movl	-8(%ebp), %eax
	cmpl	%eax, %edx
	ja	L264
L265:
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE31:
	.globl	_dessineMotif3
	.def	_dessineMotif3;	.scl	2;	.type	32;	.endef
_dessineMotif3:
LFB32:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$120, %esp
	movl	44(%ebp), %eax
	movb	%al, -84(%ebp)
	movl	32(%ebp), %eax
	movl	%eax, -36(%ebp)
	movl	36(%ebp), %eax
	movl	40(%ebp), %edx
	shrl	%edx
	addl	%edx, %eax
	movl	%eax, -32(%ebp)
	movl	40(%ebp), %eax
	shrl	%eax
	movl	%eax, -28(%ebp)
	movl	32(%ebp), %eax
	movl	40(%ebp), %edx
	shrl	%edx
	addl	%edx, %eax
	movl	%eax, -48(%ebp)
	movl	36(%ebp), %eax
	movl	40(%ebp), %edx
	shrl	%edx
	addl	%edx, %eax
	movl	%eax, -44(%ebp)
	movl	40(%ebp), %eax
	shrl	%eax
	movl	%eax, -40(%ebp)
	movl	32(%ebp), %eax
	movl	%eax, -60(%ebp)
	movl	36(%ebp), %eax
	movl	%eax, -56(%ebp)
	movl	40(%ebp), %eax
	shrl	%eax
	movl	%eax, -52(%ebp)
	movl	32(%ebp), %eax
	movl	40(%ebp), %edx
	shrl	%edx
	addl	%edx, %eax
	movl	%eax, -72(%ebp)
	movl	36(%ebp), %eax
	movl	%eax, -68(%ebp)
	movl	40(%ebp), %eax
	shrl	%eax
	movl	%eax, -64(%ebp)
	movzwl	22(%ebp), %eax
	testw	%ax, %ax
	jne	L267
	movl	16(%ebp), %edx
	movl	24(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-36(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-32(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-28(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	24(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-48(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-44(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-40(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	24(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-60(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-56(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-52(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-72(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-68(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-64(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	cmpb	$0, -84(%ebp)
	je	L278
	movl	-60(%ebp), %edx
	movl	-52(%ebp), %eax
	addl	%edx, %eax
	subl	$4, %eax
	movl	%eax, -4(%ebp)
	movl	-56(%ebp), %eax
	movl	%eax, -8(%ebp)
	jmp	L269
L270:
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	movl	-4(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-8(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	movl	-4(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-8(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	2(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %eax
	movl	-4(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-8(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	4(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC0
	fmulp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	2(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC1
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	4(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC2
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	8(%ebp), %eax
	movl	20(%eax), %eax
	movl	-4(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-8(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	fnstcw	-82(%ebp)
	movzwl	-82(%ebp), %eax
	orb	$12, %ah
	movw	%ax, -86(%ebp)
	fldcw	-86(%ebp)
	fistps	-88(%ebp)
	fldcw	-82(%ebp)
	movzwl	-88(%ebp), %eax
	movw	%ax, (%edx)
	addl	$1, -8(%ebp)
L269:
	movl	-56(%ebp), %edx
	movl	-52(%ebp), %eax
	addl	%edx, %eax
	leal	3(%eax), %edx
	movl	-8(%ebp), %eax
	cmpl	%eax, %edx
	ja	L270
	movl	-56(%ebp), %edx
	movl	-52(%ebp), %eax
	addl	%edx, %eax
	addl	$3, %eax
	movl	%eax, -20(%ebp)
	movl	-60(%ebp), %edx
	movl	-52(%ebp), %eax
	addl	%edx, %eax
	subl	$4, %eax
	movl	%eax, -4(%ebp)
	jmp	L271
L272:
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	movl	-4(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-20(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	movl	-4(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-20(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	2(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %eax
	movl	-4(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-20(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	4(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC0
	fmulp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	2(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC1
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	4(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC2
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	8(%ebp), %eax
	movl	20(%eax), %eax
	movl	-4(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-20(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	fnstcw	-82(%ebp)
	movzwl	-82(%ebp), %eax
	orb	$12, %ah
	movw	%ax, -86(%ebp)
	fldcw	-86(%ebp)
	fistps	-88(%ebp)
	fldcw	-82(%ebp)
	movzwl	-88(%ebp), %eax
	movw	%ax, (%edx)
	addl	$1, -4(%ebp)
L271:
	movl	-60(%ebp), %eax
	movl	-52(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movl	-4(%ebp), %eax
	cmpl	%eax, %edx
	ja	L272
	jmp	L278
L267:
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-36(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-32(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-28(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-48(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-44(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-40(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-60(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-56(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-52(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	24(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-72(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-68(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-64(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	cmpb	$0, -84(%ebp)
	je	L278
	movl	-60(%ebp), %edx
	movl	-52(%ebp), %eax
	addl	%edx, %eax
	addl	$3, %eax
	movl	%eax, -12(%ebp)
	movl	-56(%ebp), %eax
	movl	%eax, -16(%ebp)
	jmp	L274
L275:
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-16(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-16(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	2(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-16(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	4(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC0
	fmulp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	2(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC1
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	4(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC2
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	8(%ebp), %eax
	movl	20(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-16(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	fnstcw	-82(%ebp)
	movzwl	-82(%ebp), %eax
	orb	$12, %ah
	movw	%ax, -86(%ebp)
	fldcw	-86(%ebp)
	fistps	-88(%ebp)
	fldcw	-82(%ebp)
	movzwl	-88(%ebp), %eax
	movw	%ax, (%edx)
	addl	$1, -16(%ebp)
L274:
	movl	-56(%ebp), %edx
	movl	-52(%ebp), %eax
	addl	%edx, %eax
	leal	-4(%eax), %edx
	movl	-16(%ebp), %eax
	cmpl	%eax, %edx
	ja	L275
	movl	-56(%ebp), %edx
	movl	-52(%ebp), %eax
	addl	%edx, %eax
	subl	$4, %eax
	movl	%eax, -24(%ebp)
	movl	-60(%ebp), %edx
	movl	-52(%ebp), %eax
	addl	%edx, %eax
	addl	$3, %eax
	movl	%eax, -12(%ebp)
	jmp	L276
L277:
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-24(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-24(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	2(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-24(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	4(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC0
	fmulp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	2(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC1
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	4(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC2
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	8(%ebp), %eax
	movl	20(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-24(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	fnstcw	-82(%ebp)
	movzwl	-82(%ebp), %eax
	orb	$12, %ah
	movw	%ax, -86(%ebp)
	fldcw	-86(%ebp)
	fistps	-88(%ebp)
	fldcw	-82(%ebp)
	movzwl	-88(%ebp), %eax
	movw	%ax, (%edx)
	addl	$1, -12(%ebp)
L276:
	movl	-60(%ebp), %eax
	movl	-52(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movl	-12(%ebp), %eax
	cmpl	%eax, %edx
	ja	L277
L278:
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE32:
	.globl	_dessineMotif4
	.def	_dessineMotif4;	.scl	2;	.type	32;	.endef
_dessineMotif4:
LFB33:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$120, %esp
	movl	44(%ebp), %eax
	movb	%al, -84(%ebp)
	movl	32(%ebp), %eax
	movl	%eax, -36(%ebp)
	movl	36(%ebp), %eax
	movl	40(%ebp), %edx
	shrl	%edx
	addl	%edx, %eax
	movl	%eax, -32(%ebp)
	movl	40(%ebp), %eax
	shrl	%eax
	movl	%eax, -28(%ebp)
	movl	32(%ebp), %eax
	movl	40(%ebp), %edx
	shrl	%edx
	addl	%edx, %eax
	movl	%eax, -48(%ebp)
	movl	36(%ebp), %eax
	movl	40(%ebp), %edx
	shrl	%edx
	addl	%edx, %eax
	movl	%eax, -44(%ebp)
	movl	40(%ebp), %eax
	shrl	%eax
	movl	%eax, -40(%ebp)
	movl	32(%ebp), %eax
	movl	%eax, -60(%ebp)
	movl	36(%ebp), %eax
	movl	%eax, -56(%ebp)
	movl	40(%ebp), %eax
	shrl	%eax
	movl	%eax, -52(%ebp)
	movl	32(%ebp), %eax
	movl	40(%ebp), %edx
	shrl	%edx
	addl	%edx, %eax
	movl	%eax, -72(%ebp)
	movl	36(%ebp), %eax
	movl	%eax, -68(%ebp)
	movl	40(%ebp), %eax
	shrl	%eax
	movl	%eax, -64(%ebp)
	movzwl	22(%ebp), %eax
	testw	%ax, %ax
	jne	L280
	movl	16(%ebp), %edx
	movl	24(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-36(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-32(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-28(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-48(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-44(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-40(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	24(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-60(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-56(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-52(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	24(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-72(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-68(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-64(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	cmpb	$0, -84(%ebp)
	je	L291
	movl	-60(%ebp), %edx
	movl	-52(%ebp), %eax
	addl	%edx, %eax
	subl	$4, %eax
	movl	%eax, -4(%ebp)
	movl	-56(%ebp), %eax
	movl	-52(%ebp), %edx
	addl	%edx, %edx
	addl	%edx, %eax
	movl	%eax, -8(%ebp)
	jmp	L282
L283:
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	movl	-4(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-8(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	movl	-4(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-8(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	2(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %eax
	movl	-4(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-8(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	4(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC0
	fmulp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	2(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC1
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	4(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC2
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	8(%ebp), %eax
	movl	20(%eax), %eax
	movl	-4(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-8(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	fnstcw	-82(%ebp)
	movzwl	-82(%ebp), %eax
	orb	$12, %ah
	movw	%ax, -86(%ebp)
	fldcw	-86(%ebp)
	fistps	-88(%ebp)
	fldcw	-82(%ebp)
	movzwl	-88(%ebp), %eax
	movw	%ax, (%edx)
	subl	$1, -8(%ebp)
L282:
	movl	-56(%ebp), %edx
	movl	-52(%ebp), %eax
	addl	%edx, %eax
	leal	-4(%eax), %edx
	movl	-8(%ebp), %eax
	cmpl	%eax, %edx
	jb	L283
	movl	-56(%ebp), %edx
	movl	-52(%ebp), %eax
	addl	%edx, %eax
	subl	$4, %eax
	movl	%eax, -20(%ebp)
	movl	-60(%ebp), %edx
	movl	-52(%ebp), %eax
	addl	%edx, %eax
	subl	$4, %eax
	movl	%eax, -4(%ebp)
	jmp	L284
L285:
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	movl	-4(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-20(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	movl	-4(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-20(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	2(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %eax
	movl	-4(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-20(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	4(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC0
	fmulp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	2(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC1
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	4(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC2
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	8(%ebp), %eax
	movl	20(%eax), %eax
	movl	-4(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-20(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	fnstcw	-82(%ebp)
	movzwl	-82(%ebp), %eax
	orb	$12, %ah
	movw	%ax, -86(%ebp)
	fldcw	-86(%ebp)
	fistps	-88(%ebp)
	fldcw	-82(%ebp)
	movzwl	-88(%ebp), %eax
	movw	%ax, (%edx)
	addl	$1, -4(%ebp)
L284:
	movl	-60(%ebp), %eax
	movl	-52(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movl	-4(%ebp), %eax
	cmpl	%eax, %edx
	ja	L285
	jmp	L291
L280:
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-36(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-32(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-28(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	24(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-48(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-44(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-40(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-60(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-56(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-52(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-72(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-68(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-64(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	cmpb	$0, -84(%ebp)
	je	L291
	movl	-60(%ebp), %edx
	movl	-52(%ebp), %eax
	addl	%edx, %eax
	addl	$3, %eax
	movl	%eax, -12(%ebp)
	movl	-56(%ebp), %eax
	movl	-52(%ebp), %edx
	addl	%edx, %edx
	addl	%edx, %eax
	movl	%eax, -16(%ebp)
	jmp	L287
L288:
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-16(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-16(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	2(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-16(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	4(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC0
	fmulp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	2(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC1
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	4(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC2
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	8(%ebp), %eax
	movl	20(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-16(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	fnstcw	-82(%ebp)
	movzwl	-82(%ebp), %eax
	orb	$12, %ah
	movw	%ax, -86(%ebp)
	fldcw	-86(%ebp)
	fistps	-88(%ebp)
	fldcw	-82(%ebp)
	movzwl	-88(%ebp), %eax
	movw	%ax, (%edx)
	subl	$1, -16(%ebp)
L287:
	movl	-56(%ebp), %edx
	movl	-52(%ebp), %eax
	addl	%edx, %eax
	leal	3(%eax), %edx
	movl	-16(%ebp), %eax
	cmpl	%eax, %edx
	jb	L288
	movl	-56(%ebp), %edx
	movl	-52(%ebp), %eax
	addl	%edx, %eax
	addl	$3, %eax
	movl	%eax, -24(%ebp)
	movl	-60(%ebp), %edx
	movl	-52(%ebp), %eax
	addl	%edx, %eax
	addl	$3, %eax
	movl	%eax, -12(%ebp)
	jmp	L289
L290:
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-24(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-24(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	2(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-24(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	4(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC0
	fmulp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	2(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC1
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	4(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC2
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	8(%ebp), %eax
	movl	20(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-24(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	fnstcw	-82(%ebp)
	movzwl	-82(%ebp), %eax
	orb	$12, %ah
	movw	%ax, -86(%ebp)
	fldcw	-86(%ebp)
	fistps	-88(%ebp)
	fldcw	-82(%ebp)
	movzwl	-88(%ebp), %eax
	movw	%ax, (%edx)
	addl	$1, -12(%ebp)
L289:
	movl	-60(%ebp), %eax
	movl	-52(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movl	-12(%ebp), %eax
	cmpl	%eax, %edx
	ja	L290
L291:
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE33:
	.globl	_dessineMotif5
	.def	_dessineMotif5;	.scl	2;	.type	32;	.endef
_dessineMotif5:
LFB34:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$120, %esp
	movl	44(%ebp), %eax
	movb	%al, -84(%ebp)
	movl	32(%ebp), %eax
	movl	%eax, -40(%ebp)
	movl	36(%ebp), %eax
	movl	40(%ebp), %edx
	shrl	%edx
	addl	%edx, %eax
	movl	%eax, -36(%ebp)
	movl	40(%ebp), %eax
	shrl	%eax
	movl	%eax, -32(%ebp)
	movl	32(%ebp), %eax
	movl	40(%ebp), %edx
	shrl	%edx
	addl	%edx, %eax
	movl	%eax, -52(%ebp)
	movl	36(%ebp), %eax
	movl	40(%ebp), %edx
	shrl	%edx
	addl	%edx, %eax
	movl	%eax, -48(%ebp)
	movl	40(%ebp), %eax
	shrl	%eax
	movl	%eax, -44(%ebp)
	movl	32(%ebp), %eax
	movl	%eax, -64(%ebp)
	movl	36(%ebp), %eax
	movl	%eax, -60(%ebp)
	movl	40(%ebp), %eax
	shrl	%eax
	movl	%eax, -56(%ebp)
	movl	32(%ebp), %eax
	movl	40(%ebp), %edx
	shrl	%edx
	addl	%edx, %eax
	movl	%eax, -76(%ebp)
	movl	36(%ebp), %eax
	movl	%eax, -72(%ebp)
	movl	40(%ebp), %eax
	shrl	%eax
	movl	%eax, -68(%ebp)
	movzwl	22(%ebp), %eax
	testw	%ax, %ax
	jne	L293
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-40(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-36(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-32(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	24(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-52(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-48(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-44(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	24(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-64(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-60(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-56(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	24(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-76(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-72(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-68(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	cmpb	$0, -84(%ebp)
	je	L304
	movl	-60(%ebp), %edx
	movl	-56(%ebp), %eax
	addl	%edx, %eax
	subl	$4, %eax
	movl	%eax, -4(%ebp)
	movl	-64(%ebp), %eax
	movl	%eax, -8(%ebp)
	jmp	L295
L296:
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	movl	-8(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-4(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	movl	-8(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-4(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	2(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %eax
	movl	-8(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-4(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	4(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC0
	fmulp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	2(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC1
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	4(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC2
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	8(%ebp), %eax
	movl	20(%eax), %eax
	movl	-8(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-4(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	fnstcw	-82(%ebp)
	movzwl	-82(%ebp), %eax
	orb	$12, %ah
	movw	%ax, -86(%ebp)
	fldcw	-86(%ebp)
	fistps	-88(%ebp)
	fldcw	-82(%ebp)
	movzwl	-88(%ebp), %eax
	movw	%ax, (%edx)
	addl	$1, -8(%ebp)
L295:
	movl	-64(%ebp), %edx
	movl	-56(%ebp), %eax
	addl	%edx, %eax
	leal	3(%eax), %edx
	movl	-8(%ebp), %eax
	cmpl	%eax, %edx
	ja	L296
	movl	-64(%ebp), %edx
	movl	-56(%ebp), %eax
	addl	%edx, %eax
	addl	$3, %eax
	movl	%eax, -20(%ebp)
	movl	-60(%ebp), %edx
	movl	-56(%ebp), %eax
	addl	%edx, %eax
	subl	$4, %eax
	movl	%eax, -4(%ebp)
	jmp	L297
L298:
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	movl	-20(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-4(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	movl	-20(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-4(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	2(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %eax
	movl	-20(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-4(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	4(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC0
	fmulp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	2(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC1
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	4(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC2
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	8(%ebp), %eax
	movl	20(%eax), %eax
	movl	-20(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-4(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	fnstcw	-82(%ebp)
	movzwl	-82(%ebp), %eax
	orb	$12, %ah
	movw	%ax, -86(%ebp)
	fldcw	-86(%ebp)
	fistps	-88(%ebp)
	fldcw	-82(%ebp)
	movzwl	-88(%ebp), %eax
	movw	%ax, (%edx)
	addl	$1, -4(%ebp)
L297:
	movl	-60(%ebp), %eax
	movl	-56(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movl	-4(%ebp), %eax
	cmpl	%eax, %edx
	ja	L298
	jmp	L304
L293:
	movl	16(%ebp), %edx
	movl	24(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-40(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-36(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-32(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-52(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-48(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-44(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-64(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-60(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-56(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-76(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-72(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-68(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	cmpb	$0, -84(%ebp)
	je	L304
	movl	-60(%ebp), %edx
	movl	-56(%ebp), %eax
	addl	%edx, %eax
	addl	$3, %eax
	movl	%eax, -24(%ebp)
	movl	-64(%ebp), %eax
	movl	%eax, -12(%ebp)
	jmp	L300
L301:
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-24(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-24(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	2(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-24(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	4(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC0
	fmulp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	2(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC1
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	4(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC2
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	8(%ebp), %eax
	movl	20(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-24(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	fnstcw	-82(%ebp)
	movzwl	-82(%ebp), %eax
	orb	$12, %ah
	movw	%ax, -86(%ebp)
	fldcw	-86(%ebp)
	fistps	-88(%ebp)
	fldcw	-82(%ebp)
	movzwl	-88(%ebp), %eax
	movw	%ax, (%edx)
	addl	$1, -12(%ebp)
L300:
	movl	-64(%ebp), %edx
	movl	-56(%ebp), %eax
	addl	%edx, %eax
	leal	-4(%eax), %edx
	movl	-12(%ebp), %eax
	cmpl	%eax, %edx
	ja	L301
	movl	-64(%ebp), %edx
	movl	-56(%ebp), %eax
	addl	%edx, %eax
	subl	$4, %eax
	movl	%eax, -28(%ebp)
	movl	-60(%ebp), %edx
	movl	-56(%ebp), %eax
	addl	%edx, %eax
	addl	$3, %eax
	movl	%eax, -16(%ebp)
	jmp	L302
L303:
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	movl	-28(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-16(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	movl	-28(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-16(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	2(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %eax
	movl	-28(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-16(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	4(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC0
	fmulp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	2(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC1
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	4(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC2
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	8(%ebp), %eax
	movl	20(%eax), %eax
	movl	-28(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-16(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	fnstcw	-82(%ebp)
	movzwl	-82(%ebp), %eax
	orb	$12, %ah
	movw	%ax, -86(%ebp)
	fldcw	-86(%ebp)
	fistps	-88(%ebp)
	fldcw	-82(%ebp)
	movzwl	-88(%ebp), %eax
	movw	%ax, (%edx)
	addl	$1, -16(%ebp)
L302:
	movl	-60(%ebp), %eax
	movl	-56(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movl	-16(%ebp), %eax
	cmpl	%eax, %edx
	ja	L303
L304:
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE34:
	.globl	_dessineMotif6
	.def	_dessineMotif6;	.scl	2;	.type	32;	.endef
_dessineMotif6:
LFB35:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$120, %esp
	movl	44(%ebp), %eax
	movb	%al, -84(%ebp)
	movl	32(%ebp), %eax
	movl	%eax, -44(%ebp)
	movl	36(%ebp), %eax
	movl	40(%ebp), %edx
	shrl	%edx
	addl	%edx, %eax
	movl	%eax, -40(%ebp)
	movl	40(%ebp), %eax
	shrl	%eax
	movl	%eax, -36(%ebp)
	movl	32(%ebp), %eax
	movl	40(%ebp), %edx
	shrl	%edx
	addl	%edx, %eax
	movl	%eax, -56(%ebp)
	movl	36(%ebp), %eax
	movl	40(%ebp), %edx
	shrl	%edx
	addl	%edx, %eax
	movl	%eax, -52(%ebp)
	movl	40(%ebp), %eax
	shrl	%eax
	movl	%eax, -48(%ebp)
	movl	32(%ebp), %eax
	movl	%eax, -68(%ebp)
	movl	36(%ebp), %eax
	movl	%eax, -64(%ebp)
	movl	40(%ebp), %eax
	shrl	%eax
	movl	%eax, -60(%ebp)
	movl	32(%ebp), %eax
	movl	40(%ebp), %edx
	shrl	%edx
	addl	%edx, %eax
	movl	%eax, -80(%ebp)
	movl	36(%ebp), %eax
	movl	%eax, -76(%ebp)
	movl	40(%ebp), %eax
	shrl	%eax
	movl	%eax, -72(%ebp)
	movzwl	22(%ebp), %eax
	testw	%ax, %ax
	jne	L306
	movl	16(%ebp), %edx
	movl	24(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-44(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-40(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-36(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	24(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-56(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-52(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-48(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-68(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-64(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-60(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	24(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-80(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-76(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-72(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	cmpb	$0, -84(%ebp)
	je	L317
	movl	-64(%ebp), %edx
	movl	-60(%ebp), %eax
	addl	%edx, %eax
	addl	$3, %eax
	movl	%eax, -20(%ebp)
	movl	-68(%ebp), %eax
	movl	%eax, -4(%ebp)
	jmp	L308
L309:
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	movl	-4(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-20(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	movl	-4(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-20(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	2(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %eax
	movl	-4(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-20(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	4(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC0
	fmulp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	2(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC1
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	4(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC2
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	8(%ebp), %eax
	movl	20(%eax), %eax
	movl	-4(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-20(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	fnstcw	-82(%ebp)
	movzwl	-82(%ebp), %eax
	orb	$12, %ah
	movw	%ax, -86(%ebp)
	fldcw	-86(%ebp)
	fistps	-88(%ebp)
	fldcw	-82(%ebp)
	movzwl	-88(%ebp), %eax
	movw	%ax, (%edx)
	addl	$1, -4(%ebp)
L308:
	movl	-68(%ebp), %edx
	movl	-60(%ebp), %eax
	addl	%edx, %eax
	leal	3(%eax), %edx
	movl	-4(%ebp), %eax
	cmpl	%eax, %edx
	ja	L309
	movl	-68(%ebp), %edx
	movl	-60(%ebp), %eax
	addl	%edx, %eax
	addl	$3, %eax
	movl	%eax, -24(%ebp)
	movl	-64(%ebp), %edx
	movl	-60(%ebp), %eax
	addl	%edx, %eax
	addl	$3, %eax
	movl	%eax, -8(%ebp)
	jmp	L310
L311:
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	movl	-24(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-8(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	movl	-24(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-8(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	2(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %eax
	movl	-24(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-8(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	4(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC0
	fmulp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	2(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC1
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	4(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC2
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	8(%ebp), %eax
	movl	20(%eax), %eax
	movl	-24(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-8(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	fnstcw	-82(%ebp)
	movzwl	-82(%ebp), %eax
	orb	$12, %ah
	movw	%ax, -86(%ebp)
	fldcw	-86(%ebp)
	fistps	-88(%ebp)
	fldcw	-82(%ebp)
	movzwl	-88(%ebp), %eax
	movw	%ax, (%edx)
	subl	$1, -8(%ebp)
L310:
	movl	-64(%ebp), %edx
	movl	-8(%ebp), %eax
	cmpl	%eax, %edx
	jbe	L311
	jmp	L317
L306:
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-44(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-40(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-36(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-56(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-52(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-48(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	24(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-68(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-64(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-60(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	-80(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	-76(%ebp), %edx
	movl	%edx, 16(%esp)
	movl	-72(%ebp), %edx
	movl	%edx, 20(%esp)
	movl	(%eax), %edx
	movl	%edx, 4(%esp)
	movzwl	4(%eax), %eax
	movw	%ax, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarre
	cmpb	$0, -84(%ebp)
	je	L317
	movl	-64(%ebp), %edx
	movl	-60(%ebp), %eax
	addl	%edx, %eax
	subl	$4, %eax
	movl	%eax, -28(%ebp)
	movl	-68(%ebp), %eax
	movl	%eax, -12(%ebp)
	jmp	L313
L314:
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-28(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-28(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	2(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-28(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	4(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC0
	fmulp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	2(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC1
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	4(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC2
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	8(%ebp), %eax
	movl	20(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-28(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	fnstcw	-82(%ebp)
	movzwl	-82(%ebp), %eax
	orb	$12, %ah
	movw	%ax, -86(%ebp)
	fldcw	-86(%ebp)
	fistps	-88(%ebp)
	fldcw	-82(%ebp)
	movzwl	-88(%ebp), %eax
	movw	%ax, (%edx)
	addl	$1, -12(%ebp)
L313:
	movl	-68(%ebp), %edx
	movl	-60(%ebp), %eax
	addl	%edx, %eax
	leal	-4(%eax), %edx
	movl	-12(%ebp), %eax
	cmpl	%eax, %edx
	ja	L314
	movl	-68(%ebp), %edx
	movl	-60(%ebp), %eax
	addl	%edx, %eax
	subl	$4, %eax
	movl	%eax, -32(%ebp)
	movl	-64(%ebp), %edx
	movl	-60(%ebp), %eax
	addl	%edx, %eax
	subl	$4, %eax
	movl	%eax, -16(%ebp)
	jmp	L315
L316:
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	movl	-32(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-16(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	movl	-32(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-16(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	2(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%edx,%eax), %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %eax
	movl	-32(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-16(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movzwl	4(%ecx), %eax
	movw	%ax, (%edx)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC0
	fmulp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	2(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC1
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	16(%ebp), %edx
	movl	28(%ebp), %eax
	movl	%eax, %ecx
	movl	%ecx, %eax
	addl	%eax, %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movzwl	4(%eax), %eax
	movw	%ax, -90(%ebp)
	filds	-90(%ebp)
	fldl	LC2
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	movl	8(%ebp), %eax
	movl	20(%eax), %eax
	movl	-32(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	-16(%ebp), %edx
	addl	%edx, %edx
	addl	%eax, %edx
	fnstcw	-82(%ebp)
	movzwl	-82(%ebp), %eax
	orb	$12, %ah
	movw	%ax, -86(%ebp)
	fldcw	-86(%ebp)
	fistps	-88(%ebp)
	fldcw	-82(%ebp)
	movzwl	-88(%ebp), %eax
	movw	%ax, (%edx)
	subl	$1, -16(%ebp)
L315:
	movl	-64(%ebp), %edx
	movl	-16(%ebp), %eax
	cmpl	%eax, %edx
	jbe	L316
L317:
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE35:
	.globl	_dessineMotifNoeud
	.def	_dessineMotifNoeud;	.scl	2;	.type	32;	.endef
_dessineMotifNoeud:
LFB36:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$64, %esp
	movl	28(%ebp), %eax
	movb	%al, -20(%ebp)
	movl	20(%ebp), %eax
	movl	28(%eax), %eax
	movl	%eax, -12(%ebp)
	movl	20(%ebp), %eax
	movl	32(%eax), %eax
	movl	%eax, -8(%ebp)
	movl	$32, -4(%ebp)
	cmpl	$6, 24(%ebp)
	ja	L329
	movl	24(%ebp), %eax
	sall	$2, %eax
	addl	$L321, %eax
	movl	(%eax), %eax
	jmp	*%eax
	.section .rdata,"dr"
	.align 4
L321:
	.long	L327
	.long	L326
	.long	L325
	.long	L324
	.long	L323
	.long	L322
	.long	L320
	.text
L327:
	movzbl	-20(%ebp), %eax
	movl	%eax, 36(%esp)
	movl	-12(%ebp), %eax
	movl	%eax, 24(%esp)
	movl	-8(%ebp), %eax
	movl	%eax, 28(%esp)
	movl	-4(%ebp), %eax
	movl	%eax, 32(%esp)
	movl	20(%ebp), %eax
	movl	16(%eax), %edx
	movl	%edx, 12(%esp)
	movl	20(%eax), %edx
	movl	%edx, 16(%esp)
	movl	24(%eax), %eax
	movl	%eax, 20(%esp)
	movl	12(%ebp), %eax
	movl	16(%ebp), %edx
	movl	%eax, 4(%esp)
	movl	%edx, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineMotif0
	jmp	L328
L326:
	movzbl	-20(%ebp), %eax
	movl	%eax, 36(%esp)
	movl	-12(%ebp), %eax
	movl	%eax, 24(%esp)
	movl	-8(%ebp), %eax
	movl	%eax, 28(%esp)
	movl	-4(%ebp), %eax
	movl	%eax, 32(%esp)
	movl	20(%ebp), %eax
	movl	16(%eax), %edx
	movl	%edx, 12(%esp)
	movl	20(%eax), %edx
	movl	%edx, 16(%esp)
	movl	24(%eax), %eax
	movl	%eax, 20(%esp)
	movl	12(%ebp), %eax
	movl	16(%ebp), %edx
	movl	%eax, 4(%esp)
	movl	%edx, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineMotif1
	jmp	L328
L325:
	movzbl	-20(%ebp), %eax
	movl	%eax, 36(%esp)
	movl	-12(%ebp), %eax
	movl	%eax, 24(%esp)
	movl	-8(%ebp), %eax
	movl	%eax, 28(%esp)
	movl	-4(%ebp), %eax
	movl	%eax, 32(%esp)
	movl	20(%ebp), %eax
	movl	16(%eax), %edx
	movl	%edx, 12(%esp)
	movl	20(%eax), %edx
	movl	%edx, 16(%esp)
	movl	24(%eax), %eax
	movl	%eax, 20(%esp)
	movl	12(%ebp), %eax
	movl	16(%ebp), %edx
	movl	%eax, 4(%esp)
	movl	%edx, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineMotif2
	jmp	L328
L324:
	movzbl	-20(%ebp), %eax
	movl	%eax, 36(%esp)
	movl	-12(%ebp), %eax
	movl	%eax, 24(%esp)
	movl	-8(%ebp), %eax
	movl	%eax, 28(%esp)
	movl	-4(%ebp), %eax
	movl	%eax, 32(%esp)
	movl	20(%ebp), %eax
	movl	16(%eax), %edx
	movl	%edx, 12(%esp)
	movl	20(%eax), %edx
	movl	%edx, 16(%esp)
	movl	24(%eax), %eax
	movl	%eax, 20(%esp)
	movl	12(%ebp), %eax
	movl	16(%ebp), %edx
	movl	%eax, 4(%esp)
	movl	%edx, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineMotif3
	jmp	L328
L323:
	movzbl	-20(%ebp), %eax
	movl	%eax, 36(%esp)
	movl	-12(%ebp), %eax
	movl	%eax, 24(%esp)
	movl	-8(%ebp), %eax
	movl	%eax, 28(%esp)
	movl	-4(%ebp), %eax
	movl	%eax, 32(%esp)
	movl	20(%ebp), %eax
	movl	16(%eax), %edx
	movl	%edx, 12(%esp)
	movl	20(%eax), %edx
	movl	%edx, 16(%esp)
	movl	24(%eax), %eax
	movl	%eax, 20(%esp)
	movl	12(%ebp), %eax
	movl	16(%ebp), %edx
	movl	%eax, 4(%esp)
	movl	%edx, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineMotif4
	jmp	L328
L322:
	movzbl	-20(%ebp), %eax
	movl	%eax, 36(%esp)
	movl	-12(%ebp), %eax
	movl	%eax, 24(%esp)
	movl	-8(%ebp), %eax
	movl	%eax, 28(%esp)
	movl	-4(%ebp), %eax
	movl	%eax, 32(%esp)
	movl	20(%ebp), %eax
	movl	16(%eax), %edx
	movl	%edx, 12(%esp)
	movl	20(%eax), %edx
	movl	%edx, 16(%esp)
	movl	24(%eax), %eax
	movl	%eax, 20(%esp)
	movl	12(%ebp), %eax
	movl	16(%ebp), %edx
	movl	%eax, 4(%esp)
	movl	%edx, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineMotif5
	jmp	L328
L320:
	movzbl	-20(%ebp), %eax
	movl	%eax, 36(%esp)
	movl	-12(%ebp), %eax
	movl	%eax, 24(%esp)
	movl	-8(%ebp), %eax
	movl	%eax, 28(%esp)
	movl	-4(%ebp), %eax
	movl	%eax, 32(%esp)
	movl	20(%ebp), %eax
	movl	16(%eax), %edx
	movl	%edx, 12(%esp)
	movl	20(%eax), %edx
	movl	%edx, 16(%esp)
	movl	24(%eax), %eax
	movl	%eax, 20(%esp)
	movl	12(%ebp), %eax
	movl	16(%ebp), %edx
	movl	%eax, 4(%esp)
	movl	%edx, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineMotif6
	jmp	L328
L329:
	nop
L328:
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE36:
	.globl	_dessineCarte
	.def	_dessineCarte;	.scl	2;	.type	32;	.endef
_dessineCarte:
LFB37:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$56, %esp
	movl	24(%ebp), %eax
	movb	%al, -12(%ebp)
	cmpl	$0, 20(%ebp)
	je	L333
	movzbl	-12(%ebp), %edx
	movl	20(%ebp), %eax
	movzwl	16(%eax), %eax
	cwtl
	movl	%edx, 20(%esp)
	movl	%eax, 16(%esp)
	movl	20(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	12(%ebp), %eax
	movl	16(%ebp), %edx
	movl	%eax, 4(%esp)
	movl	%edx, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineMotifNoeud
	movzbl	-12(%ebp), %edx
	movl	20(%ebp), %eax
	movl	(%eax), %eax
	movl	%edx, 16(%esp)
	movl	%eax, 12(%esp)
	movl	12(%ebp), %eax
	movl	16(%ebp), %edx
	movl	%eax, 4(%esp)
	movl	%edx, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarte
	movzbl	-12(%ebp), %edx
	movl	20(%ebp), %eax
	movl	4(%eax), %eax
	movl	%edx, 16(%esp)
	movl	%eax, 12(%esp)
	movl	12(%ebp), %eax
	movl	16(%ebp), %edx
	movl	%eax, 4(%esp)
	movl	%edx, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarte
	movzbl	-12(%ebp), %edx
	movl	20(%ebp), %eax
	movl	8(%eax), %eax
	movl	%edx, 16(%esp)
	movl	%eax, 12(%esp)
	movl	12(%ebp), %eax
	movl	16(%ebp), %edx
	movl	%eax, 4(%esp)
	movl	%edx, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarte
	movzbl	-12(%ebp), %edx
	movl	20(%ebp), %eax
	movl	12(%eax), %eax
	movl	%edx, 16(%esp)
	movl	%eax, 12(%esp)
	movl	12(%ebp), %eax
	movl	16(%ebp), %edx
	movl	%eax, 4(%esp)
	movl	%edx, 8(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_dessineCarte
	jmp	L330
L333:
	nop
L330:
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE37:
	.section .rdata,"dr"
LC4:
	.ascii "w\0"
LC5:
	.ascii "%d mouvements\12\0"
LC6:
	.ascii "Depart: x=%d y=%d\12\0"
LC7:
	.ascii "Arrivee: x=%d y=%d\0"
LC8:
	.ascii "\12%c\0"
	.text
	.globl	_sauveDescriptionChemin
	.def	_sauveDescriptionChemin;	.scl	2;	.type	32;	.endef
_sauveDescriptionChemin:
LFB38:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$40, %esp
	movl	$LC4, 4(%esp)
	movl	12(%ebp), %eax
	movl	%eax, (%esp)
	call	_fopen
	movl	%eax, -24(%ebp)
	movl	$0, -12(%ebp)
	movl	8(%ebp), %eax
	movl	(%eax), %eax
	movl	%eax, -16(%ebp)
	jmp	L335
L336:
	addl	$1, -12(%ebp)
	movl	-16(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -16(%ebp)
L335:
	cmpl	$0, -16(%ebp)
	jne	L336
	movl	-12(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC5, 4(%esp)
	movl	-24(%ebp), %eax
	movl	%eax, (%esp)
	call	_fprintf
	movl	8(%ebp), %eax
	movl	16(%eax), %edx
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	movl	%edx, 12(%esp)
	movl	%eax, 8(%esp)
	movl	$LC6, 4(%esp)
	movl	-24(%ebp), %eax
	movl	%eax, (%esp)
	call	_fprintf
	movl	8(%ebp), %eax
	movl	24(%eax), %edx
	movl	8(%ebp), %eax
	movl	20(%eax), %eax
	movl	%edx, 12(%esp)
	movl	%eax, 8(%esp)
	movl	$LC7, 4(%esp)
	movl	-24(%ebp), %eax
	movl	%eax, (%esp)
	call	_fprintf
	movl	8(%ebp), %eax
	movl	(%eax), %eax
	movl	%eax, -16(%ebp)
	jmp	L337
L346:
	movl	-16(%ebp), %eax
	movl	8(%eax), %ecx
	movl	-16(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 8(%esp)
	movl	%edx, 12(%esp)
	movl	(%ecx), %eax
	movl	4(%ecx), %edx
	movl	%eax, (%esp)
	movl	%edx, 4(%esp)
	call	_positionVoisinage
	cmpl	$8, %eax
	ja	L338
	movl	L340(,%eax,4), %eax
	jmp	*%eax
	.section .rdata,"dr"
	.align 4
L340:
	.long	L338
	.long	L344
	.long	L338
	.long	L343
	.long	L338
	.long	L342
	.long	L338
	.long	L341
	.long	L339
	.text
L344:
	movb	$49, -17(%ebp)
	jmp	L345
L343:
	movb	$51, -17(%ebp)
	jmp	L345
L342:
	movb	$50, -17(%ebp)
	jmp	L345
L341:
	movb	$52, -17(%ebp)
	jmp	L345
L339:
	movl	-16(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -16(%ebp)
	jmp	L337
L338:
	movb	$63, -17(%ebp)
	nop
L345:
	movsbl	-17(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC8, 4(%esp)
	movl	-24(%ebp), %eax
	movl	%eax, (%esp)
	call	_fprintf
	movl	-16(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -16(%ebp)
L337:
	movl	-16(%ebp), %eax
	movl	8(%eax), %eax
	testl	%eax, %eax
	jne	L346
	movl	-24(%ebp), %eax
	movl	%eax, (%esp)
	call	_fclose
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE38:
	.section .rdata,"dr"
	.align 8
LC0:
	.long	858993459
	.long	1070281523
	.align 8
LC1:
	.long	-1972248982
	.long	1072096398
	.align 8
LC2:
	.long	1175103052
	.long	1068660005
	.ident	"GCC: (MinGW.org GCC-8.2.0-5) 8.2.0"
	.def	_malloc;	.scl	2;	.type	32;	.endef
	.def	_free;	.scl	2;	.type	32;	.endef
	.def	_estCouleurUniforme;	.scl	2;	.type	32;	.endef
	.def	_compareCouleur;	.scl	2;	.type	32;	.endef
	.def	_estCouleurUniformeDansCarre;	.scl	2;	.type	32;	.endef
	.def	_fopen;	.scl	2;	.type	32;	.endef
	.def	_fprintf;	.scl	2;	.type	32;	.endef
	.def	_fclose;	.scl	2;	.type	32;	.endef
