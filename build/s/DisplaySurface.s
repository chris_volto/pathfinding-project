	.file	"DisplaySurface.c"
	.text
	.globl	_initDisplaySurface
	.def	_initDisplaySurface;	.scl	2;	.type	32;	.endef
_initDisplaySurface:
LFB13:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$40, %esp
	movl	$32, (%esp)
	call	_malloc
	movl	%eax, -12(%ebp)
	movl	-12(%ebp), %eax
	movl	28(%ebp), %edx
	movl	%edx, 20(%eax)
	movl	-12(%ebp), %eax
	movl	24(%ebp), %edx
	movl	%edx, 16(%eax)
	movl	-12(%ebp), %eax
	movl	16(%ebp), %edx
	movl	%edx, 8(%eax)
	movl	-12(%ebp), %eax
	movl	20(%ebp), %edx
	movl	%edx, 12(%eax)
	movl	-12(%ebp), %eax
	movl	32(%ebp), %edx
	movl	%edx, 24(%eax)
	movl	-12(%ebp), %eax
	movl	36(%ebp), %edx
	movl	%edx, 28(%eax)
	movl	-12(%ebp), %eax
	movl	8(%ebp), %edx
	movl	%edx, (%eax)
	movl	-12(%ebp), %eax
	movl	12(%ebp), %edx
	movl	%edx, 4(%eax)
	movl	-12(%ebp), %eax
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE13:
	.globl	_initDisplaySurfaces
	.def	_initDisplaySurfaces;	.scl	2;	.type	32;	.endef
_initDisplaySurfaces:
LFB14:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$40, %esp
	leal	12(%ebp), %eax
	movl	%eax, -20(%ebp)
	movl	8(%ebp), %eax
	addl	$1, %eax
	sall	$2, %eax
	movl	%eax, (%esp)
	call	_malloc
	movl	%eax, -16(%ebp)
	movl	$0, -12(%ebp)
	jmp	L4
L5:
	movl	-12(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	-16(%ebp), %eax
	leal	(%edx,%eax), %ecx
	movl	-20(%ebp), %eax
	leal	4(%eax), %edx
	movl	%edx, -20(%ebp)
	movl	(%eax), %eax
	movl	%eax, (%ecx)
	addl	$1, -12(%ebp)
L4:
	movl	-12(%ebp), %eax
	cmpl	8(%ebp), %eax
	jl	L5
	movl	8(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	-16(%ebp), %eax
	addl	%edx, %eax
	movl	$0, (%eax)
	movl	-16(%ebp), %eax
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE14:
	.globl	_freeDisplaySurface
	.def	_freeDisplaySurface;	.scl	2;	.type	32;	.endef
_freeDisplaySurface:
LFB15:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$24, %esp
	movl	8(%ebp), %eax
	movl	24(%eax), %eax
	testl	%eax, %eax
	je	L8
	movl	8(%ebp), %eax
	addl	$24, %eax
	movl	%eax, (%esp)
	call	_libereImage
L8:
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE15:
	.globl	_freeDisplaySurfaces
	.def	_freeDisplaySurfaces;	.scl	2;	.type	32;	.endef
_freeDisplaySurfaces:
LFB16:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$40, %esp
	movl	$0, -12(%ebp)
	jmp	L10
L11:
	movl	-12(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	8(%ebp), %eax
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	%eax, (%esp)
	call	_freeDisplaySurface
	addl	$1, -12(%ebp)
L10:
	movl	-12(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	8(%ebp), %eax
	addl	%edx, %eax
	movl	(%eax), %eax
	testl	%eax, %eax
	jne	L11
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE16:
	.globl	_isOverDisplaySurface
	.def	_isOverDisplaySurface;	.scl	2;	.type	32;	.endef
_isOverDisplaySurface:
LFB17:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	movl	_appState, %eax
	movl	28(%eax), %edx
	movl	12(%ebp), %eax
	movl	4(%eax), %ecx
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	addl	%ecx, %eax
	cmpl	%eax, %edx
	jl	L13
	movl	_appState, %eax
	movl	28(%eax), %edx
	movl	12(%ebp), %eax
	movl	4(%eax), %ecx
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	addl	%eax, %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %eax
	addl	%ecx, %eax
	cmpl	%eax, %edx
	jg	L13
	movl	_appState, %eax
	movl	32(%eax), %edx
	movl	12(%ebp), %eax
	movl	8(%eax), %ecx
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	addl	%ecx, %eax
	cmpl	%eax, %edx
	jl	L13
	movl	_appState, %eax
	movl	32(%eax), %edx
	movl	12(%ebp), %eax
	movl	8(%eax), %ecx
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	addl	%eax, %ecx
	movl	8(%ebp), %eax
	movl	20(%eax), %eax
	addl	%ecx, %eax
	cmpl	%eax, %edx
	jg	L13
	movl	$1, %eax
	jmp	L14
L13:
	movl	$0, %eax
L14:
	popl	%ebp
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE17:
	.ident	"GCC: (MinGW.org GCC-8.2.0-5) 8.2.0"
	.def	_malloc;	.scl	2;	.type	32;	.endef
	.def	_libereImage;	.scl	2;	.type	32;	.endef
	.def	_free;	.scl	2;	.type	32;	.endef
