	.file	"Button.c"
	.text
	.section .rdata,"dr"
LC0:
	.ascii "bouton appuye\12\0"
	.text
	.globl	_simple_callback
	.def	_simple_callback;	.scl	2;	.type	32;	.endef
_simple_callback:
LFB26:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$24, %esp
	movl	$LC0, (%esp)
	call	_SDL_Log
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE26:
	.section .rdata,"dr"
	.align 4
LC1:
	.ascii "Invalid pre-set robot positions.\0"
LC2:
	.ascii "Erreur.\0"
	.text
	.globl	_trigger_error_callback
	.def	_trigger_error_callback;	.scl	2;	.type	32;	.endef
_trigger_error_callback:
LFB27:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$24, %esp
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	movl	%eax, 12(%esp)
	movl	$LC1, 8(%esp)
	movl	$LC2, 4(%esp)
	movl	$16, (%esp)
	call	_SDL_ShowSimpleMessageBox
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE27:
	.section .rdata,"dr"
	.align 4
LC3:
	.ascii "RESET\12------ LANCEMENT PARTIE 1 ------\0"
	.align 4
LC4:
	.ascii "------ LANCEMENT PARTIE 1 ------\0"
	.align 4
LC5:
	.ascii "./resources/global_assets/partie1.bmp\0"
	.align 4
LC6:
	.ascii "\12> lancement fonction lisBMPRGB\12arg1:'%s'\0"
LC7:
	.ascii "ERREUR\0"
LC8:
	.ascii "OK\0"
LC9:
	.ascii "\12> OUTPUT: %p - %s\0"
LC10:
	.ascii "\12\12> Dimensions: %dx%d.\0"
	.align 4
LC11:
	.ascii "./resources/partie1/partie1_copie.bmp\0"
	.align 4
LC12:
	.ascii "\12\12> lancement fonction ecrisBMPRGB_Dans\12arg1:'%p' arg2:'%s'\0"
LC13:
	.ascii "\12> OUTPUT: %d - %s\0"
LC14:
	.ascii "P1-Annexe 1\0"
	.align 4
LC15:
	.ascii "\12\12> lancement fonction libereDonneesImageRGB\12arg1:'%p'\0"
LC16:
	.ascii "\12> Retour main: 0 - OK.\0"
	.text
	.globl	_affiche_partie1_callback
	.def	_affiche_partie1_callback;	.scl	2;	.type	32;	.endef
_affiche_partie1_callback:
LFB28:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	pushl	%esi
	pushl	%ebx
	subl	$624, %esp
	.cfi_offset 6, -12
	.cfi_offset 3, -16
	jmp	L4
L5:
	movl	_appState, %eax
	movl	64(%eax), %eax
	addl	$4, %eax
	movl	(%eax), %eax
	movl	%eax, (%esp)
	call	_removeGui
L4:
	movl	_appState, %eax
	movl	64(%eax), %eax
	addl	$4, %eax
	movl	(%eax), %eax
	testl	%eax, %eax
	jne	L5
	movl	8(%ebp), %eax
	movl	%eax, -12(%ebp)
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$12, %eax
	movl	(%eax), %eax
	movl	40(%eax), %eax
	movl	(%eax), %eax
	movl	%eax, -16(%ebp)
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$12, %eax
	movl	(%eax), %eax
	movl	40(%eax), %eax
	movl	(%eax), %eax
	movl	28(%eax), %eax
	testl	%eax, %eax
	je	L6
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$12, %eax
	movl	(%eax), %eax
	movl	40(%eax), %eax
	movl	(%eax), %eax
	movl	28(%eax), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$12, %eax
	movl	(%eax), %eax
	movl	40(%eax), %eax
	movl	(%eax), %esi
	movl	$LC3, (%esp)
	call	_createString
	movl	%eax, 28(%esi)
	jmp	L7
L6:
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$12, %eax
	movl	(%eax), %eax
	movl	40(%eax), %eax
	movl	(%eax), %esi
	movl	$LC4, (%esp)
	call	_createString
	movl	%eax, 28(%esi)
L7:
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$8, %eax
	movl	(%eax), %eax
	movl	$0, (%eax)
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$12, %eax
	movl	(%eax), %eax
	movl	$1, (%eax)
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$16, %eax
	movl	(%eax), %eax
	movl	$0, (%eax)
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$20, %eax
	movl	(%eax), %eax
	movl	$0, (%eax)
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$24, %eax
	movl	(%eax), %eax
	movl	$0, (%eax)
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$28, %eax
	movl	(%eax), %eax
	movl	$0, (%eax)
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$32, %eax
	movl	(%eax), %eax
	movl	$0, (%eax)
	movl	$LC5, 8(%esp)
	movl	$LC6, 4(%esp)
	leal	-552(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-552(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	$LC5, (%esp)
	call	_lisBMPRGB
	movl	%eax, -556(%ebp)
	movl	-556(%ebp), %eax
	testl	%eax, %eax
	jne	L8
	movl	$LC7, %edx
	jmp	L9
L8:
	movl	$LC8, %edx
L9:
	movl	-556(%ebp), %eax
	movl	%edx, 12(%esp)
	movl	%eax, 8(%esp)
	movl	$LC9, 4(%esp)
	leal	-552(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-552(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-556(%ebp), %eax
	testl	%eax, %eax
	je	L14
	movl	-556(%ebp), %eax
	movl	4(%eax), %edx
	movl	-556(%ebp), %eax
	movl	(%eax), %eax
	movl	%edx, 12(%esp)
	movl	%eax, 8(%esp)
	movl	$LC10, 4(%esp)
	leal	-552(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-552(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-556(%ebp), %eax
	movl	$LC11, 12(%esp)
	movl	%eax, 8(%esp)
	movl	$LC12, 4(%esp)
	leal	-552(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-552(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-556(%ebp), %eax
	movl	$LC11, 4(%esp)
	movl	%eax, (%esp)
	call	_ecrisBMPRGB_Dans
	movzbl	%al, %eax
	movl	%eax, -20(%ebp)
	cmpl	$1, -20(%ebp)
	jne	L12
	movl	$LC8, %eax
	jmp	L13
L12:
	movl	$LC7, %eax
L13:
	movl	%eax, 12(%esp)
	movl	-20(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC13, 4(%esp)
	leal	-552(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-552(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	$LC14, -52(%ebp)
	movl	$805240832, -48(%ebp)
	movl	$805240832, -44(%ebp)
	movl	$256, -40(%ebp)
	movl	$256, -36(%ebp)
	movl	$0, -32(%ebp)
	movl	$4, -28(%ebp)
	movl	$LC11, (%esp)
	call	_chargeImage
	movl	$0, 28(%esp)
	movl	%eax, 24(%esp)
	movl	$256, 20(%esp)
	movl	$256, 16(%esp)
	movl	$0, 12(%esp)
	movl	$0, 8(%esp)
	movl	$0, 4(%esp)
	movl	$1, (%esp)
	call	_initDisplaySurface
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initDisplaySurfaces
	movl	%eax, %edx
	movb	$0, -572(%ebp)
	movl	$0, %eax
	movl	-572(%ebp), %ecx
	movb	%al, %ch
	andl	$-16711681, %ecx
	movl	%ecx, %eax
	orl	$-16777216, %eax
	movl	%eax, %ecx
	movb	$0, %bl
	movl	$0, %eax
	movb	%al, %bh
	movl	%ebx, %eax
	andl	$-16711681, %eax
	movl	%eax, %ebx
	movl	%ebx, %eax
	orl	$-16777216, %eax
	movl	%eax, %ebx
	movl	$0, 40(%esp)
	movl	$0, 36(%esp)
	movl	%edx, 32(%esp)
	movl	$0, 28(%esp)
	movl	%ecx, 24(%esp)
	movl	%ebx, 20(%esp)
	movl	$256, 16(%esp)
	movl	$256, 12(%esp)
	movl	$0, 8(%esp)
	movl	$0, 4(%esp)
	movl	$1, (%esp)
	call	_initFrame
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initFrames
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initView
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initViews
	movl	%eax, -24(%ebp)
	movl	-52(%ebp), %eax
	movl	%eax, (%esp)
	movl	-48(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-44(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	-40(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	-36(%ebp), %eax
	movl	%eax, 16(%esp)
	movl	-32(%ebp), %eax
	movl	%eax, 20(%esp)
	movl	-28(%ebp), %eax
	movl	%eax, 24(%esp)
	movl	-24(%ebp), %eax
	movl	%eax, 28(%esp)
	call	_addGui
	leal	-556(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC15, 4(%esp)
	leal	-552(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-552(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	leal	-556(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereDonneesImageRGB
	movl	$LC16, 4(%esp)
	leal	-552(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-552(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	$1025, (%esp)
	call	_SDL_FlushEvent
	jmp	L3
L14:
	nop
L3:
	addl	$624, %esp
	popl	%ebx
	.cfi_restore 3
	popl	%esi
	.cfi_restore 6
	popl	%ebp
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE28:
	.section .rdata,"dr"
	.align 4
LC17:
	.ascii "RESET\12------ LANCEMENT PARTIE 2 ------\0"
	.align 4
LC18:
	.ascii "------ LANCEMENT PARTIE 2 ------\0"
	.align 4
LC19:
	.ascii "./resources/global_assets/partie2.bmp\0"
	.align 4
LC20:
	.ascii "\12> lancement fonction chargeImage\12arg1:'%s'\0"
	.align 4
LC21:
	.ascii "\12\12> lancement fonction dupliqueImage\12arg1:'%p'\0"
LC22:
	.ascii "\12> OUTPUT: 1 - OK\0"
	.align 4
LC23:
	.ascii "./resources/partie2/partie2_copie.bmp\0"
	.align 4
LC24:
	.ascii "\12\12> lancement fonction sauveImage\12arg1:'%p' arg2:'%s'\0"
LC25:
	.ascii "P2-Annexe 1\0"
	.align 4
LC26:
	.ascii "./resources/partie2/partie2_ng.bmp\0"
	.align 4
LC27:
	.ascii "\12\12> lancement fonction sauveImageNG\12arg1:'%p' arg2:'%s'\0"
	.align 4
LC28:
	.ascii "./resources/partie2/partie2_copie_ng.bmp\0"
LC29:
	.ascii "P2-Annexe 2\0"
	.align 4
LC30:
	.ascii "\12> lancement fonction differenceImage\12arg1:'%p' arg2:'%p'\0"
	.align 4
LC31:
	.ascii "./resources/partie2/partie2_diff.bmp\0"
LC32:
	.ascii "P2-Annexe 3\0"
	.align 4
LC33:
	.ascii "./resources/partie2/partie2_diff_ng.bmp\0"
LC34:
	.ascii "P2-Annexe 4\0"
	.align 4
LC35:
	.ascii "\12\12> lancement fonction libereImage\12arg1:'%p'\0"
	.text
	.globl	_affiche_partie2_callback
	.def	_affiche_partie2_callback;	.scl	2;	.type	32;	.endef
_affiche_partie2_callback:
LFB29:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	pushl	%ebx
	subl	$740, %esp
	.cfi_offset 3, -12
	jmp	L16
L17:
	movl	_appState, %eax
	movl	64(%eax), %eax
	addl	$4, %eax
	movl	(%eax), %eax
	movl	%eax, (%esp)
	call	_removeGui
L16:
	movl	_appState, %eax
	movl	64(%eax), %eax
	addl	$4, %eax
	movl	(%eax), %eax
	testl	%eax, %eax
	jne	L17
	movl	8(%ebp), %eax
	movl	%eax, -12(%ebp)
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$16, %eax
	movl	(%eax), %eax
	movl	40(%eax), %eax
	movl	(%eax), %eax
	movl	28(%eax), %eax
	testl	%eax, %eax
	je	L18
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$16, %eax
	movl	(%eax), %eax
	movl	40(%eax), %eax
	movl	(%eax), %eax
	movl	28(%eax), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$16, %eax
	movl	(%eax), %eax
	movl	40(%eax), %eax
	movl	(%eax), %ebx
	movl	$LC17, (%esp)
	call	_createString
	movl	%eax, 28(%ebx)
	jmp	L19
L18:
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$16, %eax
	movl	(%eax), %eax
	movl	40(%eax), %eax
	movl	(%eax), %ebx
	movl	$LC18, (%esp)
	call	_createString
	movl	%eax, 28(%ebx)
L19:
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$16, %eax
	movl	(%eax), %eax
	movl	40(%eax), %eax
	movl	(%eax), %eax
	movl	%eax, -16(%ebp)
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$8, %eax
	movl	(%eax), %eax
	movl	$0, (%eax)
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$12, %eax
	movl	(%eax), %eax
	movl	$0, (%eax)
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$16, %eax
	movl	(%eax), %eax
	movl	$1, (%eax)
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$20, %eax
	movl	(%eax), %eax
	movl	$0, (%eax)
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$24, %eax
	movl	(%eax), %eax
	movl	$0, (%eax)
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$28, %eax
	movl	(%eax), %eax
	movl	$0, (%eax)
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$32, %eax
	movl	(%eax), %eax
	movl	$0, (%eax)
	movl	$LC19, 8(%esp)
	movl	$LC20, 4(%esp)
	leal	-644(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-644(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	$LC19, (%esp)
	call	_chargeImage
	movl	%eax, -648(%ebp)
	movl	-648(%ebp), %eax
	testl	%eax, %eax
	jne	L20
	movl	$LC7, %edx
	jmp	L21
L20:
	movl	$LC8, %edx
L21:
	movl	-648(%ebp), %eax
	movl	%edx, 12(%esp)
	movl	%eax, 8(%esp)
	movl	$LC9, 4(%esp)
	leal	-644(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-644(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-648(%ebp), %eax
	testl	%eax, %eax
	je	L30
	movl	-648(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC21, 4(%esp)
	leal	-644(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-644(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-648(%ebp), %eax
	movl	%eax, (%esp)
	call	_dupliqueImage
	movl	%eax, -652(%ebp)
	movl	$LC22, 4(%esp)
	leal	-644(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-644(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-652(%ebp), %eax
	movl	$LC23, 12(%esp)
	movl	%eax, 8(%esp)
	movl	$LC24, 4(%esp)
	leal	-644(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-644(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-652(%ebp), %eax
	movl	$LC23, 4(%esp)
	movl	%eax, (%esp)
	call	_sauveImage
	movl	$LC22, 4(%esp)
	leal	-644(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-644(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	$LC25, -144(%ebp)
	movl	$805240832, -140(%ebp)
	movl	$805240832, -136(%ebp)
	movl	$256, -132(%ebp)
	movl	$256, -128(%ebp)
	movl	$0, -124(%ebp)
	movl	$4, -120(%ebp)
	movl	$LC23, (%esp)
	call	_chargeImage
	movl	$0, 28(%esp)
	movl	%eax, 24(%esp)
	movl	$256, 20(%esp)
	movl	$256, 16(%esp)
	movl	$0, 12(%esp)
	movl	$0, 8(%esp)
	movl	$0, 4(%esp)
	movl	$1, (%esp)
	call	_initDisplaySurface
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initDisplaySurfaces
	movl	%eax, %edx
	movb	$0, -696(%ebp)
	movl	$0, %eax
	movl	-696(%ebp), %ecx
	movb	%al, %ch
	andl	$-16711681, %ecx
	movl	%ecx, %eax
	orl	$-16777216, %eax
	movl	%eax, %ecx
	movb	$0, -692(%ebp)
	movl	$0, %eax
	movl	-692(%ebp), %ebx
	movb	%al, %bh
	andl	$-16711681, %ebx
	movl	%ebx, %eax
	orl	$-16777216, %eax
	movl	$0, 40(%esp)
	movl	$0, 36(%esp)
	movl	%edx, 32(%esp)
	movl	$0, 28(%esp)
	movl	%ecx, 24(%esp)
	movl	%eax, 20(%esp)
	movl	$256, 16(%esp)
	movl	$256, 12(%esp)
	movl	$0, 8(%esp)
	movl	$0, 4(%esp)
	movl	$1, (%esp)
	call	_initFrame
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initFrames
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initView
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initViews
	movl	%eax, -116(%ebp)
	movl	-144(%ebp), %eax
	movl	%eax, (%esp)
	movl	-140(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-136(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	-132(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	-128(%ebp), %eax
	movl	%eax, 16(%esp)
	movl	-124(%ebp), %eax
	movl	%eax, 20(%esp)
	movl	-120(%ebp), %eax
	movl	%eax, 24(%esp)
	movl	-116(%ebp), %eax
	movl	%eax, 28(%esp)
	call	_addGui
	movl	-652(%ebp), %eax
	movl	$LC26, 12(%esp)
	movl	%eax, 8(%esp)
	movl	$LC27, 4(%esp)
	leal	-644(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-644(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-652(%ebp), %eax
	movl	$LC28, 4(%esp)
	movl	%eax, (%esp)
	call	_sauveImageNG
	movl	$LC22, 4(%esp)
	leal	-644(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-644(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	$LC29, -112(%ebp)
	movl	$805240832, -108(%ebp)
	movl	$805240832, -104(%ebp)
	movl	$256, -100(%ebp)
	movl	$256, -96(%ebp)
	movl	$0, -92(%ebp)
	movl	$4, -88(%ebp)
	movl	$LC28, (%esp)
	call	_chargeImage
	movl	$0, 28(%esp)
	movl	%eax, 24(%esp)
	movl	$256, 20(%esp)
	movl	$256, 16(%esp)
	movl	$0, 12(%esp)
	movl	$0, 8(%esp)
	movl	$0, 4(%esp)
	movl	$1, (%esp)
	call	_initDisplaySurface
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initDisplaySurfaces
	movl	%eax, %edx
	movb	$0, -688(%ebp)
	movl	$0, %eax
	movl	-688(%ebp), %ecx
	movb	%al, %ch
	andl	$-16711681, %ecx
	movl	%ecx, %eax
	orl	$-16777216, %eax
	movl	%eax, %ecx
	movb	$0, -684(%ebp)
	movl	$0, %eax
	movl	-684(%ebp), %ebx
	movb	%al, %bh
	andl	$-16711681, %ebx
	movl	%ebx, %eax
	orl	$-16777216, %eax
	movl	$0, 40(%esp)
	movl	$0, 36(%esp)
	movl	%edx, 32(%esp)
	movl	$0, 28(%esp)
	movl	%ecx, 24(%esp)
	movl	%eax, 20(%esp)
	movl	$256, 16(%esp)
	movl	$256, 12(%esp)
	movl	$0, 8(%esp)
	movl	$0, 4(%esp)
	movl	$1, (%esp)
	call	_initFrame
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initFrames
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initView
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initViews
	movl	%eax, -84(%ebp)
	movl	-112(%ebp), %eax
	movl	%eax, (%esp)
	movl	-108(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-104(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	-100(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	-96(%ebp), %eax
	movl	%eax, 16(%esp)
	movl	-92(%ebp), %eax
	movl	%eax, 20(%esp)
	movl	-88(%ebp), %eax
	movl	%eax, 24(%esp)
	movl	-84(%ebp), %eax
	movl	%eax, 28(%esp)
	call	_addGui
	movl	$LC28, 8(%esp)
	movl	$LC20, 4(%esp)
	leal	-644(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-644(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	$LC28, (%esp)
	call	_chargeImage
	movl	%eax, -656(%ebp)
	movl	-656(%ebp), %eax
	testl	%eax, %eax
	jne	L24
	movl	$LC7, %edx
	jmp	L25
L24:
	movl	$LC8, %edx
L25:
	movl	-656(%ebp), %eax
	movl	%edx, 12(%esp)
	movl	%eax, 8(%esp)
	movl	$LC9, 4(%esp)
	leal	-644(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-644(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-656(%ebp), %eax
	testl	%eax, %eax
	je	L31
	movl	-656(%ebp), %edx
	movl	-652(%ebp), %eax
	movl	%edx, 12(%esp)
	movl	%eax, 8(%esp)
	movl	$LC30, 4(%esp)
	leal	-644(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-644(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-656(%ebp), %edx
	movl	-652(%ebp), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_differenceImage
	movl	%eax, -660(%ebp)
	movl	-660(%ebp), %eax
	testl	%eax, %eax
	jne	L27
	movl	$LC7, %edx
	jmp	L28
L27:
	movl	$LC8, %edx
L28:
	movl	-660(%ebp), %eax
	movl	%edx, 12(%esp)
	movl	%eax, 8(%esp)
	movl	$LC9, 4(%esp)
	leal	-644(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-644(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-660(%ebp), %eax
	movl	$LC31, 12(%esp)
	movl	%eax, 8(%esp)
	movl	$LC24, 4(%esp)
	leal	-644(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-644(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-660(%ebp), %eax
	movl	$LC31, 4(%esp)
	movl	%eax, (%esp)
	call	_sauveImage
	movl	$LC22, 4(%esp)
	leal	-644(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-644(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	$LC32, -80(%ebp)
	movl	$805240832, -76(%ebp)
	movl	$805240832, -72(%ebp)
	movl	$256, -68(%ebp)
	movl	$256, -64(%ebp)
	movl	$0, -60(%ebp)
	movl	$4, -56(%ebp)
	movl	$LC31, (%esp)
	call	_chargeImage
	movl	$0, 28(%esp)
	movl	%eax, 24(%esp)
	movl	$256, 20(%esp)
	movl	$256, 16(%esp)
	movl	$0, 12(%esp)
	movl	$0, 8(%esp)
	movl	$0, 4(%esp)
	movl	$1, (%esp)
	call	_initDisplaySurface
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initDisplaySurfaces
	movl	%eax, %edx
	movb	$0, -680(%ebp)
	movl	$0, %eax
	movl	-680(%ebp), %ecx
	movb	%al, %ch
	andl	$-16711681, %ecx
	movl	%ecx, %eax
	orl	$-16777216, %eax
	movl	%eax, %ecx
	movb	$0, -676(%ebp)
	movl	$0, %eax
	movl	-676(%ebp), %ebx
	movb	%al, %bh
	andl	$-16711681, %ebx
	movl	%ebx, %eax
	orl	$-16777216, %eax
	movl	$0, 40(%esp)
	movl	$0, 36(%esp)
	movl	%edx, 32(%esp)
	movl	$0, 28(%esp)
	movl	%ecx, 24(%esp)
	movl	%eax, 20(%esp)
	movl	$256, 16(%esp)
	movl	$256, 12(%esp)
	movl	$0, 8(%esp)
	movl	$0, 4(%esp)
	movl	$1, (%esp)
	call	_initFrame
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initFrames
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initView
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initViews
	movl	%eax, -52(%ebp)
	movl	-80(%ebp), %eax
	movl	%eax, (%esp)
	movl	-76(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-72(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	-68(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	-64(%ebp), %eax
	movl	%eax, 16(%esp)
	movl	-60(%ebp), %eax
	movl	%eax, 20(%esp)
	movl	-56(%ebp), %eax
	movl	%eax, 24(%esp)
	movl	-52(%ebp), %eax
	movl	%eax, 28(%esp)
	call	_addGui
	movl	-660(%ebp), %eax
	movl	$LC33, 12(%esp)
	movl	%eax, 8(%esp)
	movl	$LC27, 4(%esp)
	leal	-644(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-644(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-660(%ebp), %eax
	movl	$LC33, 4(%esp)
	movl	%eax, (%esp)
	call	_sauveImageNG
	movl	$LC22, 4(%esp)
	leal	-644(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-644(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	$LC34, -48(%ebp)
	movl	$805240832, -44(%ebp)
	movl	$805240832, -40(%ebp)
	movl	$256, -36(%ebp)
	movl	$256, -32(%ebp)
	movl	$0, -28(%ebp)
	movl	$4, -24(%ebp)
	movl	$LC33, (%esp)
	call	_chargeImage
	movl	$0, 28(%esp)
	movl	%eax, 24(%esp)
	movl	$256, 20(%esp)
	movl	$256, 16(%esp)
	movl	$0, 12(%esp)
	movl	$0, 8(%esp)
	movl	$0, 4(%esp)
	movl	$1, (%esp)
	call	_initDisplaySurface
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initDisplaySurfaces
	movl	%eax, %edx
	movb	$0, -672(%ebp)
	movl	$0, %eax
	movl	-672(%ebp), %ecx
	movb	%al, %ch
	andl	$-16711681, %ecx
	movl	%ecx, %eax
	orl	$-16777216, %eax
	movl	%eax, %ecx
	movb	$0, -668(%ebp)
	movl	$0, %eax
	movl	-668(%ebp), %ebx
	movb	%al, %bh
	andl	$-16711681, %ebx
	movl	%ebx, %eax
	orl	$-16777216, %eax
	movl	$0, 40(%esp)
	movl	$0, 36(%esp)
	movl	%edx, 32(%esp)
	movl	$0, 28(%esp)
	movl	%ecx, 24(%esp)
	movl	%eax, 20(%esp)
	movl	$256, 16(%esp)
	movl	$256, 12(%esp)
	movl	$0, 8(%esp)
	movl	$0, 4(%esp)
	movl	$1, (%esp)
	call	_initFrame
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initFrames
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initView
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initViews
	movl	%eax, -20(%ebp)
	movl	-48(%ebp), %eax
	movl	%eax, (%esp)
	movl	-44(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-40(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	-36(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	-32(%ebp), %eax
	movl	%eax, 16(%esp)
	movl	-28(%ebp), %eax
	movl	%eax, 20(%esp)
	movl	-24(%ebp), %eax
	movl	%eax, 24(%esp)
	movl	-20(%ebp), %eax
	movl	%eax, 28(%esp)
	call	_addGui
	leal	-648(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC35, 4(%esp)
	leal	-644(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-644(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	leal	-648(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereImage
	leal	-652(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC35, 4(%esp)
	leal	-644(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-644(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	leal	-652(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereImage
	leal	-656(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC35, 4(%esp)
	leal	-644(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-644(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	leal	-656(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereImage
	leal	-660(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC35, 4(%esp)
	leal	-644(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-644(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	leal	-660(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereImage
	movl	$LC16, 4(%esp)
	leal	-644(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-644(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	$1025, (%esp)
	call	_SDL_FlushEvent
	jmp	L15
L30:
	nop
	jmp	L15
L31:
	nop
L15:
	addl	$740, %esp
	popl	%ebx
	.cfi_restore 3
	popl	%ebp
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE29:
	.section .rdata,"dr"
	.align 4
LC36:
	.ascii "RESET\12------ LANCEMENT PARTIE 3 ------\0"
	.align 4
LC37:
	.ascii "------ LANCEMENT PARTIE 3 ------\0"
	.align 4
LC38:
	.ascii "./resources/global_assets/partie3.bmp\0"
	.align 4
LC39:
	.ascii "\12> lancement fonction creeArbreNB\12arg1:'%d' arg2:'%d' arg3:'%d' arg4:'%p'\0"
	.align 4
LC40:
	.ascii "\12> lancement fonction compteFeuille\12arg1:'%p'\0"
LC41:
	.ascii "\12> OUTPUT: %d\0"
	.align 4
LC42:
	.ascii "\12> lancement fonction creeImageArbreNB\12arg1:'%p' arg2:'%p' arg3:'%d'\0"
LC43:
	.ascii "\12> OUTPUT: OK\0"
	.align 4
LC44:
	.ascii "./resources/partie3/partie3_reproduit_etiq.bmp\0"
	.align 4
LC45:
	.ascii "\12> lancement fonction sauveImage\12arg1:'%p' arg2:'%s'\0"
LC46:
	.ascii "P3-Annexe 1\0"
	.align 4
LC47:
	.ascii "./resources/partie3/partie3_reproduit.bmp\0"
LC48:
	.ascii "P3-Annexe 2\0"
LC49:
	.ascii "\12> Main return 0 - OK\0"
	.text
	.globl	_affiche_partie3_callback
	.def	_affiche_partie3_callback;	.scl	2;	.type	32;	.endef
_affiche_partie3_callback:
LFB30:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	pushl	%esi
	pushl	%ebx
	subl	$656, %esp
	.cfi_offset 6, -12
	.cfi_offset 3, -16
	jmp	L33
L34:
	movl	_appState, %eax
	movl	64(%eax), %eax
	addl	$4, %eax
	movl	(%eax), %eax
	movl	%eax, (%esp)
	call	_removeGui
L33:
	movl	_appState, %eax
	movl	64(%eax), %eax
	addl	$4, %eax
	movl	(%eax), %eax
	testl	%eax, %eax
	jne	L34
	movl	8(%ebp), %eax
	movl	%eax, -12(%ebp)
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$20, %eax
	movl	(%eax), %eax
	movl	40(%eax), %eax
	movl	(%eax), %eax
	movl	28(%eax), %eax
	testl	%eax, %eax
	je	L35
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$20, %eax
	movl	(%eax), %eax
	movl	40(%eax), %eax
	movl	(%eax), %eax
	movl	28(%eax), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$20, %eax
	movl	(%eax), %eax
	movl	40(%eax), %eax
	movl	(%eax), %esi
	movl	$LC36, (%esp)
	call	_createString
	movl	%eax, 28(%esi)
	jmp	L36
L35:
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$20, %eax
	movl	(%eax), %eax
	movl	40(%eax), %eax
	movl	(%eax), %esi
	movl	$LC37, (%esp)
	call	_createString
	movl	%eax, 28(%esi)
L36:
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$20, %eax
	movl	(%eax), %eax
	movl	40(%eax), %eax
	movl	(%eax), %eax
	movl	%eax, -16(%ebp)
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$8, %eax
	movl	(%eax), %eax
	movl	$0, (%eax)
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$12, %eax
	movl	(%eax), %eax
	movl	$0, (%eax)
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$16, %eax
	movl	(%eax), %eax
	movl	$0, (%eax)
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$20, %eax
	movl	(%eax), %eax
	movl	$1, (%eax)
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$24, %eax
	movl	(%eax), %eax
	movl	$0, (%eax)
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$28, %eax
	movl	(%eax), %eax
	movl	$0, (%eax)
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$32, %eax
	movl	(%eax), %eax
	movl	$0, (%eax)
	movl	$LC38, 8(%esp)
	movl	$LC20, 4(%esp)
	leal	-588(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-588(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	$LC38, (%esp)
	call	_chargeImage
	movl	%eax, -592(%ebp)
	movl	-592(%ebp), %eax
	testl	%eax, %eax
	jne	L37
	movl	$LC7, %edx
	jmp	L38
L37:
	movl	$LC8, %edx
L38:
	movl	-592(%ebp), %eax
	movl	%edx, 12(%esp)
	movl	%eax, 8(%esp)
	movl	$LC9, 4(%esp)
	leal	-588(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-588(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-592(%ebp), %eax
	testl	%eax, %eax
	je	L43
	movl	-592(%ebp), %eax
	movl	20(%eax), %edx
	movl	-592(%ebp), %eax
	movl	(%eax), %eax
	movl	%edx, 20(%esp)
	movl	%eax, 16(%esp)
	movl	$0, 12(%esp)
	movl	$0, 8(%esp)
	movl	$LC39, 4(%esp)
	leal	-588(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-588(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-592(%ebp), %eax
	movl	20(%eax), %edx
	movl	-592(%ebp), %eax
	movl	(%eax), %eax
	movl	%edx, 12(%esp)
	movl	%eax, 8(%esp)
	movl	$0, 4(%esp)
	movl	$0, (%esp)
	call	_creeArbreNB
	movl	%eax, -20(%ebp)
	cmpl	$0, -20(%ebp)
	jne	L41
	movl	$LC7, %eax
	jmp	L42
L41:
	movl	$LC8, %eax
L42:
	movl	%eax, 12(%esp)
	movl	-20(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC9, 4(%esp)
	leal	-588(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-588(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-20(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC40, 4(%esp)
	leal	-588(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-588(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-20(%ebp), %eax
	movl	%eax, (%esp)
	call	_compteFeuille
	movl	%eax, -24(%ebp)
	movl	-24(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC41, 4(%esp)
	leal	-588(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-588(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-20(%ebp), %eax
	movl	16(%eax), %edx
	movl	-20(%ebp), %eax
	movl	16(%eax), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_alloueImage
	movl	%eax, -596(%ebp)
	movl	-596(%ebp), %eax
	movl	$1, 16(%esp)
	movl	-20(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	%eax, 8(%esp)
	movl	$LC42, 4(%esp)
	leal	-588(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-588(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-596(%ebp), %eax
	movl	$1, 8(%esp)
	movl	-20(%ebp), %edx
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_creeImageArbreNB
	movl	$LC43, 4(%esp)
	leal	-588(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-588(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-596(%ebp), %eax
	movl	$LC44, 12(%esp)
	movl	%eax, 8(%esp)
	movl	$LC45, 4(%esp)
	leal	-588(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-588(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-596(%ebp), %eax
	movl	$LC44, 4(%esp)
	movl	%eax, (%esp)
	call	_sauveImage
	movl	$LC43, 4(%esp)
	leal	-588(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-588(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	$LC46, -88(%ebp)
	movl	$805240832, -84(%ebp)
	movl	$805240832, -80(%ebp)
	movl	$256, -76(%ebp)
	movl	$256, -72(%ebp)
	movl	$0, -68(%ebp)
	movl	$4, -64(%ebp)
	movl	$LC44, (%esp)
	call	_chargeImage
	movl	$0, 28(%esp)
	movl	%eax, 24(%esp)
	movl	$256, 20(%esp)
	movl	$256, 16(%esp)
	movl	$0, 12(%esp)
	movl	$0, 8(%esp)
	movl	$0, 4(%esp)
	movl	$1, (%esp)
	call	_initDisplaySurface
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initDisplaySurfaces
	movl	%eax, %edx
	movb	$0, -604(%ebp)
	movl	$0, %eax
	movl	-604(%ebp), %ecx
	movb	%al, %ch
	andl	$-16711681, %ecx
	movl	%ecx, %eax
	orl	$-16777216, %eax
	movl	%eax, -604(%ebp)
	movb	$0, -612(%ebp)
	movl	$0, %eax
	movl	-612(%ebp), %ecx
	movb	%al, %ch
	andl	$-16711681, %ecx
	movl	%ecx, %eax
	orl	$-16777216, %eax
	movl	$0, 40(%esp)
	movl	$0, 36(%esp)
	movl	%edx, 32(%esp)
	movl	$0, 28(%esp)
	movl	-604(%ebp), %esi
	movl	%esi, 24(%esp)
	movl	%eax, 20(%esp)
	movl	$256, 16(%esp)
	movl	$256, 12(%esp)
	movl	$0, 8(%esp)
	movl	$0, 4(%esp)
	movl	$1, (%esp)
	call	_initFrame
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initFrames
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initView
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initViews
	movl	%eax, -60(%ebp)
	movl	-88(%ebp), %eax
	movl	%eax, (%esp)
	movl	-84(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-80(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	-76(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	-72(%ebp), %eax
	movl	%eax, 16(%esp)
	movl	-68(%ebp), %eax
	movl	%eax, 20(%esp)
	movl	-64(%ebp), %eax
	movl	%eax, 24(%esp)
	movl	-60(%ebp), %eax
	movl	%eax, 28(%esp)
	call	_addGui
	movl	-596(%ebp), %eax
	movl	$0, 16(%esp)
	movl	-20(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	%eax, 8(%esp)
	movl	$LC42, 4(%esp)
	leal	-588(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-588(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-596(%ebp), %eax
	movl	$0, 8(%esp)
	movl	-20(%ebp), %edx
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_creeImageArbreNB
	movl	$LC43, 4(%esp)
	leal	-588(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-588(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-596(%ebp), %eax
	movl	$LC47, 12(%esp)
	movl	%eax, 8(%esp)
	movl	$LC45, 4(%esp)
	leal	-588(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-588(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-596(%ebp), %eax
	movl	$LC47, 4(%esp)
	movl	%eax, (%esp)
	call	_sauveImage
	movl	$LC43, 4(%esp)
	leal	-588(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-588(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	$LC48, -56(%ebp)
	movl	$805240832, -52(%ebp)
	movl	$805240832, -48(%ebp)
	movl	$256, -44(%ebp)
	movl	$256, -40(%ebp)
	movl	$0, -36(%ebp)
	movl	$4, -32(%ebp)
	movl	$LC47, (%esp)
	call	_chargeImage
	movl	$0, 28(%esp)
	movl	%eax, 24(%esp)
	movl	$256, 20(%esp)
	movl	$256, 16(%esp)
	movl	$0, 12(%esp)
	movl	$0, 8(%esp)
	movl	$0, 4(%esp)
	movl	$1, (%esp)
	call	_initDisplaySurface
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initDisplaySurfaces
	movl	%eax, %edx
	movb	$0, -608(%ebp)
	movl	$0, %eax
	movl	-608(%ebp), %ecx
	movb	%al, %ch
	andl	$-16711681, %ecx
	movl	%ecx, %eax
	orl	$-16777216, %eax
	movl	%eax, %ecx
	movb	$0, %bl
	movl	$0, %eax
	movb	%al, %bh
	movl	%ebx, %eax
	andl	$-16711681, %eax
	movl	%eax, %ebx
	movl	%ebx, %eax
	orl	$-16777216, %eax
	movl	%eax, %ebx
	movl	$0, 40(%esp)
	movl	$0, 36(%esp)
	movl	%edx, 32(%esp)
	movl	$0, 28(%esp)
	movl	%ecx, 24(%esp)
	movl	%ebx, 20(%esp)
	movl	$256, 16(%esp)
	movl	$256, 12(%esp)
	movl	$0, 8(%esp)
	movl	$0, 4(%esp)
	movl	$1, (%esp)
	call	_initFrame
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initFrames
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initView
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initViews
	movl	%eax, -28(%ebp)
	movl	-56(%ebp), %eax
	movl	%eax, (%esp)
	movl	-52(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-48(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	-44(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	-40(%ebp), %eax
	movl	%eax, 16(%esp)
	movl	-36(%ebp), %eax
	movl	%eax, 20(%esp)
	movl	-32(%ebp), %eax
	movl	%eax, 24(%esp)
	movl	-28(%ebp), %eax
	movl	%eax, 28(%esp)
	call	_addGui
	leal	-592(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereImage
	leal	-596(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereImage
	movl	-20(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereArbreBinaire
	movl	$LC49, 4(%esp)
	leal	-588(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-588(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	jmp	L32
L43:
	nop
L32:
	addl	$656, %esp
	popl	%ebx
	.cfi_restore 3
	popl	%esi
	.cfi_restore 6
	popl	%ebp
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE30:
	.section .rdata,"dr"
	.align 4
LC50:
	.ascii "RESET\12------ LANCEMENT PARTIE 4 ------\0"
	.align 4
LC51:
	.ascii "------ LANCEMENT PARTIE 4 ------\0"
	.align 4
LC52:
	.ascii "./resources/global_assets/partie4.bmp\0"
	.align 4
LC53:
	.ascii "./resources/global_assets/partie5_simple.bmp\0"
	.align 4
LC54:
	.ascii "./resources/global_assets/partie5_complexe.bmp\0"
	.align 4
LC55:
	.ascii "\12> lancement fonction creePalette\12arg1:'%p'\0"
	.align 4
LC56:
	.ascii "\12> lancement fonction creeArbreMotifs\12arg1:'%p' arg2:'%p' arg3:'%d' arg4:'%d' arg5:'%d'\0"
	.align 4
LC57:
	.ascii "./resources/partie4/description_imagePartie4.txt\0"
	.align 4
LC58:
	.ascii "\12> lancement fonction sauveDescriptionImage\12arg1:'%p' arg2:'%p'\12 arg3:'%s'\0"
LC59:
	.ascii "r\0"
LC60:
	.ascii "\12%s\0"
	.align 4
LC61:
	.ascii "./resources/partie4/description_imagePartie5_simple.txt\0"
	.align 4
LC62:
	.ascii "\12FICHIER TROP LOURD A AFFICHER\0"
	.align 4
LC63:
	.ascii "./resources/partie4/description_imagePartie5_complexe.txt\0"
	.text
	.globl	_affiche_partie4_callback
	.def	_affiche_partie4_callback;	.scl	2;	.type	32;	.endef
_affiche_partie4_callback:
LFB31:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	pushl	%ebx
	subl	$596, %esp
	.cfi_offset 3, -12
	jmp	L45
L46:
	movl	_appState, %eax
	movl	64(%eax), %eax
	addl	$4, %eax
	movl	(%eax), %eax
	movl	%eax, (%esp)
	call	_removeGui
L45:
	movl	_appState, %eax
	movl	64(%eax), %eax
	addl	$4, %eax
	movl	(%eax), %eax
	testl	%eax, %eax
	jne	L46
	movl	8(%ebp), %eax
	movl	%eax, -12(%ebp)
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$24, %eax
	movl	(%eax), %eax
	movl	40(%eax), %eax
	movl	(%eax), %eax
	movl	28(%eax), %eax
	testl	%eax, %eax
	je	L47
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$24, %eax
	movl	(%eax), %eax
	movl	40(%eax), %eax
	movl	(%eax), %eax
	movl	28(%eax), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$24, %eax
	movl	(%eax), %eax
	movl	40(%eax), %eax
	movl	(%eax), %ebx
	movl	$LC50, (%esp)
	call	_createString
	movl	%eax, 28(%ebx)
	jmp	L48
L47:
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$24, %eax
	movl	(%eax), %eax
	movl	40(%eax), %eax
	movl	(%eax), %ebx
	movl	$LC51, (%esp)
	call	_createString
	movl	%eax, 28(%ebx)
L48:
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$24, %eax
	movl	(%eax), %eax
	movl	40(%eax), %eax
	movl	(%eax), %eax
	movl	%eax, -16(%ebp)
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$8, %eax
	movl	(%eax), %eax
	movl	$0, (%eax)
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$12, %eax
	movl	(%eax), %eax
	movl	$0, (%eax)
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$16, %eax
	movl	(%eax), %eax
	movl	$0, (%eax)
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$20, %eax
	movl	(%eax), %eax
	movl	$0, (%eax)
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$24, %eax
	movl	(%eax), %eax
	movl	$1, (%eax)
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$28, %eax
	movl	(%eax), %eax
	movl	$0, (%eax)
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$32, %eax
	movl	(%eax), %eax
	movl	$0, (%eax)
	movl	$LC52, 8(%esp)
	movl	$LC20, 4(%esp)
	leal	-548(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-548(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	$LC52, (%esp)
	call	_chargeImage
	movl	%eax, -552(%ebp)
	movl	-552(%ebp), %eax
	testl	%eax, %eax
	jne	L49
	movl	$LC7, %edx
	jmp	L50
L49:
	movl	$LC8, %edx
L50:
	movl	-552(%ebp), %eax
	movl	%edx, 12(%esp)
	movl	%eax, 8(%esp)
	movl	$LC9, 4(%esp)
	leal	-548(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-548(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	$LC53, 8(%esp)
	movl	$LC20, 4(%esp)
	leal	-548(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-548(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	$LC53, (%esp)
	call	_chargeImage
	movl	%eax, -556(%ebp)
	movl	-556(%ebp), %eax
	testl	%eax, %eax
	jne	L51
	movl	$LC7, %edx
	jmp	L52
L51:
	movl	$LC8, %edx
L52:
	movl	-556(%ebp), %eax
	movl	%edx, 12(%esp)
	movl	%eax, 8(%esp)
	movl	$LC9, 4(%esp)
	leal	-548(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-548(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	$LC54, 8(%esp)
	movl	$LC20, 4(%esp)
	leal	-548(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-548(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	$LC54, (%esp)
	call	_chargeImage
	movl	%eax, -560(%ebp)
	movl	-560(%ebp), %eax
	testl	%eax, %eax
	jne	L53
	movl	$LC7, %edx
	jmp	L54
L53:
	movl	$LC8, %edx
L54:
	movl	-560(%ebp), %eax
	movl	%edx, 12(%esp)
	movl	%eax, 8(%esp)
	movl	$LC9, 4(%esp)
	leal	-548(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-548(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-552(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC55, 4(%esp)
	leal	-548(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-548(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-552(%ebp), %eax
	movl	%eax, (%esp)
	call	_creePalette
	movl	%eax, -20(%ebp)
	cmpl	$0, -20(%ebp)
	jne	L55
	movl	$LC7, %eax
	jmp	L56
L55:
	movl	$LC8, %eax
L56:
	movl	%eax, 12(%esp)
	movl	-20(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC9, 4(%esp)
	leal	-548(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-548(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-556(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC55, 4(%esp)
	leal	-548(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-548(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-556(%ebp), %eax
	movl	%eax, (%esp)
	call	_creePalette
	movl	%eax, -24(%ebp)
	cmpl	$0, -24(%ebp)
	jne	L57
	movl	$LC7, %eax
	jmp	L58
L57:
	movl	$LC8, %eax
L58:
	movl	%eax, 12(%esp)
	movl	-24(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC9, 4(%esp)
	leal	-548(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-548(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-560(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC55, 4(%esp)
	leal	-548(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-548(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-560(%ebp), %eax
	movl	%eax, (%esp)
	call	_creePalette
	movl	%eax, -28(%ebp)
	cmpl	$0, -28(%ebp)
	jne	L59
	movl	$LC7, %eax
	jmp	L60
L59:
	movl	$LC8, %eax
L60:
	movl	%eax, 12(%esp)
	movl	-28(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC9, 4(%esp)
	leal	-548(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-548(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-552(%ebp), %eax
	movl	(%eax), %edx
	movl	-552(%ebp), %eax
	movl	$0, 24(%esp)
	movl	$0, 20(%esp)
	movl	%edx, 16(%esp)
	movl	-20(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	%eax, 8(%esp)
	movl	$LC56, 4(%esp)
	leal	-548(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-548(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-552(%ebp), %eax
	movl	(%eax), %edx
	movl	-552(%ebp), %eax
	movl	$0, 16(%esp)
	movl	$0, 12(%esp)
	movl	%edx, 8(%esp)
	movl	-20(%ebp), %edx
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_creeArbreMotifs
	movl	%eax, -32(%ebp)
	cmpl	$0, -32(%ebp)
	jne	L61
	movl	$LC7, %eax
	jmp	L62
L61:
	movl	$LC8, %eax
L62:
	movl	%eax, 12(%esp)
	movl	-32(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC9, 4(%esp)
	leal	-548(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-548(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-556(%ebp), %eax
	movl	(%eax), %edx
	movl	-556(%ebp), %eax
	movl	$0, 24(%esp)
	movl	$0, 20(%esp)
	movl	%edx, 16(%esp)
	movl	-24(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	%eax, 8(%esp)
	movl	$LC56, 4(%esp)
	leal	-548(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-548(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-556(%ebp), %eax
	movl	(%eax), %edx
	movl	-556(%ebp), %eax
	movl	$0, 16(%esp)
	movl	$0, 12(%esp)
	movl	%edx, 8(%esp)
	movl	-24(%ebp), %edx
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_creeArbreMotifs
	movl	%eax, -36(%ebp)
	cmpl	$0, -36(%ebp)
	jne	L63
	movl	$LC7, %eax
	jmp	L64
L63:
	movl	$LC8, %eax
L64:
	movl	%eax, 12(%esp)
	movl	-36(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC9, 4(%esp)
	leal	-548(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-548(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-560(%ebp), %eax
	movl	(%eax), %edx
	movl	-560(%ebp), %eax
	movl	$0, 24(%esp)
	movl	$0, 20(%esp)
	movl	%edx, 16(%esp)
	movl	-28(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	%eax, 8(%esp)
	movl	$LC56, 4(%esp)
	leal	-548(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-548(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-560(%ebp), %eax
	movl	(%eax), %edx
	movl	-560(%ebp), %eax
	movl	$0, 16(%esp)
	movl	$0, 12(%esp)
	movl	%edx, 8(%esp)
	movl	-28(%ebp), %edx
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_creeArbreMotifs
	movl	%eax, -40(%ebp)
	cmpl	$0, -40(%ebp)
	jne	L65
	movl	$LC7, %eax
	jmp	L66
L65:
	movl	$LC8, %eax
L66:
	movl	%eax, 12(%esp)
	movl	-40(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC9, 4(%esp)
	leal	-548(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-548(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	$LC57, 16(%esp)
	movl	-20(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	-32(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC58, 4(%esp)
	leal	-548(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-548(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	$LC57, 8(%esp)
	movl	-20(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-32(%ebp), %eax
	movl	%eax, (%esp)
	call	_sauveDescriptionImage
	movl	$LC43, 4(%esp)
	leal	-548(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-548(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	$0, -564(%ebp)
	movl	$0, -568(%ebp)
	movl	$0, -44(%ebp)
	movl	$LC59, 4(%esp)
	movl	$LC57, (%esp)
	call	_fopen
	movl	%eax, -48(%ebp)
	cmpl	$0, -48(%ebp)
	je	L72
	jmp	L69
L70:
	movl	-564(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC60, 4(%esp)
	leal	-548(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-548(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
L69:
	movl	-48(%ebp), %eax
	movl	%eax, 8(%esp)
	leal	-568(%ebp), %eax
	movl	%eax, 4(%esp)
	leal	-564(%ebp), %eax
	movl	%eax, (%esp)
	call	_getline
	movl	%eax, -44(%ebp)
	cmpl	$-1, -44(%ebp)
	jne	L70
	movl	-48(%ebp), %eax
	movl	%eax, (%esp)
	call	_fclose
	movl	-564(%ebp), %eax
	testl	%eax, %eax
	je	L71
	movl	-564(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
L71:
	movl	$LC61, 16(%esp)
	movl	-24(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	-36(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC58, 4(%esp)
	leal	-548(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-548(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	$LC61, 8(%esp)
	movl	-24(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-36(%ebp), %eax
	movl	%eax, (%esp)
	call	_sauveDescriptionImage
	movl	$LC43, 4(%esp)
	leal	-548(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-548(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	$LC62, 4(%esp)
	leal	-548(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-548(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	$LC63, 16(%esp)
	movl	-28(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	-40(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC58, 4(%esp)
	leal	-548(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-548(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	$LC63, 8(%esp)
	movl	-28(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-40(%ebp), %eax
	movl	%eax, (%esp)
	call	_sauveDescriptionImage
	movl	$LC43, 4(%esp)
	leal	-548(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-548(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	$LC62, 4(%esp)
	leal	-548(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-548(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	leal	-552(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereImage
	leal	-556(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereImage
	leal	-560(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereImage
	movl	-20(%ebp), %eax
	movl	4(%eax), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-24(%ebp), %eax
	movl	4(%eax), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-28(%ebp), %eax
	movl	4(%eax), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-20(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-24(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-28(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-32(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereArbreMotif
	movl	-36(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereArbreMotif
	movl	-40(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereArbreMotif
	movl	$LC49, 4(%esp)
	leal	-548(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-548(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	jmp	L44
L72:
	nop
L44:
	addl	$596, %esp
	popl	%ebx
	.cfi_restore 3
	popl	%ebp
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE31:
	.section .rdata,"dr"
	.align 4
LC64:
	.ascii "./resources/partie5/partie4_robotpath.bmp\0"
	.align 4
LC65:
	.ascii "./resources/partie5/partie4_robotpath.txt\0"
	.text
	.globl	_modfify_partie5_imgP4_callback
	.def	_modfify_partie5_imgP4_callback;	.scl	2;	.type	32;	.endef
_modfify_partie5_imgP4_callback:
LFB32:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	pushl	%edi
	pushl	%esi
	pushl	%ebx
	subl	$124, %esp
	.cfi_offset 7, -12
	.cfi_offset 6, -16
	.cfi_offset 3, -20
	movl	8(%ebp), %eax
	movl	%eax, -32(%ebp)
	movl	12(%ebp), %eax
	movl	%eax, -36(%ebp)
	movl	_point1P4.7186, %eax
	movl	_point1P4.7186+4, %edx
	movl	%eax, -60(%ebp)
	movl	%edx, -56(%ebp)
	movl	_point2P4.7188, %eax
	movl	_point2P4.7188+4, %edx
	movl	%eax, -68(%ebp)
	movl	%edx, -64(%ebp)
	movl	_appState, %eax
	movzbl	24(%eax), %eax
	cmpb	$1, %al
	jne	L74
	movl	_appState, %eax
	movl	28(%eax), %edx
	movl	-36(%ebp), %eax
	movl	4(%eax), %eax
	subl	%eax, %edx
	movl	-36(%ebp), %eax
	movl	32(%eax), %eax
	movl	(%eax), %eax
	movl	8(%eax), %eax
	movl	%edx, %ecx
	subl	%eax, %ecx
	movl	_appState, %eax
	movl	32(%eax), %edx
	movl	-36(%ebp), %eax
	movl	8(%eax), %eax
	subl	%eax, %edx
	movl	-36(%ebp), %eax
	movl	32(%eax), %eax
	movl	(%eax), %eax
	movl	12(%eax), %eax
	subl	%eax, %edx
	movl	%edx, %eax
	movl	$128, %edx
	subl	%eax, %edx
	movl	%edx, %eax
	movl	%ecx, _point1P4.7186
	movl	%eax, _point1P4.7186+4
	jmp	L75
L74:
	movl	_appState, %eax
	movzbl	24(%eax), %eax
	cmpb	$3, %al
	jne	L75
	movl	_appState, %eax
	movl	28(%eax), %edx
	movl	-36(%ebp), %eax
	movl	4(%eax), %eax
	subl	%eax, %edx
	movl	-36(%ebp), %eax
	movl	32(%eax), %eax
	movl	(%eax), %eax
	movl	8(%eax), %eax
	movl	%edx, %ecx
	subl	%eax, %ecx
	movl	_appState, %eax
	movl	32(%eax), %edx
	movl	-36(%ebp), %eax
	movl	8(%eax), %eax
	subl	%eax, %edx
	movl	-36(%ebp), %eax
	movl	32(%eax), %eax
	movl	(%eax), %eax
	movl	12(%eax), %eax
	subl	%eax, %edx
	movl	%edx, %eax
	movl	$128, %edx
	subl	%eax, %edx
	movl	%edx, %eax
	movl	%ecx, _point2P4.7188
	movl	%eax, _point2P4.7188+4
L75:
	movl	_appState, %eax
	movzbl	24(%eax), %eax
	cmpb	$1, %al
	je	L76
	movl	_appState, %eax
	movzbl	24(%eax), %eax
	cmpb	$3, %al
	jne	L84
L76:
	movl	$LC52, (%esp)
	call	_chargeImage
	movl	%eax, -72(%ebp)
	movl	-72(%ebp), %eax
	movl	%eax, (%esp)
	call	_creePalette
	movl	%eax, -40(%ebp)
	movl	-72(%ebp), %eax
	movl	(%eax), %edx
	movl	-72(%ebp), %eax
	movl	$0, 16(%esp)
	movl	$0, 12(%esp)
	movl	%edx, 8(%esp)
	movl	-40(%ebp), %edx
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_creeArbreMotifs
	movl	%eax, -44(%ebp)
	movl	-72(%ebp), %eax
	movl	4(%eax), %edx
	movl	-72(%ebp), %eax
	movl	(%eax), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_alloueImage
	movl	%eax, -76(%ebp)
	movl	-76(%ebp), %ecx
	movl	$1, 16(%esp)
	movl	-44(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	-40(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 4(%esp)
	movl	%edx, 8(%esp)
	movl	%ecx, (%esp)
	call	_dessineCarte
	movl	_point2P4.7188, %eax
	movl	%eax, -96(%ebp)
	movl	_point2P4.7188+4, %eax
	movl	%eax, -92(%ebp)
	movl	_point1P4.7186, %eax
	movl	%eax, %ebx
	movl	_point1P4.7186+4, %eax
	movl	%eax, %esi
	movl	-72(%ebp), %edi
	movl	-76(%ebp), %eax
	movl	-96(%ebp), %edx
	movl	-92(%ebp), %ecx
	movl	%edx, 20(%esp)
	movl	%ecx, 24(%esp)
	movl	%ebx, 12(%esp)
	movl	%esi, 16(%esp)
	movl	-40(%ebp), %ecx
	movl	%ecx, 8(%esp)
	movl	%edi, 4(%esp)
	movl	%eax, (%esp)
	call	_calculeTrajectoire
	movl	%eax, -48(%ebp)
	cmpl	$0, -48(%ebp)
	je	L78
	movl	-48(%ebp), %eax
	movl	(%eax), %eax
	movl	%eax, -28(%ebp)
	jmp	L79
L80:
	movl	-72(%ebp), %eax
	movl	8(%eax), %edx
	movl	-28(%ebp), %eax
	movl	(%eax), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-28(%ebp), %eax
	movl	4(%eax), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movw	$0, (%eax)
	movl	-72(%ebp), %eax
	movl	12(%eax), %edx
	movl	-28(%ebp), %eax
	movl	(%eax), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-28(%ebp), %eax
	movl	4(%eax), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movw	$0, (%eax)
	movl	-72(%ebp), %eax
	movl	16(%eax), %edx
	movl	-28(%ebp), %eax
	movl	(%eax), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-28(%ebp), %eax
	movl	4(%eax), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movw	$0, (%eax)
	movl	-28(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -28(%ebp)
L79:
	cmpl	$0, -28(%ebp)
	jne	L80
	movl	-72(%ebp), %eax
	movl	$LC64, 4(%esp)
	movl	%eax, (%esp)
	call	_sauveImage
	movl	-36(%ebp), %eax
	movl	32(%eax), %eax
	movl	(%eax), %eax
	addl	$24, %eax
	movl	%eax, (%esp)
	call	_libereImage
	movl	-36(%ebp), %eax
	movl	32(%eax), %eax
	movl	(%eax), %eax
	movl	-72(%ebp), %edx
	movl	%edx, 24(%eax)
	movl	$LC65, 4(%esp)
	movl	-48(%ebp), %eax
	movl	%eax, (%esp)
	call	_sauveDescriptionChemin
	jmp	L81
L82:
	movl	-48(%ebp), %eax
	movl	(%eax), %eax
	movl	8(%eax), %eax
	movl	%eax, -52(%ebp)
	movl	-48(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereMaillon
	movl	-48(%ebp), %eax
	movl	-52(%ebp), %edx
	movl	%edx, (%eax)
L81:
	movl	-48(%ebp), %eax
	movl	(%eax), %eax
	testl	%eax, %eax
	jne	L82
	movl	-48(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
	leal	-76(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereImage
	movl	-40(%ebp), %eax
	movl	4(%eax), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-40(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-44(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereArbreMotif
	jmp	L84
L78:
	movl	12(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_trigger_error_callback
	movl	-60(%ebp), %edx
	movl	-56(%ebp), %eax
	movl	%edx, _point1P4.7186
	movl	%eax, _point1P4.7186+4
	movl	-68(%ebp), %edx
	movl	-64(%ebp), %eax
	movl	%edx, _point2P4.7188
	movl	%eax, _point2P4.7188+4
	leal	-72(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereImage
	leal	-76(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereImage
	movl	-40(%ebp), %eax
	movl	4(%eax), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-40(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-44(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereArbreMotif
L84:
	nop
	addl	$124, %esp
	popl	%ebx
	.cfi_restore 3
	popl	%esi
	.cfi_restore 6
	popl	%edi
	.cfi_restore 7
	popl	%ebp
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE32:
	.section .rdata,"dr"
	.align 4
LC66:
	.ascii "./resources/partie5/partie5_simple_robotpath.bmp\0"
	.align 4
LC67:
	.ascii "./resources/partie5/partie5_simple_robotpath.txt\0"
	.text
	.globl	_modfify_partie5_imgP5_simple_callback
	.def	_modfify_partie5_imgP5_simple_callback;	.scl	2;	.type	32;	.endef
_modfify_partie5_imgP5_simple_callback:
LFB33:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	pushl	%edi
	pushl	%esi
	pushl	%ebx
	subl	$124, %esp
	.cfi_offset 7, -12
	.cfi_offset 6, -16
	.cfi_offset 3, -20
	movl	8(%ebp), %eax
	movl	%eax, -32(%ebp)
	movl	12(%ebp), %eax
	movl	%eax, -36(%ebp)
	movl	_point1P5_simple.7217, %eax
	movl	_point1P5_simple.7217+4, %edx
	movl	%eax, -60(%ebp)
	movl	%edx, -56(%ebp)
	movl	_point2P5_simple.7219, %eax
	movl	_point2P5_simple.7219+4, %edx
	movl	%eax, -68(%ebp)
	movl	%edx, -64(%ebp)
	movl	_appState, %eax
	movzbl	24(%eax), %eax
	cmpb	$1, %al
	jne	L86
	movl	_appState, %eax
	movl	28(%eax), %edx
	movl	-36(%ebp), %eax
	movl	4(%eax), %eax
	subl	%eax, %edx
	movl	-36(%ebp), %eax
	movl	32(%eax), %eax
	movl	(%eax), %eax
	movl	8(%eax), %eax
	movl	%edx, %ecx
	subl	%eax, %ecx
	movl	_appState, %eax
	movl	32(%eax), %edx
	movl	-36(%ebp), %eax
	movl	8(%eax), %eax
	subl	%eax, %edx
	movl	-36(%ebp), %eax
	movl	32(%eax), %eax
	movl	(%eax), %eax
	movl	12(%eax), %eax
	subl	%eax, %edx
	movl	%edx, %eax
	movl	$512, %edx
	subl	%eax, %edx
	movl	%edx, %eax
	movl	%ecx, _point1P5_simple.7217
	movl	%eax, _point1P5_simple.7217+4
	jmp	L87
L86:
	movl	_appState, %eax
	movzbl	24(%eax), %eax
	cmpb	$3, %al
	jne	L87
	movl	_appState, %eax
	movl	28(%eax), %edx
	movl	-36(%ebp), %eax
	movl	4(%eax), %eax
	subl	%eax, %edx
	movl	-36(%ebp), %eax
	movl	32(%eax), %eax
	movl	(%eax), %eax
	movl	8(%eax), %eax
	movl	%edx, %ecx
	subl	%eax, %ecx
	movl	_appState, %eax
	movl	32(%eax), %edx
	movl	-36(%ebp), %eax
	movl	8(%eax), %eax
	subl	%eax, %edx
	movl	-36(%ebp), %eax
	movl	32(%eax), %eax
	movl	(%eax), %eax
	movl	12(%eax), %eax
	subl	%eax, %edx
	movl	%edx, %eax
	movl	$512, %edx
	subl	%eax, %edx
	movl	%edx, %eax
	movl	%ecx, _point2P5_simple.7219
	movl	%eax, _point2P5_simple.7219+4
L87:
	movl	_appState, %eax
	movzbl	24(%eax), %eax
	cmpb	$1, %al
	je	L88
	movl	_appState, %eax
	movzbl	24(%eax), %eax
	cmpb	$3, %al
	jne	L96
L88:
	movl	$LC53, (%esp)
	call	_chargeImage
	movl	%eax, -72(%ebp)
	movl	-72(%ebp), %eax
	movl	%eax, (%esp)
	call	_creePalette
	movl	%eax, -40(%ebp)
	movl	-72(%ebp), %eax
	movl	(%eax), %edx
	movl	-72(%ebp), %eax
	movl	$0, 16(%esp)
	movl	$0, 12(%esp)
	movl	%edx, 8(%esp)
	movl	-40(%ebp), %edx
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_creeArbreMotifs
	movl	%eax, -44(%ebp)
	movl	-72(%ebp), %eax
	movl	4(%eax), %edx
	movl	-72(%ebp), %eax
	movl	(%eax), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_alloueImage
	movl	%eax, -76(%ebp)
	movl	-76(%ebp), %ecx
	movl	$1, 16(%esp)
	movl	-44(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	-40(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 4(%esp)
	movl	%edx, 8(%esp)
	movl	%ecx, (%esp)
	call	_dessineCarte
	movl	_point2P5_simple.7219, %eax
	movl	%eax, -96(%ebp)
	movl	_point2P5_simple.7219+4, %eax
	movl	%eax, -92(%ebp)
	movl	_point1P5_simple.7217, %eax
	movl	%eax, %ebx
	movl	_point1P5_simple.7217+4, %eax
	movl	%eax, %esi
	movl	-72(%ebp), %edi
	movl	-76(%ebp), %eax
	movl	-96(%ebp), %edx
	movl	-92(%ebp), %ecx
	movl	%edx, 20(%esp)
	movl	%ecx, 24(%esp)
	movl	%ebx, 12(%esp)
	movl	%esi, 16(%esp)
	movl	-40(%ebp), %ecx
	movl	%ecx, 8(%esp)
	movl	%edi, 4(%esp)
	movl	%eax, (%esp)
	call	_calculeTrajectoire
	movl	%eax, -48(%ebp)
	cmpl	$0, -48(%ebp)
	je	L90
	movl	-48(%ebp), %eax
	movl	(%eax), %eax
	movl	%eax, -28(%ebp)
	jmp	L91
L92:
	movl	-72(%ebp), %eax
	movl	8(%eax), %edx
	movl	-28(%ebp), %eax
	movl	(%eax), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-28(%ebp), %eax
	movl	4(%eax), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movw	$0, (%eax)
	movl	-72(%ebp), %eax
	movl	12(%eax), %edx
	movl	-28(%ebp), %eax
	movl	(%eax), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-28(%ebp), %eax
	movl	4(%eax), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movw	$0, (%eax)
	movl	-72(%ebp), %eax
	movl	16(%eax), %edx
	movl	-28(%ebp), %eax
	movl	(%eax), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-28(%ebp), %eax
	movl	4(%eax), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movw	$0, (%eax)
	movl	-28(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -28(%ebp)
L91:
	cmpl	$0, -28(%ebp)
	jne	L92
	movl	-72(%ebp), %eax
	movl	$LC66, 4(%esp)
	movl	%eax, (%esp)
	call	_sauveImage
	movl	-36(%ebp), %eax
	movl	32(%eax), %eax
	movl	(%eax), %eax
	addl	$24, %eax
	movl	%eax, (%esp)
	call	_libereImage
	movl	-36(%ebp), %eax
	movl	32(%eax), %eax
	movl	(%eax), %eax
	movl	-72(%ebp), %edx
	movl	%edx, 24(%eax)
	movl	$LC67, 4(%esp)
	movl	-48(%ebp), %eax
	movl	%eax, (%esp)
	call	_sauveDescriptionChemin
	jmp	L93
L94:
	movl	-48(%ebp), %eax
	movl	(%eax), %eax
	movl	8(%eax), %eax
	movl	%eax, -52(%ebp)
	movl	-48(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereMaillon
	movl	-48(%ebp), %eax
	movl	-52(%ebp), %edx
	movl	%edx, (%eax)
L93:
	movl	-48(%ebp), %eax
	movl	(%eax), %eax
	testl	%eax, %eax
	jne	L94
	movl	-48(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
	leal	-76(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereImage
	movl	-40(%ebp), %eax
	movl	4(%eax), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-40(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-44(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereArbreMotif
	jmp	L96
L90:
	movl	12(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_trigger_error_callback
	movl	-60(%ebp), %edx
	movl	-56(%ebp), %eax
	movl	%edx, _point1P5_simple.7217
	movl	%eax, _point1P5_simple.7217+4
	movl	-68(%ebp), %edx
	movl	-64(%ebp), %eax
	movl	%edx, _point2P5_simple.7219
	movl	%eax, _point2P5_simple.7219+4
	leal	-72(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereImage
	leal	-76(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereImage
	movl	-40(%ebp), %eax
	movl	4(%eax), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-40(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-44(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereArbreMotif
L96:
	nop
	addl	$124, %esp
	popl	%ebx
	.cfi_restore 3
	popl	%esi
	.cfi_restore 6
	popl	%edi
	.cfi_restore 7
	popl	%ebp
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE33:
	.section .rdata,"dr"
	.align 4
LC68:
	.ascii "./resources/partie5/partie5_complexe_robotpath.bmp\0"
	.align 4
LC69:
	.ascii "./resources/partie5/partie5_complexe_robotpath.txt\0"
	.text
	.globl	_modfify_partie5_imgP5_complexe_callback
	.def	_modfify_partie5_imgP5_complexe_callback;	.scl	2;	.type	32;	.endef
_modfify_partie5_imgP5_complexe_callback:
LFB34:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	pushl	%edi
	pushl	%esi
	pushl	%ebx
	subl	$124, %esp
	.cfi_offset 7, -12
	.cfi_offset 6, -16
	.cfi_offset 3, -20
	movl	8(%ebp), %eax
	movl	%eax, -32(%ebp)
	movl	12(%ebp), %eax
	movl	%eax, -36(%ebp)
	movl	_point1P5_complexe.7248, %eax
	movl	_point1P5_complexe.7248+4, %edx
	movl	%eax, -60(%ebp)
	movl	%edx, -56(%ebp)
	movl	_point2P5_complexe.7250, %eax
	movl	_point2P5_complexe.7250+4, %edx
	movl	%eax, -68(%ebp)
	movl	%edx, -64(%ebp)
	movl	_appState, %eax
	movzbl	24(%eax), %eax
	cmpb	$1, %al
	jne	L98
	movl	_appState, %eax
	movl	28(%eax), %edx
	movl	-36(%ebp), %eax
	movl	4(%eax), %eax
	subl	%eax, %edx
	movl	-36(%ebp), %eax
	movl	32(%eax), %eax
	movl	(%eax), %eax
	movl	8(%eax), %eax
	movl	%edx, %ecx
	subl	%eax, %ecx
	movl	_appState, %eax
	movl	32(%eax), %edx
	movl	-36(%ebp), %eax
	movl	8(%eax), %eax
	subl	%eax, %edx
	movl	-36(%ebp), %eax
	movl	32(%eax), %eax
	movl	(%eax), %eax
	movl	12(%eax), %eax
	subl	%eax, %edx
	movl	%edx, %eax
	movl	$512, %edx
	subl	%eax, %edx
	movl	%edx, %eax
	movl	%ecx, _point1P5_complexe.7248
	movl	%eax, _point1P5_complexe.7248+4
	jmp	L99
L98:
	movl	_appState, %eax
	movzbl	24(%eax), %eax
	cmpb	$3, %al
	jne	L99
	movl	_appState, %eax
	movl	28(%eax), %edx
	movl	-36(%ebp), %eax
	movl	4(%eax), %eax
	subl	%eax, %edx
	movl	-36(%ebp), %eax
	movl	32(%eax), %eax
	movl	(%eax), %eax
	movl	8(%eax), %eax
	movl	%edx, %ecx
	subl	%eax, %ecx
	movl	_appState, %eax
	movl	32(%eax), %edx
	movl	-36(%ebp), %eax
	movl	8(%eax), %eax
	subl	%eax, %edx
	movl	-36(%ebp), %eax
	movl	32(%eax), %eax
	movl	(%eax), %eax
	movl	12(%eax), %eax
	subl	%eax, %edx
	movl	%edx, %eax
	movl	$512, %edx
	subl	%eax, %edx
	movl	%edx, %eax
	movl	%ecx, _point2P5_complexe.7250
	movl	%eax, _point2P5_complexe.7250+4
L99:
	movl	_appState, %eax
	movzbl	24(%eax), %eax
	cmpb	$1, %al
	je	L100
	movl	_appState, %eax
	movzbl	24(%eax), %eax
	cmpb	$3, %al
	jne	L108
L100:
	movl	$LC54, (%esp)
	call	_chargeImage
	movl	%eax, -72(%ebp)
	movl	-72(%ebp), %eax
	movl	%eax, (%esp)
	call	_creePalette
	movl	%eax, -40(%ebp)
	movl	-72(%ebp), %eax
	movl	(%eax), %edx
	movl	-72(%ebp), %eax
	movl	$0, 16(%esp)
	movl	$0, 12(%esp)
	movl	%edx, 8(%esp)
	movl	-40(%ebp), %edx
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_creeArbreMotifs
	movl	%eax, -44(%ebp)
	movl	-72(%ebp), %eax
	movl	4(%eax), %edx
	movl	-72(%ebp), %eax
	movl	(%eax), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_alloueImage
	movl	%eax, -76(%ebp)
	movl	-76(%ebp), %ecx
	movl	$1, 16(%esp)
	movl	-44(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	-40(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 4(%esp)
	movl	%edx, 8(%esp)
	movl	%ecx, (%esp)
	call	_dessineCarte
	movl	_point2P5_complexe.7250, %eax
	movl	%eax, -96(%ebp)
	movl	_point2P5_complexe.7250+4, %eax
	movl	%eax, -92(%ebp)
	movl	_point1P5_complexe.7248, %eax
	movl	%eax, %ebx
	movl	_point1P5_complexe.7248+4, %eax
	movl	%eax, %esi
	movl	-72(%ebp), %edi
	movl	-76(%ebp), %eax
	movl	-96(%ebp), %edx
	movl	-92(%ebp), %ecx
	movl	%edx, 20(%esp)
	movl	%ecx, 24(%esp)
	movl	%ebx, 12(%esp)
	movl	%esi, 16(%esp)
	movl	-40(%ebp), %ecx
	movl	%ecx, 8(%esp)
	movl	%edi, 4(%esp)
	movl	%eax, (%esp)
	call	_calculeTrajectoire
	movl	%eax, -48(%ebp)
	cmpl	$0, -48(%ebp)
	je	L102
	movl	-48(%ebp), %eax
	movl	(%eax), %eax
	movl	%eax, -28(%ebp)
	jmp	L103
L104:
	movl	-72(%ebp), %eax
	movl	8(%eax), %edx
	movl	-28(%ebp), %eax
	movl	(%eax), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-28(%ebp), %eax
	movl	4(%eax), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movw	$0, (%eax)
	movl	-72(%ebp), %eax
	movl	12(%eax), %edx
	movl	-28(%ebp), %eax
	movl	(%eax), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-28(%ebp), %eax
	movl	4(%eax), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movw	$0, (%eax)
	movl	-72(%ebp), %eax
	movl	16(%eax), %edx
	movl	-28(%ebp), %eax
	movl	(%eax), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-28(%ebp), %eax
	movl	4(%eax), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movw	$0, (%eax)
	movl	-28(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -28(%ebp)
L103:
	cmpl	$0, -28(%ebp)
	jne	L104
	movl	-72(%ebp), %eax
	movl	$LC68, 4(%esp)
	movl	%eax, (%esp)
	call	_sauveImage
	movl	-36(%ebp), %eax
	movl	32(%eax), %eax
	movl	(%eax), %eax
	addl	$24, %eax
	movl	%eax, (%esp)
	call	_libereImage
	movl	-36(%ebp), %eax
	movl	32(%eax), %eax
	movl	(%eax), %eax
	movl	-72(%ebp), %edx
	movl	%edx, 24(%eax)
	movl	$LC69, 4(%esp)
	movl	-48(%ebp), %eax
	movl	%eax, (%esp)
	call	_sauveDescriptionChemin
	jmp	L105
L106:
	movl	-48(%ebp), %eax
	movl	(%eax), %eax
	movl	8(%eax), %eax
	movl	%eax, -52(%ebp)
	movl	-48(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereMaillon
	movl	-48(%ebp), %eax
	movl	-52(%ebp), %edx
	movl	%edx, (%eax)
L105:
	movl	-48(%ebp), %eax
	movl	(%eax), %eax
	testl	%eax, %eax
	jne	L106
	movl	-48(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
	leal	-76(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereImage
	movl	-40(%ebp), %eax
	movl	4(%eax), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-40(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-44(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereArbreMotif
	jmp	L108
L102:
	movl	12(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_trigger_error_callback
	movl	-60(%ebp), %edx
	movl	-56(%ebp), %eax
	movl	%edx, _point1P5_complexe.7248
	movl	%eax, _point1P5_complexe.7248+4
	movl	-68(%ebp), %edx
	movl	-64(%ebp), %eax
	movl	%edx, _point2P5_complexe.7250
	movl	%eax, _point2P5_complexe.7250+4
	leal	-72(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereImage
	leal	-76(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereImage
	movl	-40(%ebp), %eax
	movl	4(%eax), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-40(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-44(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereArbreMotif
L108:
	nop
	addl	$124, %esp
	popl	%ebx
	.cfi_restore 3
	popl	%esi
	.cfi_restore 6
	popl	%edi
	.cfi_restore 7
	popl	%ebp
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE34:
	.section .rdata,"dr"
	.align 4
LC70:
	.ascii "RESET\12------ LANCEMENT PARTIE 5 ------\0"
	.align 4
LC71:
	.ascii "------ LANCEMENT PARTIE 5 ------\0"
	.align 4
LC72:
	.ascii "\12> lancement fonction dessineCarte\12arg1:'%p' arg2:'%p' arg3:'%p' arg4:'%d'\0"
	.align 4
LC73:
	.ascii "./resources/partie5/partie4_traj.bmp\0"
	.align 4
LC74:
	.ascii "./resources/partie5/partie5_simple_traj.bmp\0"
	.align 4
LC75:
	.ascii "./resources/partie5/partie5_complexe_traj.bmp\0"
LC76:
	.ascii "P5-Annexe 1\0"
LC77:
	.ascii "P5-Annexe 2\0"
LC78:
	.ascii "P5-Annexe 3\0"
	.align 4
LC79:
	.ascii "\12> lancement fonction calculeTrajectoire\12arg1:'%p' arg2:'%p' arg3:'%p' arg4:'%d %d' arg5:'%d %d'\0"
	.align 4
LC80:
	.ascii "\12> lancement fonction sauveDescriptionChemin\12arg1:'%p %s'\0"
LC81:
	.ascii "P5-Annexe 4\0"
LC82:
	.ascii "P5-Annexe 5\0"
LC83:
	.ascii "P5-Annexe 6\0"
	.text
	.globl	_affiche_partie5_callback
	.def	_affiche_partie5_callback;	.scl	2;	.type	32;	.endef
_affiche_partie5_callback:
LFB35:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	pushl	%edi
	pushl	%esi
	pushl	%ebx
	subl	$956, %esp
	.cfi_offset 7, -12
	.cfi_offset 6, -16
	.cfi_offset 3, -20
	jmp	L110
L111:
	movl	_appState, %eax
	movl	64(%eax), %eax
	addl	$4, %eax
	movl	(%eax), %eax
	movl	%eax, (%esp)
	call	_removeGui
L110:
	movl	_appState, %eax
	movl	64(%eax), %eax
	addl	$4, %eax
	movl	(%eax), %eax
	testl	%eax, %eax
	jne	L111
	movl	8(%ebp), %eax
	movl	%eax, -40(%ebp)
	movl	-40(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$28, %eax
	movl	(%eax), %eax
	movl	40(%eax), %eax
	movl	(%eax), %eax
	movl	28(%eax), %eax
	testl	%eax, %eax
	je	L112
	movl	-40(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$28, %eax
	movl	(%eax), %eax
	movl	40(%eax), %eax
	movl	(%eax), %eax
	movl	28(%eax), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-40(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$28, %eax
	movl	(%eax), %eax
	movl	40(%eax), %eax
	movl	(%eax), %ebx
	movl	$LC70, (%esp)
	call	_createString
	movl	%eax, 28(%ebx)
	jmp	L113
L112:
	movl	-40(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$28, %eax
	movl	(%eax), %eax
	movl	40(%eax), %eax
	movl	(%eax), %ebx
	movl	$LC71, (%esp)
	call	_createString
	movl	%eax, 28(%ebx)
L113:
	movl	-40(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$28, %eax
	movl	(%eax), %eax
	movl	40(%eax), %eax
	movl	(%eax), %eax
	movl	%eax, -44(%ebp)
	movl	-40(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$8, %eax
	movl	(%eax), %eax
	movl	$0, (%eax)
	movl	-40(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$12, %eax
	movl	(%eax), %eax
	movl	$0, (%eax)
	movl	-40(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$16, %eax
	movl	(%eax), %eax
	movl	$0, (%eax)
	movl	-40(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$20, %eax
	movl	(%eax), %eax
	movl	$0, (%eax)
	movl	-40(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$24, %eax
	movl	(%eax), %eax
	movl	$0, (%eax)
	movl	-40(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$28, %eax
	movl	(%eax), %eax
	movl	$1, (%eax)
	movl	-40(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$32, %eax
	movl	(%eax), %eax
	movl	$0, (%eax)
	movl	$LC52, 8(%esp)
	movl	$LC20, 4(%esp)
	leal	-796(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-796(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-44(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	$LC52, (%esp)
	call	_chargeImage
	movl	%eax, -800(%ebp)
	movl	-800(%ebp), %eax
	testl	%eax, %eax
	jne	L114
	movl	$LC7, %edx
	jmp	L115
L114:
	movl	$LC8, %edx
L115:
	movl	-800(%ebp), %eax
	movl	%edx, 12(%esp)
	movl	%eax, 8(%esp)
	movl	$LC9, 4(%esp)
	leal	-796(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-796(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-44(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	$LC53, 8(%esp)
	movl	$LC20, 4(%esp)
	leal	-796(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-796(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-44(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	$LC53, (%esp)
	call	_chargeImage
	movl	%eax, -804(%ebp)
	movl	-804(%ebp), %eax
	testl	%eax, %eax
	jne	L116
	movl	$LC7, %edx
	jmp	L117
L116:
	movl	$LC8, %edx
L117:
	movl	-804(%ebp), %eax
	movl	%edx, 12(%esp)
	movl	%eax, 8(%esp)
	movl	$LC9, 4(%esp)
	leal	-796(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-796(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-44(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	$LC54, 8(%esp)
	movl	$LC20, 4(%esp)
	leal	-796(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-796(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-44(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	$LC54, (%esp)
	call	_chargeImage
	movl	%eax, -808(%ebp)
	movl	-808(%ebp), %eax
	testl	%eax, %eax
	jne	L118
	movl	$LC7, %edx
	jmp	L119
L118:
	movl	$LC8, %edx
L119:
	movl	-808(%ebp), %eax
	movl	%edx, 12(%esp)
	movl	%eax, 8(%esp)
	movl	$LC9, 4(%esp)
	leal	-796(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-796(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-44(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-800(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC55, 4(%esp)
	leal	-796(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-796(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-44(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-800(%ebp), %eax
	movl	%eax, (%esp)
	call	_creePalette
	movl	%eax, -48(%ebp)
	cmpl	$0, -48(%ebp)
	jne	L120
	movl	$LC7, %eax
	jmp	L121
L120:
	movl	$LC8, %eax
L121:
	movl	%eax, 12(%esp)
	movl	-48(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC9, 4(%esp)
	leal	-796(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-796(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-44(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-804(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC55, 4(%esp)
	leal	-796(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-796(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-44(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-804(%ebp), %eax
	movl	%eax, (%esp)
	call	_creePalette
	movl	%eax, -52(%ebp)
	cmpl	$0, -52(%ebp)
	jne	L122
	movl	$LC7, %eax
	jmp	L123
L122:
	movl	$LC8, %eax
L123:
	movl	%eax, 12(%esp)
	movl	-52(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC9, 4(%esp)
	leal	-796(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-796(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-44(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-808(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC55, 4(%esp)
	leal	-796(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-796(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-44(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-808(%ebp), %eax
	movl	%eax, (%esp)
	call	_creePalette
	movl	%eax, -56(%ebp)
	cmpl	$0, -56(%ebp)
	jne	L124
	movl	$LC7, %eax
	jmp	L125
L124:
	movl	$LC8, %eax
L125:
	movl	%eax, 12(%esp)
	movl	-56(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC9, 4(%esp)
	leal	-796(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-796(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-44(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-800(%ebp), %eax
	movl	(%eax), %eax
	movl	-800(%ebp), %edx
	movl	$0, 24(%esp)
	movl	$0, 20(%esp)
	movl	%eax, 16(%esp)
	movl	-48(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	%edx, 8(%esp)
	movl	$LC56, 4(%esp)
	leal	-796(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-796(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-44(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-800(%ebp), %eax
	movl	(%eax), %eax
	movl	-800(%ebp), %edx
	movl	$0, 16(%esp)
	movl	$0, 12(%esp)
	movl	%eax, 8(%esp)
	movl	-48(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	%edx, (%esp)
	call	_creeArbreMotifs
	movl	%eax, -60(%ebp)
	cmpl	$0, -60(%ebp)
	jne	L126
	movl	$LC7, %eax
	jmp	L127
L126:
	movl	$LC8, %eax
L127:
	movl	%eax, 12(%esp)
	movl	-60(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC9, 4(%esp)
	leal	-796(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-796(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-44(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-804(%ebp), %eax
	movl	(%eax), %eax
	movl	-804(%ebp), %edx
	movl	$0, 24(%esp)
	movl	$0, 20(%esp)
	movl	%eax, 16(%esp)
	movl	-52(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	%edx, 8(%esp)
	movl	$LC56, 4(%esp)
	leal	-796(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-796(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-44(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-804(%ebp), %eax
	movl	(%eax), %eax
	movl	-804(%ebp), %edx
	movl	$0, 16(%esp)
	movl	$0, 12(%esp)
	movl	%eax, 8(%esp)
	movl	-52(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	%edx, (%esp)
	call	_creeArbreMotifs
	movl	%eax, -64(%ebp)
	cmpl	$0, -64(%ebp)
	jne	L128
	movl	$LC7, %eax
	jmp	L129
L128:
	movl	$LC8, %eax
L129:
	movl	%eax, 12(%esp)
	movl	-64(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC9, 4(%esp)
	leal	-796(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-796(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-44(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-808(%ebp), %eax
	movl	(%eax), %eax
	movl	-808(%ebp), %edx
	movl	$0, 24(%esp)
	movl	$0, 20(%esp)
	movl	%eax, 16(%esp)
	movl	-56(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	%edx, 8(%esp)
	movl	$LC56, 4(%esp)
	leal	-796(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-796(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-44(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-808(%ebp), %eax
	movl	(%eax), %eax
	movl	-808(%ebp), %edx
	movl	$0, 16(%esp)
	movl	$0, 12(%esp)
	movl	%eax, 8(%esp)
	movl	-56(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	%edx, (%esp)
	call	_creeArbreMotifs
	movl	%eax, -68(%ebp)
	cmpl	$0, -68(%ebp)
	jne	L130
	movl	$LC7, %eax
	jmp	L131
L130:
	movl	$LC8, %eax
L131:
	movl	%eax, 12(%esp)
	movl	-68(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC9, 4(%esp)
	leal	-796(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-796(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-44(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-800(%ebp), %eax
	movl	4(%eax), %edx
	movl	-800(%ebp), %eax
	movl	(%eax), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_alloueImage
	movl	%eax, -812(%ebp)
	movl	-804(%ebp), %eax
	movl	4(%eax), %edx
	movl	-804(%ebp), %eax
	movl	(%eax), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_alloueImage
	movl	%eax, -816(%ebp)
	movl	-808(%ebp), %eax
	movl	4(%eax), %edx
	movl	-808(%ebp), %eax
	movl	(%eax), %eax
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_alloueImage
	movl	%eax, -820(%ebp)
	movl	-812(%ebp), %edx
	movl	$1, 20(%esp)
	movl	-60(%ebp), %eax
	movl	%eax, 16(%esp)
	movl	-48(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	%edx, 8(%esp)
	movl	$LC72, 4(%esp)
	leal	-796(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-796(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-44(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-812(%ebp), %ecx
	movl	$1, 16(%esp)
	movl	-60(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	-48(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 4(%esp)
	movl	%edx, 8(%esp)
	movl	%ecx, (%esp)
	call	_dessineCarte
	movl	$LC43, 4(%esp)
	leal	-796(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-796(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-44(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-60(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereArbreMotif
	movl	-816(%ebp), %edx
	movl	$1, 20(%esp)
	movl	-64(%ebp), %eax
	movl	%eax, 16(%esp)
	movl	-52(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	%edx, 8(%esp)
	movl	$LC72, 4(%esp)
	leal	-796(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-796(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-44(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-816(%ebp), %ecx
	movl	$1, 16(%esp)
	movl	-64(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	-52(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 4(%esp)
	movl	%edx, 8(%esp)
	movl	%ecx, (%esp)
	call	_dessineCarte
	movl	$LC43, 4(%esp)
	leal	-796(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-796(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-44(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-64(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereArbreMotif
	movl	-820(%ebp), %edx
	movl	$1, 20(%esp)
	movl	-68(%ebp), %eax
	movl	%eax, 16(%esp)
	movl	-56(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	%edx, 8(%esp)
	movl	$LC72, 4(%esp)
	leal	-796(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-796(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-44(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-820(%ebp), %ecx
	movl	$1, 16(%esp)
	movl	-68(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	-56(%ebp), %eax
	movl	4(%eax), %edx
	movl	(%eax), %eax
	movl	%eax, 4(%esp)
	movl	%edx, 8(%esp)
	movl	%ecx, (%esp)
	call	_dessineCarte
	movl	$LC43, 4(%esp)
	leal	-796(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-796(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-44(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-68(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereArbreMotif
	movl	-812(%ebp), %eax
	movl	$LC73, 4(%esp)
	movl	%eax, (%esp)
	call	_sauveImage
	movl	-816(%ebp), %eax
	movl	$LC74, 4(%esp)
	movl	%eax, (%esp)
	call	_sauveImage
	movl	-820(%ebp), %eax
	movl	$LC75, 4(%esp)
	movl	%eax, (%esp)
	call	_sauveImage
	movl	$LC76, -296(%ebp)
	movl	$805240832, -292(%ebp)
	movl	$805240832, -288(%ebp)
	movl	$256, -284(%ebp)
	movl	$256, -280(%ebp)
	movl	$0, -276(%ebp)
	movl	$4, -272(%ebp)
	movl	$LC73, (%esp)
	call	_chargeImage
	movl	$0, 28(%esp)
	movl	%eax, 24(%esp)
	movl	$128, 20(%esp)
	movl	$128, 16(%esp)
	movl	$0, 12(%esp)
	movl	$0, 8(%esp)
	movl	$0, 4(%esp)
	movl	$1, (%esp)
	call	_initDisplaySurface
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initDisplaySurfaces
	movl	%eax, %edx
	movb	$0, -912(%ebp)
	movl	$0, %eax
	movl	-912(%ebp), %ebx
	movb	%al, %bh
	andl	$-16711681, %ebx
	movl	%ebx, %eax
	orl	$-16777216, %eax
	movl	%eax, %ecx
	movb	$0, -908(%ebp)
	movl	$0, %eax
	movl	-908(%ebp), %ebx
	movb	%al, %bh
	andl	$-16711681, %ebx
	movl	%ebx, %eax
	orl	$-16777216, %eax
	movl	$0, 40(%esp)
	movl	$0, 36(%esp)
	movl	%edx, 32(%esp)
	movl	$0, 28(%esp)
	movl	%ecx, 24(%esp)
	movl	%eax, 20(%esp)
	movl	$256, 16(%esp)
	movl	$256, 12(%esp)
	movl	$0, 8(%esp)
	movl	$0, 4(%esp)
	movl	$1, (%esp)
	call	_initFrame
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initFrames
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initView
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initViews
	movl	%eax, -268(%ebp)
	movl	-296(%ebp), %eax
	movl	%eax, (%esp)
	movl	-292(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-288(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	-284(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	-280(%ebp), %eax
	movl	%eax, 16(%esp)
	movl	-276(%ebp), %eax
	movl	%eax, 20(%esp)
	movl	-272(%ebp), %eax
	movl	%eax, 24(%esp)
	movl	-268(%ebp), %eax
	movl	%eax, 28(%esp)
	call	_addGui
	movl	$LC77, -264(%ebp)
	movl	$805240832, -260(%ebp)
	movl	$805240832, -256(%ebp)
	movl	$512, -252(%ebp)
	movl	$512, -248(%ebp)
	movl	$0, -244(%ebp)
	movl	$4, -240(%ebp)
	movl	$LC74, (%esp)
	call	_chargeImage
	movl	$0, 28(%esp)
	movl	%eax, 24(%esp)
	movl	$512, 20(%esp)
	movl	$512, 16(%esp)
	movl	$0, 12(%esp)
	movl	$0, 8(%esp)
	movl	$0, 4(%esp)
	movl	$1, (%esp)
	call	_initDisplaySurface
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initDisplaySurfaces
	movl	%eax, %edx
	movb	$0, -904(%ebp)
	movl	$0, %eax
	movl	-904(%ebp), %ebx
	movb	%al, %bh
	andl	$-16711681, %ebx
	movl	%ebx, %eax
	orl	$-16777216, %eax
	movl	%eax, %ebx
	movb	$0, -900(%ebp)
	movl	$0, %eax
	movl	-900(%ebp), %ecx
	movb	%al, %ch
	andl	$-16711681, %ecx
	movl	%ecx, %eax
	orl	$-16777216, %eax
	movl	$0, 40(%esp)
	movl	$0, 36(%esp)
	movl	%edx, 32(%esp)
	movl	$0, 28(%esp)
	movl	%ebx, 24(%esp)
	movl	%eax, 20(%esp)
	movl	$512, 16(%esp)
	movl	$512, 12(%esp)
	movl	$0, 8(%esp)
	movl	$0, 4(%esp)
	movl	$1, (%esp)
	call	_initFrame
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initFrames
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initView
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initViews
	movl	%eax, -236(%ebp)
	movl	-264(%ebp), %eax
	movl	%eax, (%esp)
	movl	-260(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-256(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	-252(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	-248(%ebp), %eax
	movl	%eax, 16(%esp)
	movl	-244(%ebp), %eax
	movl	%eax, 20(%esp)
	movl	-240(%ebp), %eax
	movl	%eax, 24(%esp)
	movl	-236(%ebp), %eax
	movl	%eax, 28(%esp)
	call	_addGui
	movl	$LC78, -232(%ebp)
	movl	$805240832, -228(%ebp)
	movl	$805240832, -224(%ebp)
	movl	$512, -220(%ebp)
	movl	$512, -216(%ebp)
	movl	$0, -212(%ebp)
	movl	$4, -208(%ebp)
	movl	$LC75, (%esp)
	call	_chargeImage
	movl	$0, 28(%esp)
	movl	%eax, 24(%esp)
	movl	$512, 20(%esp)
	movl	$512, 16(%esp)
	movl	$0, 12(%esp)
	movl	$0, 8(%esp)
	movl	$0, 4(%esp)
	movl	$1, (%esp)
	call	_initDisplaySurface
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initDisplaySurfaces
	movl	%eax, %edx
	movb	$0, -896(%ebp)
	movl	$0, %eax
	movl	-896(%ebp), %ebx
	movb	%al, %bh
	andl	$-16711681, %ebx
	movl	%ebx, %eax
	orl	$-16777216, %eax
	movl	%eax, %ebx
	movb	$0, -892(%ebp)
	movl	$0, %eax
	movl	-892(%ebp), %ecx
	movb	%al, %ch
	andl	$-16711681, %ecx
	movl	%ecx, %eax
	orl	$-16777216, %eax
	movl	$0, 40(%esp)
	movl	$0, 36(%esp)
	movl	%edx, 32(%esp)
	movl	$0, 28(%esp)
	movl	%ebx, 24(%esp)
	movl	%eax, 20(%esp)
	movl	$512, 16(%esp)
	movl	$512, 12(%esp)
	movl	$0, 8(%esp)
	movl	$0, 4(%esp)
	movl	$1, (%esp)
	call	_initFrame
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initFrames
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initView
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initViews
	movl	%eax, -204(%ebp)
	movl	-232(%ebp), %eax
	movl	%eax, (%esp)
	movl	-228(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-224(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	-220(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	-216(%ebp), %eax
	movl	%eax, 16(%esp)
	movl	-212(%ebp), %eax
	movl	%eax, 20(%esp)
	movl	-208(%ebp), %eax
	movl	%eax, 24(%esp)
	movl	-204(%ebp), %eax
	movl	%eax, 28(%esp)
	call	_addGui
	movl	-800(%ebp), %ecx
	movl	-812(%ebp), %edx
	movl	$68, 32(%esp)
	movl	$19, 28(%esp)
	movl	$15, 24(%esp)
	movl	$67, 20(%esp)
	movl	-48(%ebp), %eax
	movl	%eax, 16(%esp)
	movl	%ecx, 12(%esp)
	movl	%edx, 8(%esp)
	movl	$LC79, 4(%esp)
	leal	-796(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-796(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-44(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	movl	$19, -864(%ebp)
	movl	$68, -860(%ebp)
	movl	$67, %esi
	movl	$15, %edi
	movl	-800(%ebp), %ecx
	movl	-812(%ebp), %ebx
	movl	-864(%ebp), %eax
	movl	-860(%ebp), %edx
	movl	%eax, 20(%esp)
	movl	%edx, 24(%esp)
	movl	%esi, 12(%esp)
	movl	%edi, 16(%esp)
	movl	-48(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	%ecx, 4(%esp)
	movl	%ebx, (%esp)
	call	_calculeTrajectoire
	movl	%eax, -72(%ebp)
	cmpl	$0, -72(%ebp)
	jne	L132
	movl	$LC7, %eax
	jmp	L133
L132:
	movl	$LC8, %eax
L133:
	movl	%eax, 12(%esp)
	movl	-72(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC9, 4(%esp)
	leal	-796(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-796(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-44(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	cmpl	$0, -72(%ebp)
	jne	L134
	movl	-48(%ebp), %eax
	movl	4(%eax), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-48(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-52(%ebp), %eax
	movl	4(%eax), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-52(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-56(%ebp), %eax
	movl	4(%eax), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-56(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
	leal	-812(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereImage
	leal	-800(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereImage
	leal	-816(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereImage
	leal	-804(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereImage
	leal	-820(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereImage
	leal	-808(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereImage
	jmp	L109
L134:
	movl	-804(%ebp), %ecx
	movl	-816(%ebp), %edx
	movl	$503, 32(%esp)
	movl	$501, 28(%esp)
	movl	$70, 24(%esp)
	movl	$95, 20(%esp)
	movl	-52(%ebp), %eax
	movl	%eax, 16(%esp)
	movl	%ecx, 12(%esp)
	movl	%edx, 8(%esp)
	movl	$LC79, 4(%esp)
	leal	-796(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-796(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-44(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	movl	$501, -856(%ebp)
	movl	$503, -852(%ebp)
	movl	$95, -848(%ebp)
	movl	$70, -844(%ebp)
	movl	-804(%ebp), %ecx
	movl	-816(%ebp), %edx
	movl	-856(%ebp), %esi
	movl	-852(%ebp), %edi
	movl	%esi, 20(%esp)
	movl	%edi, 24(%esp)
	movl	-848(%ebp), %esi
	movl	-844(%ebp), %edi
	movl	%esi, 12(%esp)
	movl	%edi, 16(%esp)
	movl	-52(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	%ecx, 4(%esp)
	movl	%edx, (%esp)
	call	_calculeTrajectoire
	movl	%eax, -76(%ebp)
	cmpl	$0, -76(%ebp)
	jne	L136
	movl	$LC7, %eax
	jmp	L137
L136:
	movl	$LC8, %eax
L137:
	movl	%eax, 12(%esp)
	movl	-76(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC9, 4(%esp)
	leal	-796(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-796(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-44(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	cmpl	$0, -76(%ebp)
	jne	L138
	movl	-48(%ebp), %eax
	movl	4(%eax), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-48(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-52(%ebp), %eax
	movl	4(%eax), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-52(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-56(%ebp), %eax
	movl	4(%eax), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-56(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
	leal	-812(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereImage
	leal	-800(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereImage
	leal	-816(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereImage
	leal	-804(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereImage
	leal	-820(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereImage
	leal	-808(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereImage
	jmp	L109
L138:
	movl	-808(%ebp), %ecx
	movl	-820(%ebp), %edx
	movl	$503, 32(%esp)
	movl	$501, 28(%esp)
	movl	$70, 24(%esp)
	movl	$95, 20(%esp)
	movl	-56(%ebp), %eax
	movl	%eax, 16(%esp)
	movl	%ecx, 12(%esp)
	movl	%edx, 8(%esp)
	movl	$LC79, 4(%esp)
	leal	-796(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-796(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-44(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	movl	$501, -840(%ebp)
	movl	$503, -836(%ebp)
	movl	$95, -832(%ebp)
	movl	$70, -828(%ebp)
	movl	-808(%ebp), %ecx
	movl	-820(%ebp), %edx
	movl	-840(%ebp), %esi
	movl	-836(%ebp), %edi
	movl	%esi, 20(%esp)
	movl	%edi, 24(%esp)
	movl	-832(%ebp), %esi
	movl	-828(%ebp), %edi
	movl	%esi, 12(%esp)
	movl	%edi, 16(%esp)
	movl	-56(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	%ecx, 4(%esp)
	movl	%edx, (%esp)
	call	_calculeTrajectoire
	movl	%eax, -80(%ebp)
	cmpl	$0, -80(%ebp)
	jne	L139
	movl	$LC7, %eax
	jmp	L140
L139:
	movl	$LC8, %eax
L140:
	movl	%eax, 12(%esp)
	movl	-80(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC9, 4(%esp)
	leal	-796(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-796(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-44(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	cmpl	$0, -80(%ebp)
	jne	L141
	movl	-48(%ebp), %eax
	movl	4(%eax), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-48(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-52(%ebp), %eax
	movl	4(%eax), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-52(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-56(%ebp), %eax
	movl	4(%eax), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-56(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
	leal	-812(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereImage
	leal	-800(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereImage
	leal	-816(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereImage
	leal	-804(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereImage
	leal	-820(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereImage
	leal	-808(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereImage
	jmp	L109
L141:
	movl	$LC65, 12(%esp)
	movl	-72(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC80, 4(%esp)
	leal	-796(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-796(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-44(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	$LC65, 4(%esp)
	movl	-72(%ebp), %eax
	movl	%eax, (%esp)
	call	_sauveDescriptionChemin
	movl	$LC67, 12(%esp)
	movl	-76(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC80, 4(%esp)
	leal	-796(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-796(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-44(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	$LC67, 4(%esp)
	movl	-76(%ebp), %eax
	movl	%eax, (%esp)
	call	_sauveDescriptionChemin
	movl	$LC69, 12(%esp)
	movl	-80(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC80, 4(%esp)
	leal	-796(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-796(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-44(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	$LC69, 4(%esp)
	movl	-80(%ebp), %eax
	movl	%eax, (%esp)
	call	_sauveDescriptionChemin
	movl	-48(%ebp), %eax
	movl	4(%eax), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-48(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-52(%ebp), %eax
	movl	4(%eax), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-52(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-56(%ebp), %eax
	movl	4(%eax), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-56(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
	leal	-812(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereImage
	leal	-800(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereImage
	leal	-816(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereImage
	leal	-804(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereImage
	leal	-820(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereImage
	leal	-808(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereImage
	movl	$LC49, 4(%esp)
	leal	-796(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-796(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-44(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	movl	$LC52, (%esp)
	call	_chargeImage
	movl	%eax, -84(%ebp)
	movl	$LC81, -200(%ebp)
	movl	$805240832, -196(%ebp)
	movl	$805240832, -192(%ebp)
	movl	$256, -188(%ebp)
	movl	$256, -184(%ebp)
	movl	$0, -180(%ebp)
	movl	$4, -176(%ebp)
	movl	$_modfify_partie5_imgP4_callback, 28(%esp)
	movl	-84(%ebp), %eax
	movl	%eax, 24(%esp)
	movl	$128, 20(%esp)
	movl	$128, 16(%esp)
	movl	$0, 12(%esp)
	movl	$0, 8(%esp)
	movl	$0, 4(%esp)
	movl	$1, (%esp)
	call	_initDisplaySurface
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initDisplaySurfaces
	movl	%eax, %edx
	movb	$0, -888(%ebp)
	movl	$0, %eax
	movl	-888(%ebp), %ebx
	movb	%al, %bh
	andl	$-16711681, %ebx
	movl	%ebx, %eax
	orl	$-16777216, %eax
	movl	%eax, %ebx
	movb	$0, -884(%ebp)
	movl	$0, %eax
	movl	-884(%ebp), %ecx
	movb	%al, %ch
	andl	$-16711681, %ecx
	movl	%ecx, %eax
	orl	$-16777216, %eax
	movl	$0, 40(%esp)
	movl	$0, 36(%esp)
	movl	%edx, 32(%esp)
	movl	$0, 28(%esp)
	movl	%ebx, 24(%esp)
	movl	%eax, 20(%esp)
	movl	$256, 16(%esp)
	movl	$256, 12(%esp)
	movl	$0, 8(%esp)
	movl	$0, 4(%esp)
	movl	$1, (%esp)
	call	_initFrame
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initFrames
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initView
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initViews
	movl	%eax, -172(%ebp)
	movl	-200(%ebp), %eax
	movl	%eax, (%esp)
	movl	-196(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-192(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	-188(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	-184(%ebp), %eax
	movl	%eax, 16(%esp)
	movl	-180(%ebp), %eax
	movl	%eax, 20(%esp)
	movl	-176(%ebp), %eax
	movl	%eax, 24(%esp)
	movl	-172(%ebp), %eax
	movl	%eax, 28(%esp)
	call	_addGui
	movl	$LC53, (%esp)
	call	_chargeImage
	movl	%eax, -88(%ebp)
	movl	$LC82, -168(%ebp)
	movl	$805240832, -164(%ebp)
	movl	$805240832, -160(%ebp)
	movl	$512, -156(%ebp)
	movl	$512, -152(%ebp)
	movl	$0, -148(%ebp)
	movl	$4, -144(%ebp)
	movl	$_modfify_partie5_imgP5_simple_callback, 28(%esp)
	movl	-88(%ebp), %eax
	movl	%eax, 24(%esp)
	movl	$512, 20(%esp)
	movl	$512, 16(%esp)
	movl	$0, 12(%esp)
	movl	$0, 8(%esp)
	movl	$0, 4(%esp)
	movl	$1, (%esp)
	call	_initDisplaySurface
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initDisplaySurfaces
	movl	%eax, %edx
	movb	$0, -880(%ebp)
	movl	$0, %eax
	movl	-880(%ebp), %ebx
	movb	%al, %bh
	andl	$-16711681, %ebx
	movl	%ebx, %eax
	orl	$-16777216, %eax
	movl	%eax, %ebx
	movb	$0, -876(%ebp)
	movl	$0, %eax
	movl	-876(%ebp), %ecx
	movb	%al, %ch
	andl	$-16711681, %ecx
	movl	%ecx, %eax
	orl	$-16777216, %eax
	movl	$0, 40(%esp)
	movl	$0, 36(%esp)
	movl	%edx, 32(%esp)
	movl	$0, 28(%esp)
	movl	%ebx, 24(%esp)
	movl	%eax, 20(%esp)
	movl	$512, 16(%esp)
	movl	$512, 12(%esp)
	movl	$0, 8(%esp)
	movl	$0, 4(%esp)
	movl	$1, (%esp)
	call	_initFrame
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initFrames
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initView
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initViews
	movl	%eax, -140(%ebp)
	movl	-168(%ebp), %eax
	movl	%eax, (%esp)
	movl	-164(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-160(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	-156(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	-152(%ebp), %eax
	movl	%eax, 16(%esp)
	movl	-148(%ebp), %eax
	movl	%eax, 20(%esp)
	movl	-144(%ebp), %eax
	movl	%eax, 24(%esp)
	movl	-140(%ebp), %eax
	movl	%eax, 28(%esp)
	call	_addGui
	movl	$LC54, (%esp)
	call	_chargeImage
	movl	%eax, -92(%ebp)
	movl	$LC83, -136(%ebp)
	movl	$805240832, -132(%ebp)
	movl	$805240832, -128(%ebp)
	movl	$512, -124(%ebp)
	movl	$512, -120(%ebp)
	movl	$0, -116(%ebp)
	movl	$4, -112(%ebp)
	movl	$_modfify_partie5_imgP5_complexe_callback, 28(%esp)
	movl	-92(%ebp), %eax
	movl	%eax, 24(%esp)
	movl	$512, 20(%esp)
	movl	$512, 16(%esp)
	movl	$0, 12(%esp)
	movl	$0, 8(%esp)
	movl	$0, 4(%esp)
	movl	$1, (%esp)
	call	_initDisplaySurface
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initDisplaySurfaces
	movl	%eax, %edx
	movb	$0, -872(%ebp)
	movl	$0, %eax
	movl	-872(%ebp), %ebx
	movb	%al, %bh
	andl	$-16711681, %ebx
	movl	%ebx, %eax
	orl	$-16777216, %eax
	movl	%eax, %ebx
	movb	$0, -868(%ebp)
	movl	$0, %eax
	movl	-868(%ebp), %ecx
	movb	%al, %ch
	andl	$-16711681, %ecx
	movl	%ecx, %eax
	orl	$-16777216, %eax
	movl	$0, 40(%esp)
	movl	$0, 36(%esp)
	movl	%edx, 32(%esp)
	movl	$0, 28(%esp)
	movl	%ebx, 24(%esp)
	movl	%eax, 20(%esp)
	movl	$512, 16(%esp)
	movl	$512, 12(%esp)
	movl	$0, 8(%esp)
	movl	$0, 4(%esp)
	movl	$1, (%esp)
	call	_initFrame
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initFrames
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initView
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	_initViews
	movl	%eax, -108(%ebp)
	movl	-136(%ebp), %eax
	movl	%eax, (%esp)
	movl	-132(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-128(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	-124(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	-120(%ebp), %eax
	movl	%eax, 16(%esp)
	movl	-116(%ebp), %eax
	movl	%eax, 20(%esp)
	movl	-112(%ebp), %eax
	movl	%eax, 24(%esp)
	movl	-108(%ebp), %eax
	movl	%eax, 28(%esp)
	call	_addGui
	movl	-72(%ebp), %eax
	movl	(%eax), %eax
	movl	%eax, -28(%ebp)
	jmp	L142
L143:
	movl	-84(%ebp), %eax
	movl	8(%eax), %edx
	movl	-28(%ebp), %eax
	movl	(%eax), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-28(%ebp), %eax
	movl	4(%eax), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movw	$0, (%eax)
	movl	-84(%ebp), %eax
	movl	12(%eax), %edx
	movl	-28(%ebp), %eax
	movl	(%eax), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-28(%ebp), %eax
	movl	4(%eax), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movw	$0, (%eax)
	movl	-84(%ebp), %eax
	movl	16(%eax), %edx
	movl	-28(%ebp), %eax
	movl	(%eax), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-28(%ebp), %eax
	movl	4(%eax), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movw	$0, (%eax)
	movl	-28(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -28(%ebp)
L142:
	cmpl	$0, -28(%ebp)
	jne	L143
	movl	$LC64, 4(%esp)
	movl	-84(%ebp), %eax
	movl	%eax, (%esp)
	call	_sauveImage
	movl	-76(%ebp), %eax
	movl	(%eax), %eax
	movl	%eax, -32(%ebp)
	jmp	L144
L145:
	movl	-88(%ebp), %eax
	movl	8(%eax), %edx
	movl	-32(%ebp), %eax
	movl	(%eax), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-32(%ebp), %eax
	movl	4(%eax), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movw	$0, (%eax)
	movl	-88(%ebp), %eax
	movl	12(%eax), %edx
	movl	-32(%ebp), %eax
	movl	(%eax), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-32(%ebp), %eax
	movl	4(%eax), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movw	$0, (%eax)
	movl	-88(%ebp), %eax
	movl	16(%eax), %edx
	movl	-32(%ebp), %eax
	movl	(%eax), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-32(%ebp), %eax
	movl	4(%eax), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movw	$0, (%eax)
	movl	-32(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -32(%ebp)
L144:
	cmpl	$0, -32(%ebp)
	jne	L145
	movl	$LC66, 4(%esp)
	movl	-88(%ebp), %eax
	movl	%eax, (%esp)
	call	_sauveImage
	movl	-80(%ebp), %eax
	movl	(%eax), %eax
	movl	%eax, -36(%ebp)
	jmp	L146
L147:
	movl	-92(%ebp), %eax
	movl	8(%eax), %edx
	movl	-36(%ebp), %eax
	movl	(%eax), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-36(%ebp), %eax
	movl	4(%eax), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movw	$0, (%eax)
	movl	-92(%ebp), %eax
	movl	12(%eax), %edx
	movl	-36(%ebp), %eax
	movl	(%eax), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-36(%ebp), %eax
	movl	4(%eax), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movw	$0, (%eax)
	movl	-92(%ebp), %eax
	movl	16(%eax), %edx
	movl	-36(%ebp), %eax
	movl	(%eax), %eax
	sall	$2, %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-36(%ebp), %eax
	movl	4(%eax), %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movw	$0, (%eax)
	movl	-36(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -36(%ebp)
L146:
	cmpl	$0, -36(%ebp)
	jne	L147
	movl	$LC68, 4(%esp)
	movl	-92(%ebp), %eax
	movl	%eax, (%esp)
	call	_sauveImage
	jmp	L148
L149:
	movl	-72(%ebp), %eax
	movl	(%eax), %eax
	movl	8(%eax), %eax
	movl	%eax, -96(%ebp)
	movl	-72(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereMaillon
	movl	-72(%ebp), %eax
	movl	-96(%ebp), %edx
	movl	%edx, (%eax)
L148:
	movl	-72(%ebp), %eax
	movl	(%eax), %eax
	testl	%eax, %eax
	jne	L149
	movl	-72(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
	jmp	L150
L151:
	movl	-76(%ebp), %eax
	movl	(%eax), %eax
	movl	8(%eax), %eax
	movl	%eax, -100(%ebp)
	movl	-76(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereMaillon
	movl	-76(%ebp), %eax
	movl	-100(%ebp), %edx
	movl	%edx, (%eax)
L150:
	movl	-76(%ebp), %eax
	movl	(%eax), %eax
	testl	%eax, %eax
	jne	L151
	movl	-76(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
	jmp	L152
L153:
	movl	-80(%ebp), %eax
	movl	(%eax), %eax
	movl	8(%eax), %eax
	movl	%eax, -104(%ebp)
	movl	-80(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereMaillon
	movl	-80(%ebp), %eax
	movl	-104(%ebp), %edx
	movl	%edx, (%eax)
L152:
	movl	-80(%ebp), %eax
	movl	(%eax), %eax
	testl	%eax, %eax
	jne	L153
	movl	-80(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
L109:
	addl	$956, %esp
	popl	%ebx
	.cfi_restore 3
	popl	%esi
	.cfi_restore 6
	popl	%edi
	.cfi_restore 7
	popl	%ebp
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE35:
	.section .rdata,"dr"
	.align 4
LC84:
	.ascii "RESET\12------ LANCEMENT PARTIE 6 ------\0"
	.align 4
LC85:
	.ascii "------ LANCEMENT PARTIE 6 ------\0"
	.align 4
LC86:
	.ascii "./resources/global_assets/description_image.txt\0"
	.align 4
LC87:
	.ascii "\12> lancement fonction compteOccurrenceMotif\12arg1:'%s'\0"
	.align 4
LC88:
	.ascii "\12> lancement fonction creeArbreCodeOcc\12arg1:'%p'\0"
	.align 4
LC89:
	.ascii "\12> lancement fonction creeCodeAdaptatif\12arg1:'%p'\0"
	.align 4
LC90:
	.ascii "./resources/partie6/codeAdaptatif.txt\0"
	.align 4
LC91:
	.ascii "\12> lancement fonction sauveCodeAdaptatif\12arg1:'%s' \12arg2;'%p'\0"
	.align 4
LC92:
	.ascii "\12> lancement fonction litCodeAdaptatif\12arg1:'%s'\0"
	.align 4
LC93:
	.ascii "\12> lancement fonction creeArbreCodeAdaptatif\12arg1:'%p'\0"
	.align 4
LC94:
	.ascii "./resources/partie6/codeAdaptatifCode.txt\0"
	.align 4
LC95:
	.ascii "\12> lancement fonction codeFichier\12arg1:'%s' \12arg2:'%s' \12arg3:'%p'\0"
	.align 4
LC96:
	.ascii "./resources/partie6/codeAdaptatifDecode.txt\0"
	.align 4
LC97:
	.ascii "\12> lancement fonction decodeFichier\12arg1:'%s' \12arg2:'%s' \12arg3:'%p'\0"
	.align 4
LC98:
	.ascii "./resources/partie6/codeAdaptatifCodeBinaire.txt\0"
	.align 4
LC99:
	.ascii "\12> lancement fonction transcritTexteEnBinaire\12arg1:'%s' \12arg2:'%s'\0"
	.align 4
LC100:
	.ascii "./resources/partie6/codeAdaptatifCodeBinaire.bin\0"
	.align 4
LC101:
	.ascii "./resources/partie6/codeAdaptatifCodeBinaireATexte.txt\0"
	.align 4
LC102:
	.ascii "\12> lancement fonction transcritBinaireEnTexte\12arg1:'%s' \12arg2:'%s'\0"
	.align 4
LC103:
	.ascii "./resources/partie6/codeAdaptatifBinaireATexteDecode.txt\0"
	.align 4
LC104:
	.ascii "\12> lancement fonction sauveArbreNB_Texte\12arg1:'%p'\0"
	.text
	.globl	_affiche_partie6_callback
	.def	_affiche_partie6_callback;	.scl	2;	.type	32;	.endef
_affiche_partie6_callback:
LFB36:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	pushl	%ebx
	subl	$596, %esp
	.cfi_offset 3, -12
	jmp	L156
L157:
	movl	_appState, %eax
	movl	64(%eax), %eax
	addl	$4, %eax
	movl	(%eax), %eax
	movl	%eax, (%esp)
	call	_removeGui
L156:
	movl	_appState, %eax
	movl	64(%eax), %eax
	addl	$4, %eax
	movl	(%eax), %eax
	testl	%eax, %eax
	jne	L157
	movl	8(%ebp), %eax
	movl	%eax, -12(%ebp)
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$32, %eax
	movl	(%eax), %eax
	movl	40(%eax), %eax
	movl	(%eax), %eax
	movl	28(%eax), %eax
	testl	%eax, %eax
	je	L158
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$32, %eax
	movl	(%eax), %eax
	movl	40(%eax), %eax
	movl	(%eax), %eax
	movl	28(%eax), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$32, %eax
	movl	(%eax), %eax
	movl	40(%eax), %eax
	movl	(%eax), %ebx
	movl	$LC84, (%esp)
	call	_createString
	movl	%eax, 28(%ebx)
	jmp	L159
L158:
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$32, %eax
	movl	(%eax), %eax
	movl	40(%eax), %eax
	movl	(%eax), %ebx
	movl	$LC85, (%esp)
	call	_createString
	movl	%eax, 28(%ebx)
L159:
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$32, %eax
	movl	(%eax), %eax
	movl	40(%eax), %eax
	movl	(%eax), %eax
	movl	%eax, -16(%ebp)
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$8, %eax
	movl	(%eax), %eax
	movl	$0, (%eax)
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$12, %eax
	movl	(%eax), %eax
	movl	$0, (%eax)
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$16, %eax
	movl	(%eax), %eax
	movl	$0, (%eax)
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$20, %eax
	movl	(%eax), %eax
	movl	$0, (%eax)
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$24, %eax
	movl	(%eax), %eax
	movl	$0, (%eax)
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$28, %eax
	movl	(%eax), %eax
	movl	$0, (%eax)
	movl	-12(%ebp), %eax
	movl	20(%eax), %eax
	movl	(%eax), %eax
	movl	4(%eax), %eax
	addl	$32, %eax
	movl	(%eax), %eax
	movl	$1, (%eax)
	movl	$LC86, 8(%esp)
	movl	$LC87, 4(%esp)
	leal	-548(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-548(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	$LC86, (%esp)
	call	_compteOccurrenceMotif
	movl	%eax, -20(%ebp)
	cmpl	$0, -20(%ebp)
	jne	L160
	movl	$LC7, %eax
	jmp	L161
L160:
	movl	$LC8, %eax
L161:
	movl	%eax, 12(%esp)
	movl	-20(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC9, 4(%esp)
	leal	-548(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-548(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	cmpl	$0, -20(%ebp)
	je	L191
	movl	-20(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC88, 4(%esp)
	leal	-548(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-548(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-20(%ebp), %eax
	movl	%eax, (%esp)
	call	_creeArbreCodeOcc
	movl	%eax, -24(%ebp)
	cmpl	$0, -24(%ebp)
	jne	L164
	movl	$LC7, %eax
	jmp	L165
L164:
	movl	$LC8, %eax
L165:
	movl	%eax, 12(%esp)
	movl	-24(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC9, 4(%esp)
	leal	-548(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-548(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	cmpl	$0, -24(%ebp)
	je	L192
	movl	-24(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC89, 4(%esp)
	leal	-548(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-548(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-24(%ebp), %eax
	movl	%eax, (%esp)
	call	_creeCodeAdaptatif
	movl	%eax, -28(%ebp)
	cmpl	$0, -28(%ebp)
	jne	L167
	movl	$LC7, %eax
	jmp	L168
L167:
	movl	$LC8, %eax
L168:
	movl	%eax, 12(%esp)
	movl	-28(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC9, 4(%esp)
	leal	-548(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-548(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	cmpl	$0, -28(%ebp)
	je	L193
	movl	-28(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	$LC90, 8(%esp)
	movl	$LC91, 4(%esp)
	leal	-548(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-548(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-28(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	$LC90, (%esp)
	call	_sauveCodeAdaptatif
	movl	$0, -552(%ebp)
	movl	$0, -556(%ebp)
	movl	$LC59, 4(%esp)
	movl	$LC90, (%esp)
	call	_fopen
	movl	%eax, -32(%ebp)
	cmpl	$0, -32(%ebp)
	je	L194
	jmp	L171
L172:
	movl	-552(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC60, 4(%esp)
	leal	-548(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-548(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
L171:
	movl	-32(%ebp), %eax
	movl	%eax, 8(%esp)
	leal	-556(%ebp), %eax
	movl	%eax, 4(%esp)
	leal	-552(%ebp), %eax
	movl	%eax, (%esp)
	call	_getline
	movl	%eax, -36(%ebp)
	cmpl	$-1, -36(%ebp)
	jne	L172
	movl	-32(%ebp), %eax
	movl	%eax, (%esp)
	call	_fclose
	movl	-552(%ebp), %eax
	testl	%eax, %eax
	je	L173
	movl	-552(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
L173:
	movl	$LC90, 8(%esp)
	movl	$LC92, 4(%esp)
	leal	-548(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-548(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	$LC90, (%esp)
	call	_litCodeAdaptatif
	movl	%eax, -40(%ebp)
	cmpl	$0, -40(%ebp)
	jne	L174
	movl	$LC7, %eax
	jmp	L175
L174:
	movl	$LC8, %eax
L175:
	movl	%eax, 12(%esp)
	movl	-40(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC9, 4(%esp)
	leal	-548(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-548(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	cmpl	$0, -40(%ebp)
	je	L195
	movl	-40(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC93, 4(%esp)
	leal	-548(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-548(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-40(%ebp), %eax
	movl	%eax, (%esp)
	call	_creeArbreCodeAdaptatif
	movl	%eax, -44(%ebp)
	cmpl	$0, -44(%ebp)
	jne	L177
	movl	$LC7, %eax
	jmp	L178
L177:
	movl	$LC8, %eax
L178:
	movl	%eax, 12(%esp)
	movl	-44(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC9, 4(%esp)
	leal	-548(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-548(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	cmpl	$0, -44(%ebp)
	je	L196
	movl	-28(%ebp), %eax
	movl	%eax, 16(%esp)
	movl	$LC94, 12(%esp)
	movl	$LC86, 8(%esp)
	movl	$LC95, 4(%esp)
	leal	-548(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-548(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-28(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC94, 4(%esp)
	movl	$LC86, (%esp)
	call	_codeFichier
	movl	$0, -36(%ebp)
	movl	$0, -556(%ebp)
	movl	$0, -552(%ebp)
	movl	$LC59, 4(%esp)
	movl	$LC94, (%esp)
	call	_fopen
	movl	%eax, -32(%ebp)
	cmpl	$0, -32(%ebp)
	je	L197
	jmp	L181
L182:
	movl	-552(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC60, 4(%esp)
	leal	-548(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-548(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
L181:
	movl	-32(%ebp), %eax
	movl	%eax, 8(%esp)
	leal	-556(%ebp), %eax
	movl	%eax, 4(%esp)
	leal	-552(%ebp), %eax
	movl	%eax, (%esp)
	call	_getline
	movl	%eax, -36(%ebp)
	cmpl	$-1, -36(%ebp)
	jne	L182
	movl	-32(%ebp), %eax
	movl	%eax, (%esp)
	call	_fclose
	movl	-552(%ebp), %eax
	testl	%eax, %eax
	je	L183
	movl	-552(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
L183:
	movl	-28(%ebp), %eax
	movl	%eax, 16(%esp)
	movl	$LC96, 12(%esp)
	movl	$LC94, 8(%esp)
	movl	$LC97, 4(%esp)
	leal	-548(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-548(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-28(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC96, 4(%esp)
	movl	$LC94, (%esp)
	call	_decodeFichier
	movl	$LC98, 12(%esp)
	movl	$LC94, 8(%esp)
	movl	$LC99, 4(%esp)
	leal	-548(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-548(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	$LC100, 4(%esp)
	movl	$LC94, (%esp)
	call	_transcritTexteEnBinaire
	movl	$LC101, 12(%esp)
	movl	$LC98, 8(%esp)
	movl	$LC102, 4(%esp)
	leal	-548(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-548(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	$LC101, 4(%esp)
	movl	$LC100, (%esp)
	call	_transcritBinaireEnTexte
	movl	-28(%ebp), %eax
	movl	%eax, 16(%esp)
	movl	$LC103, 12(%esp)
	movl	$LC101, 8(%esp)
	movl	$LC97, 4(%esp)
	leal	-548(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-548(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-28(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC103, 4(%esp)
	movl	$LC101, (%esp)
	call	_decodeFichier
	movl	$0, -36(%ebp)
	movl	$0, -556(%ebp)
	movl	$0, -552(%ebp)
	movl	$LC59, 4(%esp)
	movl	$LC103, (%esp)
	call	_fopen
	movl	%eax, -32(%ebp)
	cmpl	$0, -32(%ebp)
	je	L198
	jmp	L185
L186:
	movl	-552(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC60, 4(%esp)
	leal	-548(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-548(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
L185:
	movl	-32(%ebp), %eax
	movl	%eax, 8(%esp)
	leal	-556(%ebp), %eax
	movl	%eax, 4(%esp)
	leal	-552(%ebp), %eax
	movl	%eax, (%esp)
	call	_getline
	movl	%eax, -36(%ebp)
	cmpl	$-1, -36(%ebp)
	jne	L186
	movl	-32(%ebp), %eax
	movl	%eax, (%esp)
	call	_fclose
	movl	-552(%ebp), %eax
	testl	%eax, %eax
	je	L187
	movl	-552(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
L187:
	movl	$LC38, (%esp)
	call	_chargeImage
	movl	%eax, -560(%ebp)
	movl	-560(%ebp), %eax
	testl	%eax, %eax
	je	L199
	movl	-560(%ebp), %eax
	movl	20(%eax), %edx
	movl	-560(%ebp), %eax
	movl	(%eax), %eax
	movl	%edx, 12(%esp)
	movl	%eax, 8(%esp)
	movl	$0, 4(%esp)
	movl	$0, (%esp)
	call	_creeArbreNB
	movl	%eax, -48(%ebp)
	cmpl	$0, -48(%ebp)
	je	L200
	movl	-48(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$LC104, 4(%esp)
	leal	-548(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-548(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	call	_update
	movl	-48(%ebp), %eax
	movl	%eax, (%esp)
	call	_sauveArbreNB_Texte
	movl	-24(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereArbreCodeAdaptatif
	movl	-44(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereArbreCodeAdaptatif
	movl	-48(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereArbreBinaire
	leal	-560(%ebp), %eax
	movl	%eax, (%esp)
	call	_libereImage
	movl	-20(%ebp), %eax
	movl	4(%eax), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-20(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-28(%ebp), %eax
	movl	4(%eax), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-28(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-40(%ebp), %eax
	movl	4(%eax), %eax
	movl	%eax, (%esp)
	call	_free
	movl	-40(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
	movl	$LC49, 4(%esp)
	leal	-548(%ebp), %eax
	movl	%eax, (%esp)
	call	_sprintf
	leal	-548(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_addText
	jmp	L155
L191:
	nop
	jmp	L155
L192:
	nop
	jmp	L155
L193:
	nop
	jmp	L155
L194:
	nop
	jmp	L155
L195:
	nop
	jmp	L155
L196:
	nop
	jmp	L155
L197:
	nop
	jmp	L155
L198:
	nop
	jmp	L155
L199:
	nop
	jmp	L155
L200:
	nop
L155:
	addl	$596, %esp
	popl	%ebx
	.cfi_restore 3
	popl	%ebp
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE36:
	.globl	_initButton
	.def	_initButton;	.scl	2;	.type	32;	.endef
_initButton:
LFB37:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$40, %esp
	movl	$56, (%esp)
	call	_malloc
	movl	%eax, -12(%ebp)
	movl	-12(%ebp), %eax
	movl	52(%ebp), %edx
	movl	%edx, (%eax)
	movl	-12(%ebp), %eax
	movl	$0, 4(%eax)
	movl	-12(%ebp), %eax
	movl	8(%ebp), %edx
	movl	%edx, 8(%eax)
	movl	-12(%ebp), %eax
	movl	12(%ebp), %edx
	movl	%edx, 12(%eax)
	movl	-12(%ebp), %eax
	movl	28(%ebp), %edx
	movl	%edx, 28(%eax)
	movl	-12(%ebp), %eax
	movl	40(%ebp), %edx
	movl	%edx, 48(%eax)
	movl	-12(%ebp), %eax
	movl	32(%ebp), %edx
	movl	%edx, 32(%eax)
	movl	-12(%ebp), %eax
	movl	36(%ebp), %edx
	movl	%edx, 36(%eax)
	movl	-12(%ebp), %eax
	movl	16(%ebp), %edx
	movl	%edx, 16(%eax)
	movl	-12(%ebp), %eax
	movl	20(%ebp), %edx
	movl	%edx, 20(%eax)
	movl	-12(%ebp), %eax
	movl	24(%ebp), %edx
	movl	%edx, 24(%eax)
	movl	-12(%ebp), %eax
	movl	44(%ebp), %edx
	movl	%edx, 40(%eax)
	movl	-12(%ebp), %eax
	movl	48(%ebp), %edx
	movl	%edx, 44(%eax)
	movl	-12(%ebp), %eax
	movl	56(%ebp), %edx
	movl	%edx, 52(%eax)
	movl	-12(%ebp), %eax
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE37:
	.globl	_initButtons
	.def	_initButtons;	.scl	2;	.type	32;	.endef
_initButtons:
LFB38:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$40, %esp
	leal	12(%ebp), %eax
	movl	%eax, -20(%ebp)
	movl	8(%ebp), %eax
	addl	$1, %eax
	sall	$2, %eax
	movl	%eax, (%esp)
	call	_malloc
	movl	%eax, -16(%ebp)
	movl	$0, -12(%ebp)
	jmp	L204
L205:
	movl	-12(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	-16(%ebp), %eax
	leal	(%edx,%eax), %ecx
	movl	-20(%ebp), %eax
	leal	4(%eax), %edx
	movl	%edx, -20(%ebp)
	movl	(%eax), %eax
	movl	%eax, (%ecx)
	addl	$1, -12(%ebp)
L204:
	movl	-12(%ebp), %eax
	cmpl	8(%ebp), %eax
	jl	L205
	movl	8(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	-16(%ebp), %eax
	addl	%edx, %eax
	movl	$0, (%eax)
	movl	-16(%ebp), %eax
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE38:
	.globl	_isOverButton
	.def	_isOverButton;	.scl	2;	.type	32;	.endef
_isOverButton:
LFB39:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	movl	_appState, %eax
	movl	28(%eax), %edx
	movl	12(%ebp), %eax
	movl	4(%eax), %ecx
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	addl	%ecx, %eax
	cmpl	%eax, %edx
	jl	L208
	movl	_appState, %eax
	movl	28(%eax), %edx
	movl	12(%ebp), %eax
	movl	4(%eax), %ecx
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	addl	%eax, %ecx
	movl	8(%ebp), %eax
	movl	16(%eax), %eax
	addl	%ecx, %eax
	cmpl	%eax, %edx
	jg	L208
	movl	_appState, %eax
	movl	32(%eax), %edx
	movl	12(%ebp), %eax
	movl	8(%eax), %ecx
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	addl	%ecx, %eax
	cmpl	%eax, %edx
	jl	L208
	movl	_appState, %eax
	movl	32(%eax), %edx
	movl	12(%ebp), %eax
	movl	8(%eax), %ecx
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	addl	%eax, %ecx
	movl	8(%ebp), %eax
	movl	20(%eax), %eax
	addl	%ecx, %eax
	cmpl	%eax, %edx
	jg	L208
	movl	$1, %eax
	jmp	L209
L208:
	movl	$0, %eax
L209:
	popl	%ebp
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE39:
	.globl	_freeButton
	.def	_freeButton;	.scl	2;	.type	32;	.endef
_freeButton:
LFB40:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$24, %esp
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE40:
	.globl	_freeButtons
	.def	_freeButtons;	.scl	2;	.type	32;	.endef
_freeButtons:
LFB41:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$40, %esp
	movl	$0, -12(%ebp)
	jmp	L212
L213:
	movl	-12(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	8(%ebp), %eax
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	%eax, (%esp)
	call	_freeButton
	addl	$1, -12(%ebp)
L212:
	movl	-12(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	8(%ebp), %eax
	addl	%edx, %eax
	movl	(%eax), %eax
	testl	%eax, %eax
	jne	L213
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE41:
	.data
	.align 4
_point1P4.7186:
	.long	67
	.long	15
	.align 4
_point2P4.7188:
	.long	19
	.long	68
	.align 4
_point1P5_simple.7217:
	.long	95
	.long	70
	.align 4
_point2P5_simple.7219:
	.long	501
	.long	503
	.align 4
_point1P5_complexe.7248:
	.long	95
	.long	70
	.align 4
_point2P5_complexe.7250:
	.long	501
	.long	503
	.ident	"GCC: (MinGW.org GCC-8.2.0-5) 8.2.0"
	.def	_SDL_Log;	.scl	2;	.type	32;	.endef
	.def	_SDL_ShowSimpleMessageBox;	.scl	2;	.type	32;	.endef
	.def	_removeGui;	.scl	2;	.type	32;	.endef
	.def	_free;	.scl	2;	.type	32;	.endef
	.def	_createString;	.scl	2;	.type	32;	.endef
	.def	_sprintf;	.scl	2;	.type	32;	.endef
	.def	_addText;	.scl	2;	.type	32;	.endef
	.def	_update;	.scl	2;	.type	32;	.endef
	.def	_lisBMPRGB;	.scl	2;	.type	32;	.endef
	.def	_ecrisBMPRGB_Dans;	.scl	2;	.type	32;	.endef
	.def	_chargeImage;	.scl	2;	.type	32;	.endef
	.def	_initDisplaySurface;	.scl	2;	.type	32;	.endef
	.def	_initDisplaySurfaces;	.scl	2;	.type	32;	.endef
	.def	_initFrame;	.scl	2;	.type	32;	.endef
	.def	_initFrames;	.scl	2;	.type	32;	.endef
	.def	_initView;	.scl	2;	.type	32;	.endef
	.def	_initViews;	.scl	2;	.type	32;	.endef
	.def	_addGui;	.scl	2;	.type	32;	.endef
	.def	_libereDonneesImageRGB;	.scl	2;	.type	32;	.endef
	.def	_SDL_FlushEvent;	.scl	2;	.type	32;	.endef
	.def	_dupliqueImage;	.scl	2;	.type	32;	.endef
	.def	_sauveImage;	.scl	2;	.type	32;	.endef
	.def	_sauveImageNG;	.scl	2;	.type	32;	.endef
	.def	_differenceImage;	.scl	2;	.type	32;	.endef
	.def	_libereImage;	.scl	2;	.type	32;	.endef
	.def	_creeArbreNB;	.scl	2;	.type	32;	.endef
	.def	_compteFeuille;	.scl	2;	.type	32;	.endef
	.def	_alloueImage;	.scl	2;	.type	32;	.endef
	.def	_creeImageArbreNB;	.scl	2;	.type	32;	.endef
	.def	_libereArbreBinaire;	.scl	2;	.type	32;	.endef
	.def	_creePalette;	.scl	2;	.type	32;	.endef
	.def	_creeArbreMotifs;	.scl	2;	.type	32;	.endef
	.def	_sauveDescriptionImage;	.scl	2;	.type	32;	.endef
	.def	_fopen;	.scl	2;	.type	32;	.endef
	.def	_getline;	.scl	2;	.type	32;	.endef
	.def	_fclose;	.scl	2;	.type	32;	.endef
	.def	_libereArbreMotif;	.scl	2;	.type	32;	.endef
	.def	_dessineCarte;	.scl	2;	.type	32;	.endef
	.def	_calculeTrajectoire;	.scl	2;	.type	32;	.endef
	.def	_sauveDescriptionChemin;	.scl	2;	.type	32;	.endef
	.def	_libereMaillon;	.scl	2;	.type	32;	.endef
	.def	_compteOccurrenceMotif;	.scl	2;	.type	32;	.endef
	.def	_creeArbreCodeOcc;	.scl	2;	.type	32;	.endef
	.def	_creeCodeAdaptatif;	.scl	2;	.type	32;	.endef
	.def	_sauveCodeAdaptatif;	.scl	2;	.type	32;	.endef
	.def	_litCodeAdaptatif;	.scl	2;	.type	32;	.endef
	.def	_creeArbreCodeAdaptatif;	.scl	2;	.type	32;	.endef
	.def	_codeFichier;	.scl	2;	.type	32;	.endef
	.def	_decodeFichier;	.scl	2;	.type	32;	.endef
	.def	_transcritTexteEnBinaire;	.scl	2;	.type	32;	.endef
	.def	_transcritBinaireEnTexte;	.scl	2;	.type	32;	.endef
	.def	_sauveArbreNB_Texte;	.scl	2;	.type	32;	.endef
	.def	_libereArbreCodeAdaptatif;	.scl	2;	.type	32;	.endef
	.def	_malloc;	.scl	2;	.type	32;	.endef
