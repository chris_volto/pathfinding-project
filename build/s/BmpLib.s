	.file	"BmpLib.c"
	.text
	.globl	_libereDonneesImageRGB
	.def	_libereDonneesImageRGB;	.scl	2;	.type	32;	.endef
_libereDonneesImageRGB:
LFB20:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$24, %esp
	cmpl	$0, 8(%ebp)
	je	L4
	movl	8(%ebp), %eax
	movl	(%eax), %eax
	testl	%eax, %eax
	je	L3
	movl	8(%ebp), %eax
	movl	(%eax), %eax
	movl	8(%eax), %eax
	movl	%eax, (%esp)
	call	_free
	movl	8(%ebp), %eax
	movl	(%eax), %eax
	movl	%eax, (%esp)
	call	_free
L3:
	movl	8(%ebp), %eax
	movl	$0, (%eax)
L4:
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE20:
	.def	_tailleScanLineRGB;	.scl	3;	.type	32;	.endef
_tailleScanLineRGB:
LFB21:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	movl	8(%ebp), %eax
	leal	1(%eax), %edx
	movl	%edx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	andl	$-4, %eax
	popl	%ebp
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE21:
	.section .rdata,"dr"
LC0:
	.ascii "rb\0"
	.text
	.globl	_lisBMPRGB
	.def	_lisBMPRGB;	.scl	2;	.type	32;	.endef
_lisBMPRGB:
LFB22:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$120, %esp
	movl	$12, 4(%esp)
	movl	$1, (%esp)
	call	_calloc
	movl	%eax, -12(%ebp)
	movb	$0, -13(%ebp)
	cmpl	$0, -12(%ebp)
	je	L8
	movl	$LC0, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_fopen
	movl	%eax, -28(%ebp)
	cmpl	$0, -28(%ebp)
	je	L9
	movl	-28(%ebp), %eax
	movl	%eax, (%esp)
	call	_fgetc
	cmpl	$66, %eax
	jne	L10
	movl	-28(%ebp), %eax
	movl	%eax, (%esp)
	call	_fgetc
	cmpl	$77, %eax
	jne	L10
	movl	-28(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	$1, 8(%esp)
	movl	$12, 4(%esp)
	leal	-52(%ebp), %eax
	movl	%eax, (%esp)
	call	_fread
	cmpl	$1, %eax
	jne	L10
	movl	-44(%ebp), %eax
	movl	%eax, (%esp)
	call	_little32VersNatif
	movl	%eax, -32(%ebp)
	movl	-28(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	$1, 8(%esp)
	movl	$40, 4(%esp)
	leal	-92(%ebp), %eax
	movl	%eax, (%esp)
	call	_fread
	cmpl	$1, %eax
	jne	L10
	movl	-80(%ebp), %eax
	movl	%eax, (%esp)
	call	_little32VersNatif
	cmpl	$1572865, %eax
	jne	L10
	movl	-76(%ebp), %eax
	movl	%eax, (%esp)
	call	_little32VersNatif
	testl	%eax, %eax
	jne	L10
	movb	$0, -14(%ebp)
	movl	-88(%ebp), %eax
	movl	%eax, (%esp)
	call	_little32VersNatif
	movl	%eax, %edx
	movl	-12(%ebp), %eax
	movl	%edx, (%eax)
	movl	-84(%ebp), %eax
	movl	%eax, (%esp)
	call	_little32VersNatif
	movl	%eax, %edx
	movl	-12(%ebp), %eax
	movl	%edx, 4(%eax)
	movl	-12(%ebp), %eax
	movl	4(%eax), %eax
	testl	%eax, %eax
	jns	L13
	movl	-12(%ebp), %eax
	movl	4(%eax), %eax
	negl	%eax
	movl	%eax, %edx
	movl	-12(%ebp), %eax
	movl	%edx, 4(%eax)
	movb	$1, -14(%ebp)
L13:
	movl	-12(%ebp), %eax
	movl	(%eax), %eax
	movl	%eax, %edx
	movl	-12(%ebp), %eax
	movl	4(%eax), %eax
	imull	%eax, %edx
	movl	%edx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	%eax, (%esp)
	call	_malloc
	movl	%eax, %edx
	movl	-12(%ebp), %eax
	movl	%edx, 8(%eax)
	movl	-12(%ebp), %eax
	movl	8(%eax), %eax
	testl	%eax, %eax
	je	L10
	movl	-12(%ebp), %eax
	movl	(%eax), %eax
	movl	%eax, (%esp)
	call	_tailleScanLineRGB
	movl	%eax, -36(%ebp)
	movl	-36(%ebp), %eax
	movl	%eax, (%esp)
	call	_malloc
	movl	%eax, -40(%ebp)
	cmpl	$0, -40(%ebp)
	je	L14
	movl	$0, 8(%esp)
	movl	-32(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-28(%ebp), %eax
	movl	%eax, (%esp)
	call	_fseek
	testl	%eax, %eax
	jne	L15
	movl	-12(%ebp), %eax
	movl	8(%eax), %ecx
	cmpb	$0, -14(%ebp)
	je	L16
	movl	-12(%ebp), %eax
	movl	(%eax), %edx
	movl	-12(%ebp), %eax
	movl	4(%eax), %eax
	subl	$1, %eax
	imull	%eax, %edx
	movl	%edx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	jmp	L17
L16:
	movl	$0, %eax
L17:
	addl	%ecx, %eax
	movl	%eax, -24(%ebp)
	movb	$1, -13(%ebp)
	movl	$0, -20(%ebp)
	jmp	L18
L23:
	movl	-36(%ebp), %eax
	movl	-28(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	$1, 8(%esp)
	movl	%eax, 4(%esp)
	movl	-40(%ebp), %eax
	movl	%eax, (%esp)
	call	_fread
	cmpl	$1, %eax
	jne	L19
	movl	-12(%ebp), %eax
	movl	(%eax), %edx
	movl	%edx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	%eax, 8(%esp)
	movl	-40(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-24(%ebp), %eax
	movl	%eax, (%esp)
	call	_memcpy
	cmpb	$0, -14(%ebp)
	je	L20
	movl	-12(%ebp), %eax
	movl	(%eax), %edx
	movl	%edx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	negl	%eax
	addl	%eax, -24(%ebp)
	jmp	L22
L20:
	movl	-12(%ebp), %eax
	movl	(%eax), %edx
	movl	%edx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	addl	%eax, -24(%ebp)
	jmp	L22
L19:
	movb	$0, -13(%ebp)
L22:
	addl	$1, -20(%ebp)
L18:
	movl	-12(%ebp), %eax
	movl	4(%eax), %eax
	cmpl	%eax, -20(%ebp)
	jge	L15
	cmpb	$0, -13(%ebp)
	jne	L23
L15:
	movl	-40(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
L14:
	movzbl	-13(%ebp), %eax
	xorl	$1, %eax
	testb	%al, %al
	je	L10
	movl	-12(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, (%esp)
	call	_free
L10:
	movl	-28(%ebp), %eax
	movl	%eax, (%esp)
	call	_fclose
L9:
	movzbl	-13(%ebp), %eax
	xorl	$1, %eax
	testb	%al, %al
	je	L8
	movl	-12(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
	movl	$0, -12(%ebp)
L8:
	movl	-12(%ebp), %eax
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE22:
	.section .rdata,"dr"
LC1:
	.ascii "wb\0"
LC2:
	.ascii "BM\0"
	.text
	.globl	_ecrisBMPRGB_Dans
	.def	_ecrisBMPRGB_Dans;	.scl	2;	.type	32;	.endef
_ecrisBMPRGB_Dans:
LFB23:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$104, %esp
	movb	$0, -9(%ebp)
	movl	$LC1, 4(%esp)
	movl	12(%ebp), %eax
	movl	%eax, (%esp)
	call	_fopen
	movl	%eax, -24(%ebp)
	cmpl	$0, -24(%ebp)
	je	L26
	movl	-24(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	$1, 8(%esp)
	movl	$2, 4(%esp)
	movl	$LC2, (%esp)
	call	_fwrite
	cmpl	$1, %eax
	jne	L27
	movl	8(%ebp), %eax
	movl	(%eax), %eax
	movl	%eax, (%esp)
	call	_tailleScanLineRGB
	movl	%eax, -28(%ebp)
	movl	$56, 8(%esp)
	movl	$0, 4(%esp)
	leal	-88(%ebp), %eax
	movl	%eax, (%esp)
	call	_memset
	movl	8(%ebp), %eax
	movl	4(%eax), %eax
	imull	-28(%ebp), %eax
	addl	$58, %eax
	movl	%eax, (%esp)
	call	_natif32VersLittle
	movl	%eax, -88(%ebp)
	movl	$0, -84(%ebp)
	movl	$58, (%esp)
	call	_natif32VersLittle
	movl	%eax, -80(%ebp)
	movl	$40, (%esp)
	call	_natif32VersLittle
	movl	%eax, -76(%ebp)
	movl	8(%ebp), %eax
	movl	(%eax), %eax
	movl	%eax, (%esp)
	call	_natif32VersLittle
	movl	%eax, -72(%ebp)
	movl	8(%ebp), %eax
	movl	4(%eax), %eax
	movl	%eax, (%esp)
	call	_natif32VersLittle
	movl	%eax, -68(%ebp)
	movl	$1572865, (%esp)
	call	_natif32VersLittle
	movl	%eax, -64(%ebp)
	movl	$0, -60(%ebp)
	movl	$0, -56(%ebp)
	movl	$0, -52(%ebp)
	movl	$0, -48(%ebp)
	movl	$0, -44(%ebp)
	movl	$0, -40(%ebp)
	movl	$0, -36(%ebp)
	movl	-24(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	$1, 8(%esp)
	movl	$56, 4(%esp)
	leal	-88(%ebp), %eax
	movl	%eax, (%esp)
	call	_fwrite
	cmpl	$1, %eax
	jne	L27
	movl	-28(%ebp), %eax
	movl	%eax, (%esp)
	call	_malloc
	movl	%eax, -32(%ebp)
	cmpl	$0, -32(%ebp)
	je	L27
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -20(%ebp)
	movb	$1, -9(%ebp)
	movl	$0, -16(%ebp)
	jmp	L29
L32:
	movl	8(%ebp), %eax
	movl	(%eax), %edx
	movl	%edx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	%eax, 8(%esp)
	movl	-20(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	-32(%ebp), %eax
	movl	%eax, (%esp)
	call	_memcpy
	movl	8(%ebp), %eax
	movl	(%eax), %edx
	movl	%edx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	addl	%eax, -20(%ebp)
	movl	-28(%ebp), %eax
	movl	-24(%ebp), %edx
	movl	%edx, 12(%esp)
	movl	$1, 8(%esp)
	movl	%eax, 4(%esp)
	movl	-32(%ebp), %eax
	movl	%eax, (%esp)
	call	_fwrite
	cmpl	$1, %eax
	je	L30
	movb	$0, -9(%ebp)
L30:
	addl	$1, -16(%ebp)
L29:
	movl	8(%ebp), %eax
	movl	4(%eax), %eax
	cmpl	%eax, -16(%ebp)
	jge	L31
	cmpb	$0, -9(%ebp)
	jne	L32
L31:
	movl	-32(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
L27:
	movl	-24(%ebp), %eax
	movl	%eax, (%esp)
	call	_fclose
L26:
	movzbl	-9(%ebp), %eax
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE23:
	.ident	"GCC: (MinGW.org GCC-8.2.0-5) 8.2.0"
	.def	_free;	.scl	2;	.type	32;	.endef
	.def	_calloc;	.scl	2;	.type	32;	.endef
	.def	_fopen;	.scl	2;	.type	32;	.endef
	.def	_fgetc;	.scl	2;	.type	32;	.endef
	.def	_fread;	.scl	2;	.type	32;	.endef
	.def	_little32VersNatif;	.scl	2;	.type	32;	.endef
	.def	_malloc;	.scl	2;	.type	32;	.endef
	.def	_fseek;	.scl	2;	.type	32;	.endef
	.def	_memcpy;	.scl	2;	.type	32;	.endef
	.def	_fclose;	.scl	2;	.type	32;	.endef
	.def	_fwrite;	.scl	2;	.type	32;	.endef
	.def	_memset;	.scl	2;	.type	32;	.endef
	.def	_natif32VersLittle;	.scl	2;	.type	32;	.endef
