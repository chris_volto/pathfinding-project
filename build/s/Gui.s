	.file	"Gui.c"
	.text
	.globl	_freeGui
	.def	_freeGui;	.scl	2;	.type	32;	.endef
_freeGui:
LFB13:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$24, %esp
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	testl	%eax, %eax
	je	L2
	movl	8(%ebp), %eax
	movl	12(%eax), %eax
	movl	%eax, (%esp)
	call	_SDL_DestroyWindow
L2:
	movl	8(%ebp), %eax
	movl	16(%eax), %eax
	testl	%eax, %eax
	je	L3
	movl	8(%ebp), %eax
	movl	16(%eax), %eax
	movl	%eax, (%esp)
	call	_SDL_DestroyRenderer
L3:
	movl	8(%ebp), %eax
	movl	20(%eax), %eax
	testl	%eax, %eax
	je	L4
	movl	8(%ebp), %eax
	movl	20(%eax), %eax
	movl	%eax, (%esp)
	call	_freeViews
L4:
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE13:
	.globl	_freeGuis
	.def	_freeGuis;	.scl	2;	.type	32;	.endef
_freeGuis:
LFB14:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$40, %esp
	movl	$0, -12(%ebp)
	jmp	L6
L7:
	movl	-12(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	8(%ebp), %eax
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	%eax, (%esp)
	call	_freeGui
	addl	$1, -12(%ebp)
L6:
	movl	-12(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	8(%ebp), %eax
	addl	%edx, %eax
	movl	(%eax), %eax
	testl	%eax, %eax
	jne	L7
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_free
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE14:
	.globl	_initGui
	.def	_initGui;	.scl	2;	.type	32;	.endef
_initGui:
LFB15:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	pushl	%edi
	pushl	%esi
	pushl	%ebx
	subl	$60, %esp
	.cfi_offset 7, -12
	.cfi_offset 6, -16
	.cfi_offset 3, -20
	movl	$24, (%esp)
	call	_malloc
	movl	%eax, -28(%ebp)
	cmpl	$0, -28(%ebp)
	jne	L9
	movl	$0, %eax
	jmp	L10
L9:
	movl	28(%ebp), %edi
	movl	24(%ebp), %esi
	movl	20(%ebp), %ebx
	movl	16(%ebp), %ecx
	movl	12(%ebp), %edx
	movl	8(%ebp), %eax
	movl	%edi, 20(%esp)
	movl	%esi, 16(%esp)
	movl	%ebx, 12(%esp)
	movl	%ecx, 8(%esp)
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_SDL_CreateWindow
	movl	%eax, %edx
	movl	-28(%ebp), %eax
	movl	%edx, 12(%eax)
	movl	-28(%ebp), %eax
	movl	12(%eax), %eax
	testl	%eax, %eax
	je	L11
	movl	32(%ebp), %edx
	movl	-28(%ebp), %eax
	movl	12(%eax), %eax
	movl	%edx, 8(%esp)
	movl	$-1, 4(%esp)
	movl	%eax, (%esp)
	call	_SDL_CreateRenderer
	movl	%eax, %edx
	movl	-28(%ebp), %eax
	movl	%edx, 16(%eax)
	movl	-28(%ebp), %eax
	movl	16(%eax), %eax
	testl	%eax, %eax
	je	L11
	movl	-28(%ebp), %eax
	movl	16(%eax), %eax
	movl	$0, 16(%esp)
	movl	$0, 12(%esp)
	movl	$0, 8(%esp)
	movl	$0, 4(%esp)
	movl	%eax, (%esp)
	call	_SDL_SetRenderDrawColor
	testl	%eax, %eax
	jne	L11
	movl	-28(%ebp), %eax
	movl	16(%eax), %eax
	movl	%eax, (%esp)
	call	_SDL_RenderClear
	testl	%eax, %eax
	je	L12
L11:
	movl	-28(%ebp), %eax
	movl	%eax, (%esp)
	call	_freeGui
	movl	$0, %eax
	jmp	L10
L12:
	movl	36(%ebp), %edx
	movl	-28(%ebp), %eax
	movl	%edx, 20(%eax)
	movl	-28(%ebp), %eax
	movl	$1, (%eax)
	movl	-28(%ebp), %eax
L10:
	addl	$60, %esp
	popl	%ebx
	.cfi_restore 3
	popl	%esi
	.cfi_restore 6
	popl	%edi
	.cfi_restore 7
	popl	%ebp
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE15:
	.globl	_addGui
	.def	_addGui;	.scl	2;	.type	32;	.endef
_addGui:
LFB16:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	pushl	%esi
	pushl	%ebx
	subl	$64, %esp
	.cfi_offset 6, -12
	.cfi_offset 3, -16
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	movl	12(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	16(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	20(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	24(%ebp), %eax
	movl	%eax, 16(%esp)
	movl	28(%ebp), %eax
	movl	%eax, 20(%esp)
	movl	32(%ebp), %eax
	movl	%eax, 24(%esp)
	movl	36(%ebp), %eax
	movl	%eax, 28(%esp)
	call	_initGui
	movl	%eax, -24(%ebp)
	cmpl	$0, -24(%ebp)
	jne	L14
	movl	$-1, %eax
	jmp	L15
L14:
	movl	$0, -20(%ebp)
	jmp	L16
L17:
	addl	$1, -20(%ebp)
L16:
	movl	_appState, %eax
	movl	64(%eax), %eax
	movl	-20(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	testl	%eax, %eax
	jne	L17
	movl	-20(%ebp), %eax
	movl	%esp, %edx
	movl	%edx, %esi
	leal	-1(%eax), %edx
	movl	%edx, -28(%ebp)
	leal	0(,%eax,4), %edx
	movl	$16, %eax
	subl	$1, %eax
	addl	%edx, %eax
	movl	$16, %ebx
	movl	$0, %edx
	divl	%ebx
	imull	$16, %eax, %eax
	call	___chkstk_ms
	subl	%eax, %esp
	leal	32(%esp), %eax
	addl	$3, %eax
	shrl	$2, %eax
	sall	$2, %eax
	movl	%eax, -32(%ebp)
	movl	$0, -16(%ebp)
	jmp	L18
L19:
	movl	_appState, %eax
	movl	64(%eax), %eax
	movl	-16(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %ecx
	movl	-32(%ebp), %eax
	movl	-16(%ebp), %edx
	movl	%ecx, (%eax,%edx,4)
	addl	$1, -16(%ebp)
L18:
	movl	-16(%ebp), %eax
	cmpl	-20(%ebp), %eax
	jl	L19
	movl	-20(%ebp), %eax
	addl	$2, %eax
	leal	0(,%eax,4), %edx
	movl	_appState, %eax
	movl	64(%eax), %eax
	movl	_appState, %ebx
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_realloc
	movl	%eax, 64(%ebx)
	movl	_appState, %eax
	movl	64(%eax), %eax
	testl	%eax, %eax
	jne	L20
	movl	$-1, %eax
	jmp	L21
L20:
	movl	$0, -12(%ebp)
	jmp	L22
L23:
	movl	_appState, %eax
	movl	64(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	leal	(%eax,%edx), %ecx
	movl	-32(%ebp), %eax
	movl	-12(%ebp), %edx
	movl	(%eax,%edx,4), %eax
	movl	%eax, (%ecx)
	addl	$1, -12(%ebp)
L22:
	movl	-12(%ebp), %eax
	cmpl	-20(%ebp), %eax
	jl	L23
	movl	_appState, %eax
	movl	64(%eax), %eax
	movl	-20(%ebp), %edx
	sall	$2, %edx
	addl	%eax, %edx
	movl	-24(%ebp), %eax
	movl	%eax, (%edx)
	movl	_appState, %eax
	movl	64(%eax), %eax
	movl	-20(%ebp), %edx
	addl	$1, %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	$0, (%eax)
	movl	$0, %eax
L21:
	movl	%esi, %esp
L15:
	leal	-8(%ebp), %esp
	popl	%ebx
	.cfi_restore 3
	popl	%esi
	.cfi_restore 6
	popl	%ebp
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE16:
	.globl	_removeGui
	.def	_removeGui;	.scl	2;	.type	32;	.endef
_removeGui:
LFB17:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	pushl	%esi
	pushl	%ebx
	subl	$48, %esp
	.cfi_offset 6, -12
	.cfi_offset 3, -16
	movl	$-1, -24(%ebp)
	movl	$0, -20(%ebp)
	jmp	L25
L27:
	movl	_appState, %eax
	movl	64(%eax), %eax
	movl	-20(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	cmpl	%eax, 8(%ebp)
	jne	L26
	movl	-20(%ebp), %eax
	movl	%eax, -24(%ebp)
L26:
	addl	$1, -20(%ebp)
L25:
	movl	_appState, %eax
	movl	64(%eax), %eax
	movl	-20(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	testl	%eax, %eax
	jne	L27
	cmpl	$-1, -24(%ebp)
	jne	L28
	movl	$-1, %eax
	jmp	L29
L28:
	movl	_appState, %eax
	movl	64(%eax), %eax
	movl	-24(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	%eax, (%esp)
	call	_freeGui
	movl	-20(%ebp), %eax
	subl	$1, %eax
	movl	%esp, %edx
	movl	%edx, %esi
	leal	-1(%eax), %edx
	movl	%edx, -28(%ebp)
	leal	0(,%eax,4), %edx
	movl	$16, %eax
	subl	$1, %eax
	addl	%edx, %eax
	movl	$16, %ebx
	movl	$0, %edx
	divl	%ebx
	imull	$16, %eax, %eax
	call	___chkstk_ms
	subl	%eax, %esp
	leal	8(%esp), %eax
	addl	$3, %eax
	shrl	$2, %eax
	sall	$2, %eax
	movl	%eax, -32(%ebp)
	movl	$0, -16(%ebp)
	jmp	L30
L33:
	movl	-16(%ebp), %eax
	cmpl	-24(%ebp), %eax
	jge	L31
	movl	_appState, %eax
	movl	64(%eax), %eax
	movl	-16(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	(%eax), %ecx
	movl	-32(%ebp), %eax
	movl	-16(%ebp), %edx
	movl	%ecx, (%eax,%edx,4)
	jmp	L32
L31:
	movl	-16(%ebp), %eax
	cmpl	-24(%ebp), %eax
	jle	L32
	movl	_appState, %eax
	movl	64(%eax), %eax
	movl	-16(%ebp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	-16(%ebp), %edx
	leal	-1(%edx), %ecx
	movl	(%eax), %edx
	movl	-32(%ebp), %eax
	movl	%edx, (%eax,%ecx,4)
L32:
	addl	$1, -16(%ebp)
L30:
	movl	-16(%ebp), %eax
	cmpl	-20(%ebp), %eax
	jl	L33
	movl	-20(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	_appState, %eax
	movl	64(%eax), %eax
	movl	_appState, %ebx
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	_realloc
	movl	%eax, 64(%ebx)
	movl	_appState, %eax
	movl	64(%eax), %eax
	testl	%eax, %eax
	jne	L34
	movl	$-1, %eax
	jmp	L35
L34:
	movl	$0, -12(%ebp)
	jmp	L36
L37:
	movl	_appState, %eax
	movl	64(%eax), %eax
	movl	-12(%ebp), %edx
	sall	$2, %edx
	leal	(%eax,%edx), %ecx
	movl	-32(%ebp), %eax
	movl	-12(%ebp), %edx
	movl	(%eax,%edx,4), %eax
	movl	%eax, (%ecx)
	addl	$1, -12(%ebp)
L36:
	movl	-20(%ebp), %eax
	subl	$1, %eax
	cmpl	%eax, -12(%ebp)
	jl	L37
	movl	_appState, %eax
	movl	64(%eax), %eax
	movl	-20(%ebp), %edx
	addl	$1073741823, %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	$0, (%eax)
	movl	$0, %eax
L35:
	movl	%esi, %esp
L29:
	leal	-8(%ebp), %esp
	popl	%ebx
	.cfi_restore 3
	popl	%esi
	.cfi_restore 6
	popl	%ebp
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE17:
	.ident	"GCC: (MinGW.org GCC-8.2.0-5) 8.2.0"
	.def	_SDL_DestroyWindow;	.scl	2;	.type	32;	.endef
	.def	_SDL_DestroyRenderer;	.scl	2;	.type	32;	.endef
	.def	_freeViews;	.scl	2;	.type	32;	.endef
	.def	_free;	.scl	2;	.type	32;	.endef
	.def	_malloc;	.scl	2;	.type	32;	.endef
	.def	_SDL_CreateWindow;	.scl	2;	.type	32;	.endef
	.def	_SDL_CreateRenderer;	.scl	2;	.type	32;	.endef
	.def	_SDL_SetRenderDrawColor;	.scl	2;	.type	32;	.endef
	.def	_SDL_RenderClear;	.scl	2;	.type	32;	.endef
	.def	_realloc;	.scl	2;	.type	32;	.endef
