# 1 "./src/lib_graphic/src/OutilsLib.c"
# 1 "<built-in>"
# 1 "<command-line>"
# 1 "./src/lib_graphic/src/OutilsLib.c"
# 1 "./src/lib_graphic/src/../include/OutilsLib.h" 1






int little32VersNatif(int little);


int natif32VersLittle(int natif);


short little16VersNatif(short little);


short natif16VersLittle(short natif);


int big32VersNatif(int big);


int natif32VersBig(int natif);


short big16VersNatif(short big);


short natif16VersBig(short natif);
# 2 "./src/lib_graphic/src/OutilsLib.c" 2
# 1 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stdbool.h" 1 3 4
# 3 "./src/lib_graphic/src/OutilsLib.c" 2


static 
# 5 "./src/lib_graphic/src/OutilsLib.c" 3 4
      _Bool 
# 5 "./src/lib_graphic/src/OutilsLib.c"
           systemeBigEndian()
{
 const short testArchitectureBig = 0x000F;
 return (*((char*)&testArchitectureBig) == 0x00);
}


static int barriereNatif32(int entier)
{
 int resultat;

 resultat = (int)((unsigned int)entier>>24);
 resultat |= (entier>>8)&0x0FF00;
 resultat |= (entier<<8)&0x0FF0000;
 resultat |= (entier<<24);

 return resultat;
}


static short barriereNatif16(short entier)
{
 return (short)((unsigned short)entier>>8) | (entier<<8);
}


int little32VersNatif(int little)
{
 if
  (systemeBigEndian())
 {
  return barriereNatif32(little);
 }
 else
 {
  return little;
 }
}


int natif32VersLittle(int natif)
{
 if
  (systemeBigEndian())
 {
   return barriereNatif32(natif);
 }
 else
 {
  return natif;
 }
}


short little16VersNatif(short little)
{
 if
  (systemeBigEndian())
 {
  return barriereNatif16(little);
 }
 else
 {
  return little;
 }
}



short natif16VersLittle(short natif)
{
 if
  (systemeBigEndian())
 {
  return barriereNatif16(natif);
 }
 else
 {
  return natif;
 }
}


int big32VersNatif(int big)
{
 if
  (systemeBigEndian())
 {
  return big;
 }
 else
 {
  return barriereNatif32(big);
 }
}


int natif32VersBig(int natif)
{
 if
  (systemeBigEndian())
 {
  return natif;
 }
 else
 {
  return barriereNatif32(natif);
 }
}


short big16VersNatif(short big)
{
 if
  (systemeBigEndian())
 {
  return big;
 }
 else
 {
  return barriereNatif16(big);
 }
}



short natif16VersBig(short natif)
{
 if
  (systemeBigEndian())
 {
  return natif;
 }
 else
 {
  return barriereNatif32(natif);
 }
}
