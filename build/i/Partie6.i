# 1 "./src/lib/src/Partie6.c"
# 1 "<built-in>"
# 1 "<command-line>"
# 1 "./src/lib/src/Partie6.c"
# 1 "./src/lib/src/../include/Partie6.h" 1



# 1 "./src/lib/src/../include/Partie3.h" 1



# 1 "./src/lib/src/../include/BmpLib.h" 1





# 1 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stdbool.h" 1 3 4
# 7 "./src/lib/src/../include/BmpLib.h" 2





typedef struct
{
 int largeurImage;
 int hauteurImage;
 unsigned char *donneesRGB;
} DonneesImageRGB;


void libereDonneesImageRGB(DonneesImageRGB **structure);



DonneesImageRGB *lisBMPRGB(char *nom);




# 28 "./src/lib/src/../include/BmpLib.h" 3 4
_Bool 
# 28 "./src/lib/src/../include/BmpLib.h"
    ecrisBMPRGB_Dans(DonneesImageRGB *donneesImage, char *nom);
# 5 "./src/lib/src/../include/Partie3.h" 2

# 1 "./src/lib/src/../include/Image.h" 1




    typedef struct Image {
        int largeur, hauteur;

        short int **rouge, **vert, **bleu, **gris;
    } Image;
# 18 "./src/lib/src/../include/Image.h"
    Image *alloueImage(int largeur, int hauteur);






    void libereImage(Image **ptrImage);







    Image *chargeImage(char *nom);






    void sauveImage(Image *monImage, char *nom);






    void sauveImageNG(Image *monImage, char *nom);






    Image *dupliqueImage(Image *monImage);







    Image *differenceImage(Image *image1, Image *image2);
# 7 "./src/lib/src/../include/Partie3.h" 2

    typedef struct noeudBinaire {

        short int value;

        int etiquette;
        int start_x;
        int start_y;
        int size;

        struct noeudBinaire *NW;
        struct noeudBinaire *NE;
        struct noeudBinaire *SE;
        struct noeudBinaire *SW;
    }noeudBinaire;
# 32 "./src/lib/src/../include/Partie3.h"
    short int calculCarreNB(int start_x, int start_y, int size, short int **matrix);
# 42 "./src/lib/src/../include/Partie3.h"
    noeudBinaire* creeArbreNB(int start_x, int start_y, int imgSideSize, short int **img);






    int compteFeuille(noeudBinaire* racine);







    void creeImageArbreNB(Image* img, noeudBinaire* rcn, 
# 57 "./src/lib/src/../include/Partie3.h" 3 4
                                                        _Bool 
# 57 "./src/lib/src/../include/Partie3.h"
                                                             etiq);





    void libereArbreBinaire(noeudBinaire *rcn);
# 5 "./src/lib/src/../include/Partie6.h" 2

    typedef struct SymbOcc{
        int symb;
        int occ;
    }SymbOcc;

    typedef struct TabSymbOcc{
        int nb;
        SymbOcc *tab;
    }TabSymbOcc;

    typedef struct NoeudCode{
        SymbOcc so;
        struct NoeudCode *ptrfils0,*ptrfils1;
    }NoeudCode;

    typedef struct SymbCode{
        int symb;
        char *symbCode;
    }SymbCode;

    typedef struct TabSymbCode{
        int nb;
        SymbCode *tab;
    }TabSymbCode;
# 38 "./src/lib/src/../include/Partie6.h"
    TabSymbOcc* compteOccurrence(char *path);







    NoeudCode* creeArbreCodeOcc(TabSymbOcc *tabSymbOcc);







    TabSymbCode* creeCodeAdaptatif(NoeudCode *racine);







    void sauveCodeAdaptatif(char *destPath, TabSymbCode *tabSymbCode);






    TabSymbCode* litCodeAdaptatif(char *path);







    NoeudCode* creeArbreCodeAdaptatif(TabSymbCode *tabSymbCode);







    void codeFichier(char *srcPath, char *destPath, TabSymbCode *tabSymbCode);
# 94 "./src/lib/src/../include/Partie6.h"
    void decodeFichier(char *srcPath, char *destPath, TabSymbCode *tabSymbCode);







    void transcritTexteEnBinaire(char *srcPath, char *destPath);







    void transcritBinaireEnTexte(char *srcPath, char *destPath);






    void printArbreCodeAdaptatif(NoeudCode *racine);






    void libereArbreCodeAdaptatif(NoeudCode *racine);







    TabSymbOcc* compteOccurrenceMotif(char * path);
# 141 "./src/lib/src/../include/Partie6.h"
    void codeFichierMotif(char *srcPath, char *destPath, TabSymbCode *tabSymbCode);
# 150 "./src/lib/src/../include/Partie6.h"
    void decodeFichierMotif(char *srcPath, char *destPath, TabSymbCode *tabSymbCode);







    TabSymbOcc* compteOccurrenceTraj(char * path);
# 167 "./src/lib/src/../include/Partie6.h"
    void codeFichierTraj(char *srcPath, char *destPath, TabSymbCode *tabSymbCode);
# 176 "./src/lib/src/../include/Partie6.h"
    void decodeFichierTraj(char *srcPath, char *destPath, TabSymbCode *tabSymbCode);






    void sauveArbreNB_Texte(noeudBinaire *racine);







    TabSymbOcc* compteOccurrenceNB(char * path);
# 200 "./src/lib/src/../include/Partie6.h"
    void codeFichierNB(char *srcPath, char *destPath, TabSymbCode *tabSymbCode);
# 209 "./src/lib/src/../include/Partie6.h"
    void decodeFichierNB(char *srcPath, char *destPath, TabSymbCode *tabSymbCode);







    void transcritTexteEnBinaireMotif(char *srcPath, char *destPath);







    void transcritTexteEnBinaireTraj(char *srcPath, char *destPath);







    void transcritTexteEnBinaireNB(char *srcPath, char *destPath);







    void transcritBinaireEnTexteMotif(char *srcPath, char *destPath);







    void transcritBinaireEnTexteTraj(char *srcPath, char *destPath);







    void transcritBinaireEnTexteNB(char *srcPath, char *destPath);
# 2 "./src/lib/src/Partie6.c" 2
# 1 "./src/lib/src/../include/Partie4.h" 1
# 10 "./src/lib/src/../include/Partie4.h"
# 1 "c:\\mingw\\include\\stdio.h" 1 3
# 38 "c:\\mingw\\include\\stdio.h" 3
       
# 39 "c:\\mingw\\include\\stdio.h" 3
# 55 "c:\\mingw\\include\\stdio.h" 3
# 1 "c:\\mingw\\include\\_mingw.h" 1 3
# 55 "c:\\mingw\\include\\_mingw.h" 3
       
# 56 "c:\\mingw\\include\\_mingw.h" 3
# 66 "c:\\mingw\\include\\_mingw.h" 3
# 1 "c:\\mingw\\include\\msvcrtver.h" 1 3
# 35 "c:\\mingw\\include\\msvcrtver.h" 3
       
# 36 "c:\\mingw\\include\\msvcrtver.h" 3
# 67 "c:\\mingw\\include\\_mingw.h" 2 3






# 1 "c:\\mingw\\include\\w32api.h" 1 3
# 35 "c:\\mingw\\include\\w32api.h" 3
       
# 36 "c:\\mingw\\include\\w32api.h" 3
# 59 "c:\\mingw\\include\\w32api.h" 3
# 1 "c:\\mingw\\include\\sdkddkver.h" 1 3
# 35 "c:\\mingw\\include\\sdkddkver.h" 3
       
# 36 "c:\\mingw\\include\\sdkddkver.h" 3
# 60 "c:\\mingw\\include\\w32api.h" 2 3
# 74 "c:\\mingw\\include\\_mingw.h" 2 3
# 174 "c:\\mingw\\include\\_mingw.h" 3
# 1 "c:\\mingw\\include\\features.h" 1 3
# 39 "c:\\mingw\\include\\features.h" 3
       
# 40 "c:\\mingw\\include\\features.h" 3
# 175 "c:\\mingw\\include\\_mingw.h" 2 3
# 56 "c:\\mingw\\include\\stdio.h" 2 3
# 68 "c:\\mingw\\include\\stdio.h" 3
# 1 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stddef.h" 1 3 4
# 216 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stddef.h" 3 4

# 216 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stddef.h" 3 4
typedef unsigned int size_t;
# 328 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stddef.h" 3 4
typedef short unsigned int wchar_t;
# 357 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stddef.h" 3 4
typedef short unsigned int wint_t;
# 69 "c:\\mingw\\include\\stdio.h" 2 3
# 95 "c:\\mingw\\include\\stdio.h" 3
# 1 "c:\\mingw\\include\\sys/types.h" 1 3
# 34 "c:\\mingw\\include\\sys/types.h" 3
       
# 35 "c:\\mingw\\include\\sys/types.h" 3
# 62 "c:\\mingw\\include\\sys/types.h" 3
  typedef long __off32_t;




  typedef __off32_t _off_t;







  typedef _off_t off_t;
# 91 "c:\\mingw\\include\\sys/types.h" 3
  typedef long long __off64_t;






  typedef __off64_t off64_t;
# 115 "c:\\mingw\\include\\sys/types.h" 3
  typedef int _ssize_t;







  typedef _ssize_t ssize_t;
# 96 "c:\\mingw\\include\\stdio.h" 2 3






# 1 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stdarg.h" 1 3 4
# 40 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stdarg.h" 3 4
typedef __builtin_va_list __gnuc_va_list;
# 103 "c:\\mingw\\include\\stdio.h" 2 3
# 210 "c:\\mingw\\include\\stdio.h" 3
typedef struct _iobuf
{
  char *_ptr;
  int _cnt;
  char *_base;
  int _flag;
  int _file;
  int _charbuf;
  int _bufsiz;
  char *_tmpfname;
} FILE;
# 239 "c:\\mingw\\include\\stdio.h" 3
extern __attribute__((__dllimport__)) FILE _iob[];
# 252 "c:\\mingw\\include\\stdio.h" 3








 __attribute__((__cdecl__)) __attribute__((__nothrow__)) FILE * fopen (const char *, const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) FILE * freopen (const char *, const char *, FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fflush (FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fclose (FILE *);






 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int remove (const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int rename (const char *, const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) FILE * tmpfile (void);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char * tmpnam (char *);


 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *_tempnam (const char *, const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _rmtmp (void);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _unlink (const char *);
# 289 "c:\\mingw\\include\\stdio.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char * tempnam (const char *, const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int rmtmp (void);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int unlink (const char *);



 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int setvbuf (FILE *, char *, int, size_t);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void setbuf (FILE *, char *);
# 342 "c:\\mingw\\include\\stdio.h" 3
extern int __attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__mingw_printf__,2,3))) __mingw_fprintf(FILE*, const char*, ...);
extern int __attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__mingw_printf__,1,2))) __mingw_printf(const char*, ...);
extern int __attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__mingw_printf__,2,3))) __mingw_sprintf(char*, const char*, ...);
extern int __attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__mingw_printf__,3,4))) __mingw_snprintf(char*, size_t, const char*, ...);
extern int __attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__mingw_printf__,2,0))) __mingw_vfprintf(FILE*, const char*, __builtin_va_list);
extern int __attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__mingw_printf__,1,0))) __mingw_vprintf(const char*, __builtin_va_list);
extern int __attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__mingw_printf__,2,0))) __mingw_vsprintf(char*, const char*, __builtin_va_list);
extern int __attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__mingw_printf__,3,0))) __mingw_vsnprintf(char*, size_t, const char*, __builtin_va_list);
# 376 "c:\\mingw\\include\\stdio.h" 3
extern unsigned int _mingw_output_format_control( unsigned int, unsigned int );
# 461 "c:\\mingw\\include\\stdio.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fprintf (FILE *, const char *, ...);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int printf (const char *, ...);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int sprintf (char *, const char *, ...);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int vfprintf (FILE *, const char *, __builtin_va_list);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int vprintf (const char *, __builtin_va_list);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int vsprintf (char *, const char *, __builtin_va_list);
# 479 "c:\\mingw\\include\\stdio.h" 3
static __inline__ __attribute__((__cdecl__)) __attribute__((__nothrow__))
int snprintf (char *__buf, size_t __len, const char *__format, ...)
{
  register int __retval;
  __builtin_va_list __local_argv; __builtin_va_start( __local_argv, __format );
  __retval = __mingw_vsnprintf( __buf, __len, __format, __local_argv );
  __builtin_va_end( __local_argv );
  return __retval;
}

static __inline__ __attribute__((__cdecl__)) __attribute__((__nothrow__))
int vsnprintf (char *__buf, size_t __len, const char *__format, __builtin_va_list __local_argv)
{
  return __mingw_vsnprintf( __buf, __len, __format, __local_argv );
}
# 513 "c:\\mingw\\include\\stdio.h" 3
 int __attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__ms_printf__,2,3))) __msvcrt_fprintf(FILE *, const char *, ...);
 int __attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__ms_printf__,1,2))) __msvcrt_printf(const char *, ...);
 int __attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__ms_printf__,2,3))) __msvcrt_sprintf(char *, const char *, ...);
 int __attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__ms_printf__,2,0))) __msvcrt_vfprintf(FILE *, const char *, __builtin_va_list);
 int __attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__ms_printf__,1,0))) __msvcrt_vprintf(const char *, __builtin_va_list);
 int __attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__ms_printf__,2,0))) __msvcrt_vsprintf(char *, const char *, __builtin_va_list);






 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _snprintf (char *, size_t, const char *, ...);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _vsnprintf (char *, size_t, const char *, __builtin_va_list);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _vscprintf (const char *, __builtin_va_list);
# 536 "c:\\mingw\\include\\stdio.h" 3
__attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__mingw_printf__,3,4)))
int snprintf (char *, size_t, const char *, ...);

__attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__format__(__mingw_printf__,3,0)))
int vsnprintf (char *, size_t, const char *, __builtin_va_list);

__attribute__((__cdecl__)) __attribute__((__nothrow__))
int vscanf (const char * __restrict__, __builtin_va_list);

__attribute__((__cdecl__)) __attribute__((__nothrow__))
int vfscanf (FILE * __restrict__, const char * __restrict__, __builtin_va_list);

__attribute__((__cdecl__)) __attribute__((__nothrow__))
int vsscanf (const char * __restrict__, const char * __restrict__, __builtin_va_list);
# 679 "c:\\mingw\\include\\stdio.h" 3
__attribute__((__cdecl__)) __attribute__((__nothrow__)) ssize_t
getdelim (char ** __restrict__, size_t * __restrict__, int, FILE * __restrict__);

__attribute__((__cdecl__)) __attribute__((__nothrow__)) ssize_t
getline (char ** __restrict__, size_t * __restrict__, FILE * __restrict__);
# 699 "c:\\mingw\\include\\stdio.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fscanf (FILE *, const char *, ...);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int scanf (const char *, ...);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int sscanf (const char *, const char *, ...);



 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fgetc (FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char * fgets (char *, int, FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fputc (int, FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fputs (const char *, FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char * gets (char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int puts (const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int ungetc (int, FILE *);
# 720 "c:\\mingw\\include\\stdio.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _filbuf (FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _flsbuf (int, FILE *);



extern inline __attribute__((__gnu_inline__)) __attribute__((__cdecl__)) __attribute__((__nothrow__)) int getc (FILE *);
extern inline __attribute__((__gnu_inline__)) __attribute__((__cdecl__)) __attribute__((__nothrow__)) int getc (FILE * __F)
{
  return (--__F->_cnt >= 0)
    ? (int) (unsigned char) *__F->_ptr++
    : _filbuf (__F);
}

extern inline __attribute__((__gnu_inline__)) __attribute__((__cdecl__)) __attribute__((__nothrow__)) int putc (int, FILE *);
extern inline __attribute__((__gnu_inline__)) __attribute__((__cdecl__)) __attribute__((__nothrow__)) int putc (int __c, FILE * __F)
{
  return (--__F->_cnt >= 0)
    ? (int) (unsigned char) (*__F->_ptr++ = (char)__c)
    : _flsbuf (__c, __F);
}

extern inline __attribute__((__gnu_inline__)) __attribute__((__cdecl__)) __attribute__((__nothrow__)) int getchar (void);
extern inline __attribute__((__gnu_inline__)) __attribute__((__cdecl__)) __attribute__((__nothrow__)) int getchar (void)
{
  return (--(&_iob[0])->_cnt >= 0)
    ? (int) (unsigned char) *(&_iob[0])->_ptr++
    : _filbuf ((&_iob[0]));
}

extern inline __attribute__((__gnu_inline__)) __attribute__((__cdecl__)) __attribute__((__nothrow__)) int putchar(int);
extern inline __attribute__((__gnu_inline__)) __attribute__((__cdecl__)) __attribute__((__nothrow__)) int putchar(int __c)
{
  return (--(&_iob[1])->_cnt >= 0)
    ? (int) (unsigned char) (*(&_iob[1])->_ptr++ = (char)__c)
    : _flsbuf (__c, (&_iob[1]));}
# 767 "c:\\mingw\\include\\stdio.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) size_t fread (void *, size_t, size_t, FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) size_t fwrite (const void *, size_t, size_t, FILE *);



 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fseek (FILE *, long, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) long ftell (FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void rewind (FILE *);
# 821 "c:\\mingw\\include\\stdio.h" 3
typedef union { long long __value; __off64_t __offset; } fpos_t;




 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fgetpos (FILE *, fpos_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fsetpos (FILE *, const fpos_t *);
# 862 "c:\\mingw\\include\\stdio.h" 3
int __attribute__((__cdecl__)) __attribute__((__nothrow__)) __mingw_fseeki64 (FILE *, long long, int);
extern inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _fseeki64 (FILE *__f, long long __o, int __w)
{ return __mingw_fseeki64 (__f, __o, __w); }


long long __attribute__((__cdecl__)) __attribute__((__nothrow__)) __mingw_ftelli64 (FILE *);
extern inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__cdecl__)) long long __attribute__((__nothrow__)) _ftelli64 (FILE *__file )
{ return __mingw_ftelli64 (__file); }





 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int feof (FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int ferror (FILE *);
# 886 "c:\\mingw\\include\\stdio.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void clearerr (FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void perror (const char *);





 __attribute__((__cdecl__)) __attribute__((__nothrow__)) FILE * _popen (const char *, const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _pclose (FILE *);


 __attribute__((__cdecl__)) __attribute__((__nothrow__)) FILE * popen (const char *, const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int pclose (FILE *);




 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _flushall (void);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _fgetchar (void);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _fputchar (int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) FILE * _fdopen (int, const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _fileno (FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _fcloseall (void);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) FILE * _fsopen (const char *, const char *, int);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _getmaxstdio (void);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _setmaxstdio (int);
# 936 "c:\\mingw\\include\\stdio.h" 3
unsigned int __attribute__((__cdecl__)) __mingw_get_output_format (void);
unsigned int __attribute__((__cdecl__)) __mingw_set_output_format (unsigned int);







int __attribute__((__cdecl__)) __mingw_get_printf_count_output (void);
int __attribute__((__cdecl__)) __mingw_set_printf_count_output (int);
# 962 "c:\\mingw\\include\\stdio.h" 3
extern inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) unsigned int __attribute__((__cdecl__)) _get_output_format (void)
{ return __mingw_get_output_format (); }

extern inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) unsigned int __attribute__((__cdecl__)) _set_output_format (unsigned int __style)
{ return __mingw_set_output_format (__style); }
# 987 "c:\\mingw\\include\\stdio.h" 3
extern inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) int __attribute__((__cdecl__)) _get_printf_count_output (void)
{ return 0 ? 1 : __mingw_get_printf_count_output (); }

extern inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) int __attribute__((__cdecl__)) _set_printf_count_output (int __mode)
{ return 0 ? 1 : __mingw_set_printf_count_output (__mode); }



 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fgetchar (void);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fputchar (int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) FILE * fdopen (int, const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fileno (FILE *);
# 1007 "c:\\mingw\\include\\stdio.h" 3
extern inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) FILE * __attribute__((__cdecl__)) __attribute__((__nothrow__)) fopen64 (const char *, const char *);
extern inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__))
FILE * __attribute__((__cdecl__)) __attribute__((__nothrow__)) fopen64 (const char * filename, const char * mode)
{ return fopen (filename, mode); }

int __attribute__((__cdecl__)) __attribute__((__nothrow__)) fseeko64 (FILE *, __off64_t, int);
# 1028 "c:\\mingw\\include\\stdio.h" 3
__off64_t __attribute__((__cdecl__)) __attribute__((__nothrow__)) ftello64 (FILE *);
# 1041 "c:\\mingw\\include\\stdio.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fwprintf (FILE *, const wchar_t *, ...);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int wprintf (const wchar_t *, ...);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int vfwprintf (FILE *, const wchar_t *, __builtin_va_list);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int vwprintf (const wchar_t *, __builtin_va_list);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _snwprintf (wchar_t *, size_t, const wchar_t *, ...);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _vscwprintf (const wchar_t *, __builtin_va_list);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _vsnwprintf (wchar_t *, size_t, const wchar_t *, __builtin_va_list);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fwscanf (FILE *, const wchar_t *, ...);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int wscanf (const wchar_t *, ...);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int swscanf (const wchar_t *, const wchar_t *, ...);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wint_t fgetwc (FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wint_t fputwc (wchar_t, FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wint_t ungetwc (wchar_t, FILE *);




 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int swprintf (wchar_t *, const wchar_t *, ...);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int vswprintf (wchar_t *, const wchar_t *, __builtin_va_list);



 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t * fgetws (wchar_t *, int, FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int fputws (const wchar_t *, FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wint_t getwc (FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wint_t getwchar (void);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wint_t putwc (wint_t, FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wint_t putwchar (wint_t);


 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t * _getws (wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _putws (const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) FILE * _wfdopen(int, const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) FILE * _wfopen (const wchar_t *, const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) FILE * _wfreopen (const wchar_t *, const wchar_t *, FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) FILE * _wfsopen (const wchar_t *, const wchar_t *, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t * _wtmpnam (wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t * _wtempnam (const wchar_t *, const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _wrename (const wchar_t *, const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _wremove (const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void _wperror (const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) FILE * _wpopen (const wchar_t *, const wchar_t *);






__attribute__((__cdecl__)) __attribute__((__nothrow__)) int snwprintf (wchar_t *, size_t, const wchar_t *, ...);
__attribute__((__cdecl__)) __attribute__((__nothrow__)) int vsnwprintf (wchar_t *, size_t, const wchar_t *, __builtin_va_list);
# 1099 "c:\\mingw\\include\\stdio.h" 3
__attribute__((__cdecl__)) __attribute__((__nothrow__)) int vwscanf (const wchar_t *__restrict__, __builtin_va_list);
__attribute__((__cdecl__)) __attribute__((__nothrow__))
int vfwscanf (FILE *__restrict__, const wchar_t *__restrict__, __builtin_va_list);
__attribute__((__cdecl__)) __attribute__((__nothrow__))
int vswscanf (const wchar_t *__restrict__, const wchar_t * __restrict__, __builtin_va_list);






 __attribute__((__cdecl__)) __attribute__((__nothrow__)) FILE * wpopen (const wchar_t *, const wchar_t *);




 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wint_t _fgetwchar (void);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wint_t _fputwchar (wint_t);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _getw (FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _putw (int, FILE *);




 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wint_t fgetwchar (void);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wint_t fputwchar (wint_t);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int getw (FILE *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int putw (int, FILE *);





# 11 "./src/lib/src/../include/Partie4.h" 2
# 24 "./src/lib/src/../include/Partie4.h"
    
# 24 "./src/lib/src/../include/Partie4.h"
   typedef struct Couleur{

        short int rouge, vert, bleu;
    }Couleur;





    typedef struct Palette {
        int nb;
        Couleur *pal;
    }Palette;





    typedef struct Motif{

        short int forme;
        short int config;

        int cfond;
        int cobjet;
    }Motif;






    typedef struct noeudMotif{

        struct noeudMotif *NO;
        struct noeudMotif *NE;
        struct noeudMotif *SO;
        struct noeudMotif *SE;

        Motif motif;
        int x,y;
        int largeur;

    }noeudMotif;







    Palette* creePalette(Image *img);
# 87 "./src/lib/src/../include/Partie4.h"
    Motif identifieMotif(Image *img, Palette *palette, int x, int y, int largeur);
# 99 "./src/lib/src/../include/Partie4.h"
    noeudMotif* creeArbreMotifs(Image *img, Palette *palette, int largeur, int x, int y);
# 108 "./src/lib/src/../include/Partie4.h"
    void sauveDescriptionImage(noeudMotif *rcn, Palette* pal,char *path);






    void affichePalette(Palette *pal);






    void libereArbreMotif(noeudMotif *ptrRcn);
# 136 "./src/lib/src/../include/Partie4.h"
    
# 136 "./src/lib/src/../include/Partie4.h" 3 4
   _Bool 
# 136 "./src/lib/src/../include/Partie4.h"
        compareCouleur (Couleur c1, Couleur c2);
# 148 "./src/lib/src/../include/Partie4.h"
    
# 148 "./src/lib/src/../include/Partie4.h" 3 4
   _Bool 
# 148 "./src/lib/src/../include/Partie4.h"
        compareCouleurVariadic (Couleur c1, Couleur c2, int nbreArgumentsOptionnels, ...);
# 158 "./src/lib/src/../include/Partie4.h"
    
# 158 "./src/lib/src/../include/Partie4.h" 3 4
   _Bool 
# 158 "./src/lib/src/../include/Partie4.h"
        sontDesCouleursVariadic(int nbreArguments, ...);
# 168 "./src/lib/src/../include/Partie4.h"
    
# 168 "./src/lib/src/../include/Partie4.h" 3 4
   _Bool 
# 168 "./src/lib/src/../include/Partie4.h"
        comparePalette(Palette p1, Palette p2);
# 178 "./src/lib/src/../include/Partie4.h"
    
# 178 "./src/lib/src/../include/Partie4.h" 3 4
   _Bool 
# 178 "./src/lib/src/../include/Partie4.h"
        compareMotif(Motif m1, Motif m2);
# 189 "./src/lib/src/../include/Partie4.h"
    
# 189 "./src/lib/src/../include/Partie4.h" 3 4
   _Bool 
# 189 "./src/lib/src/../include/Partie4.h"
        estCouleurDans(Couleur *arr, int arrSize, Couleur couleur);
# 201 "./src/lib/src/../include/Partie4.h"
    Couleur estCouleurUniforme(Image *img, int depart_x, int depart_y, int largeur, int hauteur);
# 213 "./src/lib/src/../include/Partie4.h"
    Couleur estCouleurUniformeDansCarre(Image *img, int x1, int y1, int x2, int y2);
# 3 "./src/lib/src/Partie6.c" 2
# 1 "c:\\mingw\\include\\stdlib.h" 1 3
# 34 "c:\\mingw\\include\\stdlib.h" 3
       
# 35 "c:\\mingw\\include\\stdlib.h" 3
# 55 "c:\\mingw\\include\\stdlib.h" 3
# 1 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stddef.h" 1 3 4
# 56 "c:\\mingw\\include\\stdlib.h" 2 3
# 90 "c:\\mingw\\include\\stdlib.h" 3

# 99 "c:\\mingw\\include\\stdlib.h" 3

# 99 "c:\\mingw\\include\\stdlib.h" 3
extern int _argc;
extern char **_argv;




extern __attribute__((__cdecl__)) __attribute__((__nothrow__)) int *__p___argc(void);
extern __attribute__((__cdecl__)) __attribute__((__nothrow__)) char ***__p___argv(void);
extern __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t ***__p___wargv(void);
# 142 "c:\\mingw\\include\\stdlib.h" 3
   extern __attribute__((__dllimport__)) int __mb_cur_max;
# 166 "c:\\mingw\\include\\stdlib.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int *_errno(void);


 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int *__doserrno(void);







extern __attribute__((__cdecl__)) __attribute__((__nothrow__)) char ***__p__environ(void);

extern __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t ***__p__wenviron(void);
# 202 "c:\\mingw\\include\\stdlib.h" 3
extern __attribute__((__dllimport__)) int _sys_nerr;
# 227 "c:\\mingw\\include\\stdlib.h" 3
extern __attribute__((__dllimport__)) char *_sys_errlist[];
# 238 "c:\\mingw\\include\\stdlib.h" 3
extern __attribute__((__cdecl__)) __attribute__((__nothrow__)) unsigned int *__p__osver(void);
extern __attribute__((__cdecl__)) __attribute__((__nothrow__)) unsigned int *__p__winver(void);
extern __attribute__((__cdecl__)) __attribute__((__nothrow__)) unsigned int *__p__winmajor(void);
extern __attribute__((__cdecl__)) __attribute__((__nothrow__)) unsigned int *__p__winminor(void);
# 250 "c:\\mingw\\include\\stdlib.h" 3
extern __attribute__((__dllimport__)) unsigned int _osver;
extern __attribute__((__dllimport__)) unsigned int _winver;
extern __attribute__((__dllimport__)) unsigned int _winmajor;
extern __attribute__((__dllimport__)) unsigned int _winminor;
# 289 "c:\\mingw\\include\\stdlib.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char **__p__pgmptr(void);


 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t **__p__wpgmptr(void);
# 325 "c:\\mingw\\include\\stdlib.h" 3
extern __attribute__((__dllimport__)) int _fmode;
# 335 "c:\\mingw\\include\\stdlib.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int atoi (const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) long atol (const char *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) double strtod (const char *, char **);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) double atof (const char *);


 __attribute__((__cdecl__)) __attribute__((__nothrow__)) double _wtof (const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _wtoi (const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) long _wtol (const wchar_t *);
# 378 "c:\\mingw\\include\\stdlib.h" 3
__attribute__((__cdecl__)) __attribute__((__nothrow__))
float strtof (const char *__restrict__, char **__restrict__);

__attribute__((__cdecl__)) __attribute__((__nothrow__))
long double strtold (const char *__restrict__, char **__restrict__);


 __attribute__((__cdecl__)) __attribute__((__nothrow__)) long strtol (const char *, char **, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) unsigned long strtoul (const char *, char **, int);







 __attribute__((__cdecl__)) __attribute__((__nothrow__))
long wcstol (const wchar_t *, wchar_t **, int);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
unsigned long wcstoul (const wchar_t *, wchar_t **, int);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) double wcstod (const wchar_t *, wchar_t **);





__attribute__((__cdecl__)) __attribute__((__nothrow__))
float wcstof (const wchar_t *__restrict__, wchar_t **__restrict__);

__attribute__((__cdecl__)) __attribute__((__nothrow__))
long double wcstold (const wchar_t *__restrict__, wchar_t **__restrict__);
# 451 "c:\\mingw\\include\\stdlib.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t *_wgetenv (const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _wputenv (const wchar_t *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
void _wsearchenv (const wchar_t *, const wchar_t *, wchar_t *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _wsystem (const wchar_t *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
void _wmakepath (wchar_t *, const wchar_t *, const wchar_t *, const wchar_t *,
    const wchar_t *
  );

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
void _wsplitpath (const wchar_t *, wchar_t *, wchar_t *, wchar_t *, wchar_t *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
wchar_t *_wfullpath (wchar_t *, const wchar_t *, size_t);





 __attribute__((__cdecl__)) __attribute__((__nothrow__)) size_t wcstombs (char *, const wchar_t *, size_t);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int wctomb (char *, wchar_t);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int mblen (const char *, size_t);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) size_t mbstowcs (wchar_t *, const char *, size_t);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int mbtowc (wchar_t *, const char *, size_t);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int rand (void);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void srand (unsigned int);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void abort (void) __attribute__((__noreturn__));
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void exit (int) __attribute__((__noreturn__));



int __attribute__((__cdecl__)) __attribute__((__nothrow__)) atexit (void (*)(void));

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int system (const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *getenv (const char *);






# 1 "c:\\mingw\\include\\alloca.h" 1 3
# 43 "c:\\mingw\\include\\alloca.h" 3
       
# 44 "c:\\mingw\\include\\alloca.h" 3
# 54 "c:\\mingw\\include\\alloca.h" 3
# 1 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stddef.h" 1 3 4
# 55 "c:\\mingw\\include\\alloca.h" 2 3


# 80 "c:\\mingw\\include\\alloca.h" 3
void *alloca( size_t );







void *_alloca( size_t );



# 500 "c:\\mingw\\include\\stdlib.h" 2 3


 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void *calloc (size_t, size_t) __attribute__((__malloc__));
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void *malloc (size_t) __attribute__((__malloc__));
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void *realloc (void *, size_t);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void free (void *);
# 514 "c:\\mingw\\include\\stdlib.h" 3
__attribute__((__cdecl__)) __attribute__((__nothrow__)) void *__mingw_realloc (void *, size_t);
__attribute__((__cdecl__)) __attribute__((__nothrow__)) void __mingw_free (void *);






 __attribute__((__cdecl__)) void *bsearch
(const void *, const void *, size_t, size_t, int (*)(const void *, const void *));

 __attribute__((__cdecl__)) void qsort
(void *, size_t, size_t, int (*)(const void *, const void *));

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int abs (int) __attribute__((__const__));
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) long labs (long) __attribute__((__const__));
# 538 "c:\\mingw\\include\\stdlib.h" 3
typedef struct { int quot, rem; } div_t;
typedef struct { long quot, rem; } ldiv_t;

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) div_t div (int, int) __attribute__((__const__));
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) ldiv_t ldiv (long, long) __attribute__((__const__));






 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void _exit (int) __attribute__((__noreturn__));





 __attribute__((__cdecl__)) __attribute__((__nothrow__)) long long _atoi64 (const char *);
# 564 "c:\\mingw\\include\\stdlib.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void _beep (unsigned int, unsigned int) __attribute__((__deprecated__));

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void _seterrormode (int) __attribute__((__deprecated__));
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void _sleep (unsigned long) __attribute__((__deprecated__));



typedef int (* _onexit_t)(void);
__attribute__((__cdecl__)) __attribute__((__nothrow__)) _onexit_t _onexit( _onexit_t );

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _putenv (const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__))
void _searchenv (const char *, const char *, char *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *_ecvt (double, int, int *, int *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *_fcvt (double, int, int *, int *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *_gcvt (double, int, char *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
void _makepath (char *, const char *, const char *, const char *, const char *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__))
void _splitpath (const char *, char *, char *, char *, char *);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *_fullpath (char*, const char*, size_t);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *_itoa (int, char *, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *_ltoa (long, char *, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *_ultoa(unsigned long, char *, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t *_itow (int, wchar_t *, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t *_ltow (long, wchar_t *, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t *_ultow (unsigned long, wchar_t *, int);


 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char* _i64toa (long long, char *, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char* _ui64toa (unsigned long long, char *, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) long long _wtoi64 (const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t* _i64tow (long long, wchar_t *, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t* _ui64tow (unsigned long long, wchar_t *, int);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) unsigned int (_rotl)(unsigned int, int) __attribute__((__const__));
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) unsigned int (_rotr)(unsigned int, int) __attribute__((__const__));
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) unsigned long (_lrotl)(unsigned long, int) __attribute__((__const__));
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) unsigned long (_lrotr)(unsigned long, int) __attribute__((__const__));

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _set_error_mode (int);
# 647 "c:\\mingw\\include\\stdlib.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int putenv (const char*);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void searchenv (const char*, const char*, char*);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char* itoa (int, char*, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char* ltoa (long, char*, int);


 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char* ecvt (double, int, int*, int*);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char* fcvt (double, int, int*, int*);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char* gcvt (double, int, char*);
# 668 "c:\\mingw\\include\\stdlib.h" 3
__attribute__((__cdecl__)) __attribute__((__nothrow__)) void _Exit(int) __attribute__((__noreturn__));






typedef struct { long long quot, rem; } lldiv_t;
__attribute__((__cdecl__)) __attribute__((__nothrow__)) lldiv_t lldiv (long long, long long) __attribute__((__const__));

__attribute__((__cdecl__)) __attribute__((__nothrow__)) long long llabs (long long);
# 689 "c:\\mingw\\include\\stdlib.h" 3
__attribute__((__cdecl__)) __attribute__((__nothrow__))
long long strtoll (const char *__restrict__, char **__restrict, int);

__attribute__((__cdecl__)) __attribute__((__nothrow__))
unsigned long long strtoull (const char *__restrict__, char **__restrict__, int);





__attribute__((__cdecl__)) __attribute__((__nothrow__)) long long atoll (const char *);
# 745 "c:\\mingw\\include\\stdlib.h" 3
__attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__deprecated__)) long long wtoll (const wchar_t *);
__attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__deprecated__)) char *lltoa (long long, char *, int);
__attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__deprecated__)) char *ulltoa (unsigned long long , char *, int);
__attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__deprecated__)) wchar_t *lltow (long long, wchar_t *, int);
__attribute__((__cdecl__)) __attribute__((__nothrow__)) __attribute__((__deprecated__)) wchar_t *ulltow (unsigned long long, wchar_t *, int);
# 785 "c:\\mingw\\include\\stdlib.h" 3
__attribute__((__cdecl__)) __attribute__((__nothrow__)) int mkstemp (char *);
__attribute__((__cdecl__)) __attribute__((__nothrow__)) int __mingw_mkstemp (int, char *);
# 827 "c:\\mingw\\include\\stdlib.h" 3
extern inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__))
__attribute__((__cdecl__)) __attribute__((__nothrow__)) int mkstemp (char *__filename_template)
{ return __mingw_mkstemp( 0, __filename_template ); }
# 838 "c:\\mingw\\include\\stdlib.h" 3
__attribute__((__cdecl__)) __attribute__((__nothrow__)) char *mkdtemp (char *);
__attribute__((__cdecl__)) __attribute__((__nothrow__)) char *__mingw_mkdtemp (char *);

extern inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__))
__attribute__((__cdecl__)) __attribute__((__nothrow__)) char *mkdtemp (char *__dirname_template)
{ return __mingw_mkdtemp( __dirname_template ); }






__attribute__((__cdecl__)) __attribute__((__nothrow__)) int setenv( const char *, const char *, int );
__attribute__((__cdecl__)) __attribute__((__nothrow__)) int unsetenv( const char * );

__attribute__((__cdecl__)) __attribute__((__nothrow__)) int __mingw_setenv( const char *, const char *, int );

extern inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__))
__attribute__((__cdecl__)) __attribute__((__nothrow__)) int setenv( const char *__n, const char *__v, int __f )
{ return __mingw_setenv( __n, __v, __f ); }

extern inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__))
__attribute__((__cdecl__)) __attribute__((__nothrow__)) int unsetenv( const char *__name )
{ return __mingw_setenv( __name, ((void *)0), 1 ); }





# 4 "./src/lib/src/Partie6.c" 2

# 1 "c:\\mingw\\include\\string.h" 1 3
# 34 "c:\\mingw\\include\\string.h" 3
       
# 35 "c:\\mingw\\include\\string.h" 3
# 53 "c:\\mingw\\include\\string.h" 3
# 1 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stddef.h" 1 3 4
# 54 "c:\\mingw\\include\\string.h" 2 3
# 62 "c:\\mingw\\include\\string.h" 3
# 1 "c:\\mingw\\include\\strings.h" 1 3
# 33 "c:\\mingw\\include\\strings.h" 3
       
# 34 "c:\\mingw\\include\\strings.h" 3
# 59 "c:\\mingw\\include\\strings.h" 3
# 1 "c:\\mingw\\lib\\gcc\\mingw32\\8.2.0\\include\\stddef.h" 1 3 4
# 60 "c:\\mingw\\include\\strings.h" 2 3



int __attribute__((__cdecl__)) __attribute__((__nothrow__)) strcasecmp( const char *, const char * );
int __attribute__((__cdecl__)) __attribute__((__nothrow__)) strncasecmp( const char *, const char *, size_t );
# 80 "c:\\mingw\\include\\strings.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _stricmp( const char *, const char * );
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _strnicmp( const char *, const char *, size_t );
# 100 "c:\\mingw\\include\\strings.h" 3

# 63 "c:\\mingw\\include\\string.h" 2 3







 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void *memchr (const void *, int, size_t) __attribute__((__pure__));
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int memcmp (const void *, const void *, size_t) __attribute__((__pure__));
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void *memcpy (void *, const void *, size_t);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void *memmove (void *, const void *, size_t);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void *memset (void *, int, size_t);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *strcat (char *, const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *strchr (const char *, int) __attribute__((__pure__));
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int strcmp (const char *, const char *) __attribute__((__pure__));
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int strcoll (const char *, const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *strcpy (char *, const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) size_t strcspn (const char *, const char *) __attribute__((__pure__));
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *strerror (int);

 __attribute__((__cdecl__)) __attribute__((__nothrow__)) size_t strlen (const char *) __attribute__((__pure__));
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *strncat (char *, const char *, size_t);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int strncmp (const char *, const char *, size_t) __attribute__((__pure__));
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *strncpy (char *, const char *, size_t);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *strpbrk (const char *, const char *) __attribute__((__pure__));
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *strrchr (const char *, int) __attribute__((__pure__));
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) size_t strspn (const char *, const char *) __attribute__((__pure__));
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *strstr (const char *, const char *) __attribute__((__pure__));
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *strtok (char *, const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) size_t strxfrm (char *, const char *, size_t);




 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *_strerror (const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void *_memccpy (void *, const void *, int, size_t);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _memicmp (const void *, const void *, size_t);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *_strdup (const char *) __attribute__((__malloc__));
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _strcmpi (const char *, const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _stricoll (const char *, const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *_strlwr (char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *_strnset (char *, int, size_t);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *_strrev (char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *_strset (char *, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *_strupr (char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void _swab (const char *, char *, size_t);
# 126 "c:\\mingw\\include\\string.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _strncoll(const char *, const char *, size_t);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _strnicoll(const char *, const char *, size_t);






 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void *memccpy (void *, const void *, int, size_t);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int memicmp (const void *, const void *, size_t);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *strdup (const char *) __attribute__((__malloc__));
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int strcmpi (const char *, const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int stricmp (const char *, const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int stricoll (const char *, const char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *strlwr (char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int strnicmp (const char *, const char *, size_t);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *strnset (char *, int, size_t);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *strrev (char *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *strset (char *, int);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) char *strupr (char *);





 __attribute__((__cdecl__)) __attribute__((__nothrow__)) void swab (const char *, char *, size_t);
# 170 "c:\\mingw\\include\\string.h" 3
# 1 "c:\\mingw\\include\\wchar.h" 1 3
# 35 "c:\\mingw\\include\\wchar.h" 3
       
# 36 "c:\\mingw\\include\\wchar.h" 3
# 409 "c:\\mingw\\include\\wchar.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t *wcscat (wchar_t *, const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t *wcschr (const wchar_t *, wchar_t);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int wcscmp (const wchar_t *, const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int wcscoll (const wchar_t *, const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t *wcscpy (wchar_t *, const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) size_t wcscspn (const wchar_t *, const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) size_t wcslen (const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t *wcsncat (wchar_t *, const wchar_t *, size_t);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int wcsncmp (const wchar_t *, const wchar_t *, size_t);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t *wcsncpy (wchar_t *, const wchar_t *, size_t);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t *wcspbrk (const wchar_t *, const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t *wcsrchr (const wchar_t *, wchar_t);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) size_t wcsspn (const wchar_t *, const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t *wcsstr (const wchar_t *, const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t *wcstok (wchar_t *, const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) size_t wcsxfrm (wchar_t *, const wchar_t *, size_t);




 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t *_wcsdup (const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _wcsicmp (const wchar_t *, const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _wcsicoll (const wchar_t *, const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t *_wcslwr (wchar_t*);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _wcsnicmp (const wchar_t *, const wchar_t *, size_t);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t *_wcsnset (wchar_t *, wchar_t, size_t);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t *_wcsrev (wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t *_wcsset (wchar_t *, wchar_t);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t *_wcsupr (wchar_t *);


 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _wcsncoll (const wchar_t *, const wchar_t *, size_t);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int _wcsnicoll (const wchar_t *, const wchar_t *, size_t);
# 462 "c:\\mingw\\include\\wchar.h" 3
int __attribute__((__cdecl__)) __attribute__((__nothrow__)) wcscmpi (const wchar_t *, const wchar_t *);
# 474 "c:\\mingw\\include\\wchar.h" 3
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t *wcsdup (const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int wcsicmp (const wchar_t *, const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int wcsicoll (const wchar_t *, const wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t *wcslwr (wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) int wcsnicmp (const wchar_t *, const wchar_t *, size_t);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t *wcsnset (wchar_t *, wchar_t, size_t);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t *wcsrev (wchar_t *);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t *wcsset (wchar_t *, wchar_t);
 __attribute__((__cdecl__)) __attribute__((__nothrow__)) wchar_t *wcsupr (wchar_t *);
# 508 "c:\\mingw\\include\\wchar.h" 3
extern size_t __mingw_wcsnlen (const wchar_t *, size_t);


extern inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) size_t wcsnlen (const wchar_t *__text, size_t __maxlen)
{ return __mingw_wcsnlen (__text, __maxlen); }
# 171 "c:\\mingw\\include\\string.h" 2 3
# 193 "c:\\mingw\\include\\string.h" 3
extern size_t __mingw_strnlen (const char *, size_t);


extern inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) size_t strnlen (const char *__text, size_t __maxlen)
{ return __mingw_strnlen (__text, __maxlen); }
# 210 "c:\\mingw\\include\\string.h" 3
extern char *strtok_r
(char *__restrict__, const char *__restrict__, char **__restrict__);
# 223 "c:\\mingw\\include\\string.h" 3
extern int strerror_r (int, char *, size_t);
# 247 "c:\\mingw\\include\\string.h" 3
extern inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) int strerror_s (char *__buf, size_t __len, int __err)
{ return strerror_r (__err, __buf, __len); }





# 6 "./src/lib/src/Partie6.c" 2


# 7 "./src/lib/src/Partie6.c"
void triTabSymbOcc(TabSymbOcc *tabSymbOcc)
{
    for (int i = 0; i < tabSymbOcc->nb; i++)
    {
        int min = tabSymbOcc->tab[i].occ;
        int index = -1;
        for (int j = i; j < tabSymbOcc->nb; j++)
        {
            if(tabSymbOcc->tab[j].occ < min)
            {
                min = tabSymbOcc->tab[j].occ;
                index = j;
            }
        }
        if(index > -1)
        {
            SymbOcc tmp3 = (SymbOcc){tabSymbOcc->tab[index].symb, tabSymbOcc->tab[index].occ};
            SymbOcc tmp = (SymbOcc){tabSymbOcc->tab[i].symb, tabSymbOcc->tab[i].occ};
            for(int k = i; k != index; k++)
            {
                SymbOcc tmp2 = (SymbOcc){tabSymbOcc->tab[k+1].symb, tabSymbOcc->tab[k+1].occ};
                tabSymbOcc->tab[k+1].symb = tmp.symb;
                tabSymbOcc->tab[k+1].occ = tmp.occ;
                tmp = tmp2;

            }
            tabSymbOcc->tab[i] = tmp3;
        }

    }
}

TabSymbOcc* compteOccurrence(char *path){
    FILE * fp;

    fp = fopen (path,"r");
    if(!fp) return 
# 43 "./src/lib/src/Partie6.c" 3 4
                  ((void *)0)
# 43 "./src/lib/src/Partie6.c"
                      ;


    int fileSize;
    fseek(fp, 0L, 
# 47 "./src/lib/src/Partie6.c" 3
                 2
# 47 "./src/lib/src/Partie6.c"
                         );
    fileSize = ftell(fp);
    rewind(fp);

    TabSymbOcc *ret = (TabSymbOcc*) malloc(sizeof(TabSymbCode));
    ret->nb = 0;
    ret->tab = 
# 53 "./src/lib/src/Partie6.c" 3 4
              ((void *)0)
# 53 "./src/lib/src/Partie6.c"
                  ;


    char* symb = (char*) malloc(sizeof(char)*fileSize);
    symb[0] = '\0';

    int c = 0;
    while (c != 
# 60 "./src/lib/src/Partie6.c" 3
               (-1)
# 60 "./src/lib/src/Partie6.c"
                  )
    {
        c = fgetc(fp);

        if((c >= 48 && c <= 57) || c == 45)
        {
            sprintf(symb,"%s%c",symb,c);
        }
        else
        {
            int isIn = 0;
            int val = atoi(symb);
            SymbOcc tmp[ret->nb];
            if(ret->nb){
                for(int i=0;i<ret->nb;i++){
                    if(ret->tab[i].symb==val){
                        ret->tab[i].occ++;
                        isIn = 1;
                        break;
                    }
                    else{
                        tmp[i] = ret->tab[i];
                    }
                }
            }
            if(!isIn)
            {
                ret->nb++;
                ret->tab = (SymbOcc*) realloc(ret->tab,sizeof(SymbOcc)*ret->nb);
                if(ret->nb-1) for(int i=0;i<ret->nb;i++) ret->tab[i] = tmp[i];
                ret->tab[ret->nb-1] = (SymbOcc){val,1};
            }

            symb=strcpy(symb,"");

        }

    }
    free(symb);

    fclose (fp);

    triTabSymbOcc(ret);
    return ret;
}

void triNoeudArbreCodeOcc(NoeudCode **arbre, int taille)
{

    for (int i = 0; i < taille; i++)
    {
        int min = arbre[i]->so.occ;
        int index = -1;
        for (int j = i; j < taille; j++)
        {
            if(arbre[j]->so.occ < min)
            {
                min = arbre[j]->so.occ;
                index = j;
            }
        }
        if(index > -1)
        {
            NoeudCode *tmp3 = arbre[index];
            NoeudCode *tmp = arbre[i];
            for(int k = i; k != index; k++)
            {
                NoeudCode *tmp2 = arbre[k+1];
                arbre[k+1] = tmp;
                tmp = tmp2;

            }
            arbre[i] = tmp3;
        }

    }
}







void deplaceNoeudArbreCodeOcc(NoeudCode **arbre, int taille)
{

    for (int i = 0; i < taille; i++)
    {
        if(arbre[i]->so.symb == -666)
        {
            int min = arbre[i]->so.occ;
            int index = -1;
            for (int j = i; j < taille; j++)
            {
                if(arbre[j]->so.occ <= min)
                {
                    min = arbre[j]->so.occ;
                    index = j;
                }
            }

            if(index > -1)
            {
                SymbOcc tmp3 = (SymbOcc){arbre[i]->so.symb, arbre[i]->so.occ};
                for(int k = i; k != index; k++)
                {
                    arbre[k]->so.symb = arbre[k+1]->so.symb;
                    arbre[k]->so.occ = arbre[k+1]->so.occ;
                }
                arbre[index]->so = tmp3;
            }
        }


    }
}

NoeudCode* creeArbreCodeOcc(TabSymbOcc *tabSymbOcc){
    NoeudCode **arbre = (NoeudCode**) malloc(sizeof(NoeudCode*)*tabSymbOcc->nb);
    int tailleArbre = tabSymbOcc->nb;

    triTabSymbOcc(tabSymbOcc);
    for (int i = 0; i < tabSymbOcc->nb; i++)
    {
        arbre[i] = (NoeudCode*) malloc(sizeof(NoeudCode));
        arbre[i]->ptrfils0 = 
# 186 "./src/lib/src/Partie6.c" 3 4
                            ((void *)0)
# 186 "./src/lib/src/Partie6.c"
                                ;
        arbre[i]->ptrfils1 = 
# 187 "./src/lib/src/Partie6.c" 3 4
                            ((void *)0)
# 187 "./src/lib/src/Partie6.c"
                                ;
        arbre[i]->so = tabSymbOcc->tab[i];
    }

    while (tailleArbre > 1)
    {

        for (int i = 0; i < tailleArbre; i++)
        {

            NoeudCode *nouveauNoeud = (NoeudCode*) malloc(sizeof(NoeudCode));
            nouveauNoeud->so.occ = arbre[i]->so.occ + arbre[i+1]->so.occ;
            nouveauNoeud->so.symb = -666;
            nouveauNoeud->ptrfils0 = arbre[i];
            nouveauNoeud->ptrfils1 = arbre[i+1];

            NoeudCode *tmp[tailleArbre-2];
            int index = 0;
            for(int j=0;j<tailleArbre;j++){
                if(j != i && j != i+1){
                    tmp[index] = arbre[j];
                    index++;
                }
            }
            tailleArbre--;
            free(arbre);
            arbre = (NoeudCode**) malloc(sizeof(NoeudCode*)*tailleArbre);

            for(int j=0;j<tailleArbre-1;j++)
            {
                arbre[j] = tmp[j];
            }
            arbre[tailleArbre-1] = nouveauNoeud;
            triNoeudArbreCodeOcc(arbre,tailleArbre);
            deplaceNoeudArbreCodeOcc(arbre,tailleArbre);
            break;


        }
    }

    NoeudCode *racine = *arbre;
    free(arbre);
    return racine;

}

void printArbreCodeAdaptatif(NoeudCode *racine)
{
    if(racine)
    {
        printArbreCodeAdaptatif(racine->ptrfils0);

        printArbreCodeAdaptatif(racine->ptrfils1);
        if(racine->so.symb != -666)
            printf("symb=%d occ=%d %p\n",racine->so.symb,racine->so.occ,racine);
    }
}


void libereArbreCodeAdaptatif(NoeudCode *racine)
{
    if(racine)
    {
        libereArbreCodeAdaptatif(racine->ptrfils0);
        libereArbreCodeAdaptatif(racine->ptrfils1);
        free(racine);
    }
}

TabSymbCode* creeCodeAdaptatif(NoeudCode *racine)
{
    if(racine)
    {

        TabSymbCode *ret = (TabSymbCode*) malloc(sizeof(TabSymbCode));

        if(racine->ptrfils0 == 
# 264 "./src/lib/src/Partie6.c" 3 4
                              ((void *)0) 
# 264 "./src/lib/src/Partie6.c"
                                   && racine->ptrfils1 == 
# 264 "./src/lib/src/Partie6.c" 3 4
                                                          ((void *)0)
# 264 "./src/lib/src/Partie6.c"
                                                              ){

            ret->nb = 1;
            ret->tab = (SymbCode*) malloc(sizeof(SymbCode));

            char *seq = (char*) malloc(sizeof(char));
            seq[0] ='\0';
            ret->tab[0] = (SymbCode){racine->so.symb,seq};
        }
        else{

            TabSymbCode *tmp0 = creeCodeAdaptatif(racine->ptrfils0);
            TabSymbCode *tmp1 = creeCodeAdaptatif(racine->ptrfils1);

            ret->tab = (SymbCode*) malloc(sizeof(SymbCode)*((tmp0?tmp0->nb:0)+(tmp1?tmp1->nb:0)));
            ret->nb = (tmp0?tmp0->nb:0)+(tmp1?tmp1->nb:0);

            int i = 0;
            if(tmp0)
            {

                for (int j = 0; j < tmp0->nb; j++)
                {
                    char *seq = (char*) malloc(sizeof(char)*(strlen(tmp0->tab[j].symbCode)+1));
                    seq[0] = '\0';
                    strcat(seq,"0");
                    strcat(seq,tmp0->tab[j].symbCode);
                    free(tmp0->tab[j].symbCode);
                    tmp0->tab[j].symbCode = seq;
                }

                for(int j=0;j<tmp0->nb;j++){
                    ret->tab[j] = tmp0->tab[j];
                    i++;
                }
            }

            if(tmp1)
            {
                for (int j = 0; j < tmp1->nb; j++)
                {
                    char *seq = (char*) malloc(sizeof(char)*(strlen(tmp1->tab[j].symbCode)+1));
                    seq[0] = '\0';
                    strcat(seq,"1");
                    strcat(seq,tmp1->tab[j].symbCode);
                    free(tmp1->tab[j].symbCode);
                    tmp1->tab[j].symbCode = seq;
                }

                for(int j=0;j<tmp1->nb;j++){
                    ret->tab[i+j] = tmp1->tab[j];
                }

            }

        }

        return ret;
    }

    return 
# 324 "./src/lib/src/Partie6.c" 3 4
          ((void *)0)
# 324 "./src/lib/src/Partie6.c"
              ;
}

void sauveCodeAdaptatif(char *destPath, TabSymbCode *tabSymbCode)
{
    FILE * pDestFile = 
# 329 "./src/lib/src/Partie6.c" 3 4
                      ((void *)0)
# 329 "./src/lib/src/Partie6.c"
                          ;
    FILE * pSrcFile = 
# 330 "./src/lib/src/Partie6.c" 3 4
                     ((void *)0)
# 330 "./src/lib/src/Partie6.c"
                         ;


    pDestFile = fopen (destPath,"w");
    if(!pDestFile) return;

    for (int i = 0; i < tabSymbCode->nb; i++)
    {
        fflush(pDestFile);
        fprintf(pDestFile,"%d:%s\n",tabSymbCode->tab[i].symb, tabSymbCode->tab[i].symbCode);
    }

    fclose(pDestFile);
}

TabSymbCode* litCodeAdaptatif(char *path)
{

    FILE * pSrcFile = 
# 348 "./src/lib/src/Partie6.c" 3 4
                     ((void *)0)
# 348 "./src/lib/src/Partie6.c"
                         ;


    pSrcFile = fopen (path,"r");
    if(!pSrcFile) return 
# 352 "./src/lib/src/Partie6.c" 3 4
                        ((void *)0)
# 352 "./src/lib/src/Partie6.c"
                            ;

    TabSymbCode *ret = (TabSymbCode*) malloc(sizeof(TabSymbCode));

    ret->nb = 0;
    ret->tab = 
# 357 "./src/lib/src/Partie6.c" 3 4
              ((void *)0)
# 357 "./src/lib/src/Partie6.c"
                  ;

    int symb;
    char tmpSymbCode[50];
    while (fscanf(pSrcFile,"%d:%s\n",&symb,tmpSymbCode) != 
# 361 "./src/lib/src/Partie6.c" 3
                                                          (-1)
# 361 "./src/lib/src/Partie6.c"
                                                             )
    {
        SymbCode tmp[ret->nb];
        if(ret->nb > 0) for(int i = 0;i<ret->nb;i++) tmp[i]=ret->tab[i];
        ret->tab = (SymbCode*) realloc(ret->tab,sizeof(SymbCode)*(ret->nb+1));
        if(ret->nb > 0) for(int i = 0;i<ret->nb;i++) ret->tab[i]=tmp[i];
        char *symbCode = (char*) malloc(sizeof(char)*strlen(tmpSymbCode));
        symbCode = strcpy(symbCode,tmpSymbCode);
        ret->tab[ret->nb] = (SymbCode) {symb,symbCode};
        ret->nb++;
    }


    fflush(pSrcFile);
    fclose(pSrcFile);

    return ret;
}

NoeudCode* creeArbreCodeAdaptatif(TabSymbCode *tabSymbCode)
{
    NoeudCode *racine = (NoeudCode*) malloc(sizeof(NoeudCode));
    racine->ptrfils0 = 
# 383 "./src/lib/src/Partie6.c" 3 4
                      ((void *)0)
# 383 "./src/lib/src/Partie6.c"
                          ;
    racine->ptrfils1 = 
# 384 "./src/lib/src/Partie6.c" 3 4
                      ((void *)0)
# 384 "./src/lib/src/Partie6.c"
                          ;
    racine->so.symb = -666;
    racine->so.occ = 0;

    for (int i = 0; i < tabSymbCode->nb; i++)
    {
        NoeudCode *current = racine;
        for (int j = 0; j < strlen(tabSymbCode->tab[i].symbCode); j++)
        {

            if(tabSymbCode->tab[i].symbCode[j] == '0')
            {
                if(current->ptrfils0 == 
# 396 "./src/lib/src/Partie6.c" 3 4
                                       ((void *)0)
# 396 "./src/lib/src/Partie6.c"
                                           )
                {
                    current->ptrfils0 = (NoeudCode*) malloc(sizeof(NoeudCode));
                    current->ptrfils0->ptrfils0 = 
# 399 "./src/lib/src/Partie6.c" 3 4
                                                 ((void *)0)
# 399 "./src/lib/src/Partie6.c"
                                                     ;
                    current->ptrfils0->ptrfils1 = 
# 400 "./src/lib/src/Partie6.c" 3 4
                                                 ((void *)0)
# 400 "./src/lib/src/Partie6.c"
                                                     ;
                }

                current = current->ptrfils0;


                if(j == strlen(tabSymbCode->tab[i].symbCode)-1)
                {
                    current->so.symb = tabSymbCode->tab[i].symb;
                    current->so.occ = 0;
                }

                else
                {
                    current->so.symb = -666;
                    current->so.occ = 0;
                }
            }
            else if(tabSymbCode->tab[i].symbCode[j] == '1')
            {
                if(current->ptrfils1 == 
# 420 "./src/lib/src/Partie6.c" 3 4
                                       ((void *)0)
# 420 "./src/lib/src/Partie6.c"
                                           )
                {
                    current->ptrfils1 = (NoeudCode*) malloc(sizeof(NoeudCode));
                    current->ptrfils1->ptrfils0 = 
# 423 "./src/lib/src/Partie6.c" 3 4
                                                 ((void *)0)
# 423 "./src/lib/src/Partie6.c"
                                                     ;
                    current->ptrfils1->ptrfils1 = 
# 424 "./src/lib/src/Partie6.c" 3 4
                                                 ((void *)0)
# 424 "./src/lib/src/Partie6.c"
                                                     ;
                }
                current = current->ptrfils1;


                if(j == strlen(tabSymbCode->tab[i].symbCode)-1)
                {
                    current->so.symb = tabSymbCode->tab[i].symb;
                    current->so.occ = 0;
                }
                else
                {
                    current->so.symb = -666;
                    current->so.occ = 0;
                }
            }
        }
    }

    return racine;

}







void codeFichier(char *srcPath, char *destPath, TabSymbCode *tabSymbCode)
{
    FILE * pSrcFile = 
# 455 "./src/lib/src/Partie6.c" 3 4
                     ((void *)0)
# 455 "./src/lib/src/Partie6.c"
                         ;
    FILE * pDestFile = 
# 456 "./src/lib/src/Partie6.c" 3 4
                      ((void *)0)
# 456 "./src/lib/src/Partie6.c"
                          ;


    pSrcFile = fopen (srcPath,"r");
    if(!pSrcFile) return 
# 460 "./src/lib/src/Partie6.c" 3 4
                        ((void *)0)
# 460 "./src/lib/src/Partie6.c"
                            ;

    pDestFile = fopen (destPath,"w");
    if(!pSrcFile){
        fclose(pSrcFile);
        return 
# 465 "./src/lib/src/Partie6.c" 3 4
              ((void *)0)
# 465 "./src/lib/src/Partie6.c"
                  ;
    }

    int fileSize;
    fseek(pSrcFile, 0L, 
# 469 "./src/lib/src/Partie6.c" 3
                       2
# 469 "./src/lib/src/Partie6.c"
                               );
    fileSize = ftell(pSrcFile);
    rewind(pSrcFile);

    char* symb = (char*) malloc(sizeof(char)*fileSize);
    symb[0] = '\0';

    int c = 0;
    while (c != 
# 477 "./src/lib/src/Partie6.c" 3
               (-1)
# 477 "./src/lib/src/Partie6.c"
                  )
    {
        c = fgetc(pSrcFile);
        if((c >= 48 && c <= 57) || c == 45)
        {
            sprintf(symb,"%s%c",symb,c);
        }
        else
        {
            int val = atoi(symb);
            if(strlen(symb) > 0)
            {
                for (int i = 0; i < tabSymbCode->nb; i++)
                {
                    if(tabSymbCode->tab[i].symb == val)
                    {
                        fflush(pDestFile);
                        fprintf(pDestFile,"%s",tabSymbCode->tab[i].symbCode);
                    }
                }
            }


            symb=strcpy(symb,"");

        }

    }
    free(symb);

    fclose (pSrcFile);
    fclose (pDestFile);



}

int isValueIn(int* tab, int nb, int val)
{
    for (int i = 0; i < nb; i++)
    {
        if(tab[i] == val)
            return 1;
    }

    return 0;
}

void decodeFichier(char *srcPath, char *destPath, TabSymbCode *tabSymbCode)
{
    FILE * pSrcFile = 
# 527 "./src/lib/src/Partie6.c" 3 4
                     ((void *)0)
# 527 "./src/lib/src/Partie6.c"
                         ;
    FILE * pDestFile = 
# 528 "./src/lib/src/Partie6.c" 3 4
                      ((void *)0)
# 528 "./src/lib/src/Partie6.c"
                          ;


    pSrcFile = fopen (srcPath,"r");
    if(!pSrcFile)
    {
        return ;
    }
    pDestFile = fopen (destPath,"w");
    if(!pSrcFile){
        fclose(pSrcFile);
        return ;
    }

    int fileSize;
    fseek(pSrcFile, 0L, 
# 543 "./src/lib/src/Partie6.c" 3
                       2
# 543 "./src/lib/src/Partie6.c"
                               );
    fileSize = ftell(pSrcFile);
    rewind(pSrcFile);

    int *values = 
# 547 "./src/lib/src/Partie6.c" 3 4
                 ((void *)0)
# 547 "./src/lib/src/Partie6.c"
                     ;
    int nb = 0;

    char* symbCode = (char*) malloc(sizeof(char)*fileSize);
    symbCode[0] = '\0';

    int c = 0;

    while (c != 
# 555 "./src/lib/src/Partie6.c" 3
               (-1)
# 555 "./src/lib/src/Partie6.c"
                  )
    {

        c = fgetc(pSrcFile);

        if(strlen(symbCode) > 0)
        {

            for (int i = 0; i < tabSymbCode->nb; i++)
            {

                if(strcmp(tabSymbCode->tab[i].symbCode,symbCode) == 0)
                {

                    if(!isValueIn(values,nb,tabSymbCode->tab[i].symb))
                    {

                        if(nb)
                        {
                            int tmp[nb];
                            for(int i=0;i<nb;i++) tmp[i] = values[i];
                            nb++;
                            values = (int*) realloc(values,sizeof(int)*nb);
                            for(int i=0;i<nb;i++) values[i] = tmp[i];
                        }
                        else{
                            nb++;
                            values = (int*) realloc(values,sizeof(int)*nb);
                        }

                        values[nb-1] = tabSymbCode->tab[i].symb;

                        fprintf(pDestFile,"%d:%s\n",tabSymbCode->tab[i].symb,tabSymbCode->tab[i].symbCode);
                    }
                    symbCode=strcpy(symbCode,"");
                    break;
                }
            }
        }

        if(c == 48 || c == 49)
        {
            sprintf(symbCode,"%s%c",symbCode,c);
        }

    }
    free(values);
    free(symbCode);

    fclose (pSrcFile);
    fclose (pDestFile);

}

int interpretStringToBinary(char *string)
{
    int b = 0b0;

    for(int i = 0; i < strlen(string); i++)
    {
        if(string[i] == '1')
        {
            if(strlen(string)-i-1>=0) b += 0b1 << (strlen(string)-i-1);
        }
    }
    return b;
}

void transcritTexteEnBinaire(char *srcPath, char *destPath)
{
    FILE * pSrcFile = 
# 625 "./src/lib/src/Partie6.c" 3 4
                     ((void *)0)
# 625 "./src/lib/src/Partie6.c"
                         ;
    FILE * pDestFile = 
# 626 "./src/lib/src/Partie6.c" 3 4
                      ((void *)0)
# 626 "./src/lib/src/Partie6.c"
                          ;


    pSrcFile = fopen (srcPath,"r");
    if(!pSrcFile) return 
# 630 "./src/lib/src/Partie6.c" 3 4
                        ((void *)0)
# 630 "./src/lib/src/Partie6.c"
                            ;

    pDestFile = fopen (destPath,"wb");
    if(!pSrcFile){
        fclose(pSrcFile);
        return 
# 635 "./src/lib/src/Partie6.c" 3 4
              ((void *)0)
# 635 "./src/lib/src/Partie6.c"
                  ;
    }

    int fileSize;
    fseek(pSrcFile, 0L, 
# 639 "./src/lib/src/Partie6.c" 3
                       2
# 639 "./src/lib/src/Partie6.c"
                               );
    fileSize = ftell(pSrcFile);
    rewind(pSrcFile);

    char* symb = (char*) malloc(sizeof(char)*fileSize);
    symb[0] = '\0';

    int c = 0;
    while (c != 
# 647 "./src/lib/src/Partie6.c" 3
               (-1)
# 647 "./src/lib/src/Partie6.c"
                  )
    {
        c = fgetc(pSrcFile);
        if(strlen(symb) < 7 && c != 
# 650 "./src/lib/src/Partie6.c" 3
                                   (-1)
# 650 "./src/lib/src/Partie6.c"
                                      )
        {
            sprintf(symb,"%s%c",symb,c);
        }
        else
        {
            sprintf(symb,"%s%c",symb,c);
            unsigned char b = interpretStringToBinary(symb);
            if(8-strlen(symb)<8) b <<= 8-strlen(symb);
            fflush(pDestFile);
            fwrite(&b, 1, 1, pDestFile);


            symb=strcpy(symb,"");

        }

    }
    free(symb);


    fclose (pSrcFile);
    fclose (pDestFile);
}


void transcritBinaireEnTexte(char *srcPath, char *destPath)
{
    FILE * pSrcFile = 
# 678 "./src/lib/src/Partie6.c" 3 4
                     ((void *)0)
# 678 "./src/lib/src/Partie6.c"
                         ;
    FILE * pDestFile = 
# 679 "./src/lib/src/Partie6.c" 3 4
                      ((void *)0)
# 679 "./src/lib/src/Partie6.c"
                          ;


    pSrcFile = fopen (srcPath,"rb");
    if(!pSrcFile) return 
# 683 "./src/lib/src/Partie6.c" 3 4
                        ((void *)0)
# 683 "./src/lib/src/Partie6.c"
                            ;

    pDestFile = fopen (destPath,"w");
    if(!pSrcFile){
        fclose(pSrcFile);
        return 
# 688 "./src/lib/src/Partie6.c" 3 4
              ((void *)0)
# 688 "./src/lib/src/Partie6.c"
                  ;
    }

    int c = 0;
    c = fgetc(pSrcFile);
    while (c != 
# 693 "./src/lib/src/Partie6.c" 3
               (-1)
# 693 "./src/lib/src/Partie6.c"
                  )
    {
        if(c != 
# 695 "./src/lib/src/Partie6.c" 3
               (-1)
# 695 "./src/lib/src/Partie6.c"
                  )
        {
            int tmpC = c;
            c = fgetc(pSrcFile);

            int lastIndex = -1;
            for (int i = 7; i >=0; i--) if(tmpC&0b1<<i) lastIndex=i;

            for (int i = 7; i >=0; i--)
            {
                if(c==
# 705 "./src/lib/src/Partie6.c" 3
                     (-1)
# 705 "./src/lib/src/Partie6.c"
                        )
                {
                    if(lastIndex != -1 && i >= lastIndex)
                    {
                        (tmpC&0b1<<i)?fputc('1',pDestFile):fputc('0',pDestFile);
                    }
                }
                else{
                    (tmpC&0b1<<i)?fputc('1',pDestFile):fputc('0',pDestFile);
                }
            }


        }


    }


    fclose (pSrcFile);
    fclose (pDestFile);
}

TabSymbOcc* compteOccurrenceMotif(char * path)
{
    return compteOccurrence(path);
}

void codeFichierMotif(char *srcPath, char *destPath, TabSymbCode *tabSymbCode)
{
    codeFichier(srcPath,destPath,tabSymbCode);
}

void decodeFichierMotif(char *srcPath, char *destPath, TabSymbCode *tabSymbCode)
{
    decodeFichier(srcPath,destPath,tabSymbCode);
}
TabSymbOcc* compteOccurrenceTraj(char * path)
{
    return compteOccurrence(path);
}
void codeFichierTraj(char *srcPath, char *destPath, TabSymbCode *tabSymbCode)
{
    codeFichier(srcPath,destPath,tabSymbCode);
}
void decodeFichierTraj(char *srcPath, char *destPath, TabSymbCode *tabSymbCode)
{
    decodeFichier(srcPath,destPath,tabSymbCode);
}

void fprintfArbreBinaire(FILE * fp,noeudBinaire* rcn)
{
    if(rcn){
        if(rcn->etiquette != -1)
            fprintf(fp, "\n%d %d %d %d",rcn->start_x,rcn->start_y,rcn->size,rcn->value);
        fprintfArbreBinaire(fp, rcn->NW);
        fprintfArbreBinaire(fp, rcn->NE);
        fprintfArbreBinaire(fp, rcn->SW);
        fprintfArbreBinaire(fp, rcn->SE);
    }
}

void sauveArbreNB_Texte(noeudBinaire *racine)
{
    FILE * fp;


    fp = fopen ("./resources/partie6/arbreNB_Texte.txt","w");


    fprintf (fp, "%d",compteFeuille(racine));

    fprintfArbreBinaire(fp, racine);
    fclose (fp);
}


TabSymbOcc* compteOccurrenceNB(char * path)
{
    return compteOccurrence(path);
}
void codeFichierNB(char *srcPath, char *destPath, TabSymbCode *tabSymbCode){
    codeFichier(srcPath,destPath,tabSymbCode);
}
void decodeFichierNB(char *srcPath, char *destPath, TabSymbCode *tabSymbCode)
{
    decodeFichier(srcPath,destPath,tabSymbCode);
}
void transcritTexteEnBinaireMotif(char *srcPath, char *destPath)
{
    transcritTexteEnBinaire(srcPath,destPath);
}
void transcritTexteEnBinaireTraj(char *srcPath, char *destPath)
{
    transcritTexteEnBinaireMotif(srcPath,destPath);
}
void transcritTexteEnBinaireNB(char *srcPath, char *destPath)
{
    transcritTexteEnBinaire(srcPath,destPath);
}
void transcritBinaireEnTexteMotif(char *srcPath, char *destPath)
{
    transcritBinaireEnTexte(srcPath,destPath);
}
void transcritBinaireEnTexteTraj(char *srcPath, char *destPath)
{
    transcritBinaireEnTexte(srcPath,destPath);
}
void transcritBinaireEnTexteNB(char *srcPath, char *destPath)
{
    transcritBinaireEnTexte(srcPath,destPath);
}
